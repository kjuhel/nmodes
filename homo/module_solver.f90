module module_solver

  use nrtype
  implicit none

  contains

    !=======================================================!
    !             Toroidal mode solver routines             !
    !=======================================================!

    function tor_solver(l, f) result(det)
      !--------------------------------------!
      ! This routine returns the determinant !
      ! of the toroidal modes equations      !
      !--------------------------------------!
      use nrtype
      use module_input
      use module_int
      implicit none

      integer(i4b), intent(in) :: l
      real(dp), intent(in) :: f
      real(qp) :: det

      complex(dpc) :: om, T_surface


      if (f.ne.0.0_dp .and. l.ne.0) then
        om = 2.0_dp*pi_d*f

        call set_l(l)
        call set_omega(om)

        call tor_uniform(T_surface)
        det = real(T_surface,qp)

      else
        det = 0.0_qp
      end if

    end function tor_solver


    !=======================================================!
    !              Radial mode solver routines              !
    !=======================================================!

    function rad_solver(l, f) result(det)
      !--------------------------------------!
      ! This routine returns the determinant !
      ! of the radial modes equations        !
      !--------------------------------------!
      use nrtype
      use module_input
      use module_int
      implicit none

      integer(i4b), intent(in) :: l
      real(dp), intent(in) :: f
      real(qp) :: det

      complex(dpc) :: om, R_surface


      if (f.ne.0.0_dp) then
        om = 2.0_dp*pi_d*f

        call set_l(l)
        call set_omega(om)

        call rad_uniform(R_surface)
        det = real(R_surface,qp)

      else
        det = 0.0_qp
      end if

    end function rad_solver


    !=======================================================!
    !            Spheroidal mode solver routines            !
    !=======================================================!

    function sph_solver(l, f) result(det)
      !--------------------------------------!
      ! This routine returns the determinant !
      ! of the spheroidal modes equations    !
      !--------------------------------------!
      use nrtype
      use module_input
      use module_int
      implicit none

      integer(i4b), intent(in) :: l
      real(dp), intent(in) :: f
      real(qp) :: det, nrm

      complex(dpc) :: om
      complex(dpc), dimension(:), allocatable :: R_surface, S_surface, B_surface
      logical :: gamma_flag

      if (f.ne.0.0_dp) then
        om = 2.0_dp*pi_d*f

        call set_l(l)
        call set_omega(om)

        call sph_uniform(R_surface, S_surface, B_surface, gamma_flag)

        if (gamma_flag) then
          det = det3(real(R_surface,qp), real(S_surface,qp), real(B_surface,qp))
          nrm = nrm3(real(R_surface,qp), real(S_surface,qp), real(B_surface,qp))

        else
          det = det2(real(R_surface,qp), real(S_surface,qp))
          nrm = nrm2(real(R_surface,qp), real(S_surface,qp))

        end if

        !if (nrm > 1.0E-80_qp) then
        !   det = det / nrm
        ! else
        !   det = 0.0_dp
        !end if

        det = det / nrm

        ! detection of NaN
        if (det*0.0_qp .ne. 0.0_qp) det = 0.0_qp

      else
        det = 0.0_qp
      end if

    end function sph_solver


    !===========================================!
    !             general routines              !
    !===========================================!

    subroutine get_eigen_freq(l, solver, f_init, f_zero, n_zero, type, l1, l2)
      !---------------------------------------------------------!
      ! search the eigenfrequencies for a uniform earth model : !
      !   -- type == 1 : radial modes                           !
      !   -- type == 2 : toroidal modes                         !
      !   -- type == 3 : spheroidal modes                       !
      !---------------------------------------------------------!
      use nrtype
      use module_input
      implicit none

      integer(i4b), intent(in) :: l, type
      real(dp), intent(in) :: f_init
      integer(i4b), intent(out) :: n_zero
      integer(i4b), intent(inout) :: l1, l2
      real(dp), dimension(:), intent(out) :: f_zero

      real(qp), external :: solver
      integer(i4b) :: ip, ipdeb, k
      real(dp) :: f, f1, f2, ddf
      real(qp) :: det1, det2, val1, val2

      n_zero = 0
      f_zero(:) = 0.0_dp

      ipdeb = max(int(f_init/df)-1, 1)
      det1 = solver(l, max(real(ipdeb-1,dp)*df, df))

      do ip = ipdeb,ipf
         f = real(ip-1,dp) * df
         det2 = solver(l,f)

         if ((det1*det2).lt.0.0_qp .and. (ip*df).lt.fmax .and. f.ge.fmin .and. n_zero.lt.nmax) then

           f1 = df * real(ip-2,dp)
           f2 = df * real(ip-1,dp)

           ddf = df / 2.0_dp
           f   = f1 + ddf

           val1 = det1
           val2 = det2
           det1 = det2

           do k = 1,100
              det2 = solver(l, f)

              if (val1*det2 .le. 0.0_qp) then
                val2 = det2
                f2 = f
              else
                f1 = f
                val1 = det2
              end if
              ddf = ddf / 2.0_dp
              f   = f1 + ddf
           end do

           if (f.lt.fmax .and. f.ge.fmin) then
             n_zero = n_zero + 1
             f_zero(n_zero) = f
             if (n_zero.ge.nmax) exit
           end if
         else
           det1 = det2
         end if
      end do

      if (type==1) print *, 'nb of sph modes = ', n_zero
      if (type==2) print *, 'nb of tor modes = ', n_zero

      if (n_zero /= 0) then
        if (type==1) l1 = max(l1, l)
        if (type==2) l2 = max(l2, l)
      end if

      return
    end subroutine get_eigen_freq


end module module_solver
