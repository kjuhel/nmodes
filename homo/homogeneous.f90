program homogeneous

  !========================================================================!
  ! This program computes the normal modes of a homogeneous                !
  ! earth model using the equations from Takeuchi and Saito (1972),        !
  ! and routines from stock_boule (Y. Capdeville) and yspec (D. Al Attar)  !
  ! edit : self gravitation and attenuation can be included                !
  ! K. Juhel -- 2016                                                       !
  !========================================================================!

  !==============!
  ! modules used !
  !==============!

  use nrtype
  use module_input
  use module_solver
  use module_function
  use module_write
  implicit none


  !=====================================!
  ! variables local to the main program !
  !=====================================!

  integer(i4b) :: j, l, nz, lmax_tor, lmax_sph
  integer(i4b), dimension(lmin:lmax, 2)  :: eigen_n
  real(dp) :: start, finish
  real(dp), dimension(2) :: f_init
  real(dp), dimension(nmax, lmin:lmax, 2) :: eigen_f
  logical(lgt) :: flag, flag_init=.true., flag_sph=.true., flag_tor=.true.


  ! compute execution time
  call cpu_time(start)

  ! set the values of model and integration parameters
  call initialize

  ! set the output files
  call set_outputs

  lmax_sph = lmin
  lmax_tor = lmin

  eigen_n(:,:) = 0
  eigen_f(:,:,:) = 0.0_dp


  ! start loop over l
  do l = lmin,lmax

     if (l .gt. lmin) then
       if (flag_init .or. eigen_n(l-1,1).ne.0 .or. eigen_n(l-1,2).ne.0) then
         flag = .true.
         if (eigen_n(l-1,1).eq.0) then
           flag_sph = .false.
         end if
         if (eigen_n(l-1,2).eq.0 .and. l.gt.1) then
           flag_tor = .false.
         end if
       else
         flag = .false.
       end if
     else
       flag = .true.
     end if

     if (flag) then

       ! computes f_init for the integration
       do j = 1,2
          if (l > 1) then
            if (eigen_f(1,l-1,j) > 1.0e-12_dp) then
              f_init(1) = max(fmin, eigen_f(1,l-1,1) - 1000.0_dp*df)
              f_init(2) = max(fmin, eigen_f(1,l-1,2) - 1000.0_dp*df)
            else
              f_init(j) = fmin
            end if
          else
            f_init(j) = fmin
          end if

          if (j == 1) then
            if (flag_sph) then
              print *, '--------- sph : f_init =', f_init(j)*fre_norm, 'l =', l
            end if
          else
            if (flag_tor) then
              print *, '--------- tor : f_init =', f_init(j)*fre_norm, 'l =', l
            end if
          end if
       end do


       !---------------------!
       ! solve the equations !
       !---------------------!

       if (l == 0) then
         ! solve the radial equations
         call get_eigen_freq(l, rad_solver, f_init(1), eigen_f(:,l,1), eigen_n(l,1), 1, lmax_sph, lmax_tor)
       else
         if (flag_sph) then
           ! solve the spheroidal equations
           call get_eigen_freq(l, sph_solver, f_init(1), eigen_f(:,l,1), eigen_n(l,1), 1, lmax_sph, lmax_tor)
         end if
       end if

       if (flag_tor) then
         ! solve the toroidal equations
         call get_eigen_freq(l, tor_solver, f_init(2), eigen_f(:,l,2), eigen_n(l,2), 2, lmax_sph, lmax_tor)
       end if

       if (eigen_n(l,1).ne.0 .or. eigen_n(l,2).ne.0) flag_init=.false.


       !--------------------------!
       ! write the eigenfunctions !
       !--------------------------!

       if (flag_sph) then
         ! write the radial/spheroidal eigenfunctions into files
         do nz = 1,eigen_n(l,1)
            call write_eigen_fct(eigen_f(nz,l,1), l, nz, 1)
         end do
       end if

       if (flag_tor) then
         ! write the toroidal eigenfunctions into files
         do nz = 1,eigen_n(l,2)
            call write_eigen_fct(eigen_f(nz,l,2), l, nz, 2)
         end do
       end if

     end if

  end do
  ! end loop over l

  call close_uniform(lmax_sph, lmax_tor, eigen_f, eigen_n)

  ! computes execution time
  call cpu_time(finish)
  print '("----- execution time = ", f20.3, " seconds -----")', finish-start

end program homogeneous
