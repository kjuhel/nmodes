module module_input
  use nrtype
  implicit none

  integer(i4b), parameter :: ncm=2001
  integer(i4b), parameter :: lmin=0   ! 0
  integer(i4b), parameter :: lmax=11  ! 10000
  integer(i4b), parameter :: nmax=11  ! 10001 ! nb maximal d'harmoniques
  integer(i4b), save      :: ipf
  real(dp), parameter :: r_norm=6.371e+6_dp
  real(dp), parameter :: rho_norm=5515.0_dp
  real(dp), save      :: fmin
  real(dp), save      :: fmax
  real(dp), save      :: df
  real(dp), save      :: wref
  real(dp), save      :: rt
  real(dp), save      :: rho
  real(dp), save      :: alpha
  real(dp), save      :: beta
  real(dp), save      :: mu
  real(dp), save      :: lambda
  real(dp), save      :: kappa
  real(dp), save      :: qalpha
  real(dp), save      :: qbeta
  real(dp), save      :: qmu
  real(dp), save      :: qkappa
  real(dp), save      :: ddr
  real(dp), save      :: pibigg
  real(dp), save      :: vel_norm
  real(dp), save      :: fre_norm
  real(dp), save      :: grav_norm
  real(dp), save      :: ats_switch ! switch to turn on or off attenuation:
                                    ! ats_switch = 1 -- attenuation turned on
                                    ! ats_switch = 0 -- attenuation turned off
  logical, save       :: selfgr_switch ! switch to turn on or off self-gravitation

  contains

    subroutine initialize()
      use nrtype
      implicit none

      ! set the normalization parameters
      vel_norm  = r_norm*sqrt(pi_d*bigg*rho_norm)
      fre_norm  = vel_norm/r_norm
      grav_norm = pi_d*bigg
      pibigg    = pi_d*bigg/grav_norm

      ! normalize the inputs
      rt     = 6.371e+6_dp/r_norm

      alpha  = 6400.0_dp/vel_norm
      beta   = 3700.0_dp/vel_norm

      rho    = 2700.0_dp/rho_norm

      mu     = rho*beta**2
      lambda = rho*alpha**2-2.0_dp*mu
      kappa  = lambda+2.0_dp*mu/3.0_dp

      qalpha = 10000.0_dp
      qbeta  = 10000.0_dp

      qmu    = qbeta
      qkappa = 1.0_dp/qalpha - (4.0_dp/3.0_dp)*(beta/alpha)**2/qmu
      qkappa = qkappa / (1.0_dp - (4.0_dp/3.0_dp)*(beta/alpha)**2)
      qkappa = 1.0_dp / qkappa

      ddr = rt/ncm

      fmin = 0.1_dp/(1000.0_dp*fre_norm)
      fmax = 100.0_dp/(1000.0_dp*fre_norm)
      df   = 1.0e-7_dp/fre_norm
      ipf  = int(fmax/df)+1

      wref = twopi_d/(1.0_dp*fre_norm)
      ats_switch = 0.0_dp

      selfgr_switch = .false.

    end subroutine initialize

end module module_input
