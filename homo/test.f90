program test
  implicit none
  integer, parameter :: dp = kind(1.0d0)
  integer, parameter :: dpc=kind((1.0d0,1.0d0))

  integer :: i, nzero, nmax
  real(dp) :: a, f, df
  complex(dpc) :: c
  logical, dimension(1) :: flag_a, flag_b

  ! a = 2.0_dp
  ! print *, a

  ! c = (1.0e-100_dp, 1.0e-100_dp)
  ! print *, c

  ! if (c /= 0.0_dp) then
  !   print *, 'toto'
  ! end if

  ! print *, (a+c)/a

  ! flag_a = .true.
  ! flag_b = .false.

  ! print *, flag_a
  ! print *, flag_b
  ! print *, dot_product(flag_a, flag_a)
  ! print *, dot_product(flag_b, flag_b)
  ! print *, dot_product(flag_a, flag_b)

  ! print *, 1.0_dp*10

  df = 2.0
  nmax = 10

  do i = 1,100
    f = i*df

    if (modulo(f, 3.0) < 1.0e-7_dp) then
      nzero = nzero + 1
      print *, 'f = ', f, ', nzero = ', nzero
      if (nzero .ge. nmax) exit
    end if

  end do

end program test
