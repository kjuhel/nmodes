module nrutil
use nrtype
implicit none

! parameters for crossover from serial to parallel algorithms
integer(i4b), parameter :: npar_arth=16,npar2_arth=8


! routines for argument checking and error handling (nrerror is not currently
! overloaded):
interface assert
   module procedure assert1,assert2,assert3,assert4,assert_v
end interface

! routines relating to polynomials and recurrences (cumprod, zroots_unity
! are not currently overloaded):
interface arth
   module procedure arth_r,arth_d,arth_i
end interface


contains

! routines for argument checking and error handling:

subroutine assert1(n1,string)
! report and die if any logical is false (used for arg range checking).
character(LEN=*), intent(in) :: string
logical, intent(in) :: n1
if(.not. n1) then
   write(*,*) 'nrerror: an assertation failed with his tag:',&
        string
   stop 'program terminated by assert1'
end if
end subroutine assert1


subroutine assert2(n1,n2,string)
! report and die if any logical is false (used for arg range checking).
character(LEN=*), intent(in) :: string
logical, intent(in) :: n1,n2
if(.not. (n1 .and. n2)) then
   write(*,*) 'nrerror: an assertation failed with his tag:',&
        string
   stop 'program terminated by assert2'
end if
end subroutine assert2

subroutine assert3(n1,n2,n3,string)
! report and die if any logical is false (used for arg range checking).
character(LEN=*), intent(in) :: string
logical, intent(in) :: n1,n2,n3
if(.not. (n1 .and. n2 .and. n3)) then
   write(*,*) 'nrerror: an assertation failed with his tag:',&
        string
   stop 'program terminated by assert3'
end if
end subroutine assert3

subroutine assert4(n1,n2,n3,n4,string)
! report and die if any logical is false (used for arg range checking).
character(LEN=*), intent(in) :: string
logical, intent(in) :: n1,n2,n3,n4
if(.not. (n1 .and. n2 .and. n3 .and. n4)) then
   write(*,*) 'nrerror: an assertation failed with his tag:',&
        string
   stop 'program terminated by assert4'
end if
end subroutine assert4

subroutine assert_v(n,string)
character(LEN=*), intent(in) :: string
logical, dimension(:), intent(in) :: n
if(.not. all(n)) then
   write(*,*) 'nerror: an assertation failed with this tag:',&
        string
   stop 'program terminated by assert_v'
end if
end subroutine assert_v


! routines relating to polynomials and recurrences:

function arth_r(first,increment,n)
! array fucntion returning an arithmetic progression.
real(sp), intent(in) :: first, increment
integer(i4b), intent(in) :: n
real(sp), dimension(n) :: arth_r
integer(i4b) ::k,k2
real(sp) :: temp
if(n > 0) arth_r(1)=first
if(n <= npar_arth) then
   do k=2,n
      arth_r(k)=arth_r(k-1)+increment
   end do
else
   do k=2,npar2_arth
      arth_r(k)=arth_r(k-1)+increment
   end do
   temp=increment*npar2_arth
   k=npar2_arth
   do
      if(k >= n) exit
      k2=k+k
      arth_r(k+1:min(k2,n))=temp+arth_r(1:min(k,n-k))
      temp=temp+temp
      k=k2
   end do
end if
end function arth_r


function arth_d(first,increment,n)
! array fucntion returning an arithmetic progression.
real(dp), intent(in) :: first, increment
integer(i4b), intent(in) :: n
real(dp), dimension(n) :: arth_d
integer(i4b) ::k,k2
real(dp) :: temp
if(n > 0) arth_d(1)=first
if(n <= npar_arth) then
   do k=2,n
      arth_d(k)=arth_d(k-1)+increment
   end do
else
   do k=2,npar2_arth
      arth_d(k)=arth_d(k-1)+increment
   end do
   temp=increment*npar2_arth
   k=npar2_arth
   do
      if(k >= n) exit
      k2=k+k
      arth_d(k+1:min(k2,n))=temp+arth_d(1:min(k,n-k))
      temp=temp+temp
      k=k2
   end do
end if
end function arth_d

function arth_i(first,increment,n)
! array fucntion returning an arithmetic progression.
integer(i4b), intent(in) :: first, increment,n
integer(i4b), dimension(n) :: arth_i
integer(i4b) ::k,k2,temp
if(n > 0) arth_i(1)=first
if(n <= npar_arth) then
   do k=2,n
      arth_i(k)=arth_i(k-1)+increment
   end do
else
   do k=2,npar2_arth
      arth_i(k)=arth_i(k-1)+increment
   end do
   temp=increment*npar2_arth
   k=npar2_arth
   do
      if(k >= n) exit
      k2=k+k
      arth_i(k+1:min(k2,n))=temp+arth_i(1:min(k,n-k))
      temp=temp+temp
      k=k2
   end do
end if
end function arth_i

end module nrutil
