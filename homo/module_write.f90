module module_write

  use nrtype
  implicit none

  integer(i4b) :: unit_rd, unit_ri, unit_ld, unit_li, len_r, len_l, lov_rec, rayl_rec
  character(len=105) :: file_direct_tor, file_direct_sph, file_info_tor, file_info_sph
  real(dp), dimension(:), allocatable :: r_int, r_int2
  logical :: oldminos=.false., int_param=.false.

  contains

    !=======================================================!
    !               Toroidal modes routines                 !
    !=======================================================!

    subroutine tor_eigen_fct(l, f, ww, dww, flag, qinv)
      !-----------------------------------------!
      ! this routine computes the toroidal      !
      ! eigenfunctions in a uniform earth model !
      !-----------------------------------------!
      use nrtype
      use module_input
      use module_int
      implicit none

      integer(i4b), intent(in) :: l
      real(dp), intent(in)  :: f
      real(dp), intent(out) :: qinv
      real(qp), dimension(ncm), intent(out) :: ww, dww

      integer(i4b) :: i
      real(dp) :: rr
      complex(dpc) :: w, w_out, dw_out
      logical :: flag


      w = 2.0_dp*pi_d*f

      call set_l(l)
      call set_omega(w)

      if (l /= 0) then

        do i = 1,ncm
           rr = i*ddr
           call tor_out(rr, w_out, dw_out)
           ww(i)  = real( w_out,qp)
           dww(i) = real(dw_out,qp)
        end do

        call tor_utils(ww, dww, qinv, flag)

      end if

    end subroutine tor_eigen_fct


    subroutine tor_utils(ww, dww, qinv, flag)
      !--------------------------------------------------!
      ! computes normalization coeff and quality factor  !
      ! for toroidal modes through trapezoid integration !
      !--------------------------------------------------!
      use nrtype
      use module_input
      use module_int
      use module_function
      implicit none

      real(qp), dimension(ncm), intent(inout) :: ww, dww
      real(dp), intent(out) :: qinv
      logical, intent(out) :: flag

      integer(i4b) :: i
      real(qp) :: norme


      if (.not.int_param) then
        allocate(r_int(ncm), r_int2(ncm))

        do i = 1,ncm
           r_int(i) = i*ddr
        end do

        r_int2(:) = r_int(:)**2
        int_param = .true.
      endif

      ! compute normalization coefficient
      norme = zeta2 * (ww(1)**2*r_int2(1) + ww(ncm)**2*r_int2(ncm)) * 0.5_qp*ddr
      norme = norme + sum( zeta2*ww(2:ncm-1)**2*r_int2(2:ncm-1) ) * ddr
      norme = sqrt(rho*norme)

      if (abs(norme) > 1.0e-300_qp) then
        ww(:)  =  ww(:) / norme
        dww(:) = dww(:) / norme
        flag = .false.
      else
        flag = .true.
      end if


      if (.not. flag) then

        ! compute quality factor
        if (ats_switch /= 0.0_dp) then
          qinv = (integrandmu(1) + integrandmu(ncm)) * 0.5_dp*ddr
          qinv = qinv + sum( (r_int(2:ncm-1)*dww(2:ncm-1)-ww(2:ncm-1))**2 + zeta2m2*ww(2:ncm-1)**2 ) * ddr
          qinv = qinv * mu/qmu / real(omega2,dp)
        else
          qinv = 0.0_dp
        end if

      end if


      contains

        double precision function integrandmu(idx)
          !-------------------------------------------!
          ! computes integrand Kmu of equ (9.54) from !
          ! Dahlen and Tromp at given radius rr(idx)  !
          ! for toroidal eigenfunctions               !
          !-------------------------------------------!
          implicit none
          integer(i4b), intent(in) :: idx
          integrandmu = (r_int(idx)*dww(idx) - ww(idx))**2 + zeta2m2*ww(idx)**2
        end function integrandmu

    end subroutine tor_utils


    !=======================================================!
    !                 Radial modes routines                 !
    !=======================================================!

    subroutine rad_eigen_fct(l, f, uu, vv, pp, duu, dvv, dpp, flag, qinv)
      !-----------------------------------------!
      ! this routine computes the radial        !
      ! eigenfunctions in a uniform earth model !
      !-----------------------------------------!
      use nrtype
      use module_input
      use module_int
      implicit none

      integer(i4b), intent(in) :: l
      real(dp), intent(in)  :: f
      real(dp), intent(out) :: qinv
      real(qp), dimension(ncm), intent(out) :: uu, vv, pp, duu, dvv, dpp

      integer :: i
      real(dp) :: rr
      complex(dpc) :: w, u_out, du_out
      logical :: flag


      w = 2.0_dp*pi_d*f

      call set_l(l)
      call set_omega(w)

      do i = 1,ncm
         rr = i*ddr

         call rad_out(rr, u_out, du_out)
         uu(i)  = real( u_out,qp)
         duu(i) = real(du_out,qp)
      end do

      vv(:)  = 0.0_qp
      pp(:)  = 0.0_qp
      dvv(:) = 0.0_qp
      dpp(:) = 0.0_qp

      call rad_utils(uu, pp, duu, dpp, qinv, flag)

    end subroutine rad_eigen_fct


    subroutine rad_utils(uu, pp, duu, dpp, qinv, flag)
      !-------------------------------------------------!
      ! computes normalization coeff and quality factor !
      ! for radial modes through integration            !
      !-------------------------------------------------!
      use nrtype
      use module_input
      use module_int
      use module_function
      implicit none

      real(qp), dimension(ncm), intent(inout) :: uu, pp, duu, dpp
      real(dp), intent(out) :: qinv
      logical, intent(out) :: flag

      integer(i4b) :: i
      real(dp) :: sumkappa, summu
      real(qp), dimension(ncm) :: pp_int, dpp_int
      real(qp) :: norme


      if (.not.int_param) then
        allocate(r_int(ncm), r_int2(ncm))

        do i = 1,ncm
           r_int(i) = i*ddr
        end do

        r_int2(:) = r_int(:)**2
        int_param = .true.
      endif


      ! compute normalization coefficient
      norme = (uu(1)**2*r_int2(1) + uu(ncm)**2*r_int2(ncm)) * 0.5_qp*ddr
      norme = norme + sum( uu(2:ncm-1)**2*r_int2(2:ncm-1) ) * ddr
      norme = sqrt(rho*norme)

      if (abs(norme) > 1.0e-300_qp) then
        uu(:)  =  uu(:) / norme
        duu(:) = duu(:) / norme
        flag = .false.
      else
        flag = .true.
      end if


      if (.not. flag) then

        ! compute quality factor
        if (ats_switch /= 0.0_dp) then

          sumkappa = (integrandkappa(1) + integrandkappa(ncm)) * 0.5_dp*ddr
          sumkappa = sumkappa + sum( (r_int(2:ncm-1)*duu(2:ncm-1) + 2.0_dp*uu(2:ncm-1))**2 ) * ddr
          sumkappa = sumkappa * kappa/qkappa

          summu = (integrandmu(1) + integrandmu(ncm)) * 0.5_dp*ddr
          summu = summu + sum( 4.0_dp*(r_int(2:ncm-1)*duu(2:ncm-1) - uu(2:ncm-1))**2/3.0_dp &
            + zeta2*uu(2:ncm-1)**2 ) * ddr
          summu = summu * mu/qmu

          qinv = (sumkappa + summu) / real(omega2,dp)
        else
          qinv = 0.0_dp
        end if


        ! compute gravitational perturbation
        pp_int(:)  = 0.0_qp
        dpp_int(:) = 0.0_qp

        do i = 1,ncm
           if (i /= ncm) then
             pp_int(i)  =   4.0_qp*pibigg*rho * (uu(i) + uu(ncm)) * 0.5_qp*ddr
             dpp_int(i) = - 4.0_qp*pibigg*rho*uu(i)
             if (i /= ncm-1) then
               pp_int(i) = pp_int(i) + 4.0_qp*pibigg*rho * sum(uu(i+1:ncm-1)) * ddr
             end if
           else
             pp_int(i)  = 0.0_qp
             dpp_int(i) = - 4.0_qp*pibigg*rho*uu(i)
           end if

           ! detection of NaN
           if ( pp_int(i)*0.0_dp.ne.0.0_dp)  pp_int(i) = 0.0_qp
           if (dpp_int(i)*0.0_dp.ne.0.0_dp) dpp_int(i) = 0.0_qp

        end do

        pp(:)  = pp_int(:)
        dpp(:) = dpp_int(:)

      end if


      contains

        double precision function integrandkappa(idx)
          !----------------------------------------------!
          ! computes integrand Kkappa of equ (9.54) from !
          ! Dahlen and Tromp at given radius rr(idx) for !
          ! radial eigenfunctions                        !
          !----------------------------------------------!
          implicit none
          integer(i4b), intent(in) :: idx
          integrandkappa = (r_int(idx)*duu(idx) + 2.0_dp*uu(idx))**2
        end function integrandkappa


        double precision function integrandmu(idx)
          !-------------------------------------------!
          ! computes integrand Kmu of equ (9.54) from !
          ! Dahlen and Tromp at given radius rr(idx)  !
          ! for radial eigenfunctions                 !
          !-------------------------------------------!
          implicit none
          integer(i4b), intent(in) :: idx
          integrandmu = 4.0_dp*(r_int(idx)*duu(idx) - uu(idx))**2/3.0_dp + zeta2*uu(idx)**2
        end function integrandmu

    end subroutine rad_utils


    !=======================================================!
    !               Spheroidal modes routines               !
    !=======================================================!

    subroutine sph_eigen_fct(l, f, uu, vv, pp, duu, dvv, dpp, flag, qinv)
      !-----------------------------------------!
      ! this routine computes the spheroidal    !
      ! eigenfunctions in a uniform earth model !
      !-----------------------------------------!
      use nrtype
      use module_input
      use module_int
      implicit none

      integer(i4b), intent(in) :: l
      real(dp), intent(in)  :: f
      real(dp), intent(out) :: qinv
      real(qp), dimension(ncm), intent(out) :: uu, vv, pp, duu, dvv, dpp

      integer :: i
      real(dp) :: rr
      real(qp) :: denom, a, b
      complex(dpc) :: w
      complex(dpc), dimension(:), allocatable :: R_surface, S_surface, B_surface
      complex(dpc), dimension(:), allocatable :: u_out, v_out, p_out
      complex(dpc), dimension(:), allocatable :: du_out, dv_out, dp_out
      logical :: flag, gamma_flag


      w = 2.0_dp*pi_d*f

      call set_l(l)
      call set_omega(w)

      call sph_uniform(R_surface, S_surface, B_surface, gamma_flag)

      if (gamma_flag) then
        ! compute the coeff of linear combination that respects boundary conditions
        ! ie A(rt) = A1(rt) + a*A2(rt) + b*A3(rt) = 0, with A(r) = R(r), S(r), B(r)
        denom = real(S_surface(2),qp)*real(B_surface(3),qp) - real(S_surface(3),dp)*real(B_surface(2),qp)
        if (abs(denom) > 1.0e-17_qp) then
          a = real(S_surface(1),qp)*real(B_surface(3),qp) - real(S_surface(3),qp)*real(B_surface(1),qp)
          b = real(S_surface(1),qp)*real(B_surface(2),qp) - real(S_surface(2),qp)*real(B_surface(1),qp)
          a = - a / denom
          b =   b / denom
        else

          denom = real(R_surface(2),qp)*real(B_surface(3),qp) - real(R_surface(3),qp)*real(B_surface(2),qp)
          if (abs(denom) > 1.0e-17_qp) then
            a = real(R_surface(1),qp)*real(B_surface(3),qp) - real(R_surface(3),qp)*real(B_surface(1),qp)
            b = real(R_surface(1),qp)*real(B_surface(2),qp) - real(R_surface(2),qp)*real(B_surface(1),qp)
            a = - a / denom
            b =   b / denom
          else

            denom = real(R_surface(2),qp)*real(S_surface(3),qp) - real(R_surface(3),qp)*real(S_surface(2),qp)
            if (abs(denom) > 1.0e-17_qp) then
              a = real(R_surface(1),qp)*real(S_surface(3),qp) - real(R_surface(3),qp)*real(S_surface(1),qp)
              b = real(R_surface(1),qp)*real(S_surface(2),qp) - real(R_surface(2),qp)*real(S_surface(1),qp)
              a = - a / denom
              b =   b / denom
            else

              print *, 'Warning : R1 + a*R2 + b*R3 was not done, for fr = ', f*fre_norm, ' and l = ', ll
              a = 0.0_qp
              b = 0.0_qp
            end if
          end if
        end if

        ! check the boundary conditions
        ! print *, 'R(a) = ', real(R_surface(1) + a*R_surface(2) + b*R_surface(3))
        ! print *, 'S(a) = ', real(S_surface(1) + a*S_surface(2) + b*S_surface(3))
        ! print *, 'B(a) = ', real(B_surface(1) + a*B_surface(2) + b*B_surface(3))

        do i=1,ncm
           rr = i*ddr

           call sph_out(rr, u_out, v_out, p_out, du_out, dv_out, dp_out, gamma_flag)

           uu(i)  = real(u_out(1) + a*u_out(2) + b*u_out(3), qp)
           vv(i)  = real(v_out(1) + a*v_out(2) + b*v_out(3), qp)
           pp(i)  = real(p_out(1) + a*p_out(2) + b*p_out(3), qp)

           duu(i) = real(du_out(1) + a*du_out(2) + b*du_out(3), qp)
           dvv(i) = real(dv_out(1) + a*dv_out(2) + b*dv_out(3), qp)
           dpp(i) = real(dp_out(1) + a*dp_out(2) + b*dp_out(3), qp)

        end do


      else
        ! compute the coeff of linear combination that respects boundary conditions
        ! ie A(rt) = A1(rt) + a*A2(rt) = 0, with A(r) = R(r), S(r)
        denom = real(R_surface(2),qp)

        if (abs(denom) > 1.0e-17_qp) then
          a = -real(R_surface(1),qp) / denom
        else

          denom = real(S_surface(2),qp)
          if (abs(denom) > 1.0e-17_qp) then
            a = -real(S_surface(1),qp) / denom
          else

            print *, 'Warning : R1 + a*R2 was not done, for fr = ', f*fre_norm, ' and l = ', ll
            a = 0.0_qp
          end if
        end if

        ! check the boundary conditions
        ! print *, 'R(a) = ', real(R_surface(1) + a*R_surface(2))
        ! print *, 'S(a) = ', real(S_surface(1) + a*S_surface(2))


        do i=1,ncm
           rr = i*ddr

           call sph_out(rr, u_out, v_out, p_out, du_out, dv_out, dp_out, gamma_flag)

           uu(i) = real(u_out(1) + a*u_out(2), qp)
           vv(i) = real(v_out(1) + a*v_out(2), qp)

           duu(i) = real(du_out(1) + a*du_out(2), qp)
           dvv(i) = real(dv_out(1) + a*dv_out(2), qp)

        end do

        pp(:)  = 0.0_qp
        dpp(:) = 0.0_qp

      end if

      call sph_utils(uu, vv, pp, duu, dvv, dpp, qinv, gamma_flag, flag)

    end subroutine sph_eigen_fct


    subroutine sph_utils(uu, vv, pp, duu, dvv, dpp, qinv, gamma_flag, flag)
      !--------------------------------------------------------!
      ! computes normalization coefficient, quality factor and !
      ! potential perturbation P(r) through direct integration !
      !--------------------------------------------------------!
      use nrtype
      use module_input
      use module_int
      use module_function
      implicit none

      real(qp), dimension(ncm), intent(inout)  :: uu, vv, pp, duu, dvv, dpp
      real(dp), intent(out) :: qinv
      logical, intent(in)  :: gamma_flag
      logical, intent(out) :: flag

      integer(i4b) :: i
      real(dp) :: sumkappa, summu, int1, int2
      real(dp), dimension(ncm) :: pp_int, dpp_int
      real(qp) :: norme


      if (.not.int_param) then
        allocate(r_int(ncm), r_int2(ncm))

        do i = 1,ncm
           r_int(i) = i*ddr
        end do

        r_int2(:) = r_int(:)**2
        int_param = .true.
      endif


      ! compute normalization coefficient
      norme = ((uu(1)**2+zeta2*vv(1)**2)*r_int2(1) + (uu(ncm)**2+zeta2*vv(ncm)**2)*r_int2(ncm)) * 0.5_qp*ddr
      norme = norme + sum( (uu(2:ncm-1)**2 + zeta2*vv(2:ncm-1)**2) * r_int2(2:ncm-1) ) * ddr
      norme = sqrt(rho*norme)

      if (abs(norme) > 1.0e-300_qp) then
        uu(:) =  uu(:) / norme
        vv(:) =  vv(:) / norme
        pp(:) =  pp(:) / norme

        duu(:) = duu(:) / norme
        dvv(:) = dvv(:) / norme
        dpp(:) = dpp(:) / norme

        ! print *, 'storage, norme = ', norme
        flag = .false.
      else
        print *, 'no storage, norme = ', norme
        flag = .true.
      end if


      if (.not. flag) then

        ! compute quality factor
        if (ats_switch /= 0.0_dp) then
          sumkappa = (integrandkappa(1) + integrandkappa(ncm)) * 0.5_dp*ddr
          sumkappa = sumkappa + sum( (r_int(2:ncm-1)*duu(2:ncm-1) + 2.0_dp*uu(2:ncm-1) - zeta2*vv(2:ncm-1))**2 ) * ddr
          sumkappa = sumkappa * kappa/qkappa

          summu = (integrandmu(1) + integrandmu(ncm)) * 0.5_dp*ddr
          summu = summu + sum( (2.0_dp*r_int(2:ncm-1)*duu(2:ncm-1)-2.0_dp*uu(2:ncm-1)+zeta2*vv(2:ncm-1))**2/3.0_dp &
            + (r_int(2:ncm-1)*zeta*dvv(2:ncm-1)-zeta*vv(2:ncm-1)+zeta*uu(2:ncm-1))**2 &
            + zeta2m2*zeta2*vv(2:ncm-1)**2 ) * ddr
          summu = summu * mu/qmu

          qinv = (sumkappa + summu) / real(omega2,dp)

        else
          qinv = 0.0_dp
        end if


        ! compute gravitational perturbation
        if (.not. gamma_flag) then

          pp_int(:) = 0.0_qp
          dpp_int(:) = 0.0_qp

          do i = 1,ncm

             if (i /= 1) then
               int1 = ( integrand1(1) + integrand1(i) ) * 0.5_qp*ddr
               if (i /= 2) then
                 int1 = int1 + sum( rho * (ll*uu(2:i-1) + zeta2*vv(2:i-1)) * r_int(2:i-1)**llp1 ) * ddr
               end if
             else
               int1 = 0.0_qp
             end if

             if (i /= ncm) then
               int2 = ( integrand2(i) + integrand2(ncm) ) * 0.5_qp*ddr
               if (i /= ncm-1) then
                 int2 = int2 + sum( rho * (-llp1*uu(i+1:ncm-1) + zeta2*vv(i+1:ncm-1)) / r_int(i+1:ncm-1)**ll ) * ddr
               end if
             else
               int2 = 0.0_qp
             end if

             pp_int(i) = -4.0_qp*pibigg/(2*ll+1) * (int1/r_int(i)**llp1 + int2*r_int(i)**ll)

             dpp_int(i) = -llp1*int1/r_int(i)**(ll+2) + integrand1(i)/r_int(i)**llp1
             dpp_int(i) = dpp_int(i) + ll*r_int(i)**(ll-1)*int2 - integrand2(i)*r_int(i)**ll
             dpp_int(i) = -4.0_qp*pibigg/(2*ll+1) * dpp_int(i)


             ! detection of NaN
             if ( pp_int(i)*0.0_qp.ne.0.0_qp)  pp_int(i) = 0.0_qp
             if (dpp_int(i)*0.0_qp.ne.0.0_qp) dpp_int(i) = 0.0_qp

          end do

          pp(:)  = pp_int(:)
          dpp(:) = dpp_int(:)

        end if

        ! print *, 'P(a) = ', pp(ncm)

      end if


      contains

        double precision function integrand1(idx)
          !---------------------------------------------!
          ! computes first integrand of equ (8.55) from !
          ! Dahlen and Tromp at given radius rr(idx)    !
          !---------------------------------------------!
          implicit none
          integer(i4b), intent(in) :: idx
          integrand1 = rho * (ll*uu(idx) + zeta2*vv(idx)) * r_int(idx)**llp1
        end function integrand1


        double precision function integrand2(idx)
          !----------------------------------------------!
          ! computes second integrand of equ (8.55) from !
          ! Dahlen and Tromp at given radius rr(idx)     !
          !----------------------------------------------!
          implicit none
          integer(i4b), intent(in) :: idx
          integrand2 = rho * (-llp1*uu(idx) + zeta2*vv(idx)) / r_int(idx)**ll
        end function integrand2


        double precision function integrandkappa(idx)
          !----------------------------------------------!
          ! computes integrand Kkappa of equ (9.54) from !
          ! Dahlen and Tromp at given radius rr(idx) for !
          ! spheroidal eigenfunctions                    !
          !----------------------------------------------!
          implicit none
          integer(i4b), intent(in) :: idx
          integrandkappa = (r_int(idx)*duu(idx) + 2.0_dp*uu(idx) - zeta2*vv(idx))**2
        end function integrandkappa


        double precision function integrandmu(idx)
          !-------------------------------------------!
          ! computes integrand Kmu of equ (9.54) from !
          ! Dahlen and Tromp at given radius rr(idx)  !
          ! for spheroidal eigenfunctions             !
          !-------------------------------------------!
          implicit none
          integer(i4b), intent(in) :: idx
          real(dp) :: summu

          summu = (2.0_dp*r_int(idx)*duu(idx) - 2.0_dp*uu(idx) + zeta2*vv(idx))**2 / 3.0_dp
          summu = summu + (r_int(idx)*zeta*dvv(idx) - zeta*vv(idx) + zeta*uu(idx))**2
          summu = summu + zeta2m2*zeta2*vv(idx)**2

          integrandmu = summu
        end function integrandmu

    end subroutine sph_utils


    !===========================================!
    !             general routines              !
    !===========================================!

    subroutine set_outputs
      !------------------------------------------------------!
      ! set output files for eigen functions and frequencies !
      !------------------------------------------------------!
      use nrtype
      use module_input
      implicit none

      integer(i4b) :: len_dpt,len_int,len_sp
      real(sp) :: test_sp

      unit_rd = 133
      unit_ri = 134
      unit_ld = 135
      unit_li = 136

      lov_rec = 1
      rayl_rec = 1

      if (oldminos) then
        open(unit_ld, file='fctTOR', form='unformatted')
        open(unit_rd, file='fctSPH', form='unformatted')

      else
        inquire(iolength = len_dpt) rt
        inquire(iolength = len_int) ipf
        inquire(iolength = len_sp)  test_sp

        len_r = len_int*2 + 3*len_dpt + 6*ncm*len_sp
        len_l = len_int*2 + 3*len_dpt + 2*ncm*len_sp

        open(unit_ld, file='fctTOR.direct', access='direct', recl=len_l)
        open(unit_rd, file='fctSPH.direct', access='direct', recl=len_r)

        open(unit_li, file='fctTOR.info')
        open(unit_ri, file='fctSPH.info')
      endif

      return

    end subroutine set_outputs


    subroutine write_eigen_fct(fz, l, nz, type)
       !-------------------------------------!
       ! this routine writes toroidal and    !
       ! spheroidal eigen functions in files !
       !-------------------------------------!
       use nrtype
       use module_input
       implicit none

       real(dp), intent(in) :: fz
       integer(i4b), intent(in) :: l, nz, type

       real(qp), dimension(ncm) :: uu, vv, ww, pp, duu, dvv, dww, dpp
       integer(i4b) :: i, n
       real(dp) :: om, qq, qinv
       logical :: flag


       n  = nz-1
       om = 2.0_dp*pi_d*fz

       select case(type)

       case(1)
         if (l == 0) then
           ! radial modes
           call rad_eigen_fct(l, fz, uu, vv, pp, duu, dvv, dpp, flag, qinv)
         else
           ! spheroidal modes
           call sph_eigen_fct(l, fz, uu, vv, pp, duu, dvv, dpp, flag, qinv)
         end if

         ! denormalize the eigenfrequency for the output
         om = om*fre_norm

         ! compute the quality factor Q
         if (qinv /= 0.0_dp) then
           qq = 1.0_dp / qinv
         else
           qq = 0.0_dp
         end if

         if (.not.flag) then
           ! get rid of 0S1
           ! if ( n==0 .and. l==1 ) then
           !   uu(:)  = 0.0_qp;  vv(:) = 0.0_qp;  pp(:) = 0.0_qp
           !   duu(:) = 0.0_qp; dvv(:) = 0.0_qp; dpp(:) = 0.0_qp
           ! end if

           ! write eigenfunction into file
           if (oldminos) then
             write(unit_rd) nz-1, l, om, qq, 0.0_dp, 0.0_dp, &
             (real( uu(i),sp), i=1,ncm), &
             (real(duu(i),sp), i=1,ncm), &
             (real( vv(i),sp), i=1,ncm), &
             (real(dvv(i),sp), i=1,ncm), &
             (real( pp(i),sp), i=1,ncm), &
             (real(dpp(i),sp), i=1,ncm)
           else
             rayl_rec = rayl_rec + 1
             write(unit_rd,rec=rayl_rec) nz-1, l, om, qq, 0.0_DP, &
             (real( uu(i),sp), &
              real(duu(i),sp), &
              real( vv(i),sp), &
              real(dvv(i),sp), &
              real( pp(i),sp), &
              real(dpp(i),sp), i=ncm,1,-1)
             write(unit_ri,*) nz-1, l, om, qq, 0.0_dp, rayl_rec, 0.d0, .false.
           end if
         end if

       case(2)
         ! toroidal modes
         call tor_eigen_fct(l, fz, ww, dww, flag, qinv)

         ! denormalize the eigenfrequency for the output
         om = om*fre_norm

         ! compute the quality factor Q
         if (qinv /= 0.0_dp) then
           qq = 1.0_dp / qinv
         else
           qq = 0.0_dp
         end if

         if (.not.flag) then
           ! ! get rid of 0S1
           ! if ( n==0 .and. l==1 ) then
           !   ww(:)  = 0.0_dp
           !   dww(:) = 0.0_dp
           ! end if

           ! write eigenfunction into file
           if (oldminos) then
             write(unit_ld) nz-1, l, om, qq, 0.0_dp, 0.0_dp, &
             (real( ww(i),sp), i=1,ncm), &
             (real(dww(i),sp), i=1,ncm)
           else
             lov_rec = lov_rec + 1
             write(unit_ld,rec=lov_rec) nz-1, l, om, qq, 0.0_DP, &
             (real(ww(i),sp), real(dww(i),sp), i=ncm,1,-1)
             write(unit_li,*) nz-1, l, om, qq, 0.0_dp, lov_rec, 0.d0, .false.
           end if
         end if

       case default
         stop 'write_eigen_func : incorrect value for type (1 for rad and sph, 2 for tor)'

       end select

    end subroutine write_eigen_fct


    subroutine close_uniform(lmax_pres_r, lmax_pres_l, eigen_f, eigen_n)
      !-------------------------------------------------!
      ! this routine writes end of eigenfunctions files !
      ! and print eigenfrequencies into files           !
      !-------------------------------------------------!
      use nrtype
      use module_input
      implicit none

      integer(i4b), intent(in) :: lmax_pres_r,lmax_pres_l
      integer(i4b), dimension(lmin:lmax,2), intent(in)  :: eigen_n
      real(dp), dimension(nmax,lmin:lmax,2), intent(in) :: eigen_f

      integer(i4b) :: i,l,nz


      if (.not.oldminos) then
        write(unit_rd, rec=1) len_r, lmax_pres_r, rayl_rec, 0, ncm, &
          (real(1.0_dp-(i-1)*ddr/rt,sp), i=ncm,1,-1)

        write(unit_ld,rec=1) len_l, lmax_pres_l, lov_rec, 0, NCM, &
          (real(1.0_dp-(i-1)*ddr/rt,sp), i=ncm,1,-1)
      end if

      open(23, file='freqSPH')
      do l = lmin,lmax_pres_r
         do nz = 1,eigen_n(l,1)
            write(23,*) l, nz-1, eigen_f(nz,l,1)*fre_norm
            ! if (.not. (l==1.and.nz==1) ) then
            !   write(23,*) l, nz-1, eigen_f(nz,l,1)*fre_norm
            ! end if
         enddo
      enddo
      close(23)

      open(23,file='freqTOR')
      do l = lmin,lmax_pres_l
         do nz = 1,eigen_n(l,2)
            write(23,*) l, nz-1, eigen_f(nz,l,2)*fre_norm
            ! if (.not. (l==1.and.nz==1) ) then
            !   write(23,*) l, nz-1, eigen_f(nz,l,2)*fre_norm
            ! end if
         enddo
      enddo
      close(23)

      close(unit_rd)
      close(unit_ri)
      close(unit_ld)
      close(unit_li)

    end subroutine close_uniform


end module module_write
