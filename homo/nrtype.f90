module nrtype
  ! symbolic names for kind types of 4-, 2-, and 1-byte integers:
  integer, parameter :: i4b = selected_int_kind(9)
  integer, parameter :: i2b = selected_int_kind(4)
  integer, parameter :: ib1 = selected_int_kind(2)

  ! symbolic names for kinds of single- and double-precision reals:
  integer, parameter :: sp = selected_real_kind(6, 37) ! kind(1.0)
  integer, parameter :: dp = selected_real_kind(15, 307) ! kind(1.0d0)
  integer, parameter :: qp = selected_real_kind(33, 4931)

  ! symbolic names for kinds of single- and double-precision complex:
  integer, parameter :: spc = kind((1.0,1.0))
  integer, parameter :: dpc = kind((1.0d0,1.0d0))
  integer, parameter :: qpc = kind((1.0q0,1.0q0))

  ! symbolic name for kind type of default logical:
  integer, parameter :: lgt = kind(.true.)

  ! frequently used mathematical constants (with precision to spare):
  real(dp), parameter :: pi_d=3.141592653589793238462643383279502884197_dp
  real(dp), parameter :: twopi_d=6.283185307179586476925286766559005768394_dp
  real(dp), parameter :: fourpi_d=12.56637061_dp
  real(dp), parameter :: bigg=6.6723e-11_dp ! this was changed from 6.673e-11_dp

  complex(dpc), parameter :: zero=(0.0_dp,0.0_dp)
  complex(dpc), parameter :: one=(1.0_dp,0.0_dp)
  complex(dpc), parameter :: ii=(0.0_dp,1.0_dp)

end module nrtype
