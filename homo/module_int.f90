module module_int
  !------------------------------------------------------!
  ! This module contains a range of routines used in the !
  ! numerical integration of the differential equations  !
  ! governing the deformation of a spherically symmetric !
  ! earth model.                                         !
  !------------------------------------------------------!

  use nrtype
  use module_input
  implicit none

  !=============================================!
  !       saved variables in the module         !
  !=============================================!

  integer(i4b), save, public :: ll       ! equal to the spherical harmonic degree l
  integer(i4b), save, public :: llp1     ! equal to l+1

  real(dp), save, public :: zeta         ! equal to sqrt(l(l+1))
  real(dp), save, public :: zeta2        ! equal to l(l+1)
  real(dp), save, public :: zeta2m2      ! equal to l(l+1)-2
  real(dp), save, public :: szeta2m2     ! equal to sqrt(l(l+1)-2)
  real(dp), save, public :: nu           ! equal to sqrt((2l+1)/4*pi)
  real(dp), save, public :: ats          ! parameter used to scale
                                         ! attenuation in the model
                                         ! ats=0 gives no attenuation,
                                         ! ats=1 gives full attenuation.

  complex(dpc), save, public :: omega    ! current value of angular frequency w
  complex(dpc), save, public :: omega2   ! equal to w*w
  complex(dpc), save, public :: omegai   ! equal to 1/w
  complex(dpc), save, public :: omegai2  ! equal to 1/(w*w)
  complex(dpc), save, public :: lno      ! equal to (2/pi)*ln(i*w/w0), with w0 the
                                         ! reference frequency of the earth model.
                                         ! This value is used in the computation
                                         ! of the anelastic moduli.

  contains

    !========================================================!
    !                Toroidal mode section                   !
    !========================================================!

    subroutine tor_uniform(T_surface)
      !-------------------------------------------------!
      ! This routine returns the value for the toroidal !
      ! eigenfunction T at the earth surface, according !
      ! to equation (8.119) of Dahlen and Tromp (1998)  !
      !-------------------------------------------------!
      use nrtype
      use module_input
      use module_function
      implicit none

      complex(dpc), intent(out) :: T_surface

      integer(i4b) :: lm
      complex(dpc) :: gamma, arg, jl, jlp1, cmu, cbeta
      complex(dpc), dimension(0:llp1) :: jj


      cmu   = mu*(1.0_dp + lno/qmu)
      cbeta = cmu/rho

      gamma = omega / sqrt(cbeta)
      arg   = gamma*rt

      if (arg.ne.0.0_dp .and. ll.ne.0) then
        ! compute spherical Bessel functions
        call csphj(llp1, arg, lm, jj)

        if (lm /= llp1) then
          ! print *, 'csphj failed for tor system, r = ', rt*r_norm, ', arg = ', arg
          ! print *, 'jlm = ', jj(lm), ', dl = ', lm-llp1
          jl   = 0.0_dp
          jlp1 = 0.0_dp
        else
          jl   = jj(ll)
          jlp1 = jj(llp1)
        end if

        ! detection of NaN
        if (  jl*0.0_dp.ne.0.0_dp)   jl = 0.0_dp
        if (jlp1*0.0_dp.ne.0.0_dp) jlp1 = 0.0_dp

        ! compute solutions
        T_surface = cmu*((ll-1)*jl - arg*jlp1)/rt

      else
        T_surface = 0.0_dp

      end if

      return
    end subroutine tor_uniform


    subroutine tor_out(rr, W_r, dW_r)
      !------------------------------------------!
      ! This routine returns the output toroidal !
      ! eigenfunctions W(r) and dW(r) according  !
      ! to equation (8.118) of Dahlen and Tromp  !
      !------------------------------------------!
      use nrtype
      use module_input
      use module_function
      implicit none

      real(dp), intent(in) :: rr
      complex(dpc), intent(out) :: W_r, dW_r

      integer(i4b) :: lm
      complex(dpc) :: gamma, arg, jl, djl, cmu, cbeta
      complex(dpc), dimension(0:ll) :: jj, djj


      cmu   = mu*(1.0_dp + lno/qmu)
      cbeta = cmu/rho

      gamma = omega / sqrt(cbeta)
      arg   = gamma*rr

      if (arg.ne.0.0_dp .and. ll.ne.0) then
        ! compute spherical Bessel functions
        call csphjdj(ll, arg, lm, jj, djj)

        if (lm /= ll) then
          ! print *, 'csphjdj failed for tor output, r = ', rr*r_norm, ', arg = ', arg
          ! print *, 'jlm = ', jj(lm), ', djlm = ', djj(lm), ', dl = ', lm-ll
          jl  = 0.0_dp
          djl = 0.0_dp
        else
          jl  = jj(ll)
          djl = djj(ll)
        end if

        ! detection of NaN
        if (  jl*0.0_dp.ne.0.0_dp)  jl = 0.0_dp
        if ( djl*0.0_dp.ne.0.0_dp) djl = 0.0_dp

        ! compute solutions
        W_r  = jl
        dW_r = gamma*djl

      else
        W_r  = 0.0_dp
        dW_r = 0.0_dp

      end if

      return
    end subroutine tor_out


    !=============================================================!
    !                    Radial mode section                      !
    !=============================================================!

    subroutine rad_uniform(R_surface)
      !-------------------------------------------------!
      ! This routine returns the value for the radial   !
      ! eigenfunction R at the earth surface, according !
      ! to equation (8.168) of Dahlen and Tromp (1998)  !
      !-------------------------------------------------!
      use nrtype
      use module_input
      use module_function
      implicit none

      complex(dpc), intent(out) :: R_surface

      integer(i4b) :: lm
      complex(dpc) :: gamma, arg, j0, j1, cmu, ckappa, calpha
      complex(dpc), dimension(0:1) :: jj


      cmu    = mu*(1.0_dp + lno/qmu)
      ckappa = kappa*(1.0_dp + lno/qkappa)

      calpha = (ckappa + 4.0_dp*cmu/3.0_dp)/rho

      if (selfgr_switch) then
        gamma = omega2 + 16.0_dp*pibigg*rho/3.0_dp
      else
        gamma = omega2
      end if

      gamma = sqrt(gamma/calpha)
      arg   = gamma*rt

      ! compute spherical Bessel functions
      call csphj(1, arg, lm, jj)

      if (lm /= 1) then
        ! print *, 'csphj failed for rad system, r = ', rt*r_norm, ', arg = ', arg
        ! print *, 'jlm = ', jj(lm), ', dl = ', lm-1
        j0 = 0.0_dp
        j1 = 0.0_dp
      else
        j0 = jj(0)
        j1 = jj(1)
      end if

      ! compute solution
      R_surface = rho*calpha*gamma*j0 - 4.0_dp*cmu*j1/rt

      return
    end subroutine rad_uniform


    subroutine rad_out(rr, U_r, dU_r)
      !-----------------------------------------!
      ! This routine returns the output radial  !
      ! eigenfunctions U(r) and dU(r) according !
      ! to equation (8.167) of Dahlen and Tromp !
      !-----------------------------------------!
      use nrtype
      use module_input
      use module_function
      implicit none

      real(dp), intent(in) :: rr
      complex(dpc), intent(out) :: U_r, dU_r

      integer(i4b) :: lm
      complex(dpc) :: gamma, gamma2, arg, j1, dj1, cmu, ckappa, calpha
      complex(dpc), dimension(0:1) :: jj, djj


      cmu    = mu*(1.0_dp + lno/qmu)
      ckappa = kappa*(1.0_dp + lno/qkappa)

      calpha = (ckappa + 4.0_dp*cmu/3.0_dp)/rho

      if (selfgr_switch) then
        gamma = omega2 + 16.0_dp*pibigg*rho/3.0_dp
      else
        gamma = omega2
      end if

      gamma  = sqrt(gamma / calpha)

      gamma2 = gamma*gamma
      arg    = gamma*rr

      ! compute spherical Bessel functions
      call csphjdj(1, arg, lm, jj, djj)

      if (lm /= 1) then
        ! print *, 'csphjdj failed for rad output, r = ', rr*r_norm, ', arg = ', arg
        ! print *, 'jlm = ', jj(lm), ', djlm = ', djj(lm), ', dl = ', lm-1
        j1  = 0.0_dp
        dj1 = 0.0_dp
      else
        j1  = jj(1)
        dj1 = djj(1)
      end if

      ! compute solution
      U_r  = -gamma*j1
      dU_r = -gamma2*dj1

      return
    end subroutine rad_out


    !=============================================================!
    !                   Spheroidal mode section                   !
    !=============================================================!

    subroutine sph_uniform(R_surface, S_surface, B_surface, gamma_flag)
      !-----------------------------------------------------------!
      ! This routine returns the values for the spheroidal        !
      ! eigenfunctions R, S and B at the earth surface, according !
      ! to systems (104)-(105) from Takeuchi and Saito (1972) or  !
      ! equations (8.176-8) and (8.182-3) from Dahlen and Tromp   !
      !-----------------------------------------------------------!
      use nrtype
      use module_input
      use module_function
      implicit none

      complex(dpc), dimension(:), allocatable, intent(out) :: R_surface, S_surface, B_surface
      logical, intent(out) :: gamma_flag

      integer(i4b) :: i, lm
      real(dp) :: sign, dot1, dot2, dot3
      complex(dpc) :: tmp1, tmp2, tmp3, zt, xi
      complex(dpc) :: gamma, gamma2, arg, args, jl, jlp1
      complex(dpc) :: cmu, ckappa, calpha, cbeta
      complex(dpc), dimension(0:llp1) :: jj


      cmu    = mu*(1.0_dp + lno/qmu)
      ckappa = kappa*(1.0_dp + lno/qkappa)

      calpha = (ckappa + 4.0_dp*cmu/3.0_dp)/rho
      cbeta  = cmu/rho

      tmp1   = omega2 / cbeta
      tmp2   = omega2 + 16.0_dp*pibigg*rho/3.0_dp
      tmp2   = tmp2 / calpha
      tmp3   = 8.0_dp*pibigg*zeta*rho/3.0_dp
      tmp3   = tmp3*tmp3/(calpha*cbeta)

      gamma_flag = .false.

      if (selfgr_switch) then
        gamma_flag = .true.

        allocate(R_surface(3), S_surface(3), B_surface(3))

        ! FIRST TWO SOLUTIONS
        sign = 1.0_dp

        do i=1,2
           gamma  = (tmp1-tmp2)**2 + tmp3
           gamma  = 0.5_dp*(tmp1 + tmp2) &
                   + 0.5_dp*sign*sqrt(gamma)
           gamma2 = gamma
           gamma  = sqrt(gamma)

           zt = 0.75_dp*cbeta*(gamma2-tmp1)/(pibigg*rho)
           xi = zt - real(llp1,dp)

           arg  = gamma*rt
           args = arg*arg

           if (arg.ne.0.0_dp) then
             ! compute spherical Bessel functions
             call csphj(llp1, arg, lm, jj)

             if (lm /= llp1) then
               ! print *, 'csphj failed for sph system, r = ', rt*r_norm, ', arg = ', arg
               ! print *, 'jlm = ', jj(lm), ', dl = ', lm-llp1
               jl   = 0.0_dp
               jlp1 = 0.0_dp
             else
               jl   = jj(ll)
               jlp1 = jj(llp1)
             end if

             ! detection of NaN
             if (  jl*0.0_dp.ne.0.0_dp)   jl = 0.0_dp
             if (jlp1*0.0_dp.ne.0.0_dp) jlp1 = 0.0_dp

             ! compute solutions
             R_surface(i) = -(rho*calpha*zt*gamma2 - 2.0_dp*ll*(ll-1)*cmu*xi/rt**2)
             R_surface(i) = R_surface(i)*jl + 2.0_dp*cmu*(2.0_dp*zt+zeta2)*gamma*jlp1/rt

             S_surface(i) = cmu*(gamma2+2.0_dp*(ll-1)*xi/rt**2)*jl &
               -2.0_dp*cmu*(zt+1.0_dp)*gamma*jlp1/rt

             B_surface(i) = -4.0_dp*pibigg*rho*(zeta2+llp1*zt)*jl/rt

           else
             R_surface(i) = 0.0_dp
             S_surface(i) = 0.0_dp
             B_surface(i) = 0.0_dp

           end if
           sign = -sign

        end do

        ! THIRD SOLUTION
        R_surface(3) = 2.0_dp*ll*(ll-1)*cmu*rt**(ll-2)
        S_surface(3) = 2.0_dp*cmu*(ll-1)*rt**(ll-2)
        B_surface(3) = ((2*ll+1)*omega2-8.0_dp*pibigg*ll*(ll-1)*rho/3.0_dp)*rt**(ll-1)


        ! test the independency of the three solutions
        dot1 = real(S_surface(2),dp)*real(B_surface(3),dp) - real(S_surface(3),dp)*real(B_surface(2),dp)
        dot2 = real(R_surface(2),dp)*real(B_surface(3),dp) - real(R_surface(3),dp)*real(B_surface(2),dp)
        dot3 = real(R_surface(2),dp)*real(S_surface(3),dp) - real(R_surface(3),dp)*real(S_surface(2),dp)

        if (abs(dot1).le.1.0e-17_dp .and. abs(dot2).le.1.0e-17_dp .and. abs(dot3).le.1.0e-17_dp) then
          gamma_flag = .false.
          deallocate(R_surface, S_surface, B_surface)
        end if
      end if

      if (.not. gamma_flag) then
        allocate(R_surface(2), S_surface(2))

        ! FIRST SOLUTION
        gamma  = omega2/calpha
        gamma2 = gamma
        gamma  = sqrt(gamma)
        arg    = gamma*rt

        if (arg.ne.0.0_dp) then
          ! compute spherical Bessel functions
          call csphj(llp1, arg, lm, jj)

          if (lm /= llp1) then
            ! print *, 'csphj failed for sph system, r = ', rt*r_norm, ', arg = ', arg
            ! print *, 'jlm = ', jj(lm), ', dl = ', lm-llp1
            jl   = 0.0_dp
            jlp1 = 0.0_dp
          else
            jl   = jj(ll)
            jlp1 = jj(llp1)
          end if

          ! detection of NaN
          if (  jl*0.0_dp.ne.0.0_dp)   jl = 0.0_dp
          if (jlp1*0.0_dp.ne.0.0_dp) jlp1 = 0.0_dp

          ! compute solution
          R_surface(1) = -(rho*calpha*gamma2 - 2.0_dp*real(ll*(ll-1),dp)*cmu/rt**2)
          R_surface(1) = R_surface(1)*jl + 4.0_dp*cmu*gamma*jlp1/rt
          S_surface(1) = 2.0_dp*cmu*(real(ll-1,dp)*jl/rt**2 - gamma*jlp1/rt)
        else
          R_surface(1) = 0.0_dp
          S_surface(1) = 0.0_dp
        end if

        ! SECOND SOLUTION
        gamma  = omega2/cbeta
        gamma2 = gamma
        gamma  = sqrt(gamma)
        arg    = gamma*rt

        if (arg.ne.0.0_dp) then
          ! compute spherical Bessel functions
          call csphj(llp1, arg, lm, jj)

          if (lm /= llp1) then
            ! print *, 'csphj failed for sph system, r = ', rt*r_norm, ', arg = ', arg
            ! print *, 'jlm = ', jj(lm), ', dl = ', lm-llp1
            jl   = 0.0_dp
            jlp1 = 0.0_dp
          else
            jl   = jj(ll)
            jlp1 = jj(llp1)
          end if

          ! detection of NaN
          if (  jl*0.0_dp.ne.0.0_dp)   jl = 0.0_dp
          if (jlp1*0.0_dp.ne.0.0_dp) jlp1 = 0.0_dp

          ! compute solution
          R_surface(2) = 2.0_dp*cmu*(real(-ll*(ll**2-1),dp)*jl/rt**2 + zeta2*gamma*jlp1/rt)
          S_surface(2) = cmu*(gamma2-2.0_dp*real(ll**2-1,dp)/rt**2)*jl - 2.0_dp*cmu*gamma*jlp1/rt

        else
          R_surface(2) = 0.0_dp
          S_surface(2) = 0.0_dp

        end if

      end if

      return
    end subroutine sph_uniform


    subroutine sph_out(rr, U_r, V_r, P_r, dU_r, dV_r, dP_r, gamma_flag)
      !---------------------------------------------!
      ! This routine returns the output spheroidal  !
      ! eigenfunctions U(r), V(r), P(r) and their   !
      ! derivatives according to systems (104) and  !
      ! (105) from Takeuchi and Saito (1972) or to  !
      ! equations (8.173-5) and (8.181) from Dahlen !
      ! and Tromp (1998)                            !
      !---------------------------------------------!
      use nrtype
      use module_input
      use module_function
      implicit none

      real(dp), intent(in) :: rr
      complex(dpc), dimension(:), allocatable, intent(out) :: U_r, V_r, P_r, dU_r, dV_r, dP_r
      logical, intent(in) :: gamma_flag

      integer(i4b) :: i, lm
      real(dp) :: sign
      complex(dpc) :: tmp1, tmp2, tmp3, zt, xi
      complex(dpc) :: gamma, gamma2, arg, args
      complex(dpc) :: jl, jlp1, djl, djlp1
      complex(dpc) :: cmu, ckappa, calpha, cbeta
      complex(dpc), dimension(0:llp1) :: jj, djj


      cmu    = mu*(1.0_dp + lno/qmu)
      ckappa = kappa*(1.0_dp + lno/qkappa)

      calpha = (ckappa + 4.0_dp*cmu/3.0_dp)/rho
      cbeta  = cmu/rho

      if (gamma_flag) then
        allocate(U_r(3), V_r(3), P_r(3), dU_r(3), dV_r(3), dP_r(3))

        tmp1   = omega2 / cbeta
        tmp2   = omega2 + 16.0_dp*pibigg*rho/3.0_dp
        tmp2   = tmp2 / calpha
        tmp3   = 8.0_dp*pibigg*zeta*rho/3.0_dp
        tmp3   = tmp3*tmp3/(calpha*cbeta)

        ! FIRST TWO SOLUTIONS
        sign = 1.0_dp

        do i=1,2
           gamma  = (tmp1-tmp2)**2 + tmp3
           gamma  = 0.5_dp*(tmp1 + tmp2)  &
                   + 0.5_dp*sign*sqrt(gamma)
           gamma2 = gamma
           gamma  = sqrt(gamma)

           zt = 0.75_dp*cbeta*(gamma2-tmp1)/(pibigg*rho)
           xi = zt - real(llp1,dp)

           arg  = gamma*rr
           args = arg*arg

           if (arg.ne.0.0_dp) then
             ! compute spherical Bessel functions
             call csphjdj(llp1, arg, lm, jj, djj)

             if (lm /= llp1) then
               ! print *, 'csphjdj failed for sph output, r = ', rr*r_norm, ', arg = ', arg
               ! print *, 'jlm = ', jj(lm), ', djlm = ', djj(lm), ', dl = ', lm-llp1
               jl    = 0.0_dp
               jlp1  = 0.0_dp
               djl   = 0.0_dp
               djlp1 = 0.0_dp
             else
               jl    = jj(ll)
               jlp1  = jj(llp1)
               djl   = djj(ll)
               djlp1 = djj(llp1)
             end if

             ! detection of NaN
             if (   jl*0.0_dp.ne.0.0_dp)    jl = 0.0_dp
             if ( jlp1*0.0_dp.ne.0.0_dp)  jlp1 = 0.0_dp
             if (  djl*0.0_dp.ne.0.0_dp)   djl = 0.0_dp
             if (djlp1*0.0_dp.ne.0.0_dp) djlp1 = 0.0_dp

             ! compute solutions
             U_r(i)  = ll*xi*jl/rr - zt*gamma*jlp1
             V_r(i)  = xi*jl/rr + gamma*jlp1
             P_r(i)  = -4.0_dp*pibigg*rho*zt*jl

             dU_r(i) = -ll*xi*jl/rr**2 + ll*xi*gamma*djl/rr - zt*gamma2*djlp1
             dV_r(i) = -xi*jl/rr**2 + xi*gamma*djl/rr + gamma2*djlp1
             dP_r(i) = -4.0_dp*pibigg*rho*zt*gamma*djl

           else
             U_r(i)  = 0.0_dp
             V_r(i)  = 0.0_dp
             P_r(i)  = 0.0_dp

             dU_r(i) = 0.0_dp
             dV_r(i) = 0.0_dp
             dP_r(i) = 0.0_dp

           end if
           sign = -sign

        end do

        ! THIRD SOLUTION
        U_r(3)  = ll*rr**(ll-1)
        V_r(3)  = rr**(ll-1)
        P_r(3)  = (omega2-4.0_dp*pibigg*rho*ll/3.0_dp)*rr**ll

        dU_r(3) = ll*(ll-1)*rr**(ll-2)
        dV_r(3) = (ll-1)*rr**(ll-2)
        dP_r(3) = (omega2-4.0_dp*pibigg*rho*ll/3.0_dp)*ll*rr**(ll-1)


      else

        allocate(U_r(2), V_r(2), dU_r(2), dV_r(2))

        ! FIRST SOLUTION
        gamma  = omega2/calpha
        gamma2 = gamma
        gamma  = sqrt(gamma)
        arg    = gamma*rr

        if (arg.ne.0.0_dp) then
          ! compute spherical Bessel functions
          call csphjdj(llp1, arg, lm, jj, djj)

          if (lm /= llp1) then
            ! if (rr == rt) then
            !   print *, 'csphjdj failed for sph output, r = ', rr*r_norm, ', arg = ', arg
            !   print *, 'jlm = ', jj(lm), ', djlm = ', djj(lm), ', dl = ', lm-llp1
            ! end if
            jl    = 0.0_dp
            jlp1  = 0.0_dp
            djl   = 0.0_dp
            djlp1 = 0.0_dp
          else
            jl    = jj(ll)
            jlp1  = jj(llp1)
            djl   = djj(ll)
            djlp1 = djj(llp1)
          end if

          ! detection of NaN
          if (   jl*0.0_dp.ne.0.0_dp)    jl = 0.0_dp
          if ( jlp1*0.0_dp.ne.0.0_dp)  jlp1 = 0.0_dp
          if (  djl*0.0_dp.ne.0.0_dp)   djl = 0.0_dp
          if (djlp1*0.0_dp.ne.0.0_dp) djlp1 = 0.0_dp

          ! compute solution
          U_r(1)  = ll*jl/rr - gamma*jlp1
          V_r(1)  = jl/rr

          dU_r(1) = ll*gamma*djl/rr - ll*jl/rr**2 - gamma2*djlp1
          dV_r(1) = gamma*djl/rr - jl/rr**2

        else
          U_r(1)  = 0.0_dp
          V_r(1)  = 0.0_dp

          dU_r(1) = 0.0_dp
          dV_r(1) = 0.0_dp
        end if


        ! SECOND SOLUTION
        gamma  = omega2/cbeta
        gamma2 = gamma
        gamma  = sqrt(gamma)
        arg    = gamma*rr

        if (arg.ne.0.0_dp) then
          ! compute spherical Bessel functions
          call csphjdj(llp1, arg, lm, jj, djj)

          if (lm /= llp1) then
            ! if (rr == rt) then
            !   print *, 'csphjdj failed for sph output, r = ', rr*r_norm, ', arg = ', arg
            !   print *, 'jlm = ', jj(lm), ', djlm = ', djj(lm), ', dl = ', lm-llp1
            ! end if
            jl    = 0.0_dp
            jlp1  = 0.0_dp
            djl   = 0.0_dp
            djlp1 = 0.0_dp
          else
            jl    = jj(ll)
            jlp1  = jj(llp1)
            djl   = djj(ll)
            djlp1 = djj(llp1)
          end if

          ! detection of NaN
          if (   jl*0.0_dp.ne.0.0_dp)    jl = 0.0_dp
          if ( jlp1*0.0_dp.ne.0.0_dp)  jlp1 = 0.0_dp
          if (  djl*0.0_dp.ne.0.0_dp)   djl = 0.0_dp
          if (djlp1*0.0_dp.ne.0.0_dp) djlp1 = 0.0_dp

          ! compute solution
          U_r(2)  = -zeta2*jl/rr
          V_r(2)  = -llp1*jl/rr + gamma*jlp1

          dU_r(2) = -zeta2*(gamma*djl/rr - jl/rr**2)
          dV_r(2) = -llp1*(gamma*djl/rr - jl/rr**2) + gamma2*djlp1

        else
          U_r(2)  = 0.0_dp
          V_r(2)  = 0.0_dp

          dU_r(2) = 0.0_dp
          dV_r(2) = 0.0_dp

        end if


      end if

      return
    end subroutine sph_out


    !--------------------------------------------------------!
    !              utility and storage routines              !
    !--------------------------------------------------------!

    function det3(y1, y2, y3)
      use nrtype
      implicit none
      real(qp) :: det3
      real(qp), dimension(3), intent(in) :: y1, y2, y3

      det3 = y1(1) * det2(y2(2:3), y3(2:3)) &
        - y2(1) * det2(y1(2:3), y3(2:3)) &
        + y3(1) * det2(y1(2:3), y2(2:3))

      return
    end function det3


    function det2(y1, y2)
      use nrtype
      implicit none
      real(qp) :: det2
      real(qp), dimension(2), intent(in) :: y1, y2
      det2 = y1(1)*y2(2) - y1(2)*y2(1)
      return
    end function det2


    function nrm3(y1, y2, y3)
      use nrtype
      implicit none
      real(qp) :: nrm3
      real(qp), dimension(3), intent(in) :: y1, y2, y3

      nrm3 = abs(y1(1)) * nrm2(y2(2:3), y3(2:3)) &
        + abs(y2(1)) * nrm2(y1(2:3), y3(2:3)) &
        + abs(y3(1)) * nrm2(y1(2:3), y2(2:3))

      return
    end function nrm3


    function nrm2(y1, y2)
      use nrtype
      implicit none
      real(qp) :: nrm2
      real(qp), dimension(2), intent(in) :: y1, y2
      nrm2 = abs(y1(1)*y2(2)) + abs(y1(2)*y2(1))
      return
    end function nrm2


    subroutine set_l(l_in)
      use nrtype
      implicit none
      integer(i4b), intent(in) :: l_in
      ll = l_in
      llp1 = ll + 1
      zeta2 = real(ll*llp1,dp)
      zeta  = sqrt(zeta2)
      zeta2m2 = zeta2 - 2.0_dp
      if(zeta2m2 > 0.0_dp) then
         szeta2m2 = sqrt(zeta2m2)
      else
         szeta2m2 = 0.0_dp
      end if
      nu = real(2*ll+1,dp)/fourpi_d
      nu = sqrt(nu)
      return
    end subroutine set_l


    subroutine set_omega(omega_in)
      use nrtype
      implicit none
      complex(dpc), intent(in) :: omega_in
      omega   = omega_in
      omega2  = omega*omega
      omegai  = one / omega
      omegai2 = omegai*omegai
      lno     = 2.0_dp*log(ii*omega/wref)/pi_d
      lno     = ats*lno
      return
    end subroutine set_omega


end module module_int
