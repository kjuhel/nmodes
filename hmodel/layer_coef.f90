!-------------------------------------------------------------------------------------
program layer_correction
!-------------------------------------------------------------------------------------
  implicit none
  integer :: i,j,nbcmax,idepth
  integer :: nbi ! nombre d'interface 
  integer, dimension(:), allocatable :: nbc !nombre de couches +1 dans chaque interface
  doubleprecision, dimension(:), allocatable :: di ! depth of interfaces, in meter
  doubleprecision, dimension(:,:), allocatable :: An,Cn,Fn,Ln,Nn,rhon,rad !new param elast 
  doubleprecision, dimension(:,:), allocatable :: Ao,Co,Fo,Lo,No,rhoo !orginal param elast
  doubleprecision, dimension(:,:), allocatable :: df,X1N,X1C,X1L,X1p,X1a1,X1F,X1a0 !order 1 corrector
  doubleprecision, dimension(:,:), allocatable :: X2a1,X2pa0_a
  doubleprecision :: vpv,vph,vsv,vsh,tmprho,eta,fct,rn,wn,vn,bigg,pi,rhobar,vn2,gn,dr
  doubleprecision :: x1a0CsL,x1a1Fx1a0sL,X1psL,x1lLFsC,x1psC,x1lL,x1nsL
  doubleprecision :: Tc,Tref,QQ
  logical :: psvminor=.true.  
!  logical :: psvminor=.false. 
!
!
  bigg=6.6723d-11
  pi=4.d0*atan(1.d0)
  rn=6371000.d0
  rhobar=5515.d0
  gn=pi*bigg*rhobar*rn
  vn2=gn*rn
  vn=dsqrt(vn2)
  wn=vn/rn
  QQ=191.d0
  Tref=1.d0
  Tc=150.d0
!
  fct=1.d0+1.d0/QQ*2.d0*dlog(Tref/Tc)/pi
  fct=1.d0
  print*,'fct=',fct
  nbi=1
  allocate(nbc(nbi),di(nbi+1))
  di(1)=0.d0
!  di(2)=3250.d0   
!  di(2)=7500.d0   
!  di(2)=24400.d0   
!  di(2)=5000.d0    
  di(2)=30.d0    
!  di(2)=40000.d0   
!  di(2)=10000.d0   
!  di(2)=15000.d0   
!  di(2)=5000.d0   
!  di(2)=6500.d0   
!  di(2)=00.d0   
  nbc(1)=200
  idepth=20 !depth index of the source
!  idepth=100 !depth index of the source
  nbcmax=maxval(nbc)
  allocate(An(nbcmax,nbi),Cn(nbcmax,nbi),Fn(nbcmax,nbi),Ln(nbcmax,nbi) &
          ,Nn(nbcmax,nbi),rhon(nbcmax,nbi),rad(nbcmax,nbi))
  allocate(Ao(nbcmax,nbi),Co(nbcmax,nbi),Fo(nbcmax,nbi),Lo(nbcmax,nbi) &
          ,No(nbcmax,nbi),rhoo(nbcmax,nbi))
  allocate(df(nbcmax,nbi),X1N(nbcmax,nbi),X1L(nbcmax,nbi) &
          ,X1p(nbcmax,nbi),X1a1(nbcmax,nbi),X1C(nbcmax,nbi))
  allocate(X1F(nbcmax,nbi),X1a0(nbcmax,nbi))
  allocate(X2a1(nbcmax,nbi),X2pa0_a(nbcmax,nbi))
!feeling elastic properties
  dr=di(2)/(nbc(1)-1)
  do j=1,nbi
     tmprho=3000.d0
     vpv=5000.d0
     vph=5000.d0
     vsv=3200.d0
     vsh=3200.d0
     eta=1.d0
!!$     tmprho=3383.d0
!!$     vpv=8123.0
!!$     vph=8123.0
!!$     vsv=4498.0
!!$     vsh=4498.0
!     tmprho=3380.d0
!     vpv=8110.d0
!     vph=8110.d0
!     vsv=4478.d0
!     vsh=4478.d0
!     eta=1.d0
     do i=1,nbc(j)
        rad(i,j)=6371000.d0-di(2)+dr*(i-1)
        rhon(i,j)=tmprho 
        An(i,j)=rhon(i,j)*vph**2*fct
if (i==1) print*,'A=',An(i,j)
        Cn(i,j)=rhon(i,j)*vpv**2*fct
        Ln(i,j)=rhon(i,j)*vsv**2*fct
        Nn(i,j)=rhon(i,j)*vsh**2*fct
        Fn(i,j)=eta*(An(i,j)-2.d0*Ln(i,j))
     enddo
!!$     tmprho=2000.d0
!!$     vpv=4500.d0
!!$     vph=4500.d0
!!$     vsv=3000.d0
!!$     vsh=3000.d0
!!$!-------------------------
!     tmprho=3000.d0
!     vpv=8000.d0
!     vph=8000.d0
!     vsv=3000.d0
!     vsh=3000.d0
!     tmprho=2800.d0
!!     tmprho=3383.d0
!     vpv=6300.d0
!     vph=6300.d0
!!vpv=8123.d0
!!vph=vpv
tmprho=3000.d0
     vsv=3200.d0
    vsh=3200.d0
     vpv=4000.d0
    vph=4000.d0
!-------------------------
!     tmprho=2700.d0
!     tmprho=3380.d0
!prelike2:
!    tmprho=2700.d0
!     vpv=6150.d0
!     vph=6150.d0
!     vsv=3320.d0
!     vsh=3320.d0
!!     tmprho=2700.d0
!     vpv=9073.97 
!     vph=9073.97 
!     vsv=5010.26
!     vsh=5010.26
!!
     eta=1.d0
     do i=1,nbc(j)
        rhoo(i,j)=tmprho
        Ao(i,j)=rhoo(i,j)*vph**2*fct
        Co(i,j)=rhoo(i,j)*vpv**2*fct
        Lo(i,j)=rhoo(i,j)*vsv**2*fct
        No(i,j)=rhoo(i,j)*vsh**2*fct
        Fo(i,j)=eta*(Ao(i,j)-2.d0*Lo(i,j))
!        Ao(i,j)=An(i,j)
!        Co(i,j)=Cn(i,j)
!        Lo(i,j)=Ln(i,j)
!        No(i,j)=Nn(i,j)
!        Fo(i,j)=Fn(i,j)
     enddo
  enddo
!  Ao=An
!  Co=Cn
!  Lo=Ln
!  No=Nn
!  Fo=Fn
  print*,'vpn=',sqrt(Ao(1,1)/tmprho)
  print*,'vsn=',sqrt(Lo(1,1)/tmprho)
!
!  computing first order correctors:
!
  df(:,:)=-(rhoo(:,:)-rhon(:,:))
  call integre(df,X1p)
  df(:,:)=-(No(:,:)-Nn(:,:))
  call integre(df,X1N)
  df(:,:)=-(1./Lo(:,:)-1/Ln(:,:))
  call integre(df,X1L)
  df(:,:)=-(1./Co(:,:)-1/Cn(:,:))
  call integre(df,X1C)
  df(:,:)=-(Fo(:,:)/Co(:,:)-Fn(:,:)/Cn(:,:))
  call integre(df,X1a0)
  df(:,:)=-((Ao(:,:)-Fo(:,:)**2/Co(:,:))-(An(:,:)-Fn(:,:)**2/Cn(:,:)))
  call integre(df,X1a1)
!
! second order
!
  df(:,:)=-(X1a1(:,:)-X1a1(nbc(nbi),nbi))
  call integre(df,X2a1)
!
  df(:,:)=(X1p(:,:)-X1p(nbc(nbi),nbi))*(1.d0-Fn(nbc(nbi),nbi)/Cn(nbc(nbi),nbi)) &
       +rhoo(:,:)*(X1a0(:,:)-X1a0(nbc(nbi),nbi))
  call integre(df,X2pa0_a)
!
! output
!
  open(11,file='info_layer_corr.dat')
  write(11,*) 'Layer correction type: (1=1D,2=2D):'
  write(11,*) 1  
  write(11,*) 'min radius:'
  write(11,*) 6371000.d0-di(2)
  write(11,*) 'First order coefs (X1rho,X1N,X1a1):'
  write(11,*) X1p(nbc(nbi),nbi),X1N(nbc(nbi),nbi),X1a1(nbc(nbi),nbi)
  write(11,*) X2a1(nbc(nbi),nbi),X2pa0_a(nbc(nbi),nbi)
  write(11,*)  Cn(1,1)/Co(1,1),Ln(1,1)/Lo(1,1),(Fn(1,1)-Fo(1,1))/Co(1,1) 
!attention, ne va marcher que pour nbi=1
  write(11,*) 1,nbc(nbi),wn
  write(11,*) (rad(i,1),i=1,nbc(nbi))
  write(11,*) (Cn(i,1),Co(i,1),Ln(i,1),Lo(i,1),Fn(i,1),Fo(i,1)   ,i=1,nbc(1))
  write(11,*) (x1n(i,1),x1l(i,1),x1c(i,1),i=1,nbc(1))
  write(11,*) (x1a0(i,1),x1a1(i,1),x1p(i,1),i=1,nbc(1))
  if (psvminor) write(11,*) Ln(1,1),Lo(1,1),Cn(1,1),Co(1,1),Fn(1,1),Fo(1,1) 
  close(11)
!=======================================================
contains
!=======================================================
!------------------------------------------------------
  subroutine integre(dx,x)
!------------------------------------------------------
    implicit none
    doubleprecision, dimension(:,:), intent(in ) :: dx
    doubleprecision, dimension(:,:), intent(out) ::  x    
    doubleprecision :: dr
    integer :: i,j
    x(1,1)=0.d0
    do j=1,nbi
       dr=(di(j+1)-di(j))/(nbc(j)-1)
       if (j>1) x(1,j)=x(nbc(j-1),j-1)
       do i=2,nbc(j)
          x(i,j)=x(i-1,j)+(dx(i-1,j)+dx(i,j))*dr/2.d0
       enddo
    enddo
!------------------------------------------------------
  end subroutine integre
!------------------------------------------------------
!-------------------------------------------------------------------------------------
end program layer_correction
!-------------------------------------------------------------------------------------
