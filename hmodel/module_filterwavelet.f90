!----------------------------------------------------------------------------------------
module filterwavelet
!----------------------------------------------------------------------------------------
  implicit none
  public :: init_wavelet,mwave,reinit_wavelet,deallocate_wavelet
  private
  logical, parameter :: dumpwavelet=.true.
  integer, parameter :: GAUSSIAN=1,HEAVIS=2
  integer, parameter :: sampling=128,ndeg=41, wavetype=HEAVIS
  integer :: nsamp_wave,cst
  real*8, dimension(:)  , allocatable :: xwave,vss
  real*8, dimension(:,:), allocatable :: wavebase,pwave
  real*8 :: x0wave,xsamp,l0
  real*8 ::  f31ref,f41ref
!=====================================================================
  contains
!=====================================================================
!----------------------------------------------------------------------
    subroutine deallocate_wavelet
!----------------------------------------------------------------------
      implicit none
      deallocate(xwave,wavebase,pwave)
!----------------------------------------------------------------------
    end subroutine deallocate_wavelet
!----------------------------------------------------------------------
!----------------------------------------------------------------------
  subroutine init_wavelet(l0_,cst_,rn,f31,f41,f32,f42,nbc,vss_)
!----------------------------------------------------------------------
    use module_spline
    implicit none
    integer, intent(in) :: cst_
    real*8, intent(in)  :: l0_,rn,f31,f41,f32,f42
    integer, optional :: nbc
    real*8, dimension(:), optional :: vss_
    
    integer :: i,j,nbw
!defining xwave:
    if (present(nbc)) then
       nbw=3
    else
       nbw=2
    ENDIF
!
    cst=cst_
    l0=l0_
    nsamp_wave=2*sampling*cst
    allocate(xwave(nsamp_wave),wavebase(nsamp_wave,nbw),pwave(nsamp_wave,nbw))
    xsamp=2*cst*l0/(nsamp_wave)
    do i=1,nsamp_wave
       xwave(i)=(i-1)*xsamp
    enddo
    select case(wavetype)
    case (GAUSSIAN)
       x0wave=cst*l0
       call build_wavelett(x0wave,l0,cst,ndeg,wavebase(:,1),i,j)
    case (HEAVIS)
       x0wave=cst*l0
       call build_heavis(f31,f41,wavebase(:,1))
       call build_heavis(f32,f42,wavebase(:,2))
       wavebase(:,1)=wavebase(:,1)/(SUM(wavebase(:,1))*xsamp)
       wavebase(:,2)=wavebase(:,2)/(SUM(wavebase(:,2))*xsamp)
!print*,SUM(wavebase(:))*xsamp
    case default
       stop 'init_wavelet: wavetype not written!'
    end select
    if (dumpwavelet) then
       open(11,file='filterwavelet1')
       open(12,file='filterwavelet2')
       do i=1,nsamp_wave
          write(11,*) xwave(i)*rn/1000.,wavebase(i,1)
          write(12,*) xwave(i)*rn/1000.,wavebase(i,2)
       enddo
       close(11)
       close(12)
    endif
    call spline(xwave,wavebase(:,1),0.d0,0.d0,pwave(:,1))
    call spline(xwave,wavebase(:,2),0.d0,0.d0,pwave(:,2))
!
    if (present(nbc)) then
       f31ref=f31
       f41ref=f41
       allocate(vss(nbc))
       vss(:)=vss_(nbc)/vss_(:)
    endif
!----------------------------------------------------------------------
  end subroutine init_wavelet
!----------------------------------------------------------------------
!----------------------------------------------------------------------
  subroutine reinit_wavelet(f31,f41)
!----------------------------------------------------------------------
    use module_spline
    implicit none
    real*8, intent(in)  :: f31,f41
    
    integer :: i,j,nbw
!
    x0wave=cst*l0
    call build_heavis(f31,f41,wavebase(:,3))
    wavebase(:,3)=wavebase(:,3)/(SUM(wavebase(:,3))*xsamp)
    call spline(xwave,wavebase(:,3),0.d0,0.d0,pwave(:,3))
!----------------------------------------------------------------------
  end subroutine reinit_wavelet
!----------------------------------------------------------------------
!!$!----------------------------------------------------------------------
!!$  subroutine reinit_wavelet(i)
!!$!----------------------------------------------------------------------
!!$    use module_spline
!!$    implicit none
!!$    integer, intent(in) :: i
!!$    real*8 :: f31,f41
!!$    f31=f31ref*vss(i)
!!$    f41=f41ref*vss(i)
!!$    call build_heavis(f31,f41,wavebase(:,3))
!!$    wavebase(:,3)=wavebase(:,3)/(SUM(wavebase(:,3))*xsamp)
!!$    call spline(xwave,wavebase(:,3),0.d0,0.d0,pwave(:,3))
!!$!----------------------------------------------------------------------
!!$  end subroutine reinit_wavelet
!!$!----------------------------------------------------------------------
!----------------------------------------------------------------------
  real*8 function mwave(x0,x,fid)
!----------------------------------------------------------------------
    use module_spline
    implicit none
    real*8, intent(in) :: x0,x
    integer, intent(in) :: fid
    real*8 :: xmove    
    xmove=x-x0+x0wave
    if (xmove<0.d0 .or. xmove>2*cst*l0) then
       mwave=0.0d0
    else
       mwave=splint(xwave,wavebase(:,fid),pwave(:,fid),xmove)
    endif
!----------------------------------------------------------------------
  end function mwave
!----------------------------------------------------------------------
!----------------------------------------------------------------------
  subroutine build_wavelett(x0,l0,nbla,ndeg,w,i0,j0)
!----------------------------------------------------------------------
          implicit none
          integer, intent(in) :: ndeg,nbla
          doubleprecision, intent(in) :: x0,l0
          integer, intent(out) :: i0,j0
          doubleprecision, dimension(:), intent(out) :: w
!
          doubleprecision :: x,x1,x2,y,ex
          integer :: l,k,n,m,i
          doubleprecision, dimension(ndeg) :: ad
          doubleprecision, dimension(2*ndeg) :: Nd
          doubleprecision, dimension(ndeg,ndeg) :: AA,AAi
!
          w(:)=0.d0
          x1=max(x0-nbla*l0,xwave(1)  )
          x2=min(x0+nbla*l0,xwave(nsamp_wave))
          Nd(:)=0.d0
          do l=1,nsamp_wave
             y=xwave(l)
             if  (y>=x1.and.y<=x2) then
             ex=exp(-((x0-y)/l0)**2/(ndeg+1)*2)*xsamp
             do n=1,ndeg*2-1
                Nd(n)=Nd(n)+ex*fct(x0,y,n)   
             enddo
             endif
          enddo
          do m=1,ndeg
             do n=1,ndeg
                AA(m,n)=Nd(m+n-1)
             enddo
          enddo
          i0=-10;j0=-10
          do i=1,nsamp_wave 
             x=xwave(i)
             if (x>=x2-1.e-7.and.j0==-10) j0=i
             if  (x>=x1.and.x<=x2) then
                if (x>=x1.and.i0==-10) i0=i
                ex=exp(-((x0-x)/l0)**2/(ndeg+1)*2)
                do n=1,ndeg              
                   ad(n)=ex*fct(x0,x,n)    
                enddo
                AAi=AA
                call gaussj(AAi,ndeg,ndeg,ad,1,1)
                w(i)=ad(1)
             else
                w(i)=0.d0
             endif
          enddo
!----------------------------------------------------------------------
        end subroutine build_wavelett
!---------------------------------------------------------------------- 
!---------------------------------------------------------------------- 
        subroutine build_heavis(f3,f4,wavebase)
!---------------------------------------------------------------------- 
          implicit none
!
          doubleprecision, intent(in) :: f3,f4
          doubleprecision, dimension(:), intent(out) :: wavebase
!
          complex*16, dimension(nsamp_wave) :: spectre
          integer :: j
          real*8 :: freq,wt
!
          spectre(:)=cmplx(0.d0,0.d0)
          do j=1,nsamp_wave
             if (j<=nsamp_wave/2) then
                freq=(j-1)/(xsamp*nsamp_wave)
             else if (j==nsamp_wave/2+1) then
                freq=1.d0/(2.d0*xsamp)
             else
                freq=-(nsamp_wave-j+1)/(xsamp*nsamp_wave)        
             endif
             wt=wtcoef(abs(freq),0.d0,0.d0,f3,f4)
             spectre(j)=wt
          enddo
!le premier pas de temps correcpond a t=0 avec la fft
          call dfour1(spectre,nsamp_wave,1)
          wavebase(nsamp_wave/2+1:)=real(spectre(1:nsamp_wave/2))/nsamp_wave/xsamp 
          wavebase(1:nsamp_wave/2)=real(spectre(nsamp_wave/2+1:))/nsamp_wave/xsamp 

!---------------------------------------------------------------------- 
       end subroutine build_heavis
!---------------------------------------------------------------------- 

!-----------------------------------------------------------------------------
    doubleprecision function fct(x,x0,i)
!-----------------------------------------------------------------------------
      implicit none
      doubleprecision :: x,x0
      integer :: i
      fct=(x-x0)**(i-1)
!-----------------------------------------------------------------------------
    end function fct
!-----------------------------------------------------------------------------  
!----------------------------------------------------------------------
  real*8 function wtcoef(f,f1,f2,f3,f4)
!----------------------------------------------------------------------
    implicit none
!
    real*8, parameter ::PI=3.14159265358979323844d0
    real*8, intent(in) ::  f,f1,f2,f3,f4
!
    if (f3.gt.f4) stop 'wtcoefcoef: f3>f4 '
    if (f1.gt.f2) stop 'wtcoefcoef: f1>f2 '
    if (f.le.f3.and.f.ge.f2) then
       wtcoef=1.0
    else if (f.gt.f4.or.f.lt.f1 ) then
       wtcoef=0.0
    else if (f.gt.f3.and.f.le.f4) then
       wtcoef=0.5*(1.0+cos(pi*(f-f3)/(f4-f3)))
    else if (f.ge.f1.and.f.lt.f2) then
       wtcoef=0.5*(1.0+cos(pi*(f-f2)/(f2-f1)))
    endif
!----------------------------------------------------------------------
  end function wtcoef

!-----------------------------------------------------------------------------   
!----------------------------------------------------------------------------------------
end module filterwavelet
!----------------------------------------------------------------------------------------
