!------------------------------------------------------------------------------------
program generate_cosmodel
!------------------------------------------------------------------------------------
  use earth_modele
  implicit none
  logical, parameter :: stepmodel=.false.
  doubleprecision, parameter :: Rn=6371000.
  doubleprecision :: PI
  logical :: cosmodel=.true.
  type(modele) :: mod
  character(len=30) :: name
  PI=4.d0*atan(1.d0)
  if (cosmodel) then
     call force_model(mod)
     mod%nbcou=mod%nbcou/2
     name='cosmodel'
  else
     call force_model_random(mod)
     name='randmodel'
  endif
  call write_modele(name,mod)      
!------------------------------------------------------------------------------------
contains
!------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------
  subroutine force_model(mod)
!-----------------------------------------------------------------------------
    use earth_modele
    implicit none  
    type(modele), intent(inout) :: mod
!
    real*8 amp1,amp2,amp3,amp4,amp5,phase1,phase2,phase3,phase4,phase5,r,dr,LH,rstart,coef,amp,phase
    integer :: i,j,nblh,nbpi,jj
    real*8, parameter :: rmin1=4500000.d0,rmin2=5500000.d0

    if (.not.stepmodel) then
       phase1=0.d0
       phase2=0.00d0*PI
       phase3=0.24d0*PI
       phase4=0.74d0*PI
       phase5=1.04d0*PI
       amp1  =0.20
       amp2  =0.10
       amp3  =0.13
       amp4  =0.19
       amp5  =0.08
       amp=0.2
       phase=0.d0
       call init_modele(mod)
       mod%ifanis=1
       mod%tref=0.d0
       mod%ifdeck=1
       mod%nic=0
       mod%noc=0
       mod%nbceau=0
       mod%titre='cosmodel'
       mod%nbcou=4000
       call allocate_modele(mod)    
       dr=rn/(mod%nbcou/2-1)
       do i=1,mod%nbcou/2
          mod%r(i)=(i-1)*dr
       enddo
       do i=mod%nbcou/2+1,mod%nbcou
          mod%r(i)=(i-2)*dr
       enddo
       do i=1,mod%nbcou
          r=mod%r(i)
          mod%rho(i)=3000.d0*oscillation(r,phase  ,amp )
!          mod%rho(i)=3000.d0
          mod%vpv(i)=8000.0d0*oscillation(r,phase2  ,amp2 )
          mod%vph(i)=8000.0d0*oscillation(r,phase3 ,amp3 )
          mod%vsv(i)=4600.0d0*oscillation(r,phase4 ,amp4)
          mod%vsh(i)=4600.0d0*oscillation(r,phase5 ,amp5)
!!$       mod%vpv(i)=8000.0d0*oscillation(r,phase  ,amp )**2
!!$       mod%vph(i)=8000.0d0*oscillation(r,phase2 ,amp )**2
!!$       mod%vsv(i)=4600.0d0*oscillation(r,phase  ,amp)**2
!!$       mod%vsh(i)=4600.0d0*oscillation(r,phase2 ,amp)**2
!       mod%vpv(i)=8000.0d0
!       mod%vph(i)=8000.0d0
!       mod%vsv(i)=4600.0d0
!       mod%vsh(i)=4600.0d0
          
          mod%eta(i)=1.d0
          mod%qshear(i)=1.d5
          mod%qkappa(i)=1.d5
       enddo
    else
       LH=50000.
       nblh=int(rn/LH)+1
       nbpi=50
       call init_modele(mod)
       mod%ifanis=1
       mod%tref=0.d0
       mod%ifdeck=1
       mod%nic=0
       mod%noc=0
       mod%nbceau=0
       mod%titre='stepmodel'
       mod%nbcou=nbpi*nblh*2+2
       call allocate_modele(mod)    
       jj=0
       do i=1,nblh*2
          rstart=(i-1)*LH
          dr=LH/(nbpi-1)
          do j=1,nbpi
             jj=jj+1             
             mod%r(jj)=rstart+(j-1)*dr
             r=mod%r(jj)
             coef=(1.+0.2*(-1)**i*wtcoef(r,rmin1,rmin2,2.*Rn,2.*Rn))
             mod%rho(jj)=3000.0d0*coef
             mod%vpv(jj)=8000.0d0*coef
             mod%vph(jj)=8000.0d0*coef
             mod%vsv(jj)=4600.0d0*coef
             mod%vsh(jj)=4600.0d0*coef
             if (mod%r(jj)<rn.and.mod%r(jj)+dr>rn) then
                jj=jj+1             
                mod%r(jj)=rn
                r=mod%r(jj)
                coef=(1.+0.2*(-1)**i*wtcoef(r,rmin1,rmin2,2.*Rn,2.*Rn))
                mod%rho(jj)=3000.0d0*coef
                mod%vpv(jj)=8000.0d0*coef
                mod%vph(jj)=8000.0d0*coef
                mod%vsv(jj)=4600.0d0*coef
                mod%vsh(jj)=4600.0d0*coef
                mod%nbcou=jj*2
                jj=jj+1             
                mod%r(jj)=rn
                mod%rho(jj)=3000.0d0*coef
                mod%vpv(jj)=8000.0d0*coef
                mod%vph(jj)=8000.0d0*coef
                mod%vsv(jj)=4600.0d0*coef
                mod%vsh(jj)=4600.0d0*coef
             endif
          enddo
       enddo
    endif
!-----------------------------------------------------------------------------
  end subroutine force_model
!-----------------------------------------------------------------------------
!-----------------------------------------------------------------------------
  subroutine force_model_random(mod)
!-----------------------------------------------------------------------------
    use earth_modele
    implicit none  
    type(modele), intent(inout) :: mod
!
    real*8 :: r,dr,coef1,coef2,coef3,amp,rand
    integer :: i,j,nblh,nbpi,jj,istart,iend,nbinter,nbc,base,ii,nbi,ic,bmin
    integer, parameter :: nbintermax=1000
    real*8, dimension(nbintermax) :: iinter,linter
    real*8, dimension(:), allocatable :: rad
    real*8, parameter :: rmin1=4500000.d0,rmin2=5500000.d0,vp=8000.d0 &
                         ,vs=4600.d0,rho=3000.d0
    logical :: go_one
!generating layer radius
    call random_seed
    amp=0.2
    istart=1000
    iend  =300
    nbc=3000
    nbinter=0
    base=100
    bmin=10
    allocate(rad(nbc+nbintermax))
    dr=rn/real(nbc-1)
    do i=1,istart-1
       rad(i)=(i-1)*dr
    enddo
    ic=istart-1
    go_one=.true.
    do while(go_one)
       call random_number(rand)
       nbi=max(int(rand*base),bmin)
       if (ic+nbi<nbc-iend) then
          nbinter=nbinter+1
          if (nbinter>nbintermax) stop 'nbinter>nbintermax!'
          ic=ic+1
          iinter(nbinter)=ic
          linter(nbinter)=nbi
          rad(ic)=rad(ic-1)
          do i=1,nbi-1
             ic=ic+1
             rad(ic)=rad(ic-1)+dr
          enddo
       else
          go_one=.false.
       endif      
    enddo
    nbc=nbc+nbinter
    do i=ic+1,nbc
       rad(i)=rad(i-1)+dr
    enddo
    mod%ifanis=1
    mod%tref=0.d0
    mod%ifdeck=1
    mod%nic=0
    mod%noc=0
    mod%nbceau=0
    mod%titre='radmodel'
    mod%nbcou=nbc
    call allocate_modele(mod)    
    mod%r  (:)=rad(1:nbc)
    mod%eta(:)=1.d0
    mod%qshear(:)=1.d5
    mod%qkappa(:)=1.d5
    mod%rho(:)=rho
    mod%vpv(:)=vp
    mod%vph(:)=vp
    mod%vsh(:)=vs
    mod%vsv(:)=vs
!
    ic=istart-1
    do ii=1,nbinter
       call random_number(rand)
       coef1=1.d0+(2*rand-1.d0)*amp
       call random_number(rand)
       coef2=1.d0+(2*rand-1.d0)*amp
       call random_number(rand)
       coef3=1.d0+(2*rand-1.d0)*amp
       do i=1,linter(ii)
          ic=ic+1
          mod%rho(ic)=rho*coef1
          mod%vpv(ic)=vp *coef2
          mod%vph(ic)=vp *coef2
          mod%vsh(ic)=vs *coef3
          mod%vsv(ic)=vs *coef3
       enddo
    enddo
!-----------------------------------------------------------------------------
  end subroutine force_model_random
!-----------------------------------------------------------------------------
!-----------------------------------------------------------------------------
  real*8 function oscillation(r,p,amp)
!-----------------------------------------------------------------------------
    implicit none
    real*8 :: r,p,amp
    real*8, parameter :: rmin1=2500000.d0,rmin2=3500000.d0,LH=100000.
    real*8, parameter :: rmin3=5000000.d0,rmin4=6000000.d0
!    oscillation=(1.d0+amp*cos(2.d0*PI*r/LH+p)*wtcoef(r,rmin1,rmin2,2.*Rn,2.*Rn))
    oscillation=(1.d0+amp*cos(2.d0*PI*r/LH+p)*wtcoef(r,rmin1,rmin2,rmin3,rmin4))
!----------------------------------------------------------------------------
     end function oscillation
!-----------------------------------------------------------------------------
!----------------------------------------------------------------------
  real*8 function wtcoef(f,f1,f2,f3,f4)
!----------------------------------------------------------------------
    use def_gparam
    implicit none
!
    real(DP), intent(in) ::  f,f1,f2,f3,f4
!
    if (f3.gt.f4) stop 'wtcoefcoef: f3>f4 '
    if (f1.gt.f2) stop 'wtcoefcoef: f1>f2 '
    if (f.le.f3.and.f.ge.f2) then
       wtcoef=1.0_DP
    else if (f.gt.f4.or.f.lt.f1 ) then
       wtcoef=0.0_DP
    else if (f.gt.f3.and.f.le.f4) then
       wtcoef=0.5_DP*(1.0+cos(pi*(f-f3)/(f4-f3)))
    else if (f.ge.f1.and.f.lt.f2) then
       wtcoef=0.5_DP*(1.0+cos(pi*(f-f2)/(f2-f1)))
    endif
!----------------------------------------------------------------------
  end function wtcoef

!-----------------------------------------------------------------------------
!------------------------------------------------------------------------------------
end program generate_cosmodel
!------------------------------------------------------------------------------------
