!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                                                  !
!Author : Yann Capdeville                                                          !
!                                                                                  !
!Contact: yann.capdeville at  univ-nantes.fr                                       !
!                                                                                  !
!This program is free software: you can redistribute it and/or modify it under     !
!the terms of the GNU General Public License as published by the Free Software     !
!Foundation, either version 3 of the License, or (at your option) any later        !
!version.                                                                          !
!                                                                                  !
!This program is distributed in the hope that it will be useful, but WITHOUT ANY   !
!WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A   !
!PARTICULAR PURPOSE. See the GNU General Public License for more details.          !
!                                                                                  !
!You should have received a copy of the GNU General Public License along with      !
!this program. If not, see http://www.gnu.org/licenses/.                           !
!                                                                                  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 

!--------------------------------------------------------------------
program generate_hmodel 
!nbvcmax: nombre max de points par couche
!--------------------------------------------------------------------
  use hmodel
  implicit none
!
  character(len=100) :: modname
  integer :: cst
  real*8 :: l0,rstart
!

  call init_hmodel()
!  call init_hmodel_test()
 
!--------------------------------------------------------------------
end program generate_hmodel
!--------------------------------------------------------------------
