!--------------------------------------------------------------------------
module earth_modele_b
!--------------------------------------------------------------------------
  implicit none
!
!----------------------------
  type modele
!
! titre
! ifanis,tref,ifdeck
! nbcou,nic,noc,nbceau
! r(i),rho(i),vpv(i),vsv(i),qkappa(i),qshear(i),vph(i),vsh(i),eta(i)
!
! ifanis/=0 : anisotrope
! ifdeck : je sais pas c'etait la
! nbcou : nmbre de couches du modele
! nbi   : nombre d'interface
! inti  : interface index     
! radi  : interface radius
! nic :    numero de la couche inner core
! noc   : numero de la couche outer core
! nbceau : nombre de couches de l'ocean (0 = pas d'ocean)
! champs de dimension (nbcou)
! r : rayon en km
! rho : kg/m3
! vpv,vsv,vph,vsh: km/s
! qkappa,qshear : attenuation
! eta : F/(A-2L)
!----------------------------
     character(len=80) :: titre
     integer :: nbcou
     integer :: nbi
     integer :: ifanis,ifdeck,nic,noc,nbceau
     real    :: tref
     integer, dimension(:), pointer :: inti
     real*8, dimension(:), pointer :: r,rho,vpv,vsv,qkappa,qshear,vph,vsh,eta,radi
     logical :: allocated
     real*8    :: x1p,x1n,x1a1,x2a1,x2pa0_a
!----------------------------
  end type modele
!----------------------------
  private :: locate_i

!--------------------------------------------------------------------------
contains
!--------------------------------------------------------------------------
    
!--------------------------------------------------------------------------
  subroutine  init_modele(mod)
!--------------------------------------------------------------------------
    implicit none
    type(modele), intent(out) :: mod
!
    mod%allocated=.false.
    mod%nbceau=-1
!--------------------------------------------------------------------------
  end subroutine init_modele
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
  subroutine allocate_modele(mod,nbcou)
!--------------------------------------------------------------------------
    implicit none
!
    type(modele), intent(inout) :: mod
    integer, optional,intent(in) :: nbcou
!
    if (present(nbcou)) mod%nbcou=nbcou
    if (mod%nbcou==-1) stop 'allocate_modele: le nombre de couches&
         & n''est pas connu'
    if (mod%allocated) then
       print*,'allocate_modele: le modele',mod%titre,' est deja alloue'
       stop
    endif
    allocate(mod%r(mod%nbcou),mod%rho(mod%nbcou),mod%vpv(mod%nbcou)         &
         ,mod%vsv(mod%nbcou),mod%qkappa(mod%nbcou),mod%qshear(mod%nbcou) &
         ,mod%vph(mod%nbcou),mod%vsh(mod%nbcou),mod%eta(mod%nbcou))
!
    mod%r     (:)=0.d0
    mod%rho   (:)=0.d0
    mod%vpv   (:)=0.d0
    mod%vsv   (:)=0.d0
    mod%qkappa(:)=0.d0
    mod%qshear(:)=0.d0
    mod%vph   (:)=0.d0
    mod%vsh   (:)=0.d0
    mod%eta   (:)=0.d0
!
    mod%allocated=.true.
!
!--------------------------------------------------------------------------
  end subroutine allocate_modele
!--------------------------------------------------------------------------

!--------------------------------------------------------------------------
  subroutine deallocate_modele(mod)
!--------------------------------------------------------------------------
    implicit none 
!
    type(modele), intent(inout) :: mod 
!
    if (.not.mod%allocated) then
       print*,'deallocate_modele: le modele',mod%titre,' n''est pas alloue??' 
       stop 
    endif 
    deallocate(mod%r,mod%rho,mod%vpv,mod%vsv,mod%qkappa,mod%qshear  &
              ,mod%vph,mod%vsh,mod%eta,mod%radi,mod%inti)
!
    mod%allocated=.false.
!
!--------------------------------------------------------------------------
  end subroutine deallocate_modele
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
  subroutine copy_modele(mod1,mod2)
!--------------------------------------------------------------------------
    type(modele), intent(in ) :: mod1
    type(modele), intent(out) :: mod2
!
    mod2%nbcou=mod1%nbcou
    call allocate_modele(mod2)
    mod2%nbi=mod1%nbi
    allocate(mod2%radi(mod2%nbi),mod2%inti(mod2%nbi))
    mod2%radi(:)=mod1%radi(:)
    mod2%inti(:)=mod1%inti(:)
!
    mod2%ifanis=    mod1%ifanis
    mod2%ifdeck=    mod1%ifdeck
    mod2%nic  =    mod1%nic
    mod2%noc  =    mod1%noc
    mod2%nbceau  =    mod1%nbceau
    mod2%tref  =    mod1%tref
    mod2%x1p  =    mod1%x1p
    mod2%x1n  =    mod1%x1n
    mod2%x1a1  =    mod1%x1a1
    mod2%x2a1  =    mod1%x2a1
    mod2%x2pa0_a  =    mod1%x2pa0_a
    mod2%r(:)  =    mod1%r(:)
    mod2%rho(:)  =    mod1%rho(:)
    mod2%vpv(:)  =    mod1%vpv(:)
    mod2%vsv(:)  =    mod1%vsv(:)
    mod2%qkappa(:)  =    mod1%qkappa(:)
    mod2%qshear(:)  =    mod1%qshear(:)
    mod2%vph(:)  =    mod1%vph(:)
    mod2%vsh(:)  =    mod1%vsh(:)
    mod2%eta(:)  =    mod1%eta(:)
!--------------------------------------------------------------------------
  end subroutine copy_modele
!--------------------------------------------------------------------------

!--------------------------------------------------------------------------
  subroutine read_modele(name,mod,rn)
!--------------------------------------------------------------------------
    implicit none
!
    character(*), intent(in) :: name
    type(modele), intent(out) :: mod
    doubleprecision, optional, intent(out) :: rn
! pour la version polynomial
    doubleprecision :: rx,dr,r1,r2,rt,val
    doubleprecision, parameter :: tau=1.0d3
    doubleprecision, dimension(5) :: wrk
    integer :: nreg,n,nn,knt,nlay,ind,j,jj,nbi
!
    integer :: i,unit=127,icode
!
!test
    call init_modele(mod)
    open(unit,file=name,iostat=icode,status='old',action='read')
    if (icode>0) then
       print*,'probleme d''ouverture de',name
       stop 'read_modele: prob de d''ouverture'
    endif
!
    read(unit,100,iostat=icode) mod%titre
    read(unit,*  ,iostat=icode) mod%ifanis,mod%tref,mod%ifdeck
!---------------------------------
    if (mod%ifdeck==0) then
       print*,'Modele polynomial'
!modele polynomial
!---------------------------------
!lit d'abord le fichier pour compter le mombre de couche!
       read(unit,*) nreg,mod%nic,mod%noc,rx
       n=0
       jj=5
       if(mod%ifanis.ne.0) jj=8
       do  nn=1,nreg
          read(unit,*) nlay,r1,r2
          do  i=1,nlay
             n=n+1
          enddo
          do  j=1,jj
             read(unit,110) (wrk(i),i=1,5)
          enddo
       enddo
       mod%nbcou=n
!on reroule le tout
       rewind unit
       
!cette fois, lecture pour de bon!
       
       call allocate_modele(mod)
       read(unit,100,iostat=icode) mod%titre
       read(unit,*  ,iostat=icode) mod%ifanis,mod%tref,mod%ifdeck
       read(unit,*) nreg,mod%nic,mod%noc,rx
       rx=rx*tau
       n=0
       knt=0
       jj=5
       if(mod%ifanis.ne.0) jj=8
       do  nn=1,nreg
          read(unit,*) nlay,r1,r2
          r1=r1*tau
          r2=r2*tau
          dr=(r2-r1)/float(nlay-1)
          do  i=1,nlay
             n=n+1
             mod%r(n)=r1+dr*float(i-1)
          enddo
          do  j=1,jj
             read(unit,110) (wrk(i),i=1,5)
             do  i=1,nlay
                ind=knt+i
                rt=mod%r(ind)/rx
                val=wrk(1)+rt*(wrk(2)+rt*(wrk(3)+rt*(wrk(4)+rt*wrk(5))))
                if(j.eq.1) mod%rho(ind)=val*tau
                if(j.eq.2) mod%vpv(ind)=val*tau
                if(j.eq.3) mod%vsv(ind)=val*tau
                if(j.eq.4) mod%qkappa(ind)=val
                if(j.eq.5) mod%qshear(ind)=val
                if(mod%ifanis/=0) then
                   if(j.eq.6) mod%vph(ind)=val*tau
                   if(j.eq.7) mod%vsh(ind)=val*tau
                   if(j.eq.8) mod%eta(ind)=val
                endif
             enddo
          enddo
          do i=1,nlay
             ind=knt+i
             print*,ind,mod%r(ind),mod%rho(ind),mod%vpv(ind),mod%vsv(ind)
          enddo
          knt=knt+nlay
          if(mod%ifanis==0) then
             do  i=1,n
                mod%vph(i)=mod%vpv(i)
                mod%vsh(i)=mod%vsv(i)
                mod%eta(i)=1.d0       
             enddo
          endif
       enddo
!---------------------------------
    else
!modele par couche
!---------------------------------
       read(unit,*  ,iostat=icode) mod%nbcou,mod%nic,mod%noc  &
            ,mod%nbceau
!
       call allocate_modele(mod)
!
       read(unit,105,iostat=icode) (mod%r(i),mod%rho(i),mod%vpv(i),mod%vsv(i) &
                      ,mod%qkappa(i),mod%qshear(i),mod%vph(i)       &
                      ,mod%vsh(i),mod%eta(i),i=1,mod%nbcou)
       read(unit,*,iostat=icode) mod%x1p,mod%x1n,mod%x1a1,mod%x2a1,mod%x2pa0_a
       if (icode<0) then
          print*,'The earth modele',name,' does not contain any boundry terms. Setting them to 0'
          mod%x1p=0.d0
          mod%x1n=0.d0
          mod%x1a1=0.d0
          mod%x2a1=0.d0
          mod%x2pa0_a=0.d0
       endif
       if (present(rn)) rn=mod%r(mod%nbcou)
!
       nbi=0
       do i=1,mod%nbcou-1
          if ( (mod%r(i+1)-mod%r(i))/mod%r(i+1)<1.d-7 ) then
             nbi=nbi+1
          endif
       enddo
       mod%nbi=nbi
       allocate(mod%radi(0:nbi+1),mod%inti(0:nbi+1))
       mod%inti(0)    =1
       mod%radi(0)    =0.
       mod%inti(nbi+1)=mod%nbcou
       mod%radi(nbi+1)=mod%r(mod%nbcou)
!
       nbi=0
       do i=1,mod%nbcou-1
          if ( (mod%r(i+1)-mod%r(i))/mod%r(i+1)<1.d-7 ) then
             nbi=nbi+1
             mod%radi(nbi)=mod%r(i)
             mod%inti(nbi)=i
          endif
       enddo
       if (icode>0) then
          print*,'probleme de lecture dans',name
          print*,i,mod%nbcou
          stop 'read_modele: prob de de lecture'
       endif
!---------------------------------
    endif
!---------------------------------
    close(unit)
!      
100 format(a80)
105 format(f8.0, 3f9.2, 2f9.1, 2f9.2, f9.5)     
110 format(5f9.5)
!--------------------------------------------------------------------------
  end subroutine read_modele
!--------------------------------------------------------------------------

!!$!--------------------------------------------------------------------------
!!$  subroutine read_modele_MPI(name,mod,rang)
!!$!--------------------------------------------------------------------------
!!$    implicit none
!!$    include 'mpif.h'
!!$!
!!$    integer, intent(in) :: rang
!!$    character(len=100), intent(in) :: name
!!$    type(modele), intent(out) :: mod
!!$!
!!$    integer :: i,unit=127,icode,ier,nbv
!!$    logical :: flag
!!$!
!!$    call MPI_BARRIER(MPI_COMM_WORLD,ier)
!!$    call init_modele(mod)
!!$    if (rang==0) then
!!$       inquire(file=name,exist=flag)
!!$       if (.not.flag) then
!!$          print*,'read_modele_MPI: le modele n''existe pas :',name
!!$          stop
!!$       endif     
!!$       open(unit,file=name,status='old',action='read')
!!$!
!!$       read(unit,100,iostat=icode) mod%titre
!!$       read(unit,*  ,iostat=icode) mod%ifanis,mod%tref,mod%ifdeck
!!$       read(unit,*  ,iostat=icode) mod%nbcou,mod%nic,mod%noc  &
!!$            ,mod%nbceau
!!$!
!!$       call allocate_modele(mod)
!!$!
!!$       read(unit,105,iostat=icode) (mod%r(i),mod%rho(i),mod%vpv(i),mod%vsv(i) &
!!$            ,mod%qkappa(i),mod%qshear(i),mod%vph(i)       &
!!$            ,mod%vsh(i),mod%eta(i),i=1,mod%nbcou)
!!$    endif
!!$    call MPI_BCAST(icode,1,MPI_INTEGER,0,MPI_COMM_WORLD,ier)
!!$    if (icode>0) then
!!$       if (rang==0)  print*,'probleme de lecture (ou d''ouverture) dans',name
!!$       stop 'read_modele_MPI: prob de de lecture'
!!$    endif
!!$!
!!$!broad cast du modele d0 vers tous les procs:
!!$!
!!$    call MPI_BCAST(mod%ifanis,1,MPI_INTEGER,0,MPI_COMM_WORLD,ier)
!!$    call MPI_BCAST(mod%ifdeck,1,MPI_INTEGER,0,MPI_COMM_WORLD,ier)
!!$    call MPI_BCAST(mod%nbcou ,1,MPI_INTEGER,0,MPI_COMM_WORLD,ier)
!!$    call MPI_BCAST(mod%nic   ,1,MPI_INTEGER,0,MPI_COMM_WORLD,ier)
!!$    call MPI_BCAST(mod%noc   ,1,MPI_INTEGER,0,MPI_COMM_WORLD,ier)
!!$    call MPI_BCAST(mod%nbceau,1,MPI_INTEGER,0,MPI_COMM_WORLD,ier)
!!$    call MPI_BCAST(mod%tref  ,1,MPI_REAL   ,0,MPI_COMM_WORLD,ier)
!!$!
!!$    if (rang/=0) call allocate_modele(mod)
!!$!
!!$    nbv=mod%nbcou
!!$    call MPI_BCAST(mod%r     ,nbv,MPI_REAL ,0,MPI_COMM_WORLD,ier)
!!$    call MPI_BCAST(mod%rho   ,nbv,MPI_REAL ,0,MPI_COMM_WORLD,ier) 
!!$    call MPI_BCAST(mod%vpv   ,nbv,MPI_REAL ,0,MPI_COMM_WORLD,ier)
!!$    call MPI_BCAST(mod%vsv   ,nbv,MPI_REAL ,0,MPI_COMM_WORLD,ier)
!!$    call MPI_BCAST(mod%qkappa,nbv,MPI_REAL ,0,MPI_COMM_WORLD,ier)
!!$    call MPI_BCAST(mod%qshear,nbv,MPI_REAL ,0,MPI_COMM_WORLD,ier)
!!$    call MPI_BCAST(mod%vph   ,nbv,MPI_REAL ,0,MPI_COMM_WORLD,ier)
!!$    call MPI_BCAST(mod%vsh   ,nbv,MPI_REAL ,0,MPI_COMM_WORLD,ier)
!!$    call MPI_BCAST(mod%eta   ,nbv,MPI_REAL ,0,MPI_COMM_WORLD,ier)
!!$    
!!$!      
!!$100 format(a80)
!!$105 format(f8.0, 3f9.2, 2f9.1, 2f9.2, f9.5)     
!!$!--------------------------------------------------------------------------
!!$  end subroutine read_modele_MPI
!!$!--------------------------------------------------------------------------

!--------------------------------------------------------------------------
  subroutine write_modele(name,mod)
!--------------------------------------------------------------------------
    character(*), intent(in) :: name
    type(modele), intent(in) :: mod
!
    integer :: i,unit=127,icode
!
    open(unit,file=name,iostat=icode,action='write')
    if (icode>0) then
       print*,'probleme d''ouverture de',name
       stop 'write_modele: prob de d''ouverture'
    endif
!
    write(unit,100,iostat=icode) mod%titre
    write(unit,*  ,iostat=icode) mod%ifanis,mod%tref,mod%ifdeck
    write(unit,*  ,iostat=icode) mod%nbcou,mod%nic,mod%noc  &
                             ,mod%nbceau
!
    write(unit,105,iostat=icode) (mod%r(i),mod%rho(i),mod%vpv(i),mod%vsv(i) &
                   ,mod%qkappa(i),mod%qshear(i),mod%vph(i)       &
                   ,mod%vsh(i),mod%eta(i),i=1,mod%nbcou)
    write(unit,*) sngl(mod%x1p),sngl(mod%x1n),sngl(mod%x1a1),sngl(mod%x2a1),sngl(mod%x2pa0_a)
    if (icode>0) then
       print*,'probleme de lecture dans',name
       stop 'write_modele: prob d''ecriture'
    endif
    close(unit)
!      
100 format(a80)
105 format(f8.0, 3f9.2, 2f9.1, 2f9.2, f9.5)     
!--------------------------------------------------------------------------
  end subroutine write_modele
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
  subroutine mix_interface(mod1,mod2,mod3)
!adding new interfaces from mod2 into mod1. Rrsult in mod3
!--------------------------------------------------------------------------
    implicit none
    type(modele), intent(in)  :: mod1,mod2    
    type(modele), intent(inout) :: mod3
!
    integer :: nbni,i,ii,nbci,nbi,j
    integer, dimension(:), allocatable :: tmp_ni
    real*8, dimension(:), allocatable :: tmpradi
    logical :: newinterface
!
!counting new interfaces
    allocate(tmp_ni(mod2%nbi))
    nbni=0
    do i=1,mod2%nbi
       newinterface=.true.
       do ii=1,mod1%nbi
          if (abs(mod1%radi(ii)-mod2%radi(i))/mod2%radi(i) < 1.d-7 ) newinterface=.false.
       enddo
       if (newinterface) then
          nbni=nbni+1
          tmp_ni(nbni)=i
       endif
    enddo
    print*,'Number of new interfaces founded :',nbni
    mod3%nbi=mod1%nbi+nbni
    allocate(mod3%radi(mod3%nbi),mod3%inti(mod3%nbi+1),tmpradi(mod3%nbi+2))
!    mod3%radi(1:mod1%nbi)=mod1%radi(1:mod1%nbi)
    tmpradi(1)=0.d0
    tmpradi(2:mod1%nbi+1)=mod1%radi(1:mod1%nbi)
    tmpradi(mod1%nbi+2)=mod1%r(mod1%nbcou)
    tmpradi(mod1%nbi+3:)=-1
    do i=1,nbni
       call add_vl(mod2%radi(tmp_ni(i)),tmpradi)
    enddo
    mod3%radi(1:mod3%nbi)=tmpradi(2:mod3%nbi+1)
    deallocate(tmpradi)
    mod3%nbcou=mod1%nbcou+nbni*2
    allocate(mod3%r(mod3%nbcou),tmpradi(0:mod1%nbcou+nbni*2)) 
!
!    mod3%nbi=mod1%nbi
     mod3%nbcou=mod1%nbcou
    mod3%r(1:mod1%nbcou)=mod1%r(1:mod1%nbcou)
    do i=1,nbni
       nbci=mod3%nbcou
       tmpradi(1:nbci+1)=mod3%r(1:nbci+1)
       do ii=1,nbci-1
          if ( mod2%radi(tmp_ni(i)) > tmpradi(ii) .and. &
               mod2%radi(tmp_ni(i)) < tmpradi(ii+1) ) then
             mod3%nbcou=mod3%nbcou+2
             mod3%r(1:ii)=tmpradi(1:ii)
             mod3%r(ii+1)=mod2%radi(tmp_ni(i))
             mod3%r(ii+2)=mod2%radi(tmp_ni(i))
             mod3%r(ii+3:mod3%nbcou)=tmpradi(ii+1:nbci)
          endif
          if (mod2%radi(tmp_ni(i)) == tmpradi(ii+1)) then
             mod3%nbcou=mod3%nbcou+1
             mod3%r(1:ii)=tmpradi(1:ii)
             mod3%r(ii+1)=mod2%radi(tmp_ni(i))
             mod3%r(ii+2:mod3%nbcou)=tmpradi(ii+1:nbci)
          endif
       enddo
    enddo
!relocating interfaces on mod3:
    nbi=0
    mod3%inti(1)=1
    mod3%inti(mod3%nbi+1)=mod3%nbcou
    do i=1,mod3%nbcou-1
       if ( (mod3%r(i+1)-mod3%r(i))/mod3%r(i+1)<1.d-7 ) then
          nbi=nbi+1
          if (abs(mod3%radi(nbi)-mod3%r(i))/mod3%r(i)>1.d-7) STOP 'mix_interface has a problem'
          mod3%inti(nbi)=i
       endif
    enddo
    allocate(mod3%rho(mod3%nbcou),mod3%vpv(mod3%nbcou)         &
         ,mod3%vsv(mod3%nbcou),mod3%qkappa(mod3%nbcou),mod3%qshear(mod3%nbcou) &
         ,mod3%vph(mod3%nbcou),mod3%vsh(mod3%nbcou),mod3%eta(mod3%nbcou))
!
    mod3%rho   (:)=0.d0
    mod3%vpv   (:)=0.d0
    mod3%vsv   (:)=0.d0
    mod3%qkappa(:)=0.d0
    mod3%qshear(:)=0.d0
    mod3%vph   (:)=0.d0
    mod3%vsh   (:)=0.d0
    mod3%eta   (:)=0.d0
!
    mod3%allocated=.true.

    
!!$do i=0,mod3%nbi+1
!!$print*,i,mod3%radi(i),mod3%inti(i)
!!$enddo
!!$do i=0,mod1%nbi+1
!!$print*,i,mod1%radi(i),mod1%inti(i)
!!$enddo
!--------------------------------------------------------------------------
  end subroutine mix_interface
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
  subroutine reinterp_modele(modin,modout)
!on suppose que modout a ete initialise (nombre de couche et rayon)
!--------------------------------------------------------------------------
    use def_gparam
    use module_spline,only:    spline,splint 
    implicit none
    type(modele), intent(in) :: modin
    type(modele), intent(out) :: modout
    integer :: ir,ii
    integer :: i,k,ka,kb,nd,si,kas,kbs
    real(DP), dimension(:,:,:),allocatable   :: y2
    real(DP)        :: yp1,ypn,rad
    logical :: intu,intl
    real(DP), parameter :: eps=1.d0
!

    modout%ifanis=    modin%ifanis
    modout%ifdeck=    modin%ifdeck
    modout%nic  =    modin%nic
    modout%noc  =    modin%noc
    modout%nbceau  =    modin%nbceau
    modout%tref  =    modin%tref
    modout%x1p  =    modin%x1p
    modout%x1n  =    modin%x1n
    modout%x1a1  =    modin%x1a1
    modout%x2a1  =    modin%x2a1
    modout%x2pa0_a  =    modin%x2pa0_a


    allocate(y2(modin%nbcou,modin%nbi+1,8))
    si=0
    do nd=1,modin%nbi+1
       ka=modin%inti(nd-1)+1 
       kb=modin%inti(nd)     
       kbs=kb+si
       kas=ka+si
!
       yp1=(modin%rho(kas+1)-modin%rho(kas  ))/(modin%r(ka+1)-modin%r(ka  ))
       ypn=(modin%rho(kbs  )-modin%rho(kbs-1))/(modin%r(kb  )-modin%r(kb-1))
       call spline(modin%r(ka:kb),modin%rho(kas:kbs),yp1,ypn,y2(ka:kb,nd,1))                 
!
       yp1=(modin%vpv(kas+1)-modin%vpv(kas  ))/(modin%r(ka+1)-modin%r(ka  ))
       ypn=(modin%vpv(kbs  )-modin%vpv(kbs-1))/(modin%r(kb  )-modin%r(kb-1))
       call spline(modin%r(ka:kb),modin%vpv(kas:kbs),yp1,ypn,y2(ka:kb,nd,2))
!                 
       yp1=(modin%vsv(kas+1)-modin%vsv(kas  ))/(modin%r(ka+1)-modin%r(ka  ))
       ypn=(modin%vsv(kbs  )-modin%vsv(kbs-1))/(modin%r(kb  )-modin%r(kb-1))
       call spline(modin%r(ka:kb),modin%vsv(kas:kbs),yp1,ypn,y2(ka:kb,nd,3))                 
!
       yp1=(modin%vph(kas+1)-modin%vph(kas  ))/(modin%r(ka+1)-modin%r(ka  ))
       ypn=(modin%vph(kbs  )-modin%vph(kbs-1))/(modin%r(kb  )-modin%r(kb-1))
       call spline(modin%r(ka:kb),modin%vph(kas:kbs),yp1,ypn,y2(ka:kb,nd,4))
!                 
       yp1=(modin%vsh(kas+1)-modin%vsh(kas  ))/(modin%r(ka+1)-modin%r(ka  ))
       ypn=(modin%vsh(kbs  )-modin%vsh(kbs-1))/(modin%r(kb  )-modin%r(kb-1))
       call spline(modin%r(ka:kb),modin%vsh(kas:kbs),yp1,ypn,y2(ka:kb,nd,5))                 
!
       yp1=(modin%eta(kas+1)-modin%eta(kas  ))/(modin%r(ka+1)-modin%r(ka  ))
       ypn=(modin%eta(kbs  )-modin%eta(kbs-1))/(modin%r(kb  )-modin%r(kb-1))
       call spline(modin%r(ka:kb),modin%eta(kas:kbs),yp1,ypn,y2(ka:kb,nd,6))                 
!
       yp1=(modin%qshear(kas+1)-modin%qshear(kas  ))/(modin%r(ka+1)-modin%r(ka  ))
       ypn=(modin%qshear(kbs  )-modin%qshear(kbs-1))/(modin%r(kb  )-modin%r(kb-1))
       call spline(modin%r(ka:kb),modin%qshear(kas:kbs),yp1,ypn,y2(ka:kb,nd,7))                 
!
       yp1=(modin%qkappa(kas+1)-modin%qkappa(kas  ))/(modin%r(ka+1)-modin%r(ka  ))
       ypn=(modin%qkappa(kbs  )-modin%qkappa(kbs-1))/(modin%r(kb  )-modin%r(kb-1))
       call spline(modin%r(ka:kb),modin%qkappa(kas:kbs),yp1,ypn,y2(ka:kb,nd,8))                 
!
    enddo
    do i=1,modout%nbcou
       rad=modout%r(i)
       intl=.false.
       intu=.false.
       do ii=1,modout%nbi
          if (i==modout%inti(ii)) intl=.true.
          if (i==modout%inti(ii)+1) intu=.true.
       enddo
       if (intl) rad=rad-eps
       if (intu) rad=rad+eps
!

       nd=locate_i(modin%radi,modin%nbi,rad)
       ka=modin%inti(nd-1)+1 
       kb=modin%inti(nd)     
       kbs=kb+si
       kas=ka+si              
       modout%rho(i)=splint(modin%r(ka:kb),modin%rho(kas:kbs)   ,y2(ka:kb,nd,1),rad)
       modout%vpv(i)=splint(modin%r(ka:kb),modin%vpv(kas:kbs)   ,y2(ka:kb,nd,2),rad)
       modout%vsv(i)=splint(modin%r(ka:kb),modin%vsv(kas:kbs)   ,y2(ka:kb,nd,3),rad)
       modout%vph(i)=splint(modin%r(ka:kb),modin%vph(kas:kbs)   ,y2(ka:kb,nd,4),rad)
       modout%vsh(i)=splint(modin%r(ka:kb),modin%vsh(kas:kbs)   ,y2(ka:kb,nd,5),rad)
       modout%eta(i)=splint(modin%r(ka:kb),modin%eta(kas:kbs)   ,y2(ka:kb,nd,6),rad)
       modout%qshear (i)=splint(modin%r(ka:kb),modin%qshear(kas:kbs),y2(ka:kb,nd,7),rad)
       modout%qkappa (i)=splint(modin%r(ka:kb),modin%qkappa(kas:kbs),y2(ka:kb,nd,8),rad)
    enddo
!scanning for liquid area
    modout%nic=-1
    modout%noc=-1
    if (modout%vsv(i) > 0.d0) then !solid inner core
       do i=1,modout%nbcou
          if (abs(modout%vsv(i)) < 1.d-2 .and. modout%nic<0) modout%nic=i-1
          if (abs(modout%vsv(i)) > 1.d-2 .and. modout%noc<0 .and. modout%nic> 0) modout%noc=i-1
       enddo
       if (abs(modout%vsv(modout%nbcou)) < 1.d-2    ) then
          i=0
          do while (abs(modout%vsv(modout%nbcou-i)) < 1.d-2  )
             i=i+1
          enddo
          modout%nbceau=i
       else
          modout%nbceau=0
       endif
    else !no solid inner core
       modout%nic=0
       do i=1,modout%nbcou
          if (abs(modout%vsv(i)) > 1.d-2 .and. modout%noc<0) modout%noc=i-1
       enddo
       if (abs(modout%vsv(modout%nbcou)) < 1.d-2    ) then
          i=0
          do while (abs(modout%vsv(modout%nbcou-i)) < 1.d-2  )
             i=i+1
          enddo
          modout%nbceau=i
       else
          modout%nbceau=0
       endif
    endif
       
!--------------------------------------------------------------------------
  end subroutine reinterp_modele
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
  subroutine get_beta_value(mod,vp,vs,r,rho,lamb,mu)
!--------------------------------------------------------------------------
    use def_gparam
    implicit none
    type(modele), intent(in) :: mod
    real(DP), intent(out) :: vp,vs,r,rho,lamb,mu
    integer :: n
!
    n  =mod%nbcou
    vp =mod%vpv(n)
    vs =mod%vsv(n)
    r  =mod%r  (n)
    rho=mod%rho(n)
!
    mu    =rho*vs**2
    lamb  =rho*vp**2-2.00_DP*mu       
!
!--------------------------------------------------------------------------
  end subroutine get_beta_value
!--------------------------------------------------------------------------

!--------------------------------------------------------------------------
  logical function modele_different(mod,mod1)
!--------------------------------------------------------------------------
    use def_gparam
    implicit none
    type(modele), intent(in) :: mod,mod1
!
    real :: epsi=1.E-9
    real:: mrho,mr,mvpv,mvph,mvsv,mvsh,mqkappa,mqshear,meta
!
    mrho   =maxval(abs(mod1%rho   (:)))
    mr     =maxval(abs(mod1%r     (:)))
    mvpv   =maxval(abs(mod1%vpv   (:)))
    mvph   =maxval(abs(mod1%vph   (:)))
    mvsv   =maxval(abs(mod1%vsv   (:)))
    mvsh   =maxval(abs(mod1%vsh   (:)))
    mqshear=maxval(abs(mod1%qshear(:)))
    mqkappa=maxval(abs(mod1%qkappa(:)))
    meta   =maxval(abs(mod1%eta   (:)))
!
    if (mod1%nbcou /=mod%nbcou .or.mod1%ifanis/=mod%ifanis  .or.   &
        mod1%nic   /=mod%nic   .or.mod1%noc   /=mod%noc     .or.   &
        mod1%nbceau/=mod%nbceau.or.mod1%ifdeck/=mod%ifdeck  .or.   &
        abs(mod1%tref-mod%tref)>epsi) then
!
       modele_different=.true.
!
    else if (maxval(abs(mod1%rho   (:)-mod%rho   (:)))/mrho   >epsi .or.   &
             maxval(abs(mod1%r     (:)-mod%r     (:)))/mr     >epsi .or.   &
             maxval(abs(mod1%vpv   (:)-mod%vpv   (:)))/mvpv   >epsi .or.   &
             maxval(abs(mod1%vph   (:)-mod%vph   (:)))/mvph   >epsi .or.   &
             maxval(abs(mod1%vsv   (:)-mod%vsv   (:)))/mvsv   >epsi .or.   &
             maxval(abs(mod1%vsh   (:)-mod%vsh   (:)))/mvsh   >epsi .or.   &
             maxval(abs(mod1%qkappa(:)-mod%qkappa(:)))/mqkappa>epsi .or.   &
             maxval(abs(mod1%qshear(:)-mod%qshear(:)))/mqshear>epsi .or.   &
             maxval(abs(mod1%eta   (:)-mod%eta   (:)))/meta   >epsi ) then
!
       modele_different=.true.
!
    else
!
       modele_different=.false.
!
    endif        
!--------------------------------------------------------------------------
  end function modele_different
!--------------------------------------------------------------------------
!-----------------------------------------------------------------
      integer function locate_i(inter,n,x)
!-----------------------------------------------------------------
         use def_gparam
         implicit none
         integer, intent(in) :: n
         real(DP), dimension(0:n+1), intent(in) :: inter
         real(DP), intent(in) :: x
!
         integer :: i
         i=0
         if (abs(x-inter(0))<1.d-8) then
            locate_i=1
            return
         endif
         if (abs (x-inter(0))/max(x,inter(0)) <1.E-10_DP) then
            i=1
         else if ( abs (x-inter(n+1))/max(x,inter(n+1))  <1.E-10_DP) then
            i=n
         else
            do while (  x < inter(i) .or. x > inter(i+1) )
               i=i+1
               if (i > n) then
                  print*,'i=',i,'n=',n
                  print*,'x=',x
                  print*,'inter=',inter
                  stop 'locate_i: failed to locate x in inter!'
               endif
            enddo
         endif
         locate_i=i+1
!-----------------------------------------------------------------
      end function locate_i
!-----------------------------------------------------------------
!------------------------------------------------------------------
  subroutine add_vl(a,t)
!------------------------------------------------------------------
    use def_gparam
 !    implicit none
!    integer, intent(in) :: l
    real(DP), intent(in) :: a
    real(DP), dimension(:), intent(inout) :: t
!
    real(DP), dimension(:),allocatable :: tmp
    integer :: i,nr,n,nb_nomodes
    logical :: deja_la,flag,plein
!
    n=UBOUND(t,dim=1)
    nb_nomodes=0
    plein=.false.
    if (a>0.0_DP) then
       nr=1
       do while (t(nr)>=0.0_DP)
          nr=nr+1
          if (nr>n) then
             stop 'add_v: le tableau est plein!'
             plein=.true.
          endif
       enddo
       nr=nr-1
       if (nr==0) then
          !le tableau est vide
          t(1)=a
       else
          !la valeur est elle deja presente:      
          deja_la=.false.
          do i=1,nr
             if (abs((t(i)-a)/a)<1.E-5_DP) deja_la=.true.
          enddo
          if (.not.deja_la) then
             if (a>t(nr)) then
                if (.not.plein) t(nr+1)=a
             else
                allocate(tmp(n))
                if (a<t(1)) then
                   tmp(:)=t(:)
                   t(1)=a
                   t(2:n)=tmp(1:n-1)
                else
                   i=1
                   do while(.not.(a>t(i).and.a<t(i+1)))
                      i=i+1
                      if (i>=n) stop 'add_v: pfff ca chie'                      
                   enddo
                   tmp(:)=t(:)
                   t(i+1)=a
                   t(i+2:n)=tmp(i+1:n-1)
                endif
                deallocate(tmp)
             endif
          else
             nb_nomodes=nb_nomodes+1
          endif
       end if
    end if
!
!------------------------------------------------------------------
  end subroutine add_vl
!------------------------------------------------------------------
!--------------------------------------------------------------------------
end module earth_modele_b
!--------------------------------------------------------------------------
