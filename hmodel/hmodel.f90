!-----------------------------------------------------------------------------
module hmodel
!-----------------------------------------------------------------------------
  use earth_modele_b
  use filterwavelet
  implicit none  
  public :: init_hmodel, init_hmodel_test,dum_spectra

  private

    integer, parameter :: NBL=8000
    real*8, dimension(NBL) :: vpv,vph,vsv,vsh,rad,qkappa,qshear,eta &
            ,NNN,LLL,u0save,sigma1bar,u1bar,am,cm,lm,nm,fm,acos,ccos,fcos,ncos,lcos
    real*8, dimension(NBL)     :: dxiN,xiN,xiA,xiB,xiC,xiD,XiY,xir
    real*8, dimension(3,NBL)     :: acspl,ccspl,fcspl,lcspl,ncspl
    real*8, dimension(3,NBL)     :: amspl,cmspl,fmspl,lmspl,nmspl
    real*8, dimension(5,NBL) :: asave
    real*8 :: ro,drhmodel,dr,normL,Acorr,Bcorr,rmax,LH
    real*8, parameter ::  vpm=8000.d0,vsm=6000.d0,rhom=3000.d0
    real*8, parameter :: phase=0.
!    real*8, parameter :: phase=LH/4.
!    real*8, parameter :: Ra=6371000.d0,rmin1=4500000.d0,rmin2=5000000.d0
!    real*8, parameter :: phase=LH/5.
!pour l'integration:
    integer :: nbpi,istop
    doubleprecision, dimension(:), allocatable :: xint,wint
!normalization paramters:
    real*8 :: gn,vn2,vn,wn,rn
    real*8, parameter ::bigg=6.6723d-11,rhobar=5515.d0,PI=3.14159265358979323844d0
!
    integer :: cst,nbc,nbcm
    real*8  :: l0,rstart
    integer :: istart
    type(modele) :: mod
    real*8, dimension(:,:,:), allocatable :: AT,AS,ATf,dXi,Xi,Xim,AXi,AXIf,dXi2,Xi2,Xi2m &
                   ,tmp,dxis,xis,xism,ASm,ASb,dxis2,xis2,xis2m,AXis,AXIsf,AXis2,AXIsf2  &
                   ,dXis3,Xis3,iXis2,iXis,dtmp
    real*8, dimension(:,:), allocatable :: ASc,ATc
    real*8, dimension(:)    , allocatable :: AA,CC,FF,LL,NN,radint,drint,rho,AAs,CCs,FFs,LLs,NNs,rhos
    logical ::        doneradint=.false.
    real*8  :: AAc,CCc,FFc,LLc,NNc
    integer, parameter :: SYMETRIC=1,LASTV=2
    integer, parameter :: prolongement=SYMETRIC
!    integer, parameter :: prolongement=LASTV
!
!    logical :: force_cos=.true.
    logical :: force_cos=.false.
!    logical :: stepmodel=.true.
    logical :: stepmodel=.false.
    logical, parameter :: remove_discontinuities=.false.
    logical, parameter :: isotrope=.false.
!    logical, parameter :: isotrope=.true.
    integer :: psvdim=5
!
    character(len=100) :: modname,modname2,fileT,fileS,file_model
    doubleprecision :: kf11,kf12,kf21,kf22,l0filter
    logical :: psvminor
    doubleprecision, dimension(:), allocatable :: Cn,Co,Ln,Lo,Fn,Fo
!pour build_astart
   logical :: standard=.true., velocity_av=.false., slowness_av=.false.
!  logical :: standard=.false.
    logical :: test_spectra=.true.,windowfilter=.true.,vfiltering,rhomogeneization
    integer :: htype !htype=0 (homo standard) 1 (smooth param elastique) 2(smootth velocity)    
    real*8 :: f31ref,f41ref,f32ref,f42ref    
    real*8 :: vdepth1,vdepth2,vamp
    logical :: modal_filtering

contains

!-----------------------------------------------------------------------------
  subroutine init_hmodel()
!-----------------------------------------------------------------------------
    use earth_modele_b
    use mode_filter
    implicit none
    integer :: l,i,k,j
    real*8  :: wdim,f31,f41,f32,f42
    character(len=12) :: name
    real*8, dimension(:,:,:),  allocatable :: A0,Am,dX1,X1,X1m,AX1,iX1 &
                                              ,iX1m,AX1m,dAX1,iAX1,iAX1m,tmp,tmp2
    real*8, dimension(:),  allocatable :: vssmooth
    integer, dimension(1) :: itmp
    real*8 :: x1p,x1n,x1a0,x1a1,x2a1,x2p,x2pa0_am,x2pa0_bm,x2pa0_a,x2pa0_b
    type(modele) :: modf,modper,modref,mod2
!
!    standard=.true.
    test_spectra=.false.
    windowfilter=.false.
    l0filter=100.d0*1000.d0
    print*,'loading hmodel.dat ...'
    call load_hmodel_dat()
!reading original model:
    print*,'loading model: ',modname
    if (force_cos) then
       rn=6371000.d0
!
!normalization paramters:
!
       gn=pi*bigg*rhobar*rn
       vn2=gn*rn
       vn=dsqrt(vn2)
       wn=vn/rn
       l0=l0/rn
       rstart=rstart/rn
!
       call force_model(mod)
       mod%nbcou=mod%nbcou/2
       name='cosmodel'
       call write_modele(name,mod)      
       mod%nbcou=mod%nbcou*2
       call norme_model(mod)
       mod%nbcou=mod%nbcou/2
       nbc=mod%nbcou
       nbcm=2*nbc
!STOP
    else
       call read_modele(modname,mod,rn)
       if (rhomogeneization) then
          call read_modele(modname2,mod2,rn)
          call mix_interface(mod,mod2,modref)
          call mix_interface(mod,mod2,modper)
          call reinterp_modele(mod,modref)
          call reinterp_modele(mod2,modper)
       endif
!
!normalization paramters:
!
       gn=pi*bigg*rhobar*rn
       vn2=gn*rn
       vn=dsqrt(vn2)
       wn=vn/rn
       l0=l0/rn
       l0filter=l0filter/rn
       rstart=rstart/rn

       if (rhomogeneization) then
          call deallocate_modele(mod)
          nbc=modref%nbcou
          call norme_model(modref)
          call norme_model(modper)
          call copy_modele(modref,mod)
       else
          nbc=mod%nbcou
          call norme_model(mod)
       endif
    endif
    itmp(:)=minloc((mod%r(:)-rstart)**2)
    istart=itmp(1)
!initialize filter wavelet:

    f31ref=rn/kf12
    f41ref=rn/kf11
    f32ref=rn/kf22
    f42ref=rn/kf21
    print*,'Building wavelet ...'
    call init_wavelet(l0,cst,rn,f31ref,f41ref,f32ref,f42ref)
    if (modal_filtering) then
       print*,'Loading normal modes for modal fitlering ...'
       call init_mode_filter(fileS,fileT,file_model,radint(1:nbc),drint(1:nbc),nbc,f31ref,f41ref)
       print*,'mode loading done.'
    endif
!    
    allocate(A0(6,1,nbcm),Am(6,1,nbcm),dX1(6,1,nbcm),X1(6,1,nbcm),X1m(6,1,nbcm) &
            ,iX1(6,1,nbcm),iX1m(6,1,nbcm),AX1(6,6,nbcm),AX1m(6,6,nbcm)          &
            ,dAX1(6,6,nbcm),iAX1(6,6,nbcm),iAX1m(6,6,nbcm))
    allocate(Cn(nbc),Co(nbc),Ln(nbc),Lo(nbc),Fn(nbc),Fo(nbc))
    print*,'Building smooth model ...'
!standard=.false. 
    if (rhomogeneization) then
       call build_astart_residual(modref,modper,A0,Am,modf)
    else
       call build_astart(mod,A0,Am,modf)
    endif
!++++++++++++++++++++++++++++++++++++++++++++++++++++
    print*,'Building first order corrector'
! building tools for first order corrector X1:
    dX1(:,:,:)=A0(:,:,:)-Am(:,:,:)
    call primitive(dX1,X1)
!
    if (vfiltering) then
       call filter_matrix_v(X1,X1m,2)
    else if (modal_filtering) then
       call filter_matrix_mode(X1,X1m,2)
    else
       call filter_matrix(X1,X1m,2)
    endif
!do i=1,2*nbc
!write(90,*) A0(6,1,i)
!write(91,*) Am(6,1,i)
!write(92,*) dX1(6,1,i)
!write(100,*) X1(6,1,i)
!write(200,*) X1m(6,1,i)
!enddo
    X1(:,:,:)=X1(:,:,:)-X1m(:,:,:)    
!le sign - est pour etre coherant avec le cas layer
    x1p =-X1(6,1,nbc)
    x1n =-X1(5,1,nbc)
    x1a0=-X1(2,1,nbc)
    x1a1=-X1(3,1,nbc)
!++++++++++++++++++++++++++++++++++++++++++++++++++++
    print*,'Building 2nd order correctors ...'
!dx1 contient A0-Am
    do i=1,6
       do j=1,6
          AX1(i,j,:)=dX1(i,1,:)*X1(j,1,:)
       enddo
    enddo
    if (vfiltering) then
       call filter_matrix_v(AX1,AX1m,2)
    else if (modal_filtering) then
       call filter_matrix_mode(AX1,AX1m,2)
    else
       call filter_matrix(AX1,AX1m,2)
    endif

!preparing tools for 2nd order corrector:
    print*,'Building second order corrector ...'
    call primitive(X1,iX1)
    if (vfiltering) then
       call filter_matrix_v(iX1,iX1m,2)    
    else
       call filter_matrix(iX1,iX1m,2)    
    endif
!!$do i=1,2*nbc
!!$write(301,*) iX1(6,1,i)
!!$write(302,*) iX1m(6,1,i)
!!$enddo
    iX1(:,:,:)=iX1(:,:,:)-iX1m(:,:,:)
!
    dAX1=AX1-AX1m
    call primitive(dAX1,iAX1)
    if (vfiltering) then
       call filter_matrix_v(iAX1,iAX1m,2)    
    else if (modal_filtering) then
       call filter_matrix_mode(AX1,AX1m,2)
    else
       call filter_matrix(iAX1,iAX1m,2)    
    endif
    iAX1(:,:,:)=iAX1(:,:,:)-iAX1m(:,:,:)
!
    x2a1=-iX1(3,1,nbc)
!\int X^1_{\rho} +\int  p  X^1_{a0} -\int<a_0> X^1_{\rho}
    x2pa0_a=+ iX1(6,1,nbc)                          &
         -(              iX1(6,1,nbc)*Am(2,1,nbc))   &
         -(iAX1(6,2,nbc)+iX1(2,1,nbc)*Am(6,1,nbc)) 
!print*,iX1(6,1,nbc)  ,iX1(6,1,nbc)*Am(2,1,nbc),iAX1(6,2,nbc),iX1(2,1,nbc)*Am(6,1,nbc)

!
    print*,'Writing output and exiting ...'
!
    if (rhomogeneization) then
!       x1p=x1p+(modper%x1p-modref%x1p)/(rn*rhobar)
!       x1n=x1n+(modper%x1n-modref%x1n)/(vn2*rn*rhobar)
!       x1a1=x1a1+(modper%x1a1-modref%x1a1)/(vn2*rn*rhobar)
!       x2a1=x2a1+(modper%x2a1-modref%x2a1)/(vn2*rhobar*rn**2)
!       x2pa0_a=x2pa0_a +(modper%x2pa0_a-modref%x2pa0_a)/(rn**2*rhobar)
       x1p=x1p+(modper%x1p)/(rn*rhobar)
       x1n=x1n+(modper%x1n)/(vn2*rn*rhobar)
       x1a1=x1a1+(modper%x1a1)/(vn2*rn*rhobar)
       x2a1=x2a1+(modper%x2a1)/(vn2*rhobar*rn**2)
       x2pa0_a=x2pa0_a +(modper%x2pa0_a)/(rn**2*rhobar)
    else
       x1p=x1p+mod%x1p/(rn*rhobar)
       x1n=x1n+mod%x1n/(vn2*rn*rhobar)
       x1a1=x1a1+mod%x1a1/(vn2*rn*rhobar)
       x2a1=x2a1+mod%x2a1/(vn2*rhobar*rn**2)
       x2pa0_a=x2pa0_a +mod%x2pa0_a/(rn**2*rhobar)
    endif
    modf%x1p=x1p*rn*rhobar
    modf%x1n=x1n*vn2*rn*rhobar
    modf%x1a1=x1a1*vn2*rn*rhobar
    modf%x2a1=x2a1*vn2*rhobar*rn**2
    modf%x2pa0_a=x2pa0_a*rn**2*rhobar

    if (.not.psvminor) then
!sortie pour elements spectraux, il faut denormaliser:
       x1p=x1p*rn*rhobar
       x1n=x1n*vn2*rn*rhobar
       x1a1=x1a1*vn2*rn*rhobar
       x2a1=x2a1*vn2*rhobar*rn**2
       x2pa0_a=x2pa0_a*rn**2*rhobar 
       Cn(:)=Cn(:)*vn2*rhobar
       Co(:)=Co(:)*vn2*rhobar
       Ln(:)=Ln(:)*vn2*rhobar
       Lo(:)=Lo(:)*vn2*rhobar 
       Fn(:)=Fn(:)*vn2*rhobar
       Fo(:)=Fo(:)*vn2*rhobar
       x1(1,1,:)=x1(1,1,:)*(vn2*rhobar)*rn !x1c
       x1(2,1,:)=x1(2,1,:)*rn              !x1a0
       x1(3,1,:)=x1(3,1,:)/(vn2*rhobar)*rn !x1a1
       x1(4,1,:)=x1(4,1,:)*(vn2*rhobar)*rn !x1l
       x1(5,1,:)=x1(5,1,:)/(vn2*rhobar)*rn !x1n
       x1(6,1,:)=x1(6,1,:)*rhobar*rn       !x1p
    endif
    open(11,file='info_layer_corr.dat')
    write(11,*) 'Layer correction type: (1=1D,2=2D):'
    write(11,*) 1  
    write(11,*) 'min radius:'
    write(11,*) mod%r(1)*rn
    write(11,*) 'First and second order coefs (normelized) (X1rho,X1N,X1a1,X2a1,X2pa0_a):'
    write(11,*) X1p,X1N,X1a1
    write(11,*) X2a1,X2pa0_a
    write(11,*) '------------------------------------------'
    write(11,*) istart,nbc,wn
    write(11,*) (mod%r(i)*rn,i=1,nbc)
    write(11,*) (Cn(i),Co(i),Ln(i),Lo(i),Fn(i),Fo(i)   ,i=istart,nbc)
!il y a un signe moins de plus car on utilse dans les codes (nms et hsemm) 
!I-\eps X (ce qui vient des matching conditions) alors que pour l'homogenization
!on utilise (I+\veps X)!
    write(11,*) (-x1(5,1,i),-x1(4,1,i),-x1(1,1,i),i=istart,nbc)
    write(11,*) (-x1(2,1,i),-x1(3,1,i),-x1(6,1,i),i=istart,nbc)
    close(11)
    name='premf'
    call write_modele(name,modf)
!-----------------------------------------------------------------------------
  end subroutine init_hmodel
!-----------------------------------------------------------------------------
!-----------------------------------------------------------------------------
  subroutine dum_spectra()
!-----------------------------------------------------------------------------
    use earth_modele_b
    use mode_filter
    implicit none
    integer :: l,i,k,j
    real*8  :: wdim,f31,f41,f32,f42
    character(len=12) :: name
    real*8, dimension(:,:,:),  allocatable :: A0,Am,dX1,X1,X1m,AX1,iX1 &
                                              ,iX1m,AX1m,dAX1,iAX1,iAX1m,tmp,tmp2
    real*8, dimension(:),  allocatable :: vssmooth
    integer, dimension(1) :: itmp
    real*8 :: x1p,x1n,x1a0,x1a1,x2a1,x2p,x2pa0_am,x2pa0_bm,x2pa0_a,x2pa0_b
    type(modele) :: modf,modper,modref,mod2
!
!    standard=.true.
    test_spectra=.false.
    windowfilter=.false.
    l0filter=100.d0*1000.d0
    print*,'loading hmodel.dat ...'
    call load_hmodel_dat(force_mode=.true.)
    if (.not.rhomogeneization)  STOP 'dum_spectra: need to be for residual homo'
!reading original model:
    print*,'loading model: ',modname
    call read_modele(modname,mod,rn)
    
    call read_modele(modname2,mod2,rn)
    call mix_interface(mod,mod2,modref)
    call mix_interface(mod,mod2,modper)
    call reinterp_modele(mod,modref)
    call reinterp_modele(mod2,modper)
!
!normalization paramters:
!
    gn=pi*bigg*rhobar*rn
    vn2=gn*rn
    vn=dsqrt(vn2)
    wn=vn/rn
    l0=l0/rn
    l0filter=l0filter/rn
    rstart=rstart/rn
    
    call deallocate_modele(mod)
    nbc=modref%nbcou
    call norme_model(modref)
    call norme_model(modper)
    call copy_modele(modref,mod)
 !
    itmp(:)=minloc((mod%r(:)-rstart)**2)
    istart=itmp(1)
!initialize filter wavelet:
    f31ref=rn
    f41ref=rn
    print*,'Building wavelet ...'
    
    print*,'Loading normal modes for modal fitlering ...'
    call init_mode_filter(fileS,fileT,file_model,radint(1:nbc),drint(1:nbc),nbc,f31ref,f41ref)
    print*,'mode loading done.'
!    
    allocate(A0(6,1,nbcm),Am(6,1,nbcm),dX1(6,1,nbcm),X1(6,1,nbcm),X1m(6,1,nbcm) &
            ,iX1(6,1,nbcm),iX1m(6,1,nbcm),AX1(6,6,nbcm),AX1m(6,6,nbcm)          &
            ,dAX1(6,6,nbcm),iAX1(6,6,nbcm),iAX1m(6,6,nbcm))
    allocate(Cn(nbc),Co(nbc),Ln(nbc),Lo(nbc),Fn(nbc),Fo(nbc))
    print*,'Building spectra and dumping'
!standard=.false. 
    call dump_aspectra_residual(modref,modper)
!-----------------------------------------------------------------------------
  end subroutine dum_spectra
!-----------------------------------------------------------------------------
!-----------------------------------------------------------------------------
  subroutine init_hmodel_test()
!-----------------------------------------------------------------------------
    use earth_modele_b
    implicit none
    integer :: l,i,k,j
    real*8  :: wdim,f31,f41,f32,f42
    character(len=12) :: name
    real*8, dimension(:,:,:),  allocatable :: A0,Am,A0h,dX1,X1,X1m,AX1,iX1,Amiso &
                                              ,iX1m,AX1m,dAX1,iAX1,iAX1m,tmp,tmp2
    real*8, dimension(:),  allocatable :: vssmooth
    integer, dimension(1) :: itmp
    real*8 :: x1p,x1n,x1a0,x1a1,x2a1,x2p,x2pa0_am,x2pa0_bm,x2pa0_a,x2pa0_b
    type(modele) :: mods2,modf
!
!    l0filter=27.552*1000.d0

    standard=.true.
    test_spectra=.false.
    windowfilter=.false.
    print*,'loading hmodel.dat ...'
    call load_hmodel_dat()
!reading original model:
    print*,'loading model: ',modname
    call read_modele(modname,mod,rn)
!
!normalization paramters:
!
    gn=pi*bigg*rhobar*rn
    vn2=gn*rn
    vn=dsqrt(vn2)
    wn=vn/rn
    l0=l0/rn
    l0filter=l0filter/rn
    rstart=rstart/rn
    nbc=mod%nbcou
    call norme_model(mod)
    l0filter=drint(1)*81.d0
!    l0filter=28.d0*1000.d0/rn
    print*,'l0filter=',l0filter*rn/1000.d0
    itmp(:)=minloc((mod%r(:)-rstart)**2)
    istart=itmp(1)
!initialize filter wavelet:

    f31ref=rn/kf12
    f41ref=rn/kf11
    f32ref=rn/kf22
    f42ref=rn/kf21
    print*,'Building wavelet ...'
    call init_wavelet(l0,cst,rn,f31ref,f41ref,f32ref,f42ref)
!    
    allocate(A0(6,1,nbcm),A0h(6,1,nbcm),Am(6,1,nbcm),Amiso(6,1,nbcm),dX1(6,1,nbcm),X1(6,1,nbcm),X1m(6,1,nbcm) &
            ,iX1(6,1,nbcm),iX1m(6,1,nbcm),AX1(6,6,nbcm),AX1m(6,6,nbcm)          &
            ,dAX1(6,6,nbcm),iAX1(6,6,nbcm),iAX1m(6,6,nbcm))
    allocate(Cn(nbc),Co(nbc),Ln(nbc),Lo(nbc),Fn(nbc),Fo(nbc))
    print*,'Building smooth model ...'
!start isotrope:
    standard=.false.
!    standard=.true.
    test_spectra=.false.
    windowfilter=.false.
    call build_astart(mod,A0,Amiso,modf)
!test 
!Amiso=0.d0
!
!!$    do i=1,6
!!$       A0h(i,1,:)=A0(i,1,:)-Amiso(i,1,:)+A0(i,1,1)
!!$    enddo
!!$!lecture bidon pour allouer mods2   
!!$    call read_modele(modname,mods2,rn)
!!$
!!$!on normalise le rayon
!!$    mods2%r(:)=mods2%r(:)/rn
!!$    !
!!$    call mat2mod(A0h,mods2,1,.false.)
!!$!
!!$    standard=.true.
!!$    test_spectra=.false.
!!$    windowfilter=.true.
!!$    call build_astart2(mods2,A0h,Am,Amiso,A0)
    call build_astart3(A0,Am,Amiso)
    A0=Am
    call build_astart3(A0,Am,Amiso)
!lecture bidon pour allouer mods2   
    call read_modele(modname,mods2,rn)

!on normalise le rayon
    mods2%r(:)=mods2%r(:)/rn
    !
    call mat2mod(Am,mods2,1,.true.)
    call write_modele('premf',mods2)
!
!La suite n'est pas a jour!!!!!!!!!!!!!!!!!!!
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++
    print*,'Building first order corrector'
! building tools for first order corrector X1:
    dX1(:,:,:)=A0(:,:,:)-Am(:,:,:)
    call primitive(dX1,X1)
!
    call filter_matrix(X1,X1m,2)
    X1(:,:,:)=X1(:,:,:)-X1m(:,:,:)    
!le sign - est pour etre coherant avec le cas layer
    x1p =-X1(6,1,nbc)
    x1n =-X1(5,1,nbc)
    x1a0=-X1(2,1,nbc)
    x1a1=-X1(3,1,nbc)
!++++++++++++++++++++++++++++++++++++++++++++++++++++
! building tools for first order body forces
! note: A0_barre=dX1
    print*,'Building first order body forces ...'
    do i=1,6
       do j=1,6
          AX1(i,j,:)=dX1(i,1,:)*X1(j,1,:)
       enddo
    enddo
    call filter_matrix(AX1,AX1m,2)

!preparing tools for 2nd order corrector:
    print*,'Building second order corrector ...'
    call primitive(X1,iX1)
    call filter_matrix(iX1,iX1m,2)    
    iX1(:,:,:)=iX1(:,:,:)-iX1m(:,:,:)
!
    dAX1=AX1-AX1m
    call primitive(dAX1,iAX1)
    call filter_matrix(iAX1,iAX1m,2)    
    iAX1(:,:,:)=iAX1(:,:,:)-iAX1m(:,:,:)
!signe moins ici car il y avait une erreur de signe dans layer_coef
    x2a1=-iX1(3,1,nbc)
    if (psvminor) then
       x2p     = 0.d0
       x2pa0_bm= 0.d0
       x2pa0_am= iX1(6,1,nbc) &
               -(iAX1(2,6,nbc)+iX1(6,1,nbc)*Am(2,1,nbc)) &
               +(iAX1(6,2,nbc)+iX1(2,1,nbc)*Am(6,1,nbc))
    endif
    x2pa0_a= iX1(6,1,nbc)                          &
         -(iAX1(2,6,nbc)+iX1(6,1,nbc)*Am(2,1,nbc)) &
         +(iAX1(6,2,nbc)+iX1(2,1,nbc)*Am(6,1,nbc))
    x2pa0_b= iX1(6,1,nbc)                          &
         -(iAX1(2,6,nbc)+iX1(6,1,nbc)*Am(2,1,nbc)) &
         +(iAX1(6,2,nbc)+iX1(2,1,nbc)*Am(6,1,nbc))
       

!
    print*,'Writing output and exiting ...'

    if (.not.psvminor) then
!sortie pour elements spectraux, il faut denormalizer:
       x1p=x1p*rn*rhobar
       x1n=x1n*vn2*rn*rhobar
       x1a1=x1a1*vn2*rn*rhobar
       x2a1=x2a1*vn2*rhobar*rn**2
       x2pa0_a=x2pa0_a*rn**2*rhobar 
       x2pa0_b=x2pa0_b*rn**2*rhobar  
       x1a0   =x1a0*rn
       Cn(:)=Cn(:)*vn2*rhobar
       Co(:)=Co(:)*vn2*rhobar
       Ln(:)=Ln(:)*vn2*rhobar
       Lo(:)=Lo(:)*vn2*rhobar
       Fn(:)=Fn(:)*vn2*rhobar
       Fo(:)=Fo(:)*vn2*rhobar
       x1(1,1,:)=x1(1,1,:)*(vn2*rhobar)*rn !x1c
       x1(2,1,:)=x1(2,1,:)*rn              !x1a0
       x1(3,1,:)=x1(3,1,:)/(vn2*rhobar)*rn !x1a1
       x1(4,1,:)=x1(4,1,:)*(vn2*rhobar)*rn !x1l
       x1(5,1,:)=x1(5,1,:)/(vn2*rhobar)*rn !x1n
       x1(6,1,:)=x1(6,1,:)*rhobar*rn       !x1p
    endif
    open(11,file='info_layer_corr.dat')
    write(11,*) 'Layer correction type: (1=1D,2=2D):'
    write(11,*) 1  
    write(11,*) 'min radius:'
    write(11,*) rn
    write(11,*) 'First order coefs (X1rho,X1N,X1a1):'
    write(11,*) X1p,X1N,X1a1,X1a0
    write(11,*) X2a1,X2a1/Am(2,1,nbc)
    if (psvminor) then
       write(11,*) X2pa0_bm,X2pa0_am
       write(11,*) X2pa0_b,X2pa0_a
    else
       write(11,*) X2pa0_b+X1a0*X1p,X2pa0_a
    endif
    if (psvminor) write(11,*) X2p
  write(11,*) '------------------------------------------'
  write(11,*) istart,nbc,wn
  write(11,*) (mod%r(i)*rn,i=1,nbc)
  write(11,*) (Cn(i),Co(i),Ln(i),Lo(i),Fn(i),Fo(i)   ,i=istart,nbc)
  write(11,*) (-x1(5,1,i),-x1(4,1,i),-x1(1,1,i),i=istart,nbc)
  write(11,*) (-x1(2,1,i),-x1(3,1,i),-x1(6,1,i),i=istart,nbc)
  close(11)
!-----------------------------------------------------------------------------
  end subroutine init_hmodel_test
!-----------------------------------------------------------------------------
!-----------------------------------------------------------------------------
  subroutine build_astart(mod,A0,As,modf)
!cc=1/C      -> 1
!dd=F/C      -> 2
!aa=A-F**2/C -> 3
!ll=1/L      -> 4
!nn=N        -> 5
!rho         -> 6
!-----------------------------------------------------------------------------
    use earth_modele_b
    use mode_filter
    implicit none
    type(modele), intent(inout) :: mod
    real*8, dimension(:,:,:), intent(out) :: A0,As
!
    real*8 :: AAt,CCt,FFt,LLt,NNt,w1,rho
    integer :: i,j,fid,ii
    character(len=10) :: name='premf'
    type(modele), intent(out) :: modf
!    
    if (.not.standard) then
       print*,'*************** WARNING *****************'
       print*,'The non-standard is used for test        '
       print*,' the smooth model will be isotropic      '
       print*,'*************** WARNING *****************'
    endif
    do i=istart,nbc 
       NNt=mod%rho(i)*mod%vsh(i)**2
       LLt=mod%rho(i)*mod%vsv(i)**2
       AAt=mod%rho(i)*mod%vph(i)**2
       CCt=mod%rho(i)*mod%vpv(i)**2
       FFt=mod%eta(i)*(AAt-2.d0*LLt)  
       Co(i)=CCt
       Lo(i)=LLt
       Fo(i)=FFt
       if (standard) then
          A0(1,1,i)=1.d0/CCt
          A0(2,1,i)=FFt/CCt
          A0(3,1,i)=(AAt-FFt**2/CCt)
          A0(4,1,i)=1.d0/LLt
          A0(5,1,i)=NNt   
          A0(6,1,i)=mod%rho(i) 
       else
!just for test and nothing else!
          if (velocity_av) then
             NNt=mod%vsh(i)
             LLt=mod%vsv(i)
             AAt=mod%vph(i)
             CCt=mod%vpv(i)
             FFt=mod%eta(i)
          endif
          if (slowness_av) then
             if (mod%vsh(i)<=0.)  STOP 'slowness average not possible: zero s wave speed!'
             NNt=1./mod%vsh(i)
             LLt=1./mod%vsv(i)
             AAt=1./mod%vph(i)
             CCt=1./mod%vpv(i)
             FFt=mod%eta(i)
          endif
          A0(1,1,i)=CCt
          A0(2,1,i)=FFt
          A0(3,1,i)=AAt
          A0(4,1,i)=LLt
          A0(5,1,i)=NNt   
          A0(6,1,i)=mod%rho(i) 
       endif
    enddo
    select case(prolongement) 
    case (SYMETRIC)
       do i=1,istart-1
          j=2*istart+1-i !le +1 car on double la couche pour avoir une discontinuite
          j=max(j,1) !si j<=0, on prend la derniere valeur
          j=min(j,nbc)
          NNt=mod%rho(j)*mod%vsh(j)**2
          LLt=mod%rho(j)*mod%vsv(j)**2
          AAt=mod%rho(j)*mod%vph(j)**2
          CCt=mod%rho(j)*mod%vpv(j)**2
          FFt=mod%eta(j)*(AAt-2.d0*LLt)  
          if (standard) then
             A0(1,1,i)=1.d0/CCt
             A0(2,1,i)=FFt/CCt
             A0(3,1,i)=(AAt-FFt**2/CCt)
             A0(4,1,i)=1.d0/LLt
             A0(5,1,i)=NNt     
             A0(6,1,i)=mod%rho(j) 
          else   
             A0(1,1,i)=CCt
             A0(2,1,i)=FFt
             A0(3,1,i)=AAt
             A0(4,1,i)=LLt
             A0(5,1,i)=NNt   
             A0(6,1,i)=mod%rho(j) 
          endif
       enddo
       do i=nbc+1,2*nbc     
          j=2*nbc+1-i
          A0(:,1,i)=A0(:,1,j)
       enddo
    case (LASTV)
       do i=1,istart-1
          j=istart
          NNt=mod%rho(j)*mod%vsh(j)**2
          LLt=mod%rho(j)*mod%vsv(j)**2
          AAt=mod%rho(j)*mod%vph(j)**2
          CCt=mod%rho(j)*mod%vpv(j)**2
          FFt=mod%eta(j)*(AAt-2.d0*LLt)  
          if (standard) then
             A0(1,1,i)=1.d0/CCt
             A0(2,1,i)=FFt/CCt
             A0(3,1,i)=(AAt-FFt**2/CCt)
             A0(4,1,i)=1.d0/LLt
             A0(5,1,i)=NNt      
             A0(6,1,i)=mod%rho(j)  
          else
             if (velocity_av) then
                NNt=mod%vsh(i)
                LLt=mod%vsv(i)
                AAt=mod%vph(i)
                CCt=mod%vpv(i)
                FFt=mod%eta(i)
             endif
             if (slowness_av) then
                if (mod%vsh(i)<=0.)  STOP 'slowness average not possible: zero S wave speed!'
                NNt=1./mod%vsh(i)
                LLt=1./mod%vsv(i)
                AAt=1./mod%vph(i)
                CCt=1./mod%vpv(i)
                FFt=mod%eta(i)
             endif
             A0(1,1,i)=CCt
             A0(2,1,i)=FFt
             A0(3,1,i)=AAt
             A0(4,1,i)=LLt
             A0(5,1,i)=NNt   
             A0(6,1,i)=mod%rho(j) 
          endif
       enddo
       do i=nbc+1,2*nbc     
          j=nbc
          A0(:,1,i)=A0(:,1,j)
       enddo
    case default
       stop 'prolongenement case undefined!'
    end select
    fid=1
    if (windowfilter) fid=0
!test
!A0=A0*A0
    if (vfiltering) then
       call filter_matrix_v(A0,As,fid)
    else if (modal_filtering) then
       A0(:,:,1:istart-1)=0.d0
       call filter_matrix_mode(A0,As,1)
    else
       call filter_matrix(A0,As,fid)
    endif
!As=sqrt(As)
!A0=sqrt(A0)
    if (test_spectra) then
       call fourier_matrix(A0,1)
       call fourier_matrix(As,2)
    endif
    call init_modele(modf)
    call allocate_modele(modf,nbc)
    modf%ifanis=mod%ifanis
    modf%tref=mod%tref
    modf%ifdeck=mod%ifdeck  
    modf%nic=mod%nic
    modf%noc=mod%noc  
    modf%nbceau=mod%nbceau
    modf%titre='premf'    
    i=istart-1
    do ii=istart,nbc
       if (remove_discontinuities) then
          if (abs(mod%r(ii)-mod%r(ii-1))>1.d-6) then
             i=i+1
             modf%r(i)=mod%r(ii)
             modf%qshear(i)=mod%qshear(ii)
             modf%qkappa(i)=mod%qkappa(ii)
          endif          
       else
          i=ii
          modf%r(i)=mod%r(ii)
          modf%qshear(i)=mod%qshear(ii)
          modf%qkappa(i)=mod%qkappa(ii)
       endif
       if (standard) then
          CCt=1.d0/As(1,1,ii)
          FFt=CCt*As(2,1,ii)
          AAt=As(3,1,ii)+FFt**2/CCt
          LLt=1.d0/As(4,1,ii)
          NNt=As(5,1,ii)
          rho=As(6,1,ii)
       else
          CCt=As(1,1,ii)
          FFt=As(2,1,ii)
          AAt=As(3,1,ii)
          LLt=As(4,1,ii)
          NNt=As(5,1,ii)
          rho=As(6,1,ii)
       endif
       Cn(ii)=CCt
       Ln(ii)=LLt
       Fn(ii)=FFt
       if (isotrope) then
          modf%vsh(i)=sqrt(NNt/rho)*vn
          modf%vsv(i)=sqrt(NNt/rho)*vn
          modf%vph(i)=sqrt(AAt/rho)*vn
          modf%vpv(i)=sqrt(AAt/rho)*vn  
          modf%eta(i)=1.d0
       else
          modf%vsh(i)=sqrt(NNt/rho)*vn
          modf%vsv(i)=sqrt(LLt/rho)*vn
          modf%vph(i)=sqrt(AAt/rho)*vn
          modf%vpv(i)=sqrt(CCt/rho)*vn  
          modf%eta(i)=FFt/(AAt-2.d0*LLt)       
!test
!          modf%vsh(i)=As(1,1,ii)*vn
!          modf%vsv(i)=As(2,1,ii)*vn
!          modf%vph(i)=As(3,1,ii)*vn
!          modf%vpv(i)=As(4,1,ii)*vn
!          modf%eta(i)=As(5,1,i)
       endif
!
       modf%rho(i)=rho*rhobar
    enddo
    modf%nbcou=i
    do i=1,istart-1
       As(:,1,i)=A0(:,1,i)
       modf%vsh(i)=mod%vsh(i)*vn
       modf%vsv(i)=mod%vsv(i)*vn
       modf%vph(i)=mod%vph(i)*vn
       modf%vpv(i)=mod%vpv(i)*vn
       modf%eta(i)=mod%eta(i)
!
       modf%rho(i)=mod%rho(i)*rhobar
       modf%r(i)=mod%r(i)
    enddo
    modf%r(:)=modf%r(:)*rn
    modf%qkappa(:)=mod%qkappa(:)
    modf%qshear(:)=mod%qshear(:)
!    call write_modele(name,modf)
!-----------------------------------------------------------------------------
  end subroutine build_astart
!-----------------------------------------------------------------------------

!-----------------------------------------------------------------------------
  subroutine build_astart_residual(modref,modper,A0,As,modf)
!cc=1/C      -> 1
!dd=F/C      -> 2
!aa=A-F**2/C -> 3
!ll=1/L      -> 4
!nn=N        -> 5
!rho         -> 6
!-----------------------------------------------------------------------------
    use earth_modele_b
    use mode_filter
    implicit none
    type(modele), intent(in) :: modref,modper
    real*8, dimension(:,:,:), intent(out) :: A0,As
!
    real*8 :: AAt,CCt,FFt,LLt,NNt,w1,rho,AAtr,CCtr,FFtr,LLtr,NNtr,rhor
    real*8, dimension(6,1,nbc) :: A0ref
    integer :: i,j,fid,ii
    character(len=10) :: name='premf'
    type(modele), intent(out) :: modf

!    logical :: velocity_av=.false.
!standard=.true.    
    if (.not.standard) then
       print*,'*************** WARNING *****************'
       print*,'The non-standard is used for test        '
       print*,' the smooth model will be isotropic      '
       print*,'*************** WARNING *****************'
    endif
    do i=istart,nbc 
       NNt=modper%rho(i)*modper%vsh(i)**2
       LLt=modper%rho(i)*modper%vsv(i)**2
       AAt=modper%rho(i)*modper%vph(i)**2
       CCt=modper%rho(i)*modper%vpv(i)**2
       FFt=modper%eta(i)*(AAt-2.d0*LLt)  
       Co(i)=CCt
       Lo(i)=LLt
       Fo(i)=FFt
       NNtr=modref%rho(i)*modref%vsh(i)**2
       LLtr=modref%rho(i)*modref%vsv(i)**2
       AAtr=modref%rho(i)*modref%vph(i)**2
       CCtr=modref%rho(i)*modref%vpv(i)**2
       FFtr=modref%eta(i)*(AAtr-2.d0*LLtr)  
       if (standard) then
          A0ref(1,1,i)=1.d0/CCtr        
          A0ref(2,1,i)=FFtr/CCtr         
          A0ref(3,1,i)=(AAtr-FFtr**2/CCtr)
          A0ref(4,1,i)=1.d0/LLtr     
          A0ref(5,1,i)=NNtr          
          A0ref(6,1,i)=modref%rho(i)
!
          A0(1,1,i)=1.d0/CCt        -1.d0/CCtr
          A0(2,1,i)=FFt/CCt         -FFtr/CCtr
          A0(3,1,i)=(AAt-FFt**2/CCt)-(AAtr-FFtr**2/CCtr)
          A0(4,1,i)=1.d0/LLt        -1.d0/LLtr
          A0(5,1,i)=NNt             -NNtr   
          A0(6,1,i)=modper%rho(i)   -modref%rho(i) 
       else
!just for test and nothing else!
          if (velocity_av) then
             NNt=modper%vsh(i)
             LLt=modper%vsv(i)
             AAt=modper%vph(i)
             CCt=modper%vpv(i)
             FFt=modper%eta(i)
             NNtr=modref%vsh(i)
             LLtr=modref%vsv(i)
             AAtr=modref%vph(i)
             CCtr=modref%vpv(i)
             FFtr=modref%eta(i)
          endif
          if (slowness_av) then
             if (modper%vsh(i)<=0.)  STOP 'slowness average not possible: zero S wave speed!'
             NNt=1./modper%vsh(i)
             LLt=1./modper%vsv(i)
             AAt=1./modper%vph(i)
             CCt=1./modper%vpv(i)
             FFt=modper%eta(i)
             NNtr=1./modref%vsh(i)
             LLtr=1./modref%vsv(i)
             AAtr=1./modref%vph(i)
             CCtr=1./modref%vpv(i)
             FFtr=modref%eta(i)
          endif
!
          A0ref(1,1,i)=CCtr
          A0ref(2,1,i)=FFtr
          A0ref(3,1,i)=AAtr
          A0ref(4,1,i)=LLtr
          A0ref(5,1,i)=NNtr   
          A0ref(6,1,i)=modref%rho(i) 
!
          A0(1,1,i)=CCt-CCtr
          A0(2,1,i)=FFt-FFtr
          A0(3,1,i)=AAt-AAtr
          A0(4,1,i)=LLt-LLtr
          A0(5,1,i)=NNt-NNtr   
          A0(6,1,i)=modper%rho(i) -modref%rho(i) 
       endif
!write(100,*) i,NNt
!ywrite(101,*) i,LLt
    enddo
    select case(prolongement) 
    case (SYMETRIC)
       do i=1,istart-1
          j=2*istart+1-i !le +1 car on double la couche pour avoir une discontinuite
          j=max(j,1) !si j<=0, on prend la derniere valeur
          j=min(j,nbc)
          NNt=modper%rho(j)*modper%vsh(j)**2
          LLt=modper%rho(j)*modper%vsv(j)**2
          AAt=modper%rho(j)*modper%vph(j)**2
          CCt=modper%rho(j)*modper%vpv(j)**2
          FFt=modper%eta(j)*(AAt-2.d0*LLt)  
          NNtr=modref%rho(j)*modref%vsh(j)**2
          LLtr=modref%rho(j)*modref%vsv(j)**2
          AAtr=modref%rho(j)*modref%vph(j)**2
          CCtr=modref%rho(j)*modref%vpv(j)**2
          FFtr=modref%eta(j)*(AAtr-2.d0*LLtr)  
          if (standard) then
             A0(1,1,i)=1.d0/CCt-1.d0/CCtr
             A0(2,1,i)=FFt/CCt-FFtr/CCtr
             A0(3,1,i)=(AAt-FFt**2/CCt)-(AAtr-FFtr**2/CCtr)
             A0(4,1,i)=1.d0/LLt-1.d0/LLtr
             A0(5,1,i)=NNt    -NNtr 
             A0(6,1,i)=modper%rho(j) -modref%rho(j) 
          else   
             if (velocity_av) then
                NNt=modper%vsh(j)
                LLt=modper%vsv(j)
                AAt=modper%vph(j)
                CCt=modper%vpv(j)
                FFt=modper%eta(j)
                NNtr=modref%vsh(j)
                LLtr=modref%vsv(j)
                AAtr=modref%vph(j)
                CCtr=modref%vpv(j)
                FFtr=modref%eta(j)
             endif
             if (slowness_av) then
                if (modper%vsh(j)<=0.)  STOP 'slowness average not possible: zero S wave speed!'
                NNt=1./modper%vsh(j)
                LLt=1./modper%vsv(j)
                AAt=1./modper%vph(j)
                CCt=1./modper%vpv(j)
                FFt=modper%eta(j)
                NNtr=1./modref%vsh(j)
                LLtr=1./modref%vsv(j)
                AAtr=1./modref%vph(j)
                CCtr=1./modref%vpv(j)
                FFtr=modref%eta(j)
             endif
             A0(1,1,i)=CCt-CCtr
             A0(2,1,i)=FFt-FFtr
             A0(3,1,i)=AAt-AAtr
             A0(4,1,i)=LLt-LLtr
             A0(5,1,i)=NNt   -NNtr
             A0(6,1,i)=modper%rho(j) -modref%rho(j) 
          endif
       enddo
       do i=nbc+1,2*nbc     
          j=2*nbc+1-i
          A0(:,1,i)=A0(:,1,j)
       enddo
    case (LASTV)
       do i=1,istart-1
          j=istart
          NNt=modper%rho(j)*modper%vsh(j)**2
          LLt=modper%rho(j)*modper%vsv(j)**2
          AAt=modper%rho(j)*modper%vph(j)**2
          CCt=modper%rho(j)*modper%vpv(j)**2
          FFt=modper%eta(j)*(AAt-2.d0*LLt)  
          NNtr=modref%rho(j)*modref%vsh(j)**2
          LLtr=modref%rho(j)*modref%vsv(j)**2
          AAtr=modref%rho(j)*modref%vph(j)**2
          CCtr=modref%rho(j)*modref%vpv(j)**2
          FFtr=modref%eta(j)*(AAtr-2.d0*LLtr)  
          if (standard) then
             A0(1,1,i)=1.d0/CCt-1.d0/CCtr
             A0(2,1,i)=FFt/CCt-FFtr/CCtr
             A0(3,1,i)=(AAt-FFt**2/CCt)-(AAtr-FFtr**2/CCtr)
             A0(4,1,i)=1.d0/LLt-1.d0/LLtr
             A0(5,1,i)=NNt    -NNtr 
             A0(6,1,i)=modper%rho(j) -modref%rho(j) 
          else
             if (velocity_av) then
                NNt=modper%vsh(j)
                LLt=modper%vsv(j)
                AAt=modper%vph(j)
                CCt=modper%vpv(j)
                FFt=modper%eta(j)
                NNtr=modref%vsh(j)
                LLtr=modref%vsv(j)
                AAtr=modref%vph(j)
                CCtr=modref%vpv(j)
                FFtr=modref%eta(j)
             endif
             if (slowness_av) then
                if (modper%vsh(j)<=0.)  STOP 'slowness average not possible: zero S wave speed!'
                NNt=1./modper%vsh(j)
                LLt=1./modper%vsv(j)
                AAt=1./modper%vph(j)
                CCt=1./modper%vpv(j)
                FFt=modper%eta(j)
                NNtr=1./modref%vsh(j)
                LLtr=1./modref%vsv(j)
                AAtr=1./modref%vph(j)
                CCtr=1./modref%vpv(j)
                FFtr=modref%eta(i)
             endif
             A0(1,1,i)=CCt-CCtr
             A0(2,1,i)=FFt-FFtr
             A0(3,1,i)=AAt-AAtr
             A0(4,1,i)=LLt-LLtr
             A0(5,1,i)=NNt   -NNtr
             A0(6,1,i)=modper%rho(j) -modref%rho(j) 
          endif
       enddo
       do i=nbc+1,2*nbc     
          j=nbc
          A0(:,1,i)=A0(:,1,j)
       enddo
    case default
       stop 'prolongenement case undefined!'
    end select
    fid=1
    if (windowfilter) fid=0
    if (vfiltering) then
       call filter_matrix_v(A0,As,fid)
    else if (modal_filtering) then
       A0(:,:,1:istart-1)=0.d0
       call filter_matrix_mode(A0,As,1)
       call dump_mode_spectra(As)
    else
       call filter_matrix(A0,As,fid)
    endif
    call write_param(A0,.true.)
    call write_param(As)
!!$do i=1,2*nbc
!!$write(100+j,*) radint(i),A0(j,1,i)
!!$write(200+j,*) radint(i),As(j,1,i)
!!$enddo
!!$enddo
    if (test_spectra) then
       call fourier_matrix(A0,1)
       call fourier_matrix(As,2)
    endif
    call init_modele(modf)
    call allocate_modele(modf,nbc)
    modf%ifanis=modper%ifanis
    modf%tref=modper%tref
    modf%ifdeck=modper%ifdeck  
    modf%nic=modper%nic
    modf%noc=modper%noc  
    modf%nbceau=modper%nbceau
    modf%titre='premf'    
    i=istart-1
    As(:,:,istart:nbc)=As(:,:,istart:nbc)+A0ref(:,:,istart:nbc)
    do ii=istart,nbc
       i=ii
       if (standard) then
          CCt=1.d0/As(1,1,ii)
          FFt=CCt*As(2,1,ii)
          AAt=As(3,1,ii)+FFt**2/CCt
          LLt=1.d0/As(4,1,ii)
          NNt=As(5,1,ii)
          rho=As(6,1,ii)
       else
          CCt=As(1,1,ii)
          FFt=As(2,1,ii)
          AAt=As(3,1,ii)
          LLt=As(4,1,ii)
          NNt=As(5,1,ii)
          rho=As(6,1,ii)
       endif
       Cn(i)=CCt
       Ln(i)=LLt
       Fn(i)=FFt
       if (isotrope) then
          modf%vsh(i)=sqrt(NNt/rho)*vn
          modf%vsv(i)=sqrt(NNt/rho)*vn
          modf%vph(i)=sqrt(AAt/rho)*vn
          modf%vpv(i)=sqrt(AAt/rho)*vn  
          modf%eta(i)=1.d0
       else
          if (velocity_av) then
             modf%vsh(i)=NNt*vn
             modf%vsv(i)=LLt*vn
             modf%vph(i)=AAt*vn
             modf%vpv(i)=CCt*vn  
             modf%eta(i)=FFt
          else if (slowness_av) then
             modf%vsh(i)=1./NNt*vn
             modf%vsv(i)=1./LLt*vn
             modf%vph(i)=1./AAt*vn
             modf%vpv(i)=1./CCt*vn  
             modf%eta(i)=FFt
          else
             modf%vsh(i)=sqrt(NNt/rho)*vn
             modf%vsv(i)=sqrt(LLt/rho)*vn
             modf%vph(i)=sqrt(AAt/rho)*vn
             modf%vpv(i)=sqrt(CCt/rho)*vn  
             modf%eta(i)=FFt/(AAt-2.d0*LLt)       
          endif
       endif
!
       modf%rho(i)=rho*rhobar
    enddo
    As(:,:,istart:nbc)=As(:,:,istart:nbc)-A0ref(:,:,istart:nbc)
    modf%nbcou=i
    do i=1,istart-1
       As(:,1,i)=A0ref(:,1,i)
       modf%vsh(i)=modper%vsh(i)*vn
       modf%vsv(i)=modper%vsv(i)*vn
       modf%vph(i)=modper%vph(i)*vn
       modf%vpv(i)=modper%vpv(i)*vn
       modf%eta(i)=modper%eta(i)
!
       modf%rho(i)=modper%rho(i)*rhobar
    enddo
    modf%r(:)=modper%r(:)*rn
    modf%qkappa(:)=modper%qkappa(:)
    modf%qshear(:)=modper%qshear(:)
!test
    As(:,:,1:istart)=0.d0
    A0(:,:,1:istart)=0.d0
    As(:,:,2*nbc-istart:2*nbc)=0.d0
    A0(:,:,2*nbc-istart:2*nbc)=0.d0
!la modif suivante n'est correcte que pour le correcteur d'ordre 2 a la surface,
! et c'est ce qui nous interesse.
    do i=1,2*nbc
       As(:,:,i)=As(:,:,i)+A0ref(:,:,nbc)
       A0(:,:,i)=A0(:,:,i)+A0ref(:,:,nbc)
    enddo
!fin test 
!    call write_modele(name,modf)
!-----------------------------------------------------------------------------
  end subroutine build_astart_residual
!-----------------------------------------------------------------------------
!-----------------------------------------------------------------------------
  subroutine dump_aspectra_residual(modref,modper)
!cc=1/C      -> 1
!dd=F/C      -> 2
!aa=A-F**2/C -> 3
!ll=1/L      -> 4
!nn=N        -> 5
!rho         -> 6
!-----------------------------------------------------------------------------
    use earth_modele_b
    use mode_filter
    implicit none
    type(modele), intent(in) :: modref,modper
!
    real*8 :: AAt,CCt,FFt,LLt,NNt,w1,rho,AAtr,CCtr,FFtr,LLtr,NNtr,rhor
    real*8, dimension(6,1,nbc) :: A0ref,A0
    integer :: i,j,fid,ii

!    logical :: velocity_av=.false.
!standard=.true.    
    if (.not.standard) then
       print*,'*************** WARNING *****************'
       print*,'The non-standard is used for test        '
       print*,' the smooth model will be isotropic      '
       print*,'*************** WARNING *****************'
    endif
    do i=istart,nbc 
       NNt=modper%rho(i)*modper%vsh(i)**2
       LLt=modper%rho(i)*modper%vsv(i)**2
       AAt=modper%rho(i)*modper%vph(i)**2
       CCt=modper%rho(i)*modper%vpv(i)**2
       FFt=modper%eta(i)*(AAt-2.d0*LLt)  
       Co(i)=CCt
       Lo(i)=LLt
       Fo(i)=FFt
       NNtr=modref%rho(i)*modref%vsh(i)**2
       LLtr=modref%rho(i)*modref%vsv(i)**2
       AAtr=modref%rho(i)*modref%vph(i)**2
       CCtr=modref%rho(i)*modref%vpv(i)**2
       FFtr=modref%eta(i)*(AAtr-2.d0*LLtr)  
       if (standard) then
          A0ref(1,1,i)=1.d0/CCtr        
          A0ref(2,1,i)=FFtr/CCtr         
          A0ref(3,1,i)=(AAtr-FFtr**2/CCtr)
          A0ref(4,1,i)=1.d0/LLtr     
          A0ref(5,1,i)=NNtr          
          A0ref(6,1,i)=modref%rho(i)
!
          A0(1,1,i)=1.d0/CCt        -1.d0/CCtr
          A0(2,1,i)=FFt/CCt         -FFtr/CCtr
          A0(3,1,i)=(AAt-FFt**2/CCt)-(AAtr-FFtr**2/CCtr)
          A0(4,1,i)=1.d0/LLt        -1.d0/LLtr
          A0(5,1,i)=NNt             -NNtr   
          A0(6,1,i)=modper%rho(i)   -modref%rho(i) 
       else
!just for test and nothing else!
          if (velocity_av) then
             NNt=modper%vsh(i)
             LLt=modper%vsv(i)
             AAt=modper%vph(i)
             CCt=modper%vpv(i)
             FFt=modper%eta(i)
             NNtr=modref%vsh(i)
             LLtr=modref%vsv(i)
             AAtr=modref%vph(i)
             CCtr=modref%vpv(i)
             FFtr=modref%eta(i)
          endif
          if (slowness_av) then
             if (modper%vsh(i)<=0.)  STOP 'slowness average not possible: zero S wave speed!'
             NNt=1./modper%vsh(i)
             LLt=1./modper%vsv(i)
             AAt=1./modper%vph(i)
             CCt=1./modper%vpv(i)
             FFt=modper%eta(i)
             NNtr=1./modref%vsh(i)
             LLtr=1./modref%vsv(i)
             AAtr=1./modref%vph(i)
             CCtr=1./modref%vpv(i)
             FFtr=modref%eta(i)
          endif
!
          A0ref(1,1,i)=CCtr
          A0ref(2,1,i)=FFtr
          A0ref(3,1,i)=AAtr
          A0ref(4,1,i)=LLtr
          A0ref(5,1,i)=NNtr   
          A0ref(6,1,i)=modref%rho(i) 
!
          A0(1,1,i)=CCt-CCtr
          A0(2,1,i)=FFt-FFtr
          A0(3,1,i)=AAt-AAtr
          A0(4,1,i)=LLt-LLtr
          A0(5,1,i)=NNt-NNtr   
          A0(6,1,i)=modper%rho(i) -modref%rho(i) 
       endif
!write(100,*) i,NNt
!ywrite(101,*) i,LLt
    enddo
    select case(prolongement) 
    case (SYMETRIC)
       do i=1,istart-1
          j=2*istart+1-i !le +1 car on double la couche pour avoir une discontinuite
          j=max(j,1) !si j<=0, on prend la derniere valeur
          j=min(j,nbc)
          NNt=modper%rho(j)*modper%vsh(j)**2
          LLt=modper%rho(j)*modper%vsv(j)**2
          AAt=modper%rho(j)*modper%vph(j)**2
          CCt=modper%rho(j)*modper%vpv(j)**2
          FFt=modper%eta(j)*(AAt-2.d0*LLt)  
          NNtr=modref%rho(j)*modref%vsh(j)**2
          LLtr=modref%rho(j)*modref%vsv(j)**2
          AAtr=modref%rho(j)*modref%vph(j)**2
          CCtr=modref%rho(j)*modref%vpv(j)**2
          FFtr=modref%eta(j)*(AAtr-2.d0*LLtr)  
          if (standard) then
             A0(1,1,i)=1.d0/CCt-1.d0/CCtr
             A0(2,1,i)=FFt/CCt-FFtr/CCtr
             A0(3,1,i)=(AAt-FFt**2/CCt)-(AAtr-FFtr**2/CCtr)
             A0(4,1,i)=1.d0/LLt-1.d0/LLtr
             A0(5,1,i)=NNt    -NNtr 
             A0(6,1,i)=modper%rho(j) -modref%rho(j) 
          else   
             if (velocity_av) then
                NNt=modper%vsh(i)
                LLt=modper%vsv(i)
                AAt=modper%vph(i)
                CCt=modper%vpv(i)
                FFt=modper%eta(i)
                NNtr=modref%vsh(i)
                LLtr=modref%vsv(i)
                AAtr=modref%vph(i)
                CCtr=modref%vpv(i)
                FFtr=modref%eta(i)
             endif
             if (slowness_av) then
                if (mod%vsh(i)<=0.)  STOP 'slowness average not possible: zero S wave speed!'
                NNt=1./modper%vsh(i)
                LLt=1./modper%vsv(i)
                AAt=1./modper%vph(i)
                CCt=1./modper%vpv(i)
                FFt=modper%eta(i)
                NNtr=1./modref%vsh(i)
                LLtr=1./modref%vsv(i)
                AAtr=1./modref%vph(i)
                CCtr=1./modref%vpv(i)
                FFtr=modref%eta(i)
             endif
             A0(1,1,i)=CCt-CCtr
             A0(2,1,i)=FFt-FFtr
             A0(3,1,i)=AAt-AAtr
             A0(4,1,i)=LLt-LLtr
             A0(5,1,i)=NNt   -NNtr
             A0(6,1,i)=modper%rho(j) -modref%rho(j) 
          endif
       enddo
       do i=nbc+1,2*nbc     
          j=2*nbc+1-i
          A0(:,1,i)=A0(:,1,j)
       enddo
    case (LASTV)
       do i=1,istart-1
          j=istart
          NNt=modper%rho(j)*modper%vsh(j)**2
          LLt=modper%rho(j)*modper%vsv(j)**2
          AAt=modper%rho(j)*modper%vph(j)**2
          CCt=modper%rho(j)*modper%vpv(j)**2
          FFt=modper%eta(j)*(AAt-2.d0*LLt)  
          NNtr=modref%rho(j)*modref%vsh(j)**2
          LLtr=modref%rho(j)*modref%vsv(j)**2
          AAtr=modref%rho(j)*modref%vph(j)**2
          CCtr=modref%rho(j)*modref%vpv(j)**2
          FFtr=modref%eta(j)*(AAtr-2.d0*LLtr)  
          if (standard) then
             A0(1,1,i)=1.d0/CCt-1.d0/CCtr
             A0(2,1,i)=FFt/CCt-FFtr/CCtr
             A0(3,1,i)=(AAt-FFt**2/CCt)-(AAtr-FFtr**2/CCtr)
             A0(4,1,i)=1.d0/LLt-1.d0/LLtr
             A0(5,1,i)=NNt    -NNtr 
             A0(6,1,i)=modper%rho(j) -modref%rho(j) 
          else
             if (velocity_av) then
                NNt=modper%vsh(i)
                LLt=modper%vsv(i)
                AAt=modper%vph(i)
                CCt=modper%vpv(i)
                FFt=modper%eta(i)
                NNtr=modref%vsh(i)
                LLtr=modref%vsv(i)
                AAtr=modref%vph(i)
                CCtr=modref%vpv(i)
                FFtr=modref%eta(i)
             endif
             if (slowness_av) then
                if (modper%vsh(i)<=0.)  STOP 'slowness average not possible: zero S wave speed!'
                NNt=1./modper%vsh(i)
                LLt=1./modper%vsv(i)
                AAt=1./modper%vph(i)
                CCt=1./modper%vpv(i)
                FFt=modper%eta(i)
                NNtr=1./modref%vsh(i)
                LLtr=1./modref%vsv(i)
                AAtr=1./modref%vph(i)
                CCtr=1./modref%vpv(i)
                FFtr=modref%eta(i)
             endif
             A0(1,1,i)=CCt-CCtr
             A0(2,1,i)=FFt-FFtr
             A0(3,1,i)=AAt-AAtr
             A0(4,1,i)=LLt-LLtr
             A0(5,1,i)=NNt   -NNtr
             A0(6,1,i)=modper%rho(j) -modref%rho(j) 
          endif
       enddo
       do i=nbc+1,2*nbc     
          j=nbc
          A0(:,1,i)=A0(:,1,j)
       enddo
    case default
       stop 'prolongenement case undefined!'
    end select
    A0(:,:,1:istart-1)=0.d0
    call write_param(A0)
    call dump_mode_spectra(A0)
!-----------------------------------------------------------------------------
  end subroutine dump_aspectra_residual
!-----------------------------------------------------------------------------
  subroutine write_param(As,flag)
!-----------------------------------------------------------------------------
    implicit none
    real*8, dimension(:,:,:), intent(in) :: As    
    logical, optional, intent(in) :: flag
    real*8, dimension(6) :: Anorm
    real*8 :: A,C,F,L,N
    real*8, parameter :: rho_norm=3400.d0,vp_norm=8600.d0,vs_norm=4600.d0
    integer:: j,jj,i
    character(len=10) :: name2    
    A=rho_norm*vp_norm**2/rhobar/vn**2
    C=rho_norm*vp_norm**2/rhobar/vn**2
    L=rho_norm*vs_norm**2/rhobar/vn**2
    N=rho_norm*vs_norm**2/rhobar/vn**2
    F=1.d0*(A-2.d0*L)
    Anorm(1)=rho_norm/rhobar
    Anorm(2)=1.d0/C
    Anorm(3)=1.d0/L
    Anorm(4)=A-F**2/C
    Anorm(5)=F/C
    Anorm(6)=N
!
    do j=1,6
       select case(j)
       case (1)
          jj=2
       case (2)
          jj=5
       case (3)
          jj=4
       case (4)
          jj=3
       case (5)
          jj=6
       case(6)
          jj=1
       end select
       if (present(flag)) then
          write(name2,'("param_o",i1)') jj
       else
          write(name2,'("param",i1)') jj
       endif
       open(11,file=name2)
       do i=nbc/2,nbc
          write(11,*) radint(i)*rn,As(j,1,i)/Anorm(jj)
       enddo
       close(11)
    enddo    
!-----------------------------------------------------------------------------
  end subroutine write_param
!-----------------------------------------------------------------------------

!-----------------------------------------------------------------------------
  subroutine mat2mod(A,mod,id,norm)
!cc=1/C      -> 1
!dd=F/C      -> 2
!aa=A-F**2/C -> 3
!ll=1/L      -> 4
!nn=N        -> 5
!rho         -> 6
!-----------------------------------------------------------------------------
    use earth_modele_b
    implicit none
    type(modele), intent(out) :: mod
    integer, intent(in) :: id
    logical, intent(in) :: norm
    real*8, dimension(:,:,:), intent(in) :: A
    real*8, dimension(nbc) :: rtmp,qs,qk
    real*8 :: AAt,CCt,FFt,LLt,NNt,rho
    integer :: ii,i

    rtmp(:)=mod%r(:)
    qk(:)=mod%qkappa(:)
    qs(:)=mod%qshear(:)
    do ii=1,nbc
!removing discontinuities
       if (ii>1) then
          if (abs(rtmp(ii)-rtmp(ii-1))>1.d-6) then
             i=i+1
             mod%r(i)=rtmp(ii)
             mod%qshear(i)=qs(ii)
             mod%qkappa(i)=qk(ii)
          endif
       endif
!
       if (id==0) then
          CCt=1.d0/A(1,1,ii)
          FFt=CCt*A(2,1,ii)
          AAt=A(3,1,ii)+FFt**2/CCt
          LLt=1.d0/A(4,1,ii)
          NNt=A(5,1,ii)
          rho=A(6,1,ii)
       else
          CCt=A(1,1,ii)
          FFt=A(2,1,ii)
          AAt=A(3,1,ii)
          LLt=A(4,1,ii)
          NNt=A(5,1,ii)
          rho=A(6,1,ii)
       endif
       if (norm) then
          mod%vsh(i)=sqrt(NNt/rho)*vn
          mod%vsv(i)=sqrt(LLt/rho)*vn
          mod%vph(i)=sqrt(AAt/rho)*vn
          mod%vpv(i)=sqrt(CCt/rho)*vn  
          mod%eta(i)=FFt/(AAt-2.d0*LLt)       
!
          mod%rho(i)=rho*rhobar
          mod%r(i)=mod%r(i)*rn
       else
          mod%vsh(i)=sqrt(NNt/rho)
          mod%vsv(i)=sqrt(LLt/rho)
          mod%vph(i)=sqrt(AAt/rho)
          mod%vpv(i)=sqrt(CCt/rho)
          mod%eta(i)=FFt/(AAt-2.d0*LLt)       
!
          mod%rho(i)=rho
       endif
    enddo       
    mod%nbcou=i
!-----------------------------------------------------------------------------
  end subroutine mat2mod
!-----------------------------------------------------------------------------
!-----------------------------------------------------------------------------
  subroutine build_astart2(mod,A0,As,Amiso,A0iso)
!cc=1/C      -> 1
!dd=F/C      -> 2
!aa=A-F**2/C -> 3
!ll=1/L      -> 4
!nn=N        -> 5
!rho         -> 6
!-----------------------------------------------------------------------------
    use earth_modele_b
    implicit none
    type(modele), intent(inout) :: mod
    real*8, dimension(:,:,:), intent(in) :: A0iso,Amiso
    real*8, dimension(:,:,:), intent(out) :: A0,As
!
    real*8, dimension(6,1,nbcm) :: A1
!
    real*8 :: AAt,CCt,FFt,LLt,NNt,w1,rho
    integer :: i,j,fid
    character(len=10) :: name='premf'
    type(modele) :: modf
!    
    if (.not.standard) then
       print*,'*************** WARNING *****************'
       print*,'The non-standard is used for test        '
       print*,' the smooth model will be isotropic      '
       print*,'*************** WARNING *****************'
    endif
    do i=istart,nbc 
       NNt=mod%rho(i)*mod%vsh(i)**2
       LLt=mod%rho(i)*mod%vsv(i)**2
       AAt=mod%rho(i)*mod%vph(i)**2
       CCt=mod%rho(i)*mod%vpv(i)**2
       FFt=mod%eta(i)*(AAt-2.d0*LLt)  
       Co(i)=CCt
       Lo(i)=LLt
       Fo(i)=FFt
       if (standard) then
          A0(1,1,i)=1.d0/CCt
          A0(2,1,i)=FFt/CCt
          A0(3,1,i)=(AAt-FFt**2/CCt)
          A0(4,1,i)=1.d0/LLt
          A0(5,1,i)=NNt   
          A0(6,1,i)=mod%rho(i) 
       else
!just for test and nothing else!
          A0(1,1,i)=CCt
          A0(2,1,i)=FFt
          A0(3,1,i)=AAt
          A0(4,1,i)=LLt
          A0(5,1,i)=NNt   
          A0(6,1,i)=mod%rho(i) 
       endif
!write(100,*) i,NNt
!ywrite(101,*) i,LLt
    enddo
    select case(prolongement) 
    case (SYMETRIC)
       do i=1,istart-1
          j=2*istart+1-i !le +1 car on double la couche pour avoir une discontinuite
          j=max(j,1) !si j<=0, on prend la derniere valeur
          j=min(j,nbc)
          NNt=mod%rho(j)*mod%vsh(j)**2
          LLt=mod%rho(j)*mod%vsv(j)**2
          AAt=mod%rho(j)*mod%vph(j)**2
          CCt=mod%rho(j)*mod%vpv(j)**2
          FFt=mod%eta(j)*(AAt-2.d0*LLt)  
          if (standard) then
             A0(1,1,i)=1.d0/CCt
             A0(2,1,i)=FFt/CCt
             A0(3,1,i)=(AAt-FFt**2/CCt)
             A0(4,1,i)=1.d0/LLt
             A0(5,1,i)=NNt     
             A0(6,1,i)=mod%rho(j) 
          else   
             A0(1,1,i)=CCt
             A0(2,1,i)=FFt
             A0(3,1,i)=AAt
             A0(4,1,i)=LLt
             A0(5,1,i)=NNt   
             A0(6,1,i)=mod%rho(i) 
          endif
       enddo
       do i=nbc+1,2*nbc     
          j=2*nbc+1-i
          A0(:,1,i)=A0(:,1,j)
       enddo
    case (LASTV)
       do i=1,istart-1
          j=istart
          NNt=mod%rho(j)*mod%vsh(j)**2
          LLt=mod%rho(j)*mod%vsv(j)**2
          AAt=mod%rho(j)*mod%vph(j)**2
          CCt=mod%rho(j)*mod%vpv(j)**2
          FFt=mod%eta(j)*(AAt-2.d0*LLt)  
          if (standard) then
             A0(1,1,i)=1.d0/CCt
             A0(2,1,i)=FFt/CCt
             A0(3,1,i)=(AAt-FFt**2/CCt)
             A0(4,1,i)=1.d0/LLt
             A0(5,1,i)=NNt      
             A0(6,1,i)=mod%rho(j)  
          else
             A0(1,1,i)=CCt
             A0(2,1,i)=FFt
             A0(3,1,i)=AAt
             A0(4,1,i)=LLt
             A0(5,1,i)=NNt   
             A0(6,1,i)=mod%rho(i) 
          endif
       enddo
       do i=nbc+1,2*nbc     
          j=nbc
          A0(:,1,i)=A0(:,1,j)
       enddo
    case default
       stop 'prolongenement case undefined!'
    end select
    fid=1
    if (windowfilter) fid=0
    call filter_matrix(A0,As,fid)
    call filter_matrix(As,A1,fid)
    As=A1
    call filter_matrix(As,A1,fid)
    As=A1
    if (test_spectra) then
       call fourier_matrix(A0,1)
       call fourier_matrix(As,2)
    endif
    call init_modele(modf)
    call allocate_modele(modf,nbc)
    modf%ifanis=mod%ifanis
    modf%tref=mod%tref
    modf%ifdeck=mod%ifdeck  
    modf%nic=mod%nic
    modf%noc=mod%noc  
    modf%nbceau=mod%nbceau
    modf%titre='premf'    
    do i=istart,nbc
       if (standard) then
          CCt=1.d0/As(1,1,i)
          FFt=CCt*As(2,1,i)
          AAt=As(3,1,i)+FFt**2/CCt
          LLt=1.d0/As(4,1,i)
          NNt=As(5,1,i)
          rho=As(6,1,i)
          CCt=CCt+Amiso(1,1,i)-A0iso(1,1,1)
          FFt=FFt+Amiso(2,1,i)-A0iso(2,1,1)
          AAt=AAt+Amiso(3,1,i)-A0iso(3,1,1)
          LLt=LLt+Amiso(4,1,i)-A0iso(4,1,1)
          NNt=NNt+Amiso(5,1,i)-A0iso(5,1,1)
          rho=rho+Amiso(6,1,i)-A0iso(6,1,1)
       else
          CCt=As(1,1,i)
          FFt=As(2,1,i)
          AAt=As(3,1,i)
          LLt=As(4,1,i)
          NNt=As(5,1,i)
          rho=As(6,1,i)
!!$          CCt=A0(1,1,i)-As(1,1,i)+A0(1,1,1)
!!$          FFt=A0(2,1,i)-As(2,1,i)+A0(2,1,1)
!!$          AAt=A0(3,1,i)-As(3,1,i)+A0(3,1,1)
!!$          LLt=A0(4,1,i)-As(4,1,i)+A0(4,1,1)
!!$          NNt=A0(5,1,i)-As(5,1,i)+A0(5,1,1)
!!$          rho=A0(6,1,i)-As(6,1,i)+A0(6,1,1)
       endif
!write(200,*) i,NNt
!write(201,*) i,LLt
!write(300,*) i,A0(5,1,1)
       Cn(i)=CCt
       Ln(i)=LLt
       Fn(i)=FFt
       modf%vsh(i)=sqrt(NNt/rho)*vn
       modf%vsv(i)=sqrt(LLt/rho)*vn
       modf%vph(i)=sqrt(AAt/rho)*vn
       modf%vpv(i)=sqrt(CCt/rho)*vn  
       modf%eta(i)=FFt/(AAt-2.d0*LLt)       
!
       modf%rho(i)=rho*rhobar
    enddo
    do i=1,istart-1
       modf%vsh(i)=mod%vsh(i)*vn
       modf%vsv(i)=mod%vsv(i)*vn
       modf%vph(i)=mod%vph(i)*vn
       modf%vpv(i)=mod%vpv(i)*vn
       modf%eta(i)=mod%eta(i)
!
       modf%rho(i)=mod%rho(i)*rhobar
    enddo
    modf%r(:)=mod%r(:)*rn
    modf%qkappa(:)=mod%qkappa(:)
    modf%qshear(:)=mod%qshear(:)
    call write_modele(name,modf)
!-----------------------------------------------------------------------------
  end subroutine build_astart2
!-----------------------------------------------------------------------------

!-----------------------------------------------------------------------------
  subroutine build_astart3(A0in,As,Amiso)
!cc=1/C      -> 1
!dd=F/C      -> 2
!aa=A-F**2/C -> 3
!ll=1/L      -> 4
!nn=N        -> 5
!rho         -> 6
!-----------------------------------------------------------------------------
    use earth_modele_b
    implicit none
    real*8, dimension(:,:,:), intent(in) :: A0in,Amiso
    real*8, dimension(:,:,:), intent(out) :: As
!
    real*8, dimension(6,1,nbcm) :: A1,A0
!
    real*8 :: AAt,CCt,FFt,LLt,NNt,w1,rho
    integer :: i,j,fid,ii,iend
    character(len=10) :: name='premf'
    type(modele) :: modf
!    
    print*,'build_astart3 called ...'
    As(:,:,:)=A0in(:,:,:)
    iend=nbc+istart+1
    do i=istart,iend 
       do ii=1,iend
          NNt=A0in(5,1,ii)-Amiso(5,1,ii)+Amiso(5,1,i)
          LLt=A0in(4,1,ii)-Amiso(4,1,ii)+Amiso(4,1,i)
          AAt=A0in(3,1,ii)-Amiso(3,1,ii)+Amiso(3,1,i)
          CCt=A0in(1,1,ii)-Amiso(1,1,ii)+Amiso(1,1,i)
          FFt=A0in(2,1,ii)-Amiso(2,1,ii)+Amiso(2,1,i)
          rho=A0in(6,1,ii)-Amiso(6,1,ii)+Amiso(6,1,i)
!
          A0(1,1,ii)=1.d0/CCt
          A0(2,1,ii)=FFt/CCt
          A0(3,1,ii)=(AAt-FFt**2/CCt)
          A0(4,1,ii)=1.d0/LLt
          A0(5,1,ii)=NNt   
          A0(6,1,ii)=rho
       enddo
       fid=0
!       fid=2
       call filter_matrix_i(i,A0,A1,fid)
!
       CCt=1.d0/A1(1,1,i)
       FFt=CCt*A1(2,1,i)
       AAt=A1(3,1,i)+FFt**2/CCt
       LLt=1.d0/A1(4,1,i)
       NNt=A1(5,1,i)
       rho=A1(6,1,i)
!
       As(1,1,i)=CCt
       As(2,1,i)=FFt
       As(3,1,i)=AAt
       As(4,1,i)=LLt
       As(5,1,i)=NNt
       As(6,1,i)=rho
    enddo
!-----------------------------------------------------------------------------
  end subroutine build_astart3
!-----------------------------------------------------------------------------


!-----------------------------------------------------------------------------
  subroutine prod(A,B,C,dim)
!-----------------------------------------------------------------------------
    implicit none
    integer, intent(in) :: dim
    real*8, dimension(:,:,:), intent(in) :: A,B
    real*8, dimension(:,:,:), intent(out):: C
!
    integer :: i,j,l
!
    do l=1,nbcm
       do j=1,dim
          do i=1,dim
             C(i,j,l)=SUM(A(i,:,l)*B(:,j,l))
          enddo
       enddo
    enddo
    
!-----------------------------------------------------------------------------
  end subroutine prod
!-----------------------------------------------------------------------------

!-----------------------------------------------------------------------------
  subroutine norme_model(mod)
!-----------------------------------------------------------------------------
    use earth_modele_b
    implicit none  
    type(modele), intent(inout) :: mod
!
    integer :: i,j
    nbc=mod%nbcou
    do i=1,mod%nbcou
       mod%r(i)  =mod%r(i)/rn
       mod%rho(i)=mod%rho(i)/rhobar
       mod%vpv(i)=mod%vpv(i)/vn
       mod%vph(i)=mod%vph(i)/vn
       mod%vsv(i)=mod%vsv(i)/vn
       mod%vsh(i)=mod%vsh(i)/vn
    enddo
    if (.not.doneradint) then
       allocate(radint(2*nbc),drint(2*nbc))
       do i=1,nbc 
          radint(i)=mod%r(i)
       enddo
       do i=nbc+1,2*nbc
          j=2*nbc+1-i
          radint(i)=2*mod%r(nbc)-mod%r(j)
       enddo
       nbcm=2*nbc
       drint(1   )=(radint(2     )-radint(1   ))/2.d0
       drint(nbcm)=(radint(nbcm)-radint(nbcm-1))/2.d0
       do i=2,nbcm-1
          drint(i)=(radint(i+1)-radint(i-1))/2.d0
       enddo
       do i=2,nbc 
          if (abs(radint(i)-radint(i-1))<1.d-9) then
             radint(i-1)=radint(i-1)-1.d-8
             radint(i  )=radint(i  )+1.d-8
          endif
       enddo
       doneradint=.true.
    endif
!-----------------------------------------------------------------------------
  end subroutine norme_model
!-----------------------------------------------------------------------------
!-----------------------------------------------------------------------------
  subroutine force_model(mod)
!-----------------------------------------------------------------------------
    use earth_modele_b
    implicit none  
    type(modele), intent(inout) :: mod
!
    real*8 amp1,amp2,amp3,amp4,amp5,phase1,phase2,phase3,phase4,phase5,r,dr,LH,rstart,coef1,coef2
    integer :: i,j,nblh,nbip,jj,j1,j2
    real*8, parameter :: rmin1=2500000.d0,rmin2=3500000.d0
    if (.not.stepmodel) then
!!$       phase1=0.d0
!!$       phase2=0.40d0*PI
!!$       phase3=0.24d0*PI
!!$       phase4=0.74d0*PI
!!$       phase5=1.04d0*PI
!!$       amp1  =0.20
!!$       amp2  =0.10
!!$       amp3  =0.13
!!$       amp4  =0.19
!!$       amp5  =0.08
       phase1=0.d0
       phase2=0.00d0*PI
       phase3=0.50d0*PI
       phase4=0.5d0*PI
       phase5=0.00d0*PI
       amp1  =0.00
       amp2  =0.20
       amp3  =0.20
       amp4  =0.20
       amp5  =0.20
       call init_modele(mod)
       mod%ifanis=1
       mod%tref=0.d0
       mod%ifdeck=1
       mod%nic=0
       mod%noc=0
       mod%nbceau=0
       mod%titre='cosmodel'
       mod%nbcou=12000
       call allocate_modele(mod)    
       dr=rn/(mod%nbcou/2-1)
       do i=1,mod%nbcou/2
          mod%r(i)=(i-1)*dr
       enddo
       do i=mod%nbcou/2+1,mod%nbcou
          mod%r(i)=(i-2)*dr
       enddo
       do i=1,mod%nbcou
          r=mod%r(i)
          mod%rho(i)=3000.d0*oscillation(r,phase1  ,amp1 )
!       mod%rho(i)=3000.d0 
          mod%vpv(i)=8000.0d0*oscillation(r,phase2  ,amp2 )
          mod%vph(i)=8000.0d0*oscillation(r,phase3 ,amp3 )
          mod%vsv(i)=4600.0d0*oscillation(r,phase4 ,amp4)
          mod%vsh(i)=4600.0d0*oscillation(r,phase5 ,amp5)
!       mod%vpv(i)=8000.0d0*oscillation(r,phase  ,amp )
!       mod%vph(i)=8000.0d0*oscillation(r,phase2 ,amp )
!       mod%vsv(i)=4600.0d0*oscillation(r,phase  ,amp)
!       mod%vsh(i)=4600.0d0*oscillation(r,phase2 ,amp)
!       mod%vpv(i)=8000.0d0
!       mod%vph(i)=8000.0d0
!       mod%vsv(i)=4600.0d0
!       mod%vsh(i)=4600.0d0
          
          mod%eta(i)=1.d0
       enddo
    else
       LH=25000.
       nbpi=48
!       nbpi=24
!!$       LH=12500.
!!$       nbpi=12
       nblh=int(rn/LH)+1
       call init_modele(mod)
       mod%ifanis=1
       mod%tref=0.d0
       mod%ifdeck=1
       mod%nic=0
       mod%noc=0
       mod%nbceau=0
       mod%titre='stepmodel'
       mod%nbcou=nbpi*nblh*2+2
       call allocate_modele(mod)    
       mod%eta(:)=1.0d0
       mod%qkappa(:)=1.d5
       mod%qshear(:)=1.d5
       jj=0
       do i=1,nblh*2
          rstart=(i-1)*LH
          dr=LH/(nbpi-1)
          do j=1,nbpi
             jj=jj+1             
             mod%r(jj)=rstart+(j-1)*dr
             r=mod%r(jj)
             j1=i
             j2=i
!!$             j1=int(i/2)
!!$             j2=int((i+1)/2)
             coef1=(1.+0.2*(-1)**j1*wtcoef(r,rmin1,rmin2,2.*Rn,2.*Rn))
             coef2=(1.+0.2*(-1)**j2*wtcoef(r,rmin1,rmin2,2.*Rn,2.*Rn))
             mod%rho(jj)=3000.0d0*coef2
             mod%vpv(jj)=8000.0d0*coef1
             mod%vph(jj)=8000.0d0*coef2
             mod%vsv(jj)=4600.0d0*coef2
             mod%vsh(jj)=4600.0d0*coef1
             if (mod%r(jj)<rn.and.mod%r(jj)+dr>rn) then
                jj=jj+1             
                mod%r(jj)=rn
                r=mod%r(jj)
                j1=i
                j2=i
!!$                j1=int(i/2)
!!$                j2=int((i+1)/2)
                coef1=(1.+0.2*(-1)**j1*wtcoef(r,rmin1,rmin2,2.*Rn,2.*Rn))
                coef2=(1.+0.2*(-1)**j2*wtcoef(r,rmin1,rmin2,2.*Rn,2.*Rn))
                mod%rho(jj)=3000.0d0*coef2
                mod%vpv(jj)=8000.0d0*coef1
                mod%vph(jj)=8000.0d0*coef2
                mod%vsv(jj)=4600.0d0*coef2
                mod%vsh(jj)=4600.0d0*coef1
                mod%nbcou=jj*2
                jj=jj+1             
                mod%r(jj)=rn
                mod%rho(jj)=3000.0d0*coef2
                mod%vpv(jj)=8000.0d0*coef1
                mod%vph(jj)=8000.0d0*coef2
                mod%vsv(jj)=4600.0d0*coef2
                mod%vsh(jj)=4600.0d0*coef1
             endif
          enddo
       enddo
    endif
!-----------------------------------------------------------------------------
  end subroutine force_model
!-----------------------------------------------------------------------------
!-----------------------------------------------------------------------------
  real*8 function oscillation(r,p,amp)
!-----------------------------------------------------------------------------
    implicit none
    real*8 :: r,p,amp
!    real*8, parameter :: rmin1=4500000.d0,rmin2=5500000.d0,LH=100000.
    real*8, parameter :: rmin1=2500000.d0,rmin2=3500000.d0,LH=50000.
    oscillation=(1.d0+amp*cos(2.d0*PI*r/LH+p)*wtcoef(r,rmin1,rmin2,2.*Rn,2.*Rn))
!----------------------------------------------------------------------------
     end function oscillation
!-----------------------------------------------------------------------------
!----------------------------------------------------------------------
  real*8 function wtcoef(f,f1,f2,f3,f4)
!----------------------------------------------------------------------
    use def_gparam
    implicit none
!
    real(DP), intent(in) ::  f,f1,f2,f3,f4
!
    if (f3.gt.f4) stop 'wtcoefcoef: f3>f4 '
    if (f1.gt.f2) stop 'wtcoefcoef: f1>f2 '
    if (f.le.f3.and.f.ge.f2) then
       wtcoef=1.0_DP
    else if (f.gt.f4.or.f.lt.f1 ) then
       wtcoef=0.0_DP
    else if (f.gt.f3.and.f.le.f4) then
       wtcoef=0.5_DP*(1.0+cos(pi*(f-f3)/(f4-f3)))
    else if (f.ge.f1.and.f.lt.f2) then
       wtcoef=0.5_DP*(1.0+cos(pi*(f-f2)/(f2-f1)))
    endif
!----------------------------------------------------------------------
  end function wtcoef

!-----------------------------------------------------------------------------
!-----------------------------------------------------------------------------
  subroutine unnorme_model(mod)
!-----------------------------------------------------------------------------
    use earth_modele_b
    implicit none  
    type(modele), intent(inout) :: mod
!
    integer :: i
    do i=1,mod%nbcou
       mod%r(i)  =mod%r(i)*rn
       mod%rho(i)=mod%rho(i)*rhobar
       mod%vpv(i)=mod%vpv(i)*vn
       mod%vph(i)=mod%vph(i)*vn
       mod%vsv(i)=mod%vsv(i)*vn
       mod%vsh(i)=mod%vsh(i)*vn
    enddo

!-----------------------------------------------------------------------------
  end subroutine unnorme_model
!-----------------------------------------------------------------------------
!-----------------------------------------------------------------------------
  subroutine build_smooth(mod)
!-----------------------------------------------------------------------------
    use earth_modele_b
    implicit none
    type(modele), intent(inout) :: mod
!
    real*8 :: AAt,CCt,FFt,LLt,NNt,w1
    real*8, dimension(5,1,nbc*2) :: atmp,atmpf
    integer :: i,j
!
    allocate(AAs(2*nbc),CCs(2*nbc),FFs(2*nbc),LLs(2*nbc),NNs(2*nbc),rhos(2*nbc))  
!
    if (force_cos) then
       do i=1,2*nbc 
          rhos(i)=mod%rho(i)
          NNt=mod%rho(i)*mod%vsh(i)**2
          LLt=mod%rho(i)*mod%vsv(i)**2
          AAt=mod%rho(i)*mod%vph(i)**2
          CCt=mod%rho(i)*mod%vpv(i)**2
          FFt=mod%eta(i)*(AAt-2.d0*LLt)  
          atmp(1,1,i)=1.d0/CCt
          atmp(2,1,i)=FFt/CCt
          atmp(3,1,i)=(AAt-FFt**2/CCt)
          atmp(4,1,i)=1.d0/LLt
          atmp(5,1,i)=NNt   
       enddo
    else
       do i=1,nbc 
          rhos(i)=mod%rho(i)
          NNt=mod%rho(i)*mod%vsh(i)**2
          LLt=mod%rho(i)*mod%vsv(i)**2
          AAt=mod%rho(i)*mod%vph(i)**2
          CCt=mod%rho(i)*mod%vpv(i)**2
          FFt=mod%eta(i)*(AAt-2.d0*LLt)  
          atmp(1,1,i)=1.d0/CCt
          atmp(2,1,i)=FFt/CCt
          atmp(3,1,i)=(AAt-FFt**2/CCt)
          atmp(4,1,i)=1.d0/LLt
          atmp(5,1,i)=NNt   
       enddo
       select case(prolongement) 
       case (SYMETRIC)
          do i=nbc+1,2*nbc     
             j=2*nbc+1-i
             rhos(i)=mod%rho(j)
             NNt=mod%rho(j)*mod%vsh(j)**2
             LLt=mod%rho(j)*mod%vsv(j)**2
             AAt=mod%rho(j)*mod%vph(j)**2
             CCt=mod%rho(j)*mod%vpv(j)**2
             FFt=mod%eta(j)*(AAt-2.d0*LLt)  
             atmp(1,1,i)=1.d0/CCt
             atmp(2,1,i)=FFt/CCt
             atmp(3,1,i)=(AAt-FFt**2/CCt)
             atmp(4,1,i)=1.d0/LLt
             atmp(5,1,i)=NNt        
          enddo
       case (LASTV)
          do i=nbc+1,2*nbc     
             j=nbc
             rhos(i)=mod%rho(j)
             NNt=mod%rho(j)*mod%vsh(j)**2
             LLt=mod%rho(j)*mod%vsv(j)**2
             AAt=mod%rho(j)*mod%vph(j)**2
             CCt=mod%rho(j)*mod%vpv(j)**2
             FFt=mod%eta(j)*(AAt-2.d0*LLt)  
             atmp(1,1,i)=1.d0/CCt
             atmp(2,1,i)=FFt/CCt
             atmp(3,1,i)=(AAt-FFt**2/CCt)
             atmp(4,1,i)=1.d0/LLt
             atmp(5,1,i)=NNt        
          enddo
       case default
          stop 'prolongenement case undefined!'
       end select
    endif
    call filter_matrix(atmp,atmpf,1)
    do i=1,nbcm 
       w1=wtcoef(radint(i),rstart,rstart+l0,radint(nbcm)-rstart-l0,radint(nbcm)-rstart)
       atmpf(:,:,i)=atmpf(:,:,i)*w1+atmp(:,:,i)*(1.d0-w1)
    enddo
    do i=1,nbcm 
       CCs(i)=1.d0/atmpf(1,1,i)
       FFs(i)=CCs(i)*atmpf(2,1,i)
       LLs(i)=1.d0/atmpf(4,1,i)
       NNs(i)=atmpf(5,1,i)
       AAs(i)=atmpf(3,1,i)+FFs(i)**2/CCs(i)       
    enddo
    CCc=SUM(CCs)/2./nbc
    FFc=SUM(FFs)/2./nbc
    LLc=SUM(LLs)/2./nbc
    NNc=SUM(NNs)/2./nbc
    AAc=SUM(AAs)/2./nbc
!-----------------------------------------------------------------------------
  end subroutine build_smooth
!-----------------------------------------------------------------------------

!-----------------------------------------------------------------------------
  subroutine build_Amatrix(mod,l,wdim)
!-----------------------------------------------------------------------------
    use earth_modele_b
    implicit none
    type(modele), intent(inout) :: mod
    integer, intent(in) :: l
    real*8, intent(in) :: wdim
!
    integer :: i,j
    real*8 :: w,Ol,z,z2,d,a,bl,el,gl,aaa,lll
!
    w=wdim/wn
!    Ol=real((l-1)*(l+2))
!test
Ol=1
    gl=sqrt(real(l*(l+1)))
    allocate(AT(2,2,2*nbc),AS(psvdim,psvdim,2*nbc))
    allocate(AA(2*nbc),CC(2*nbc),FF(2*nbc),LL(2*nbc),NN(2*nbc),rho(2*nbc))
    At(:,:,1)=0.d0;     At(:,:,nbcm)=0.d0
    if (force_cos) then
       do i=1,2*nbc 
          rho(i)=mod%rho(i)
          NN(i)=mod%rho(i)*mod%vsh(i)**2-NNs(i)+NNc
          lll  =mod%rho(i)*mod%vsv(i)**2
          LL(i)=lll                     -LLs(i)+LLc
          aaa  =mod%rho(i)*mod%vph(i)**2
          AA(i)=aaa                      -AAs(i)+AAc
          CC(i)=mod%rho(i)*mod%vpv(i)**2-CCs(i)+CCc
          FF(i)=mod%eta(i)*(aaa-2.d0*lll)-FFs(i)+FFc
       enddo
    else
       do i=1,nbc 
          rho(i)=mod%rho(i)
          NN(i)=mod%rho(i)*mod%vsh(i)**2-NNs(i)+NNc
          lll  =mod%rho(i)*mod%vsv(i)**2
          LL(i)=lll                     -LLs(i)+LLc
          aaa  =mod%rho(i)*mod%vph(i)**2
          AA(i)=aaa                      -AAs(i)+AAc
          CC(i)=mod%rho(i)*mod%vpv(i)**2-CCs(i)+CCc
          FF(i)=mod%eta(i)*(aaa-2.d0*lll)-FFs(i)+FFc
       enddo
!
       select case(prolongement) 
       case (SYMETRIC)
          do i=nbc+1,2*nbc    
             j=2*nbc+1-i
             rho(i)=mod%rho(j)
             NN(i)=mod%rho(j)*mod%vsh(j)**2-NNs(i)+NNc
             lll=mod%rho(j)*mod%vsv(j)**2
             LL(i)=lll-LLs(i)+LLc
             aaa=mod%rho(j)*mod%vph(j)**2
             AA(i)=aaa-AAs(i)+AAc
             CC(i)=mod%rho(j)*mod%vpv(j)**2-CCs(i)+CCc
             FF(i)=mod%eta(j)*(aaa-2.d0*lll)-FFs(i)+FFc           
          enddo
       case (LASTV)
       case default
          stop 'prolongenement case undefined!'
       end select
    endif
    do i=1,2*nbc
       if (i>1.and.i<2*nbc) then
          z=1./radint(i)
       else
          z=1.d0
       endif
       z2=z**2
       At(1,2,i)=1.d0 /LL(i)
       At(1,1,i)=z
       At(2,1,i)=-rho(i)*w**2+Ol*NN(i)*z2
       At(2,2,i)=-3.d0*z
!
       a=4.d0*z2*(AA(i)-FF(i)**2/CC(i)-NN(i))
       d=1.d0-2.d0*FF(i)/CC(i)
       bl=gl**2*z2*(AA(i)-FF(i)**2/CC(i))-2.*NN(i)*z2
       el=gl*FF(i)/CC(i)
!
       AS(1,1,i)=d*z     ; AS(1,2,i)=1./CC(i); AS(1,3,i)=el*z     ; AS(1,4,i)=0.d0
       AS(2,1,i)=a       ; AS(2,2,i)=-d*z    ; AS(2,3,i)=a*gl/2.d0; AS(2,4,i)=gl*z
       AS(3,1,i)=-gl*z   ; AS(3,2,i)=0.d0    ; AS(3,3,i)=2.d0*z   ; AS(3,4,i)=1.d0/LL(i)
       AS(4,1,i)=a*gl/2. ; AS(4,2,i)=-el*z   ; AS(4,3,i)=bl       ; AS(4,4,i)=-2.d0*z
    enddo
     
!-----------------------------------------------------------------------------
  end subroutine build_Amatrix
!-----------------------------------------------------------------------------
!-----------------------------------------------------------------------------
  subroutine build_allmatrix(mod,l,wdim)
!-----------------------------------------------------------------------------
    use earth_modele_b
    implicit none
    type(modele), intent(inout) :: mod
    integer, intent(in) :: l
    real*8, intent(in) :: wdim
!
    integer :: i,j
    real*8 :: w,Ol,z,z2,d,a,bl,el,gl,aaa,lll,w2,rw2
!
    w=wdim/wn
    w2=w**2
    Ol=real((l-1)*(l+2))
!test
!Ol=1
    gl=sqrt(real(l*(l+1)))
    allocate(AT(2,2,2*nbc),AS(psvdim,psvdim,2*nbc))
    allocate(AA(2*nbc),CC(2*nbc),FF(2*nbc),LL(2*nbc),NN(2*nbc),rho(2*nbc))
    At(:,:,1)=0.d0;     At(:,:,nbcm)=0.d0
    if (force_cos) then
       do i=1,nbc*2 
          rho(i)=mod%rho(i)
          NN(i)=mod%rho(i)*mod%vsh(i)**2
          LL(i)=mod%rho(i)*mod%vsv(i)**2
          aaa  =mod%rho(i)*mod%vph(i)**2
          AA(i)=mod%rho(i)*mod%vph(i)**2
          CC(i)=mod%rho(i)*mod%vpv(i)**2
          FF(i)=mod%eta(i)*(AA(i)-2.d0*LL(i))
       enddo
    else
       do i=1,nbc    
          j=i
          rho(i)=mod%rho(j)
          NN(i)=mod%rho(j)*mod%vsh(j)**2
          LL(i)=mod%rho(j)*mod%vsv(j)**2
          aaa  =mod%rho(j)*mod%vph(j)**2
          AA(i)=mod%rho(j)*mod%vph(j)**2
          CC(i)=mod%rho(j)*mod%vpv(j)**2
          FF(i)=mod%eta(j)*(AA(j)-2.d0*LL(j))
       enddo
       select case(prolongement) 
       case (SYMETRIC)
          do i=nbc+1,2*nbc    
             j=2*nbc+1-i
             rho(i)=mod%rho(j)
             NN(i)=mod%rho(j)*mod%vsh(j)**2
             LL(i)=mod%rho(j)*mod%vsv(j)**2
             aaa  =mod%rho(j)*mod%vph(j)**2
             AA(i)=mod%rho(j)*mod%vph(j)**2
             CC(i)=mod%rho(j)*mod%vpv(j)**2
             FF(i)=mod%eta(j)*(AA(j)-2.d0*LL(j))
          enddo
       case (LASTV)
          do i=nbc+1,2*nbc    
             j=nbc
             rho(i)=mod%rho(j)
             NN(i)=mod%rho(j)*mod%vsh(j)**2
             LL(i)=mod%rho(j)*mod%vsv(j)**2
             aaa  =mod%rho(j)*mod%vph(j)**2
             AA(i)=mod%rho(j)*mod%vph(j)**2
             CC(i)=mod%rho(j)*mod%vpv(j)**2
             FF(i)=mod%eta(j)*(AA(j)-2.d0*LL(j))
          enddo
       case default
          stop 'prolongenement case undefined!'
       end select
    endif
    do i=1,2*nbc
       if (i>1.and.i<2*nbc) then
          z=1./radint(i)
       else
          z=1.d0
       endif
       z2=z**2
       At(1,2,i)=1.d0 /LL(i)
       At(1,1,i)=z
       At(2,1,i)=-rho(i)*w**2+Ol*NN(i)*z2
       At(2,2,i)=-3.d0*z
          !
       a=4.d0*z2*(AA(i)-FF(i)**2/CC(i)-NN(i))
       d=1.d0-2.d0*FF(i)/CC(i)
       bl=gl**2*z2*(AA(i)-FF(i)**2/CC(i))-2.*NN(i)*z2
       el=gl*FF(i)/CC(i)
       rw2=rho(i)*w2
          !
       if (psvdim==4) then
          AS(1,1,i)=d*z     ; AS(1,2,i)=1./CC(i); AS(1,3,i)=el*z     ; AS(1,4,i)=0.d0
          AS(2,1,i)=a       ; AS(2,2,i)=-d*z    ; AS(2,3,i)=a*gl/2.d0; AS(2,4,i)=gl*z
          AS(3,1,i)=-gl*z   ; AS(3,2,i)=0.d0    ; AS(3,3,i)=2.d0*z   ; AS(3,4,i)=1.d0/LL(i)
          AS(4,1,i)=a*gl/2. ; AS(4,2,i)=-el*z   ; AS(4,3,i)=bl       ; AS(4,4,i)=-2.d0*z
       else
          AS(1,1,i)=z*(d+2.d0); AS(1,2,i)=0.d0      ; AS(1,3,i)=1.d0/LL(i);  AS(1,4,i)=-1.d0/CC(i); AS(1,5,i)=0.0d0
          AS(2,1,i)=-gl*a/2.d0; AS(2,2,i)=0.d0      ; AS(2,3,i)=z*gl       ; AS(2,4,i)=el*z        ; AS(2,5,i)=0.0d0
          AS(3,1,i)=-rw2+bl   ; AS(3,2,i)=-2.d0*el*z; AS(3,3,i)=z*(d-2.d0) ; AS(3,4,i)=0.d0        ; AS(3,5,i)=1.d0/CC(i)          
          AS(4,1,i)= rw2-a    ; AS(4,2,i)=-2.d0*gl*z; AS(4,3,i)=0.d0       ; AS(4,4,i)=-z*(d-2.d0) ; AS(4,5,i)=-1.d0/LL(i)
          AS(5,1,i)=0.d0      ; AS(5,2,i)=gl*a      ; AS(5,3,i)=-rw2+a     ; AS(5,4,i)=rw2-bl      ; AS(5,5,i)=-z*(d+2.d0)
       endif
!
       Atf(1,2,i)=1.d0 /LLs(i)
       Atf(1,1,i)=z
       Atf(2,1,i)=-rho(i)*w**2+Ol*NNs(i)*z2
       Atf(2,2,i)=-3.d0*z
          !
       a=4.d0*z2*(AAs(i)-FFs(i)**2/CCs(i)-NNs(i))
       d=1.d0-2.d0*FFs(i)/CCs(i)
       bl=gl**2*z2*(AAs(i)-FFs(i)**2/CCs(i))-2.*NNs(i)*z2
       el=gl*FFs(i)/CCs(i)
       rw2=rhos(i)*w2
       if (psvdim==4) then
          ASm(1,1,i)=d*z     ; ASm(1,2,i)=1./CCs(i); ASm(1,3,i)=el*z     ; ASm(1,4,i)=0.d0
          ASm(2,1,i)=a       ; ASm(2,2,i)=-d*z    ; ASm(2,3,i)=a*gl/2.d0; ASm(2,4,i)=gl*z
          ASm(3,1,i)=-gl*z   ; ASm(3,2,i)=0.d0    ; ASm(3,3,i)=2.d0*z   ; ASm(3,4,i)=1.d0/LLs(i)
          ASm(4,1,i)=a*gl/2. ; ASm(4,2,i)=-el*z   ; ASm(4,3,i)=bl       ; ASm(4,4,i)=-2.d0*z
       else
          ASm(1,1,i)=z*(d+2.d0); ASm(1,2,i)=0.d0      ; ASm(1,3,i)=1.d0/LLs(i); ASm(1,4,i)=-1.d0/CCs(i); ASm(1,5,i)=0.0d0
          ASm(2,1,i)=-gl*a/2.d0; ASm(2,2,i)=0.d0      ; ASm(2,3,i)=z*gl       ; ASm(2,4,i)=el*z        ; ASm(2,5,i)=0.0d0
          ASm(3,1,i)=-rw2+bl   ; ASm(3,2,i)=-2.d0*el*z; ASm(3,3,i)=z*(d-2.d0) ; ASm(3,4,i)=0.d0        ; ASm(3,5,i)=1.d0/CCs(i)          
          ASm(4,1,i)= rw2-a    ; ASm(4,2,i)=-2.d0*gl*z; ASm(4,3,i)=0.d0       ; ASm(4,4,i)=-z*(d-2.d0) ; ASm(4,5,i)=-1.d0/LLs(i)
          ASm(5,1,i)=0.d0      ; ASm(5,2,i)=gl*a      ; ASm(5,3,i)=-rw2+a     ; ASm(5,4,i)=rw2-bl      ; ASm(5,5,i)=-z*(d+2.d0)
       endif
    enddo
    ASb(:,:,:)=AS(:,:,:)-ASm(:,:,:)
!-----------------------------------------------------------------------------
  end subroutine build_allmatrix
!-----------------------------------------------------------------------------
!-----------------------------------------------------------------------------
  subroutine unbuild_Amatrix(l,wdim)
!-----------------------------------------------------------------------------
    use earth_modele_b
    implicit none
    integer, intent(in) :: l
    real*8, intent(in) :: wdim
!
    real*8 :: w,Ol    
!
    integer :: i,j
    real*8 :: AAf,CCf,FFf,LLf,NNf
    type(modele) :: modf
    character(len=10) :: name='premf'
!
    w=wdim/wn
    Ol=real((l-1)*(l+2))
!test
!Ol=1
    call init_modele(modf)
    call allocate_modele(modf,nbc)
    modf%ifanis=mod%ifanis
    modf%tref=mod%tref
    modf%ifdeck=mod%ifdeck  
    modf%nic=mod%nic
    modf%noc=mod%noc  
    modf%nbceau=mod%nbceau
    modf%titre='premf'
!   
    do i=1,nbc 
       modf%r(i)=radint(i)*rn
       if (i==1) then
          LLf=LL(1)
          NNf=NN(1)
          AAf=AA(1)
          CCf=CC(1)
          FFf=FF(1)
       else
!!$          LLf=1./Atf(1,2,i)
!!$          NNf=(Atf(2,1,i)+rho(i)*w**2)*radint(i)**2/Ol
!!$          CCf=1./Asf(1,2,i)
!!$          FFf=(1.d0-Asf(1,1,i)*radint(i))*CCf/2.d0
!!$          AAf=Asf(2,1,i)*radint(i)**2/4.d0+FFf**2/CCf+NNf
       endif
!!$       modf%vsh(i)=sqrt((NNf+NNs(i)-NNc)/rho(i))*vn
!!$       modf%vsv(i)=sqrt((LLf+LLs(i)-LLc)/rho(i))*vn
!!$       modf%vph(i)=sqrt((AAf+AAs(i)-AAc)/rho(i))*vn
!!$       modf%vpv(i)=sqrt((CCf+CCs(i)-CCc)/rho(i))*vn  
!       modf%eta(i)=(FFf+FFs(i)-FFc)/(AAf+AAs(i)-AAc-2.d0*(LLf+LLs(i)-LLc))       

       modf%vsh(i)=sqrt((NNs(i))/rho(i))*vn
       modf%vsv(i)=sqrt((LLs(i))/rho(i))*vn
       modf%vph(i)=sqrt((AAs(i))/rho(i))*vn
       modf%vpv(i)=sqrt((CCs(i))/rho(i))*vn  
       modf%eta(i)=(FFs(i))/(AAs(i)-2.d0*LLs(i))       
!
       modf%rho(i)=rho(i)*rhobar
    enddo
    modf%qkappa(:)=mod%qkappa(:)
    modf%qshear(:)=mod%qshear(:)
    call write_modele(name,modf)
!-----------------------------------------------------------------------------
  end subroutine unbuild_Amatrix
!-----------------------------------------------------------------------------
!-----------------------------------------------------------------------------
  subroutine filter_matrix(A,Af,fid)
!fid: filter id
!-----------------------------------------------------------------------------
    use filterwavelet
    implicit none
    real*8, dimension(:,:,:), intent(in ) :: A
    real*8, dimension(:,:,:), intent(out) :: Af
    integer, intent(in) :: fid
!
    integer :: i,j,iend
    real*8 :: x,x0,norm,f3n,f4n
!    do i=1,nbcm
!       if   ( (radint(i) < rstart) .or.  (radint(i) > radint(nbcm) -rstart)) then
!          Af(:,:,i)=A(:,:,i)
!       else
    Af(:,:,:)=A(:,:,:)
    if (fid/=0) then
       iend=nbc+istart+1
       if (test_spectra) iend=2*nbc-istart
       do i=istart,iend
          Af(:,:,i)=0.d0
          x0=radint(i)
          norm=0.d0
          do j=1,nbcm
             x=radint(j)
             if (x >= x0-cst*l0.and.x <= x0+cst*l0) then
                Af(:,:,i)=Af(:,:,i)+A(:,:,j)*mwave(x0,x,fid)*drint(j)
                norm=norm+mwave(x0,x,fid)*drint(j)
             endif
          enddo
          Af(:,:,i)=Af(:,:,i)/norm                       
       enddo
    else
       iend=nbc+istart+1
       if (test_spectra) iend=2*nbc-istart
       do i=istart,iend
          Af(:,:,i)=0.d0
          x0=radint(i)
          norm=0.d0
          do j=1,nbcm
             x=radint(j)
             if (x >= x0-l0filter.and.x <= x0+l0filter) then
                Af(:,:,i)=Af(:,:,i)+A(:,:,j)*drint(j)
                norm=norm+drint(j)
             endif
          enddo
          Af(:,:,i)=Af(:,:,i)/norm                       
       enddo
    endif
!do i=1,2*nbc
!write(201,*) A(1,1,i)
!write(202,*) Af(1,1,i)
!enddo
!STOP
!-----------------------------------------------------------------------------
  end subroutine filter_matrix
!-----------------------------------------------------------------------------  
!-----------------------------------------------------------------------------
  subroutine filter_matrix_v(A,Af,fid)
!fid: filter id
!-----------------------------------------------------------------------------
    use filterwavelet
    implicit none
    real*8, dimension(:,:,:), intent(in ) :: A
    real*8, dimension(:,:,:), intent(out) :: Af
    integer, intent(in) :: fid
!
    integer :: i,j,iend,fidu
    real*8 :: x,x0,norm,f3n,f4n,depthv,cpef,coef,xx,depthv2,dist,amp
    depthv =vdepth1*1000.d0/rn
    depthv2=vdepth2*1000.d0/rn
    amp=vamp
!depthv=700000.d0/rn
    Af(:,:,:)=A(:,:,:)
    if (fid/=0) then
       iend=nbc+istart+1
       if (test_spectra) iend=2*nbc-istart
       do i=istart,iend
          Af(:,:,i)=0.d0
          x0=radint(i)
          x=abs(radint(nbc)-x0)
          if (abs(x0-radint(nbc))<=depthv) then
             if (abs(x0-radint(nbc))>=depthv2) then
                xx=atan(1.d0)*4.d0*abs(radint(nbc)-depthv2-x0)/(depthv-depthv2)
                coef=1.+0.5*(1.d0+cos(xx))*(amp-1.d0)
             else
                coef=amp
             endif
!coef=3*(depthv-x)/depthv+1.*x/depthv
!write(107,*) x,coef
             if (fid==1) then
                f3n=f31ref*coef
                f4n=f41ref*coef
             else if (fid==2) then
                f3n=f32ref*coef
                f4n=f42ref*coef
             else
                STOP 'AAAAAAAAAAAAA'
             endif
             call reinit_wavelet(f3n,f4n)
             fidu=3
          else             
             fidu=fid
          endif
          norm=0.d0
          do j=1,nbcm
             x=radint(j)
             if (x >= x0-cst*l0.and.x <= x0+cst*l0) then
                Af(:,:,i)=Af(:,:,i)+A(:,:,j)*mwave(x0,x,fidu)*drint(j)
!                Af(:,:,i)=Af(:,:,i)+A(:,:,j)*drint(j)
                norm=norm+mwave(x0,x,fid)*drint(j)
             endif
!print*,Af(6,1,i),A(6,1,1),Af(6,1,i)/A(6,1,1),norm 
          enddo
          Af(:,:,i)=Af(:,:,i)/norm                       
!       endif
       enddo
    else
       iend=nbc+istart+1
       if (test_spectra) iend=2*nbc-istart
       do i=istart,iend
          Af(:,:,i)=0.d0
          x0=radint(i)
          norm=0.d0
          do j=1,nbcm
             x=radint(j)
             if (x >= x0-l0filter.and.x <= x0+l0filter) then
                Af(:,:,i)=Af(:,:,i)+A(:,:,j)*drint(j)
                norm=norm+drint(j)
             endif
          enddo
          Af(:,:,i)=Af(:,:,i)/norm                       
       enddo
    endif
!-----------------------------------------------------------------------------
  end subroutine filter_matrix_v
!-----------------------------------------------------------------------------  

!-----------------------------------------------------------------------------
  subroutine filter_matrix_i(ii,A,Af,fid)
!fid: filter id
!-----------------------------------------------------------------------------
    use filterwavelet
    implicit none
    integer, intent(in) :: ii
    real*8, dimension(:,:,:), intent(in ) :: A
    real*8, dimension(:,:,:), intent(out) :: Af
    integer, intent(in) :: fid
!
    integer :: j,iend
    real*8 :: x,x0,norm

    if (fid/=0) then
       iend=nbc+istart+1
       if (test_spectra) iend=2*nbc-istart
       if (ii>=istart .and. ii<=iend) then
          Af(:,:,ii)=0.d0
          x0=radint(ii)
          norm=0.d0
          do j=1,nbcm
             x=radint(j)
             if (x >= x0-cst*l0.and.x <= x0+cst*l0) then
                Af(:,:,ii)=Af(:,:,ii)+A(:,:,j)*mwave(x0,x,fid)*drint(j)
             endif
          enddo
       else
          Af(:,:,ii)=A(:,:,ii)
       endif
    else
       iend=nbc+istart+1
       if (test_spectra) iend=2*nbc-istart
       if (ii>=istart .and. ii<=iend) then
          Af(:,:,ii)=0.d0
          x0=radint(ii)
          norm=0.d0
          do j=1,nbcm
             x=radint(j)
             if (x >= x0-l0filter.and.x <= x0+l0filter) then
                Af(:,:,ii)=Af(:,:,ii)+A(:,:,j)*drint(j)
                norm=norm+drint(j)
             endif
          enddo
          Af(:,:,ii)=Af(:,:,ii)/norm                       
       else
          Af(:,:,ii)=A(:,:,ii)
       endif
    endif
!-----------------------------------------------------------------------------
  end subroutine filter_matrix_i
!-----------------------------------------------------------------------------  
!!$!-----------------------------------------------------------------------------
!!$  subroutine filter_matrix_cw(A,Af)
!!$!fid: filter id
!!$!-----------------------------------------------------------------------------
!!$    use filterwavelet
!!$    implicit none
!!$    real*8, dimension(:,:,:), intent(in ) :: A
!!$    real*8, dimension(:,:,:), intent(out) :: Af
!!$!
!!$    integer :: i,j
!!$    real*8 :: x,x0,norm
!!$    Af(:,:,:)=A(:,:,:)
!!$    do i=istart,nbc+istart+1
!!$          Af(:,:,i)=0.d0
!!$          x0=radint(i)
!!$          norm=0.d0
!!$          call reinit_wavelet(i)
!!$          do j=1,nbcm
!!$             x=radint(j)
!!$             if (x >= x0-cst*l0.and.x <= x0+cst*l0) then
!!$                Af(:,:,i)=Af(:,:,i)+A(:,:,j)*mwave(x0,x,3)*drint(j)
!!$             endif        
!!$          enddo
!!$    enddo
!!$!-----------------------------------------------------------------------------
!!$  end subroutine filter_matrix_cw
!!$!-----------------------------------------------------------------------------  
!-----------------------------------------------------------------------------
  subroutine filter_matrix2(A,Af,fid)
!fid: filter id
!-----------------------------------------------------------------------------
    use filterwavelet
    implicit none
    real*8, dimension(:,:,:), intent(in ) :: A
    real*8, dimension(:,:,:), intent(out) :: Af
    integer, intent(in) :: fid
!
    integer :: i,j
    real*8 :: x,x0,norm
!    do i=1,nbcm
!       if   ( (radint(i) < rstart) .or.  (radint(i) > radint(nbcm) -rstart)) then
!          Af(:,:,i)=A(:,:,i)
!       else
    Af(:,:,:)=A(:,:,:)
    do i=istart,nbc+istart+1
          Af(:,:,i)=0.d0
          x0=radint(i)
          norm=0.d0
          do j=1,nbcm
             x=radint(j)
             if (x >= x0-cst*l0.and.x <= x0+cst*l0) then
                Af(:,:,i)=Af(:,:,i)+A(:,:,j)*drint(j)
                norm=norm+drint(j)
             endif        
          enddo
          Af(:,:,i)=Af(:,:,i)/norm
!       endif
    enddo
!-----------------------------------------------------------------------------
  end subroutine filter_matrix2
!-----------------------------------------------------------------------------  
!-----------------------------------------------------------------------
  subroutine primitive(A,pA)
!-----------------------------------------------------------------------------
    use module_spline
    implicit none
    real*8, dimension(:,:,:), intent(in) ::   A
    real*8, dimension(:,:,:), intent(out) :: pA
!
    integer :: i
!
    pA(:,:,:)=0.d0
    do i=2,nbcm
       if   ( (radint(i) < cst*l0) .or.  (radint(i) > radint(nbc) + cst*l0)) then
          pA(:,:,i)=0.d0
       else
          pA(:,:,i)=pA(:,:,i-1)+(radint(i)-radint(i-1))*(A(:,:,i-1)+A(:,:,i))/2.d0
       endif
    enddo    
!-----------------------------------------------------------------------------
  end subroutine primitive
!-----------------------------------------------------------------------------
!-----------------------------------------------------------------------------
  subroutine load_hmodel_dat(force_mode)
!-----------------------------------------------------------------------------
    implicit none
    integer :: unit
    logical, optional, intent(in) :: force_mode
!
    unit=92
!
    open(unit,file='hmodel.dat',status='old')
    read(unit,*)
    read(unit,*) rhomogeneization,htype !htype=0 (homo standard) 1 (smooth param elastique) 2(smootth velocity). 3 homostandard mais with nomral filtering, 4 slowness filtering
    if (htype==3.or.present(force_mode)) then
       modal_filtering=.true.
       htype=0
    else
       modal_filtering=.false.
    endif
    read(unit,*)
    read(unit,'(a)') modname
!    if (rhomogeneization) then
       read(unit,*)
       read(unit,'(a)') modname2
!    endif
    read(unit,*)
    read(unit,*) psvminor    
    read(unit,*)
    read(unit,*) rstart
    read(unit,*)
    read(unit,*) l0
    read(unit,*)
    read(unit,*) cst
    read(unit,*)
    read(unit,*) kf11,kf12
    read(unit,*)
    read(unit,*) kf21,kf22
    read(unit,*)
    read(unit,*) modal_filtering
    if (modal_filtering) then
       read(unit,*)
       read(unit,'(a)') fileS
       read(unit,'(a)') fileT
       read(unit,'(a)') file_model
    endif
    close(unit)
!km->m :
    kf11=kf11*1000.d0
    kf12=kf12*1000.d0
    kf21=kf21*1000.d0
    kf22=kf22*1000.d0
    rstart=rstart*1000.d0
    l0    =l0    *1000.d0
!
    select case(htype)
    case(0)
       standard=.true.
       velocity_av=.false.
       slowness_av=.false.
    case(1)
       standard=.false.
       velocity_av=.false.
       slowness_av=.false.
    case(2)
       standard=.false.
       velocity_av=.true.
       slowness_av=.false.
    case(4)
       standard=.false.
       velocity_av=.false.
       slowness_av=.true.
    case default
       STOP 'htype should be 0, 1 or 2!'
    end select
!-----------------------------------------------------------------------------
  end subroutine load_hmodel_dat
!-----------------------------------------------------------------------------
!-----------------------------------------------------------------------------
  subroutine fourier_matrix(A_,id)
!fid: filter id
!-----------------------------------------------------------------------------
    use module_spline
    implicit none
    real*8, dimension(:,:,:), intent(in ) :: A_
    integer, intent(in) :: id
    integer, parameter :: NF=2048,NC=6
    integer :: nbd,i,ii,iend,nbl,irad
    doubleprecision, dimension(:), allocatable :: rad_disc,ind_disc !rayons et indices de couronnes
    integer        , dimension(:), allocatable :: nbl_disc !nombre de couches par couronne    
    doubleprecision :: rs,rad,yp1,ypn
    complex*16, dimension(:,:), allocatable :: value
    real*8, dimension(NC,1,2*nbc) :: A
!
    type for_spline
       doubleprecision, dimension(:), pointer :: r
       doubleprecision, dimension(:,:), pointer :: y,y2
    end type for_spline
    type(for_spline), dimension(:), allocatable :: splineall
!
    if (standard) then
       A(1,:,:)=1.d0    /A_(1,:,:)
       A(2,:,:)=A(1,:,:)*A_(2,:,:)
       A(3,:,:)=A_(3,:,:)+A(2,:,:)**2/A(1,:,:)
       A(4,:,:)=1.d0/A_(4,:,:)
       A(5,:,:)=A_(5,:,:)
       A(6,:,:)=A_(6,:,:)
    else 
       A(:,:,:)=A_(:,:,:)
    endif
!do i=1,2*nbc
!write(50+id,*) radint(i)*6371.,A(1,1,i)
!enddo
    rs=radint(istart)
    nbd=0
    iend=2*nbc-istart
    nbl =iend-istart+1
    do i=istart,iend
       if (abs(radint(i)-radint(i+1))/radint(nbc) < 1.d-8) nbd=nbd+1
    enddo
    allocate(rad_disc(nbd+2),ind_disc(nbd+2))
    rad_disc(1    )=radint(istart)-rs
    rad_disc(nbd+2)=radint(2*nbc-istart)-rs
    ind_disc(1    )=istart-1
    ind_disc(nbd+2)=iend
    ii=0
    do i=istart,iend
       if (abs(radint(i)-radint(i+1))/radint(nbc) < 1.d-8) then
          ii=ii+1
          rad_disc(ii+1)=radint(i)-rs
          ind_disc(ii+1)=i
       endif
    enddo
    allocate(splineall(nbd+1),nbl_disc(nbd+1))  
    do i=1,nbd+1
       nbl_disc(i)=ind_disc(i+1)-ind_disc(i)
       allocate(splineall(i)%y(nbl_disc(i),NC) &
            ,splineall(i)%y2(nbl_disc(i),NC),splineall(i)%r(nbl_disc(i)))
    enddo
    do i=1,nbd+1
       splineall(i)%r(:)  =radint(ind_disc(i)+1:ind_disc(i+1))-rs
       do ii=1,NC
          splineall(i)%y(:,ii)=A(ii,1,ind_disc(i)+1:ind_disc(i+1))
       enddo
    enddo

    do i=1,nbd+1
       do ii=1,NC
          yp1=(splineall(i)%y(2,ii)-splineall(i)%y(1,ii)) &
               /(splineall(i)%r(2)   -splineall(i)%r(1)   )
          ypn=(splineall(i)%y(nbl_disc(i),ii)-splineall(i)%y(nbl_disc(i)-1,ii)) &
               /(splineall(i)%r(nbl_disc(i))   -splineall(i)%r(nbl_disc(i)-1)   )
          call spline(splineall(i)%r,splineall(i)%y(:,ii),yp1,ypn,splineall(i)%y2(:,ii))
       enddo
    enddo
    allocate(value(NF,NC))
    do i=1,NF
       rad=(i-1)*splineall(nbd+1)%r(nbl_disc(nbd+1))/real(NF-1)
       irad=locate(rad_disc,rad)
       do ii=1,NC
          value(i,ii)=splint(splineall(irad)%r &
               ,splineall(irad)%y(:,ii),splineall(irad)%y2(:,ii),rad)
!write(200+id*100+ii,*) (i-1)/(radint(nbc)-rs)/2.d0/6371.d0,real(value(i,ii))
       enddo
    enddo
!spectra:
    do ii=1,NC
       call dfour1(value(:,ii),NF,1)
    enddo
!
    do ii=1,NC
!   do i=1,NF/2
!       write(id*100+ii,*) (i-1)/(radint(nbc)-rs)/2.d0/6371.d0,abs(value(i,ii))
!    enddo
!    call flush(id*100+ii)
    enddo
!cleaning
    deallocate(rad_disc,ind_disc,nbl_disc,value)
    do i=1,nbd+1
       deallocate(splineall(i)%y,splineall(i)%y2)
    enddo
    deallocate(splineall)
!-----------------------------------------------------------------------------
    end subroutine fourier_matrix
!-----------------------------------------------------------------------------
!-----------------------------------------------------------------------------
end module hmodel
!-----------------------------------------------------------------------------
