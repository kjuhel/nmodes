!----------------------------------------------------------------------------------
module mode_filter
!----------------------------------------------------------------------------------
  implicit none
  public :: init_mode_filter,filter_matrix_mode,dump_mode_spectra
  
  private
  integer :: Nmax,nbr
  doubleprecision, dimension(:), allocatable :: rad,dr,freq
  doubleprecision, dimension(:,:), allocatable :: modes
  doubleprecision :: f1,f2,ra
!----------------------------------------------------
contains
!----------------------------------------------------
  subroutine init_mode_filter(modefileS,modefileT,model,rad_,dr_,nbr_,f1_,f2_)
!----------------------------------------------------
    use util_fctp, only: open_fctp, read_interpS, get_lmax, get_Nmax, get_eigenw , get_1Dmodel
    implicit none
    character(len=*), intent(in) :: modefileS,modefileT,model
    doubleprecision, intent(in) :: f1_,f2_
    doubleprecision, dimension(:), intent(in) :: rad_,dr_
    integer, intent(in) :: nbr_
!
    integer :: n,lms,lmt,i,lp
    doubleprecision, dimension(:,:,:), allocatable :: tmpf,tmpb
    doubleprecision, dimension(nbr_) :: rho,bid
    doubleprecision, dimension(4,nbr_) :: xdum4
    doubleprecision :: pi
    integer, dimension(:), allocatable :: NallS,NallT
!
    call open_fctp(modefileS,modefileT,model1Dfile=model,planet_radius=ra)
    lp=0
    pi=atan(1.d0)*4.d0
    f1=f1_*8000./ra; f2=f2_*8000/ra
!    f1=f1_; f2=f2_
    nbr=nbr_
    allocate(rad(nbr),dr(nbr))
    rad(:)=rad_(:)*ra
    dr(:)=dr_(:)
    call get_lmax(f2,lms,lmt)
    allocate(NallS(0:lms),NallT(0:lmt))
    call get_Nmax(f2,NallS,NallT)    
    Nmax=NallS(lp)
    print*,'f2,Nmax=',f2,Nmax
    allocate(modes(nbr,0:Nmax))
!
    do n=0,Nmax
       call read_interpS(n,lp,nbr,rad,xdum4)
       modes(:,n)=xdum4(1,:)
    enddo
    allocate(tmpf(0:Nmax,0:1,2),tmpb(0:Nmax,0:1,2))
    call get_eigenw(tmpf,tmpb,lp,NallS,0,NallT)
    allocate(freq(0:Nmax))
    freq(:)=tmpf(0:Nmax,0,1)/2.d0/pi
    call get_1Dmodel(rho,bid,bid,bid,nbr,rad)
    do n=0,Nmax
       modes(:,n)=modes(:,n)*sqrt(rho(:))*rad(:)/modes(nbr,n)
    enddo
    deallocate(NallS,NallT)
!
!----------------------------------------------------
  end subroutine init_mode_filter
!----------------------------------------------------
!-----------------------------------------------------------------------------
  subroutine filter_matrix_mode(A,Af,id)
!fid: filter id
!-----------------------------------------------------------------------------
    use filterwavelet
    implicit none
    real*8, dimension(:,:,:), intent(in ) :: A
    real*8, dimension(:,:,:), intent(out) :: Af
    integer, intent(in) :: id
!
    integer :: i,j,dim1,dim2,n
    doubleprecision, dimension(:,:,:), allocatable :: coef
    doubleprecision :: norm,wt,f
!    Af(:,:,:)=A(:,:,:)
    dim1=ubound(A,dim=1); dim2=ubound(A,dim=2)
    allocate(coef(dim1,dim2,0:Nmax))
    do n=0,Nmax
       do j=1,dim2
          do i=1,dim1
             coef(i,j,n)=SUM(modes(:,n)*A(i,j,1:nbr)*dr(:))
          enddo
       enddo
       norm=SUM(modes(:,n)**2*dr(:))
       coef(:,:,n)=coef(:,:,n)/norm
    enddo
    Af(:,:,:)=0.0d0
!test
!coef=0.d0
!coef(2,1,100)=0.05d0/maxval(abs(modes(:,100)))
    do n=0,Nmax
       f=freq(n)
       if (id==1) then
          wt=wtcoef_l(f,-1.d0,-1.d0,f1,f2)
       else
!          wt=wtcoef_l(f,-1.d0,-1.d0,f1/3.d0,f2/3.d0)
          if (n==0) then
             wt=1.d0
          else
             wt=0.d0
          endif
       endif
!       print*,'filtering modes: ',n,sngl(f),sngl(f1),sngl(f2),sngl(wt)
!test
!wt=1.d0
       do j=1,dim2
          do i=1,dim1    
             Af(i,j,1:nbr)= Af(i,j,1:nbr)+wt*coef(i,j,n)*modes(:,n)
          enddo
       enddo
    enddo
    Af(:,:,nbr+1:2*nbr)=Af(:,:,nbr:2:-1)
!do i=1,nbr
!write(131,*)rad(i),A(3,1,i)
!write(132,*)rad(i),Af(3,1,i)
!do j=0,Nmax
!write(1000+j,*)rad(i),modes(i,j)
!enddo
!enddo
!!$do j=0,Nmax
!!$do n=0,Nmax
!!$print*,j,n,SUM(modes(:,n)*modes(:,j)*dr(:))/SUM(modes(:,n)*modes(:,n)*dr(:))
!!$enddo
!!$enddo
!-----------------------------------------------------------------------------
  end subroutine filter_matrix_mode
!-----------------------------------------------------------------------------  
!-----------------------------------------------------------------------------
  subroutine dump_mode_spectra(AA)
!fid: filter id
!-----------------------------------------------------------------------------
    use filterwavelet
    implicit none
    real*8, dimension(:,:,:), intent(in ) :: AA
!
    integer :: i,j,dim1,dim2,nn,jj
    doubleprecision, dimension(:,:,:), allocatable :: coef
    doubleprecision :: norm,wt
    character(len=7) :: name
    real*8, dimension(6) :: Anorm
    real*8 :: A,C,F,L,N,vn,rn,gn,vn2
    real*8, parameter ::bigg=6.6723d-11,rhobar=5515.d0,PI=3.14159265358979323844d0
    real*8, parameter :: rho_norm=3400.d0,vp_norm=8600.d0,vs_norm=4600.d0
    
!
    dim1=ubound(AA,dim=1); dim2=ubound(AA,dim=2)
    allocate(coef(dim1,dim2,0:Nmax))
    do nn=0,Nmax
       do j=1,dim2
          do i=1,dim1
             coef(i,j,nn)=SUM(modes(:,nn)*AA(i,j,1:nbr)*dr(:))
          enddo
       enddo
       norm=SUM(modes(:,nn)**2*dr(:))
       coef(:,:,nn)=coef(:,:,nn)/norm
    enddo
!
    rn=ra
    gn=pi*bigg*rhobar*rn
    vn2=gn*rn
    vn=dsqrt(vn2)
    A=rho_norm*vp_norm**2/rhobar/vn**2
    C=rho_norm*vp_norm**2/rhobar/vn**2
    L=rho_norm*vs_norm**2/rhobar/vn**2
    N=rho_norm*vs_norm**2/rhobar/vn**2
    F=1.d0*(A-2.d0*L)
    Anorm(1)=rho_norm/rhobar
    Anorm(2)=1.d0/C
    Anorm(3)=1.d0/L
    Anorm(4)=A-F**2/C
    Anorm(5)=F/C
    Anorm(6)=N
!
    do i=1,dim1
       select case(i)
       case (1)
          jj=2
       case (2)
          jj=5
       case (3)
          jj=4
       case (4)
          jj=3
       case (5)
          jj=6
       case(6)
          jj=1
       end select
       write(name,'("paramf",i1)') jj
       open(11,file=name) 
       do nn=1,Nmax
          write(11,*) nn,coef(i,1,nn)/Anorm(jj)
       enddo
       close(11)
    enddo
!-----------------------------------------------------------------------------
  end subroutine dump_mode_spectra
!-----------------------------------------------------------------------------  
!----------------------------------------------------------------------
  real*8 function wtcoef_l(f,f1,f2,f3,f4)
!----------------------------------------------------------------------
    use def_gparam
    implicit none
!
    real(DP), intent(in) ::  f,f1,f2,f3,f4
!
    if (f3.gt.f4) stop 'wtcoefcoef: f3>f4 '
    if (f1.gt.f2) stop 'wtcoefcoef: f1>f2 '
    if (f.le.f3.and.f.ge.f2) then
       wtcoef_l=1.0_DP
    else if (f.gt.f4.or.f.lt.f1 ) then
       wtcoef_l=0.0_DP
    else if (f.gt.f3.and.f.le.f4) then
       wtcoef_l=0.5_DP*(1.0+cos(pi*(f-f3)/(f4-f3)))
    else if (f.ge.f1.and.f.lt.f2) then
       wtcoef_l=0.5_DP*(1.0+cos(pi*(f-f2)/(f2-f1)))
    endif
!----------------------------------------------------------------------
  end function wtcoef_l
!-----------------------------------------------------------------------------
!----------------------------------------------------------------------------------
end module mode_filter
!----------------------------------------------------------------------------------
