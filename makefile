include flags.mk

all: minosy nms_ generate_prem get_fctp hmodel_ #homo_

minosy:
	cd yannos; $(MAKE) minosy

yannos_MPI:
	cd yannos; $(MAKE) yannos_MPI

nms_:
	cd nms; $(MAKE) all

generate_prem:
	cd prem; $(MAKE) all

get_fctp:
	cd fctp; $(MAKE) all

homo_:
	cd homo; $(MAKE) all

hmodel_:
	cd hmodel; $(MAKE) all

clean:
	cd yannos; $(MAKE) clean_minosy; $(MAKE) clean_yannos_MPI
	cd hmodel; $(MAKE) clean
	cd homo; $(MAKE) clean
	cd prem; $(MAKE) clean
	cd fctp; $(MAKE) clean
	cd nms; $(MAKE) clean
