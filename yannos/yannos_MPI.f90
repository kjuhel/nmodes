!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                                                  !
!Author (partial) : Yann Capdeville                                                !
!                                                                                  !
!Contact: yann.capdeville at  univ-nantes.fr                                       !
!                                                                                  !
!This program is free software: you can redistribute it and/or modify it under     !
!the terms of the GNU General Public License as published by the Free Software     !
!Foundation, either version 3 of the License, or (at your option) any later        !
!version.                                                                          !
!                                                                                  !
!This program is distributed in the hope that it will be useful, but WITHOUT ANY   !
!WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A   !
!PARTICULAR PURPOSE. See the GNU General Public License for more details.          !
!                                                                                  !
!You should have received a copy of the GNU General Public License along with      !
!this program. If not, see http://www.gnu.org/licenses/.                           !
!                                                                                  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
program yannos_MPI
!version a moi de minos avec MPI
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  use minos
  use neominos
  use bits, only: l,eps,cond_limite,wgrav,nord,epsint,epsint_base
  use rindex, only: n
  use def_gparam
  use earth_modele
  use yannos_flag
  use layer
  use correction
  implicit none
  include 'mpif.h'
  integer, dimension(MPI_STATUS_SIZE) :: istat
  character(len=100) :: modele_name,fichier_per,prefix,fichierdirect &
                      ,fichierinfo,fichierlost,ext,prefix_keep,fichier_per_keep
  integer  :: type_fctp_in,type_fctp,lmin,lmax,nvec,ieigen,len,iinfo,iper,nbran,lmax_p &
              ,dl,ldeb,i,nmin,ilost,ier,rang,nbproc,j,indicerec,unit,nnn  &
              ,lll,k,nzz,itag,len_int,len_real,tdeb,tfin
  real(DP) :: epsin,wgravin,fmin,fmax,fdeb,ra,bid,gc,ww,qq,wdiff,WMHZ,TCOM,rn_,vn2_,rhobar_
  type(modele):: modele_de_terre
  logical :: flag,flag_deb,first,termine
  doubleprecision, dimension(:,:), allocatable :: bufoutinfo
  doubleprecision, dimension(:,:), allocatable :: bufout
  integer, dimension(:,:), allocatable :: ibufout
  logical :: fflag
!
  real(DP), dimension(:,:), allocatable :: f
  integer , dimension(:)  , allocatable :: n_zero
!
  call MPI_INIT(ier)
  call MPI_COMM_SIZE(MPI_COMM_WORLD,nbproc,ier)
  call MPI_COMM_RANK(MPI_COMM_WORLD,rang  ,ier)  
  if (rang==0)  print*,'Yannos_MPI est une version de minos faites par Y.Capdeville (140999)'
!lecture des entrees:
  unit=117
  open(unit,file='yannos.dat',status='old')
!lecture des entrees:
!  print*,'Entrer le nom du modele:'
  read(unit,*)
  read(unit,100) modele_name
!  print*,'Entrer le nom du fichier de sortie:'
  read(unit,*)
  read(unit,100) fichier_per_keep
!  print*,'Entrer le prefix des fichiers de fonctions propres'
  read(unit,*)
  read(unit,100) prefix_keep
!  print*,'entrer le type de fctp (2 pour t 3 pour s):'
  read(unit,*)
  read(unit,*) type_fctp_in
  if (type_fctp_in/=2.and.type_fctp_in/=0.and.type_fctp_in/=3) stop '2,  3 ou 0 qu''on t''dit!'
!  print*,'enter eps,eps integration and wgrav'
  read(unit,*)
  read(unit,*) epsin,epsint_base,wgravin
!  print*,'enter lmin,lmax,fmin*1000,fmax*1000,nbran'
  read(unit,*)
  read(unit,*) lmin,lmax,fmin,fmax,nbran
  read(unit,*) 
  call read_layers(unit)
  read(unit,*) 
  call read_flags(unit)
  if (wgravin>0.d0 .and. cancel_gravity.and.rang==0) then
     print*,' cancel_gravity requested, I''m setting wgrav to 0!'
     wgravin=0.
  endif
  close(unit)
  if (fmin>fmax) stop 'fmin>fmax !'
  if (lmin>lmax) stop 'lmin>lmax !'
  if (nbran<=0) stop 'Nombre de branche demandee <= 0 !'
!
!bord libre
  cond_limite=1
!
  fmin=fmin/1000._DP
  fmax=fmax/1000._DP
  if (type_fctp_in/=0) then
     tdeb=type_fctp_in; tfin=type_fctp_in
  else
     tdeb=2; tfin=3
  endif
  do type_fctp=tdeb, tfin
     if (rang==0) then
        print*,'*********************************************************************'
        if (type_fctp==2) then
           print*,'             Working on Toroidal modes            '
        else
           print*,'             Working on Spheroidal modes            '
        endif
        print*,'*********************************************************************'
     endif
!
! lecture du modele de terre
!
  call read_modele(modele_name,modele_de_terre)
  call model(modele_de_terre,rn_,vn2_,rhobar_)
  call init_layer(modele_de_terre)
  if (layer_corr) call init_correction(modele_de_terre,rn_,vn2_,rhobar_)
  call MPI_BARRIER(MPI_COMM_WORLD,ier)  
  if (rang==0) then
     print*,'++++++++++++++++++++++++LAYER++++++++++++++++++++++++'
     do i=1,nb_lay
        print*,'Couche:',i
        print*,'Indice debut:',i_la1(i)
        print*,'Indice fin  :',i_la2(i)        
     enddo
     print*,'+++++++++++++++++++++++++++++++++++++++++++++++++++++'
  endif
  call MPI_BARRIER(MPI_COMM_WORLD,ier)
!
!ouverture des fichiers sorties
!
  ieigen=31
  iinfo =32
  iper  =33
  ilost =34
  ext=' '
  ext='.lost'
  fichier_per=fichier_per_keep
  prefix     =prefix_keep
  if (type_fctp_in/=0) then
     call extension(fichier_per,fichierlost,'.lost ')
     call extension(prefix,fichierdirect,'.direct ')
     call extension(prefix,fichierinfo,'.info ')
  else
     if (type_fctp==3) then
        call extension(fichier_per_keep,fichier_per,'S ')
        call extension(fichier_per,fichierlost,'.lost ')
        call extension(prefix,fichierdirect,'S.direct ')
        call extension(prefix,fichierinfo,'S.info ')        
     else   if (type_fctp==2) then
        call extension(fichier_per_keep,fichier_per,'T ')
        call extension(fichier_per,fichierlost,'.lost ')
        call extension(prefix,fichierdirect,'T.direct ')
        call extension(prefix,fichierinfo,'T.info ')        
     endif
  endif
  if (type_fctp.eq.3) then
!     nvec=6*n
     nvec=6*nbcou_lay
  else if (type_fctp.eq.2) then
!     nvec=2*n
     nvec=2*nbcou_lay
  endif
  inquire(iolength=len_int) i
  inquire(iolength=len_real)ww
!        len=8+3*8+nvec*4
  len=2*len_int+3*len_real+nvec*len_real
!  len=8+3*8+nvec*4
  if (rang==0) then
     open(iper,file=fichier_per,status='replace')
     open(ieigen,file=fichierdirect,access='direct',recl=len,status='replace')
     ra=modele_de_terre%r(modele_de_terre%nbcou)
!     write(ieigen,REC=1) len,-1,-1,modele_de_terre%nbceau,modele_de_terre%nbcou &
!          ,(real(modele_de_terre%r(i)/ra,SP),i=1,modele_de_terre%nbcou)        
!the 4 correspond to version 0.4 for check
     write(ieigen,REC=1) len,-1,-1,modele_de_terre%nbceau,nbcou_lay &
          ,((real(modele_de_terre%r(i)/ra,SP),i=i_la1(j),i_la2(j)),j=1,nb_lay) &
          ,4,ra
     open(iinfo,file=fichierinfo,status='replace')
     open(ilost,file=fichierlost,status='replace')
     WRITE(iper,111) real(EPSint_base),real(EPS),real(WGRAV)
111  FORMAT(/,'INTEGRATION PRECISION =',G12.4,'  ROOT PRECISION =',  &
          G12.4,'  GRAVITY CUT OFF =',G12.4,' RAD/S',///,6x,'MODE',      &
          8x,'W(RAD/S)',7x,'W(MHZ)',10x,'T(SECS)',6x,'GRP VEL(KM/S)',    &
          8x,'Q',13x,'RAYLQUO',/)
    write(iper,*)'==============================================================================='
    write(iper,*)'Flag selectionne pour ce run (yannos_flag.f90):'
    if (force_fmin) then
       write(iper,*)'La recherche commence toujours a fmin (pas standard)'
    else
       write(iper,*)'La recherche commence a un fmin flottant (standard)'
    endif
    if (cancel_gravity) then
       write(iper,*)'La gravite a ete desactivee totalement (pas standard)'
    else
       write(iper,*)'Les termes de gravite (pas de redistribution) sont actives (standard) '
    endif
    if (never_use_startlevel) then
       write(iper,*)'Startlevel n''est jamais utilise (pas standard, mais conseille pour les test homogenes)'
    else
       write(iper,*)'Startlevel est utilise a partir de l=',l_startlevel,' (standard)'
    endif
    if (use_tref) then
       write(iper,*)'Utilisation de la periode de reference pour calculer les params elastics'
    else
       write(iper,*)'La periode de reference pour calculer les params elastics est descativee'
    endif
    if (check_modes) then
       write(iper,*)'check_modes active. (Pas standart: vire les modes de la graine)'
    else
       write(iper,*)'check_modes desactive. (standard)'
    endif
    if (use_remedy) then
       write(iper,*)'Utilisation de remdy (standard)'
    else
       write(iper,*)'Desactivation de remdy (standard)'
    endif
    if (rescue) then
       write(iper,*)'Rescue active: recherche systematique des modes perdus'
    else
       write(iper,*)'Rescue desactive'
    endif
    if (keep_bad_modes) then
       print*,'On garde les modes mal calcules (pas une bonne idee ca!)'
    else 
       print*,'On jete les modes mal calcule!'
    endif
    if (force_systemic_search) then
       write(iper,*)' ATTENTION, La recherche systematique a ete activee pour les toroidaux!!!!!!'
       print*,' ATTENTION, La recherche systematique a ete activee pour les toroidaux!!!!!!'
    endif
    write(iper,*)'Format de sortie: ',modout_format
    write(iper,*)'==============================================================================='

  endif  
!
!c'est parti...
!
  if (modout_format/='ipg') stop 'Le format de sortie doit etre ''ipg'' &
                                  & pour la version MPI'
  eps=epsin
  epsint=epsint_base
  wgrav=wgravin
!dans minos:
  call steps(epsint_base)
  if (type_fctp==2) then
     lmin=min(lmin,1)     
  endif
  ldeb=lmin+rang
  dl  =nbproc
  do while(mod(Lmax-lmin+1,dl)/=0)
     Lmax=Lmax+1
  enddo
  termine=.false.
  allocate(f(nbran,lmin:lmax))
  allocate(n_zero(lmin:lmax))
  f(:,:)=0.0_DP
  n_zero(:)=0
  flag_deb=.true.
  lmin=ldeb
  indicerec=1
!=============================
  do l=lmin,lmax,dl
!=============================
     if (never_use_startlevel) then
        use_startlevel=.false.
     else
        if (l>l_startlevel) then
           use_startlevel=.true.
        else
           use_startlevel=.false.
        endif
     endif
     if (l.gt.ldeb) then
        if (flag_deb.or.n_zero(l-dl).ne.0     &
             .or.n_zero(l-dl).ne.0) then
           flag=.true.
           lmax_p=l
        else
           flag=.false.
        endif
     else
        flag=.true.
     endif
     call MPI_BARRIER(MPI_COMM_WORLD,ier)
!"""""""""""""""""""""""""""""""""
     if (.not.termine) then
!"""""""""""""""""""""""""""""""""
     if (flag) then
        if ( l > max(ldeb+dl,1)) then
           if (f(1,l-dl) > 1.0E-12_DP.and.nmin==0) &
                     fdeb=max(fmin,f(1,l-dl)-2.E-5_DP)
           fdeb=max(fdeb,3000.*(l+0.5)/8./atan(1.d0)/6371000.)
           first  =.false.
        else
           fdeb=max(fmin ,3000.*(l+0.5)/8./atan(1.d0)/6371000.)
           first  =.true.
        endif
        if (force_fmin) fdeb=fmin 
     endif
     if (type_fctp==3) then
        call get_fp_modele_R(l,nbran,fdeb,fmax,f(:,l),n_zero(l),first,nmin)
     else
        call get_fp_modele_L(l,nbran,fdeb,fmax,f(:,l),n_zero(l),first,nmin)
     endif
!"""""""""""""""""""""""""""""""""
  else
!"""""""""""""""""""""""""""""""""
     n_zero(l)=0
!"""""""""""""""""""""""""""""""""
  endif
!"""""""""""""""""""""""""""""""""     
     if (n_zero(l)>0) then
        allocate(bufout(nvec,n_zero(l)),ibufout(2,n_zero(l)) &
             ,bufoutinfo(5,n_zero(l)))
        do i=1,n_zero(l)
           nord=i-1+nmin
           if ((f(i,l)+2._DP)>1.E-15_DP.and.f(i,l)>1.E-15_DP) then
              if (type_fctp==3) then
                 call get_fctpR(l,f(i,l),ibufout(:,i),bufoutinfo(:,i),bufout(:,i))
              else
                 call get_fctpL(l,f(i,l),ibufout(:,i),bufoutinfo(:,i),bufout(:,i))
              endif
           endif
        enddo
     else if (.not.termine) then
        if (l>ldeb) then
           termine=.true.
           write(*,257) rang
        endif
     endif
     itag=100
!pas besoin de send receive pour rang 0
     call MPI_BARRIER(MPI_COMM_WORLD,ier)
     do j=0,nbproc-1 
        if (j/=0) then
           if (j==rang) then
!sending   
              call MPI_SEND(n_zero(l),1,MPI_INTEGER,0,itag,MPI_COMM_WORLD,ier)
              if (n_zero(l)>0) then
                 len=n_zero(l)*2              
                 call MPI_SEND(ibufout,len,MPI_INTEGER,0,itag,MPI_COMM_WORLD,ier)
                 len=n_zero(l)*5
                 call MPI_SEND(bufoutinfo,len,MPI_DOUBLE_PRECISION,0 &
                      ,itag,MPI_COMM_WORLD,ier)
!                 len=n_zero(l)*nvec
!                 call MPI_SEND(bufout,len,MPI_DOUBLE_PRECISION,0 &
!                      ,itag,MPI_COMM_WORLD,ier)
                 do i=1,n_zero(l)
                    len=nvec
                    call MPI_SEND(bufout(:,i),len,MPI_DOUBLE_PRECISION,0 &
                         ,itag,MPI_COMM_WORLD,ier)
                 enddo
              endif
        
           else if (rang==0) then
!receiving
              call MPI_RECV(nzz      ,1,MPI_INTEGER,j,itag,MPI_COMM_WORLD,istat,ier)
              if (nzz>0) then
                 len=nzz*2
                 allocate(bufout(nvec,nzz),ibufout(2,nzz),bufoutinfo(5,nzz),stat=ier)
                 if (ier/=0) STOP 'allocate buffer error!'
                 call MPI_RECV(ibufout,len,MPI_INTEGER,j,itag,MPI_COMM_WORLD,istat,ier)
                 len=nzz*5
                 call MPI_RECV(bufoutinfo,len,MPI_DOUBLE_PRECISION,j &
                      ,itag,MPI_COMM_WORLD,istat,ier)
!!$                 len=nzz*nvec
!!$                 call MPI_RECV(bufout,len,MPI_DOUBLE_PRECISION,j &
!!$                      ,itag,MPI_COMM_WORLD,istat,ier)
                 do i=1,nzz
                 len=nvec
                 call MPI_RECV(bufout(:,i),len,MPI_DOUBLE_PRECISION,j &
                      ,itag,MPI_COMM_WORLD,istat,ier)
                 enddo
              endif
           endif
        else
           nzz=n_zero(l)
        endif
        if (rang==0) then
           if (nzz>0) then
              do i=1,nzz
                 inquire(ieigen,nextrec=indicerec)
                 if (abs(bufoutinfo(5,i)-1.d0) < 1.d-14 ) then
                    fflag=.true.
                 else
                    fflag=.false.
                 endif
                 nnn=ibufout(1,i)
                 lll=ibufout(2,i)
                 ww=bufoutinfo(1,i)
                 qq=bufoutinfo(2,i)
                 gc=bufoutinfo(3,i)
                 wdiff=bufoutinfo(4,i)
                 TCOM=2.D0*PI/ww
                 WMHZ=1000.D0/TCOM
                 if ((abs(wdiff)<seuil_ray.or..not.flag).or.keep_bad_modes) then
                    write(ieigen,rec=indicerec) nnn,lll,ww,qq,gc,(bufout(k,i),k=1,nvec)
                    write(iinfo,*) nnn,lll,ww,qq,gc,indicerec,wdiff,fflag
                    WRITE(iper,200) nnn,'S',lll,ww,WMHZ,TCOM,GC,QQ,WDIFF,flag
                 else
                    write(ilost,200) nnn,'S',lll,ww,WMHZ,TCOM,GC,QQ,WDIFF,flag
                 endif
              enddo
              deallocate(bufout,ibufout,bufoutinfo)
           endif
           call flush(ieigen)
        endif
     enddo
     if (rang/=0.and.nzz>0)  deallocate(bufout,ibufout,bufoutinfo)
!
!=============================
  enddo
!=============================
  if (rang==0) close(iper);close(ieigen);close(iinfo);close(ilost)
  deallocate(f,n_zero)
  call clean_minos_memory
  enddo
  call MPI_FINALIZE(ier)
!
100 format(a100)
101 format(i5,1x,a1,1x,i4,' indeterminee, hors precision')
200 FORMAT(I5,A2,I5,6G16.7,l7)
257 format('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!',/, &
           'Le rang ',i3,' a termin� son travail.',/,   &
           '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!' )
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
end program yannos_MPI
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
