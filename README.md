# NMODES
Normal modes computation and summation.

NMODES is widely based on the open-source [modeslib](https://gitlab.univ-nantes.fr/capdeville-y/modeslib) distribution written by
Yann Capdeville (Université de Nantes).

It simulates the propagation of elastic waves in homogeneous or layered
self-gravitating earth models (Takeuchi & Saito, 1972, Woodhouse, 1988).
It also enables to compute synthetics of prompt elasto-gravity signals (PEGS),
as described in [Juhel et al. (2019)](https://doi.org/10.1093/gji/ggy436).

## Generate PREM models

Uses the Fortran file `generate_PREM.f90` or the Python script `generate.PREM.py` to generate PREM-like earth models ([Dziewonski & Anderson, 1981](https://doi.org/10.1016/0031-9201(81)90046-7)), with or without an ocean layer, and with or without attenuation:

![PREM](examples/PREM.png)


## Homogenization

`generate_hmodel` computes (residual) homogenized spherically-symmetric earth models, as described by Capdeville et al. ([2007](https://doi.org/10.1111/j.1365-246X.2007.03462.x), 2008, [2013](https://doi.org/10.1093/gji/ggt102)):

![Homogenization](examples/hmodel.residual.png)


## Normal modes computation

Uses the `minosy` program based on the MINOS program.
An earth model file and `yannos.dat` are needed as input files
(available in the examples/ section).
Once executed, the `minosy` program generates several output files, typically:
* ASCII files `freqS` and `freqT` with informations on
spheroidal and toroidal eigenfrequencies, respectively.
* `fctS.direct` and `fctT.direct` for the complete catalog of spheroidal
and toroidal modes (eigenfrequencies, eigenfunctions, and so on).

Using the `dispersion_diagram.py` script, the dispersion diagram of spheroidal
oscillations for the PREM earth model should look as follows:

![Dispersion diagram](examples/dispersion_diagram.png)

Using the `eigenfunctions.py` script, you shoud then get the following
output for the `U(r)`, `V(r)` and `P(r)` radial eigenfunctions:

![Eigenfunctions](examples/eigenfunctions.png)


## Normal modes summation

Uses the `nms` program (stands for normal modes summation),
updated to compute prompt elasto-gravity signals (PEGS).

Informations on source(s), station(s) and modes are required
and should be indicated in the `sources.dat`, `receivers.dat`
and `nms.dat` input files (available in the examples/ section).

Here is an example of synthetic seismograms computed with `nms`
at Tamanrasset (G.TAM) after the Tohoku earthquake:

![Synthetics](examples/synthetics.png)


## Contact information
* [Kévin Juhel](mailto:kjuhel.pro@gmail.com)
* [Yann Capdeville](mailto:yann.capdeville@univ-nantes.fr)


## References
* Backus G. *Long-wave elastic anisotropy produced by horizontal layering* (1962). DOI: [10.1029/JZ067i011p04427](https://doi.org/10.1029/JZ067i011p04427)

* Takeuchi H. & Saito M. (1972). *Seismic surface waves*, in “Methods in Computational Physics”, ed. by BA Bolt, 11, 217-295.

* Dziewonski A. M. & Anderson D. L. (1981). *Preliminary reference Earth model.* Physics of the earth and planetary interiors, 25(4), 297-356. DOI: [10.1016/0031-9201(81)90046-7](https://doi.org/10.1016/0031-9201(81)90046-7)

* Woodhouse J. H. (1988). *The calculation of the eigenfrequencies and eigenfunctions of the free oscillations of the Earth and Sun.* Seismological Algorithms: Computational Methods and Computer Programs, 321-370.

* Capdeville Y. and Marigo J.-J.  *Second order homogenization of the elastic wave equation for non-periodic layered media* (2007). DOI: [10.1111/j.1365-246X.2007.03462.x](https://doi.org/10.1111/j.1365-246X.2007.03462.x)

* Capdeville Y., Stutzmann E., Wang N. and Montagner J.-P. *Residual homogenization for seismic forward and inverse problems in layered media* (2013). DOI: [10.1093/gji/ggt102](https://doi.org/10.1093/gji/ggt102)

* Capdeville Y., Cupillard P. and Singh S. *An introduction to the two-scale homogenization method for seismology* (2020). DOI: [10.1016/bs.agph.2020.07.001](https://doi.org/10.1016/bs.agph.2020.07.001)

* Juhel K., Montagner J. P., et al. (2019). *Normal mode simulation of prompt elastogravity signals induced by an earthquake rupture.* Geophysical Journal International, 216(2), 935-947. DOI: [10.1093/gji/ggy436](https://doi.org/10.1093/gji/ggy436)