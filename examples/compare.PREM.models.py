#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import subprocess

from matplotlib.ticker import MultipleLocator as ML


# !--------------------------!
# !  read PREM informations  !
# !--------------------------!

filename1 = 'prem'  # './prem.2000.ocean.1.attenuation.1.txt'
filename2 = 'premt' # './prem.2000.ocean.0.attenuation.1.txt'

kw = dict(usecols=(0,1,2,3,6,7), unpack=True, skiprows=3)
radius1, rho1, vpv1, vsv1, vph1, vsh1 = np.loadtxt(filename1, **kw)

kw = dict(usecols=(0,1,2,3,6,7), unpack=True, skiprows=3) #, max_rows=XXXX)
radius2, rho2, vpv2, vsv2, vph2, vsh2 = np.loadtxt(filename2, **kw)

# radius (m) --> radius (km) <--> depth (km)
radius1 = 0.001*radius1
radius2 = 0.001*radius2

depth1  = (6371.0 - radius1)
depth2  = (6371.0 - radius2)

# wave speed (m/s) --> (km/s)
rho1, vpv1, vsv1, vph1, vsh1 = 0.001*rho1, 0.001*vpv1, 0.001*vsv1, 0.001*vph1, 0.001*vsh1
rho2, vpv2, vsv2, vph2, vsh2 = 0.001*rho2, 0.001*vpv2, 0.001*vsv2, 0.001*vph2, 0.001*vsh2


# !-------------------------!
# !  focus on (XX - YY) km  !
# !-------------------------!

zoom = True

if zoom:
    mask1 = (depth1 <= 700.0) * (depth1 >= 0.0)
    mask2 = (depth2 <= 700.0) * (depth2 >= 0.0)

    radius1, depth1 = radius1[mask1], depth1[mask1]
    radius2, depth2 = radius2[mask2], depth2[mask2]

    rho1 = rho1[mask1]
    rho2 = rho2[mask2]

    vpv1, vsv1, vph1, vsh1 = vpv1[mask1], vsv1[mask1], vph1[mask1], vsh1[mask1]
    vpv2, vsv2, vph2, vsh2 = vpv2[mask2], vsv2[mask2], vph2[mask2], vsh2[mask2]


# !------------------------!
# !  set figure (profile)  !
# !------------------------!

fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(6.4, 6.4), sharey=True, constrained_layout=True)

ax1.plot(rho1, depth1, 'C0', lw=2.0, label='model1')
ax1.plot(rho2, depth2, 'C1', lw=1.0, label='model2')

ax2.plot(vpv1, depth1, 'C0', lw=2.0)
ax2.plot(vpv2, depth2, 'C1', lw=1.0)

ax2.plot(vph1, depth1, 'C0', lw=2.0)
ax2.plot(vph2, depth2, 'C1', lw=1.0)

ax2.plot(vsv1, depth1, 'C0', lw=2.0)
ax2.plot(vsv2, depth2, 'C1', lw=1.0)

ax2.plot(vsh1, depth1, 'C0', lw=2.0)
ax2.plot(vsh2, depth2, 'C1', lw=1.0)

ax1.invert_yaxis()
ax2.invert_yaxis()

ax1.set_ylabel('Depth (in km)')

if not zoom:
    for ax in [ax1, ax2]:
        ax.axhspan(5150, 6371, alpha=0.6, facecolor='ivory')
        ax.axhspan(2891, 5150, alpha=0.6, facecolor='gold')
        ax.axhspan(2891,  660, alpha=0.6, facecolor='darkorange')
        ax.axhspan( 660,  220, alpha=0.6, facecolor='peru')
        ax.axhspan( 220,    0, alpha=0.6, facecolor='sienna')

        # set radius layer
        rlayer = np.array([   0.0, 1221.5, 3480.0, 3630.0, 5600.0, 5701.0, 5771.0,
                        5971.0, 6151.0, 6291.0, 6346.6, 6356.0, 6368.0, 6371.0])

        rlayer = np.array([   0.0, 1221.5, 3480.0, 5701.0, 5971.0, 6151.0, 6371.0])
        dlayer = 6371.0 - rlayer

        ax.set_yticks(dlayer)

        ax.xaxis.set_major_locator(ML(3))
        ax.xaxis.set_minor_locator(ML(1))

        ax.set_ylim(6371, -100)

        ax1.text(4, 5800, 'inner core')
        ax1.text(4, 4300, 'outer core')
        ax1.text(7, 1600, 'lower mantle')
        ax1.text(7, 600, 'transition zone')
        ax1.text(7, 180, 'crust/LID/LVZ')

        ax2.text(4.1, 5800, r'$V_s$')
        ax2.text(9.9, 5800, r'$V_p$')
else:
    ax1.set_ylim(depth1.max(), depth1.min())
    ax2.set_ylim(depth2.max(), depth2.min())

ax1.set_xlabel(r'$\rho$ (in 10$^3$ kg/m$^3$)')
ax2.set_xlabel(r'wave speed (in km/s)')

fig.legend(loc='outside upper right', ncol=2)

# save figure into .pdf file
figfile = './compare.PREM.png'
fig.savefig(figfile)

figfile = './compare.PREM.pdf'
fig.savefig(figfile)

subprocess.call(['open', '-a', '/Applications/Skim.app', figfile])
