#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import subprocess

x1, filterwavelet1 = np.loadtxt('./filterwavelet1', unpack=True)
x2, filterwavelet2 = np.loadtxt('./filterwavelet2', unpack=True)

fourier1 = np.fft.rfft(filterwavelet1)
fourier2 = np.fft.rfft(filterwavelet2)

n1, n2 = x1.size, x2.size

sample_rate1, sample_rate2 = 1.0/(x1[1]-x1[0]), 1.0/(x2[1]-x2[0])

k1 = np.fft.rfftfreq(n1, d=1./sample_rate1)
k2 = np.fft.rfftfreq(n2, d=1./sample_rate2)

fig, (ax1, ax2) = plt.subplots(1, 2, constrained_layout=True)

ax1.plot(x1, filterwavelet1)
ax1.plot(x2, filterwavelet2)

ax2.plot(k1, np.abs(fourier1))
ax2.plot(k2, np.abs(fourier2))

ax2.axvline(1.0/160.0, color='C0', lw=1.0, ls='--', alpha=0.5)
ax2.axvline(1.0/320.0, color='C0', lw=1.0, ls='--', alpha=0.5)

ax2.axvline(1.0/500.0, color='C1', lw=1.0, ls='--', alpha=0.5)
ax2.axvline(1.0/900.0, color='C1', lw=1.0, ls='--', alpha=0.5)

ax1.set_xlabel('Space domain, x (in km)')
ax2.set_xlabel('Spatial frequency domain, k (in 1/km)')

ax1.set_title('Filter wavelet')
ax2.set_title('Spectrum norm')

ax2.set_xlim(0.0, 0.008)

figfile = './filterwavelet.pdf'
fig.savefig(figfile)

subprocess.call(['open', '-a', '/Applications/Skim.app', figfile])
#plt.show()
