#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import subprocess

"""
Plot radial eigenfunctions U(r), V(r) and P(r) of a spheroidal
mode, using input files from the get_fctp program.
See equations (8.34) of Dahlen & Tromp (1998) for a definition.
"""

# !-----------------------!
# !  read eigenfunctions  !
# !-----------------------!

# set overtone (n) and angular degree (l)
n, l = 0, 2

# set file name
file = f'./{n:04d}S{l:04d}.ocean.1.attenuation.1.txt'

# read eigenfunctions
zz, uu, vv, pp = np.loadtxt(file, unpack=True, usecols=(0, 1, 3, 5))

# set radius vector
rad = 6371.0 - 0.001*zz


# !--------------!
# !  set figure  !
# !--------------!

fig, axs = plt.subplots(1, 3, sharey=True, constrained_layout=True)

ax1, ax2, ax3 = axs

kw = dict(c='k')
ax1.plot(uu, rad, **kw)
ax2.plot(vv, rad, **kw)
ax3.plot(pp, rad, **kw)

ax1.set(xlabel=f'Eigenfunction $_{n}$U$_{l}$(r)', ylabel='Radius (km)')
ax2.set(xlabel=f'Eigenfunction $_{n}$V$_{l}$(r)')
ax3.set(xlabel=f'Eigenfunction $_{n}$P$_{l}$(r)')

ax1.set(ylim=(0, 6371))

figfile = './eigenfunctions.png'
fig.savefig(figfile)

figfile = './eigenfunctions.pdf'
fig.savefig(figfile)

subprocess.call(['open', '-a', '/Applications/Skim.app', figfile])
#plt.show()
