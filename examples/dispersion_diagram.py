#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import subprocess

from matplotlib.ticker import MultipleLocator as ML

"""
Plot dispersion diagram for the spheroidal or toroidal oscillations of a
homogeneous or layered earth model, using input file freqXXX from yannos
"""

# !-------------------------!
# !  read eigenfrequencies  !
# !-------------------------!

# set file names
file0 = './freq.ocean.1.attenuation.1.S'
file1 = './freq.ocean.1.attenuation.1.S'
file2 = './freq.ocean.1.attenuation.0.S'
file3 = './freq.ocean.0.attenuation.0.S'
file4 = './freq.ocean.1.attenuation.0.S'

# read eigenfrequencies
nn0, ll0, ff0 = np.loadtxt(file0, skiprows=17, unpack=True, usecols=(0, 2, 4))
nn1, ll1, ff1 = np.loadtxt(file1, skiprows=17, unpack=True, usecols=(0, 2, 4))
nn2, ll2, ff2 = np.loadtxt(file2, skiprows=17, unpack=True, usecols=(0, 2, 4))
nn3, ll3, ff3 = np.loadtxt(file3, skiprows=17, unpack=True, usecols=(0, 2, 4))
nn4, ll4, ff4 = np.loadtxt(file4, skiprows=17, unpack=True, usecols=(0, 2, 4))


# !--------------------------!
# !  set dispersion diagram  !
# !--------------------------!

# set figure
fig, ax = plt.subplots(figsize=(6.4, 6.4), constrained_layout=True)

# plot radial-mode (l = 0) eigenfrequencies
ax.plot(ll0[ll0 == 0], ff0[ll0 == 0], ls='',  c='k', marker='o', ms=3)

nmin, nmax = int(nn0.min()), int(nn0.max())

for n in range(nmin, nmax):
    # select a given overtone
    mask0 = (nn0 == n) * (ll0 > 0)
    mask1 = (nn1 == n) * (ll1 > 0)
    mask2 = (nn2 == n) * (ll2 > 0)
    mask3 = (nn3 == n) * (ll3 > 0)
    mask4 = (nn4 == n) * (ll4 > 0)

    if n == nmin:
        # plot eigenfrequencies
        label = 'with ocean and attenuation'
        kw = dict(ls='-', lw=0.5, c='k', marker='o', ms=3, zorder=10)
        ax.plot(ll0[mask0], ff0[mask0], label=label, **kw)

        label1 = 'with ocean and attenuation'
        label3 = 'with ocean, no attenuation'
        label4 = 'no ocean, no attenuation'
        label4 = 'with ocean, no attenuation'

        kw = dict(c='none', marker='o', s=8, zorder=20)
        #ax.scatter(ll1[mask1], ff1[mask1], edgecolors='C0', label=label1, **kw)
        #ax.scatter(ll2[mask2], ff2[mask2], edgecolors='C1', label=label2, **kw)
        #ax.scatter(ll3[mask3], ff3[mask3], edgecolors='C2', label=label3, **kw)
        ax.scatter(ll4[mask4], ff4[mask4], edgecolors='C3', label=label4, **kw)
    else:
        # plot eigenfrequencies
        kw = dict(ls='-', lw=0.5, c='k', marker='o', ms=3, zorder=10)
        ax.plot(ll0[mask0], ff0[mask0], **kw)

        kw = dict(c='none', marker='o', s=8, zorder=20)
        #ax.scatter(ll1[mask1], ff1[mask1], edgecolors='C0', **kw)
        #ax.scatter(ll2[mask2], ff2[mask2], edgecolors='C1', **kw)
        #ax.scatter(ll3[mask3], ff3[mask3], edgecolors='C2', **kw)
        ax.scatter(ll4[mask4], ff4[mask4], edgecolors='C3', **kw)

ax.set(xlabel='Angular degree (l)', ylabel='Eigenfrequency (mHz)')

if True:
    ax.set(xlim=(-1, 50), ylim=(0, 10))

    ax.xaxis.set_major_locator(ML(5))
    ax.xaxis.set_minor_locator(ML(1))

    ax.yaxis.set_major_locator(ML(1))
    ax.yaxis.set_minor_locator(ML(1))

fig.legend(loc='outside upper right', ncol=2)

figfile = './dispersion_diagram.png'
fig.savefig(figfile)

figfile = './dispersion_diagram.pdf'
fig.savefig(figfile)

subprocess.call(['open', '-a', '/Applications/Skim.app', figfile])
#plt.show()
