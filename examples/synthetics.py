#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import subprocess

from matplotlib.ticker import MultipleLocator as ML

from scipy.signal import iirfilter, sosfilt
from scipy.interpolate import interp1d


# !-----------------!
# !  set functions  !
# !-----------------!

def stf_linear(m0, time, hdur):
    """ compute moment-rate STF with a triangular shape,
    for a given seismic moment M0 and half-duration """

    dur = 2.0*hdur

    mask1 = (time >= 0) * (time <= hdur)
    mask2 = (time > hdur) * (time <= dur)

    stf = np.zeros_like(time)

    stf[mask1] = +time[mask1]/hdur**2
    stf[mask2] = -time[mask2]/hdur**2 + 2.0/hdur

    stf = m0 * stf

    return stf


def annotate_subplot(ax, text, loc='upper right', size=10):
    """ Draw text box anchored to one corner of the figure """

    from matplotlib.offsetbox import AnchoredText

    at = AnchoredText(text, loc=loc, prop=dict(size=size),
                      bbox_transform=ax.transAxes)

    at.patch.set_boxstyle("round, pad=0., rounding_size=0.2")
    at.zorder = 99

    ax.add_artist(at)


# !------------------------------------------------!
# !  read and process seismograms, and set figure  !
# !------------------------------------------------!

# set sampling rate
fs = 10.0

# set bandpass filter
sos1 = iirfilter(6, 0.030/(0.5*fs), ftype='butter', output='sos', btype='low')
sos2 = iirfilter(2, 0.002/(0.5*fs), ftype='butter', output='sos', btype='high')

# set source half-duration
half, full = 70.0, 140.0

# set time vector
time = np.linspace(0.0, full, int(full*fs)+1)

# set moment rate source time function
stf = stf_linear(1.0, time, half)

# set unit type
unit = 'VEL'

# set station
station = 'G.TAM'

# set source
source = 'THKU..GCMT.MT'

# set components
component = ['Z', 'N', 'E']

# set figure and axes
fig, axs = plt.subplots(3, figsize=(6.4, 6.4), sharex=True, constrained_layout=True)

ax1, ax2, ax3 = axs[0], axs[1], axs[2]

for (ax, comp) in zip(axs, component):
    file1 = f'./{station}.{unit}.{comp}.{source}.ocean.1.attenuation.0.txt'
    file2 = f'./{station}.{unit}.{comp}.{source}.ocean.1.attenuation.1.txt'

    time, trace1 = np.loadtxt(file1, unpack=True)
    time, trace2 = np.loadtxt(file2, unpack=True)

    # interpolate traces
    func1 = interp1d(time, trace1, kind='cubic')
    func2 = interp1d(time, trace2, kind='cubic')

    time = np.linspace(time[0], time[-1], 10*(time.size-1)+1)

    trace1 = func1(time)
    trace2 = func2(time)

    # convolve with STF
    trace1 = np.convolve(trace1, stf)[:trace1.size] / fs
    trace2 = np.convolve(trace2, stf)[:trace2.size] / fs

    # bandpass filtering
    trace1 = sosfilt(sos1, trace1)
    trace2 = sosfilt(sos1, trace2)

    trace1 = sosfilt(sos2, trace1)
    trace2 = sosfilt(sos2, trace2)

    # seconds --> hours
    time /= 3600.0

    if comp == 'Z':
        ax.plot(time, trace1, 'k', lw=1.2, label='no attenuation')
        ax.plot(time, trace2, 'r', lw=1.2, label='attenuation')
    else:
        ax.plot(time, trace1, 'k', lw=1.2)
        ax.plot(time, trace2, 'r', lw=1.2)

    #ax.set_xlim(time[0], time[-1])
    ax.set_xlim(0, 6)

    if unit == 'DIS':
        ax.set_ylabel('Displacement (m)')
    elif unit == 'VEL':
        ax.set_ylabel('Velocity (m/s)')
    elif unit == 'ACC':
        ax.set_ylabel('Acceleration (m/s/s)')
    else:
        print('Wrong input unit!')

    annotate_subplot(ax, comp, loc='upper left')

    ax.yaxis.set_major_locator(ML(0.0005))
    ax.yaxis.set_minor_locator(ML(0.0001))

    ax.xaxis.set_major_locator(ML(1.00))
    ax.xaxis.set_minor_locator(ML(0.25))

fig.legend(ncol=2, loc='outside upper right')
ax3.set_xlabel('Time (in hours)')

fig.align_labels()

figfile = './synthetics.png'
fig.savefig(figfile)

figfile = './synthetics.pdf'
fig.savefig(figfile)

subprocess.call(['open', '-a', '/Applications/Skim.app', figfile])
#plt.show()
