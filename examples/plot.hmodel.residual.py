#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import subprocess
import sys

from scipy.interpolate import interp1d


# !------------------!
# !  read PREM info  !
# !------------------!

kw = dict(usecols=(0,1,2,3), unpack=True, skiprows=3)

prem0_radius, prem0_rho, prem0_vp, prem0_vs = np.loadtxt('./prem', max_rows=1380, **kw)
premt_radius, premt_rho, premt_vp, premt_vs = np.loadtxt('./premt', max_rows=1351, **kw)
premf_radius, premf_rho, premf_vp, premf_vs = np.loadtxt('./premf', max_rows=1386, **kw)

prem0_rho, prem0_vp, prem0_vs = 0.001*prem0_rho, 0.001*prem0_vp, 0.001*prem0_vs
premt_rho, premt_vp, premt_vs = 0.001*premt_rho, 0.001*premt_vp, 0.001*premt_vs
premf_rho, premf_vp, premf_vs = 0.001*premf_rho, 0.001*premf_vp, 0.001*premf_vs

# radius (m) --> depth (km)
prem0_radius = 0.001*prem0_radius
premt_radius = 0.001*premt_radius
premf_radius = 0.001*premf_radius

prem0_depth = (6371.0 - prem0_radius)
premt_depth = (6371.0 - premt_radius)
premf_depth = (6371.0 - premf_radius)

mask0 = (prem0_depth >= 0.0) * (prem0_depth <= 700.0)
maskt = (premt_depth >= 0.0) * (premt_depth <= 700.0)
maskf = (premf_depth >= 0.0) * (premf_depth <= 700.0)

prem0_depth, prem0_radius = prem0_depth[mask0], prem0_radius[mask0]
premt_depth, premt_radius = premt_depth[maskt], premt_radius[maskt]
premf_depth, premf_radius = premf_depth[maskf], premf_radius[maskf]

prem0_vp, prem0_vs, prem0_rho = prem0_vp[mask0], prem0_vs[mask0], prem0_rho[mask0]
premt_vp, premt_vs, premt_rho = premt_vp[maskt], premt_vs[maskt], premt_rho[maskt]
premf_vp, premf_vs, premf_rho = premf_vp[maskf], premf_vs[maskf], premf_rho[maskf]


# !---------------------------!
# !  interpolate (if needed)  !
# !---------------------------!

#depth = np.unique(np.sort(np.append(prem0_depth, premf_depth)))
#
#func0_vp  = interp1d(prem0_depth, prem0_vp)
#func0_vs  = interp1d(prem0_depth, prem0_vs)
#func0_rho = interp1d(prem0_depth, prem0_rho)
#
#funct_vp  = interp1d(premt_depth, premt_vp)
#funct_vs  = interp1d(premt_depth, premt_vs)
#funct_rho = interp1d(premt_depth, premt_rho)
#
#funcf_vp  = interp1d(premf_depth, premf_vp)
#funcf_vs  = interp1d(premf_depth, premf_vs)
#funcf_rho = interp1d(premf_depth, premf_rho)
#
#vp_0, vs_0, rho_0 = func0_vp(depth), func0_vs(depth), func0_rho(depth)
#vp_t, vs_t, rho_t = funct_vp(depth), funct_vs(depth), funct_rho(depth)
#vp_f, vs_f, rho_f = funcf_vp(depth), funcf_vs(depth), funcf_rho(depth)


# !------------------------!
# !  set figure (profile)  !
# !------------------------!

fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(6.4, 6.4), sharey=True, constrained_layout=True)

label1 = 'reference'
label2 = 'to be homogenized w.r.t. reference'
label3 = 'homogenized'

ax1.plot(prem0_rho, prem0_depth,  'k', lw=2.0)
ax1.plot(premt_rho, premt_depth, 'C0', lw=1.5)
ax1.plot(premf_rho, premf_depth, 'C1', lw=1.5)

ax2.plot(prem0_vs, prem0_depth,  'k', lw=2.0)
ax2.plot(premt_vs, premt_depth, 'C0', lw=1.5)
ax2.plot(premf_vs, premf_depth, 'C1', lw=1.5)

ax3.plot(prem0_vp, prem0_depth,  'k', lw=2.0, label=label1)
ax3.plot(premt_vp, premt_depth, 'C0', lw=1.5, label=label2)
ax3.plot(premf_vp, premf_depth, 'C1', lw=1.5, label=label3)

ax1.invert_yaxis()
ax2.invert_yaxis()
ax3.invert_yaxis()

ax1.set_ylabel('Depth (in km)')

ax1.set_xlabel(r'$\rho$ (in 10$^3$ kg/m$^3$)')
ax2.set_xlabel(r'$V_s$ wave speed (in km/s)')
ax3.set_xlabel(r'$V_p$ wave speed (in km/s)')

fig.legend(loc='outside upper right', ncol=3, fontsize=10)


# !------------------------------!
# !  save figure into .pdf file  !
# !------------------------------!

figfile = './hmodel.residual.png'
fig.savefig(figfile)

figfile = './hmodel.residual.pdf'
fig.savefig(figfile)

subprocess.call(['open', '-a', '/Applications/Skim.app', figfile])
