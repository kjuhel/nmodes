#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import subprocess

from matplotlib.ticker import MultipleLocator as ML


# !------------------!
# !  read PREM info  !
# !------------------!

filename = './prem.2000.ocean.1.attenuation.1.txt'

kw = dict(usecols=(0,1,2,3), unpack=True, skiprows=3)
radius, prem_rho, prem_vp, prem_vs = np.loadtxt(filename, **kw)

# radius (m) --> radius (km) <--> depth (km)
radius = 0.001*radius
depth  = (6371.0 - radius)

# get radius, density, compresionnal and shear wave speeds
prem_rho, prem_vp, prem_vs = 0.001*prem_rho, 0.001*prem_vp, 0.001*prem_vs

# get shear modulus = rigidity (Lamé's second elastic coefficient)
prem_mu = prem_vs**2 * prem_rho

# get Lamé's first elastic coefficient
prem_la = prem_vp**2 * prem_rho - 2*prem_mu

# get incompressibility
prem_k = prem_la + 2.0/3.0*prem_mu


# !------------------------!
# !  set figure (profile)  !
# !------------------------!

fig, axs = plt.subplots(1, 2, figsize=(6.4, 6.4), sharey=True, constrained_layout=True)

ax1, ax2 = axs[0], axs[1]

ax1.plot(prem_vp,  depth, 'w', lw=2.5)
ax1.plot(prem_vs,  depth, 'w', lw=2.5)
ax1.plot(prem_rho, depth, 'w', lw=2.5)

ax2.plot(prem_k,  depth, 'w', lw=2.5)
ax2.plot(prem_mu, depth, 'w', lw=2.5)
ax2.plot(prem_la, depth, 'w', lw=2.5)

ax1.plot(prem_vp, depth, 'C0', lw=1.5, label=r'$V_p$')
ax1.plot(prem_vs, depth, 'C2', lw=1.5, label=r'$V_s$')
ax1.plot(prem_rho, depth, 'k', lw=1.5, label=r'$\rho$')

ax2.plot(prem_k , depth, 'C0', lw=1.6, label=r'incompressibility $\kappa$')
ax2.plot(prem_mu, depth, 'C2', lw=1.6, label=r'rigidity $\mu$')
ax2.plot(prem_la, depth,  'k', lw=1.6, label=r'Lamé 1st parameter $\lambda$')

ax1.invert_yaxis()
ax2.invert_yaxis()

ax1.set_ylabel('Depth (in km)')

for ax in [ax1, ax2]:
    ax.axhspan(5150, 6371, alpha=0.6, facecolor='ivory')
    ax.axhspan(2891, 5150, alpha=0.6, facecolor='gold')
    ax.axhspan(2891,  660, alpha=0.6, facecolor='darkorange')
    ax.axhspan( 660,  220, alpha=0.6, facecolor='peru')
    ax.axhspan( 220,    0, alpha=0.6, facecolor='sienna')

# set radius layer
rlayer = np.array([   0.0, 1221.5, 3480.0, 3630.0, 5600.0, 5701.0, 5771.0,
                   5971.0, 6151.0, 6291.0, 6346.6, 6356.0, 6368.0, 6371.0])

rlayer = np.array([   0.0, 1221.5, 3480.0, 5701.0, 5971.0, 6151.0, 6371.0])
dlayer = 6371.0 - rlayer

ax1.set_yticks(dlayer)

ax1.xaxis.set_major_locator(ML(3))
ax1.xaxis.set_minor_locator(ML(1))

ax2.xaxis.set_major_locator(ML(500))
ax2.xaxis.set_minor_locator(ML(100))

ax1.set_ylim(6371, -100)

ax2.text(550, 5800, 'inner core')
ax2.text(550, 4300, 'outer core')
ax2.text(890, 1600, 'lower mantle')
ax2.text(810, 600, 'transition zone')
ax2.text(800, 180, 'crust/LID/LVZ')

ax1.set_xlabel(r'$\rho$ (in 10$^3$ kg/m$^3$) or wave speed (in km/s)')
ax2.set_xlabel('Elastic parameters (in GPa)')

ax1.legend(loc='lower right', bbox_to_anchor=(1.0, 1.0), ncol=3)
ax2.legend(loc='lower right', bbox_to_anchor=(1.0, 1.0), ncol=1)

# save figure into .pdf file
figfile = './PREM.png'
fig.savefig(figfile)

figfile = './PREM.pdf'
fig.savefig(figfile)

subprocess.call(['open', '-a', '/Applications/Skim.app', figfile])
