module module_model

  implicit none

  type mod_der
    doubleprecision :: vs,vsp,vspp,vp,vpp,vppp,ro,rop,ropp
  end type mod_der


  type modele
    !----------------------------------------------------------------------------!
    ! titre                                                                      !
    ! ifanis, tref, ifdeck                                                       !
    ! nbcou, nic, noc, nbceau                                                    !
    ! r(i), rho(i), vpv(i), vsv(i), qkappa(i), qshear(i), vph(i), vsh(i), eta(i) !
    ! ifanis /= 0 : anisotrope                                                   !
    ! ifdeck                                                                     !
    ! nbcou : nmbre de couches du modele                                         !
    ! nic   : numero de la couche inner core                                     !
    ! noc   : numero de la couche outer core                                     !
    ! nbceau : nombre de couches de l'ocean (0 = pas d'ocean)                    !
    ! champs de dimension (nbcou)                                                !
    ! r   : rayon en km                                                          !
    ! rho : kg/m3                                                                !
    ! vpv, vsv, vph, vsh : km/s                                                  !
    ! qkappa, qshear : attenuation                                               !
    ! eta : F/(A-2L)                                                             !
    !----------------------------------------------------------------------------!
    character(len=80) :: titre
    integer :: ifanis, ifdeck, nbcou, nic, noc, nbceau
    real    :: tref
    type(mod_der) :: der
    doubleprecision, dimension(:), pointer :: r,rho,vpv,vsv,qkappa,qshear,vph,vsh,eta
    logical :: allocated

  end type modele


  contains

    subroutine init_modele(mod)
      implicit none
      type(modele), intent(out) :: mod

      mod%allocated = .false.
      mod%nbceau = -1

    end subroutine init_modele


    subroutine allocate_modele(mod, nbcou)
      implicit none
      type(modele), intent(inout)   :: mod
      integer, optional, intent(in) :: nbcou

      if (present(nbcou)) mod%nbcou = nbcou
      if (mod%nbcou == -1) stop 'allocate_modele : nbre de couches inconnu'
      if (mod%allocated) then
        print *, 'allocate_modele : le modele', mod%titre, ' est deja alloue'
        stop
      end if

      allocate( mod%r(mod%nbcou), mod%rho(mod%nbcou), mod%vpv(mod%nbcou), &
        mod%vsv(mod%nbcou), mod%qkappa(mod%nbcou), mod%qshear(mod%nbcou), &
        mod%vph(mod%nbcou), mod%vsh(mod%nbcou), mod%eta(mod%nbcou) )

      mod%r     (:) = 0.d0
      mod%rho   (:) = 0.d0
      mod%vpv   (:) = 0.d0
      mod%vsv   (:) = 0.d0
      mod%qkappa(:) = 0.d0
      mod%qshear(:) = 0.d0
      mod%vph   (:) = 0.d0
      mod%vsh   (:) = 0.d0
      mod%eta   (:) = 0.d0

      mod%allocated=.true.

    end subroutine allocate_modele


    subroutine deallocate_modele(mod)
      implicit none
      type(modele), intent(inout) :: mod

      if (.not.mod%allocated) then
        print *, 'deallocate_modele : le modele', mod%titre, ' n''est pas alloue??'
        stop
      end if

      deallocate(mod%r, mod%rho, mod%vpv, mod%vsv, &
        mod%qkappa, mod%qshear, mod%vph, mod%vsh, mod%eta)

      mod%allocated = .false.

    end subroutine deallocate_modele


    subroutine read_modele(name, mod)
      implicit none
      character(*), intent(in) :: name
      type(modele), intent(out) :: mod

      integer :: i, unit=127, icode

      call init_modele(mod)

      open(unit, file=name, iostat=icode, status='old', action='read')
      if (icode > 0) then
        print *, 'probleme d''ouverture de ', name
        stop 'read_modele : prob de d''ouverture'
      end if

      read(unit, 100, iostat=icode) mod%titre
      read(unit, *, iostat = icode) mod%ifanis, mod%tref, mod%ifdeck
      read(unit, *, iostat = icode) mod%nbcou, mod%nic, mod%noc, mod%nbceau
      ! read(unit, *, iostat = icode) mod%der%ro, mod%der%vp, mod%der%vs
      ! read(unit, *, iostat = icode) mod%der%rop, mod%der%vpp, mod%der%vsp
      ! read(unit, *, iostat = icode) mod%der%ropp, mod%der%vppp, mod%der%vspp

      call allocate_modele(mod)

      read(unit, 105, iostat=icode) (mod%r(i), mod%rho(i), mod%vpv(i), mod%vsv(i), &
        mod%qkappa(i), mod%qshear(i), mod%vph(i), mod%vsh(i), mod%eta(i), i=1,mod%nbcou)

      if (icode > 0) then
        print *, 'probleme de lecture dans ', name
        stop 'read_modele : prob de de lecture'
      end if

      close(unit)
      100 format(a80)
      105 format(f8.0, 3f9.2, 2f9.1, 2f9.2, f9.5)

    end subroutine read_modele


end module module_model
