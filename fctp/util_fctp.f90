module util_fctp
  use def_gparam
  use module_model
  implicit none
  public :: RA, open_fctp, readS, readT, write_modeT_ascii, write_modeS_ascii
  private
  integer :: LLmaxS, NNmaxS, LLmaxT, NNmaxT, nkS, nkT, nb_discT, nb_discS
  integer, dimension(:,:), allocatable :: irecS, irecT
  integer, dimension(:)  , allocatable :: discS_index, discT_index
  integer, parameter :: unitfctpS=155, unitfctpT=154
  real(DP) :: RA=-1.0d0 ! initialized by util_fctp
  real(DP), parameter :: pi_d=3.141592653589793238462643383279502884197_DP
  real(DP), parameter :: bigg=6.6723e-11_DP ! this was changed from 6.673e-11_dp


  real(DP), dimension(:,:), allocatable :: omtabS, qtabS, gctabS
  real(DP), dimension(:,:), allocatable :: omtabT, qtabT, gctabT
  real(DP), dimension(:), allocatable :: rayS, rayT, discS_rad, discT_rad
  logical :: files_open=.false., files_openS=.false., files_openT=.false.

  type(modele) :: modele1D

  contains

    subroutine open_fctp(fichierfctp, type)
      !-----------------------------------------------------------------!
      ! fichierfctp : character*90, nom du fichier de fonction propre   !
      !                sans extension                                   !
      ! unitfctp   : entier, unite logique du fichier de fctp desiree   !
      !                                                                 !
      ! irec(0:LLmax,0:NNmax) : tableaux d'entier des numeros de record !
      !                      des fonctions propres                      !
      ! omtab(0:LLmax,0:NNmax) : tableau de real*8, w en rad/s          !
      ! qtab(0:LLmax,0:NNmax) : tableau de real*8, attenuations         !
      ! gctab(0:LLmax,0:NNmax) : tableau de real*8, vitesses de groupe  !
      ! nocean : entier, nb de couches de l'ocean                       !
      ! nk : entier, nombre de couches du model                         !
      ! ray(NCM) : tableau de real*4, couches du model                  !
      !-----------------------------------------------------------------!
      implicit none
      character(len=90), intent(in) :: fichierfctp
      character(len=1), intent(in) :: type
      character(len=90) :: fichierdirect, fichierinfo
      integer :: LEN, unitinfo, indicepointeur, n, l, ibid, i, nocean, ier, k, unitfctp, iversion
      real(DP) :: ww, qq, gc, wdiff
      real(SP), dimension(:), allocatable :: ray_tmp, ray_tmp2
      logical :: flag

      unitfctp=118
      unitinfo=73

      ! read spheroidal file
      if (type=='S') then

              call extension(fichierfctp, fichierdirect, '.direct ')
              call extension(fichierfctp, fichierinfo, '.info ')

              inquire(file=fichierdirect,EXIST=flag)
              if (.not.flag) then
                      print *, 'the file', fichierdirect, 'does not exist !'
                      stop
              end if

              inquire(file=fichierinfo,EXIST=flag)
              if (.not.flag) then
                      print *, 'the file', fichierdirect, 'does not exist !'
                      stop
              end if

              call get_len(fichierdirect,LEN)

              open(unitfctpS,file=fichierdirect,status='old',access='direct',recl=LEN)
              open(unitinfo,file=fichierinfo)

              read(unitfctpS,rec=1) ibid,ibid,ibid,nocean,nkS
              allocate(rayS(nkS),ray_tmp(nkS))

              ! reading radius and track for discontinuities
              read(unitfctpS,rec=1) ibid,ibid,ibid,nocean,nkS,(ray_tmp(i),i=1,nkS),iversion,RA
              if (iversion/=4) STOP 'The program expect minosy version >= 0.4 !!'
              ! km SP -> m DP
              rayS(:)=dble(ray_tmp(1:nkS))*RA
              nb_discS=0
              do i=1,nkS-1
                if (abs(rayS(i)-rayS(i+1))<1e-8) nb_discS=nb_discS+1
              end do
              allocate(discS_index(0:nb_discS+1),discS_rad(0:nb_discS+1))
              discS_index(0)=0
              discS_index(nb_discS+1)=nkS
              discS_rad(0)=rayS(1)
              discS_rad(nb_discS+1) =rayS(nkS)
              k=0
              do i=1,nkS-1
                if (abs(rayS(i)-rayS(i+1))<1e-8) then
                        k=k+1
                        discS_index(k)=i
                        discS_rad(k)=rayS(i)
                end if
              end do
              deallocate(ray_tmp)

              ! scanning for LLmax and NNmax:
              LLmaxS=0;NNmaxS=0
              10 read(unitinfo,*,END=99) n,l,ww,qq,gc,indicepointeur,wdiff,flag
              if (l>LLmaxS) LLmaxS=l
              if (n>NNmaxS) NNmaxS=n
              goto 10

              99 continue
              allocate(irecS(0:LLmaxS,0:NNmaxS),omtabS(0:LLmaxS,0:NNmaxS)  &
              ,qtabS(0:LLmaxS,0:NNmaxS),gctabS(0:LLmaxS,0:NNmaxS), stat=ier  )
              irecS(:,:)=0
              if (ier/=0) then
                      print*,'LLmaxS=',LLmaxS
                      print*,'NNmaxS=',NNmaxS
                      print*,(4.+8.*3.)*dble(LLmaxS+1)*dble(NNmaxS+1)/1024./1024.,' Mb'
                      stop 'open_fctp: probleme d''allocation memoire'
              endif
              rewind(unitinfo)
              ! relecture
              11 read(unitinfo,*,END=98) n,l,ww,qq,gc,indicepointeur,wdiff,flag
              irecS (l,n)=indicepointeur
              omtabS(l,n)=ww
              qtabS (l,n)=qq
              gctabS(l,n)=gc
              if ((abs(wdiff).gt.1.d-9.and.flag).or.abs(wdiff).gt.1d-5) irecS(l,n)=0
              goto 11
              98 continue
              close(unitinfo)
              fichierdirect(:)=' '
              fichierinfo(:)=' '
              files_openS=.true.

      ! read toroidal file
      else if (type=='T') then

              call extension(fichierfctp,fichierdirect,'.direct ')
              call extension(fichierfctp,fichierinfo,'.info ')

              inquire(file=fichierdirect,EXIST=flag)
              if (.not.flag) then
                      print *, 'the file', fichierdirect,'does not exist !'
                      stop
              end if

              inquire(file=fichierinfo,EXIST=flag)
              if (.not.flag) then
                      print *, 'the file', fichierdirect,'does not exist !'
                      stop
              end if

              call get_len(fichierdirect,LEN)

              open(unitfctpT,file=fichierdirect,status='old',access='direct',recl=LEN)
              open(unitinfo,file=fichierinfo)

              read(unitfctpT,rec=1)ibid,ibid,ibid,nocean,nkT
              allocate(rayT(nkT),ray_tmp(nkT))

              ! reading radius and track for discontinuities
              read(unitfctpT,rec=1) ibid,ibid,ibid,nocean,nkT,(ray_tmp(i),i=1,nkT),iversion,RA
              if (iversion/=4) STOP 'The program expect minosy version >= 0.4 !!'
              ! km SP -> m DP
              rayT(:)=dble(ray_tmp(1:nkT))*RA
              nb_discT=0
              do i=1,nkT-1
                if (abs(rayT(i)-rayT(i+1))<1e-8)  nb_discT=nb_discT+1
              end do
              allocate(discT_index(0:nb_discT+1),discT_rad(0:nb_discT+1))
              discT_index(0)=0
              discT_index(nb_discT+1)=nkT
              discT_rad(0)  =rayT(1)
              discT_rad(nb_discT+1) =rayT(nkT)
              k=0
              do i=1,nkT-1
                if (abs(rayT(i)-rayT(i+1))<1e-8) then
                        k=k+1
                        discT_index(k)=i
                        discT_rad  (k)=rayT(i)
                endif
              enddo
              deallocate(ray_tmp)

              ! scanning for LLmax and NNmax:
              LLmaxT=0;NNmaxT=0
              110 read(unitinfo,*,END=199) n,l,ww,qq,gc,indicepointeur,wdiff,flag
              if (l>LLmaxT) LLmaxT=l
              if (n>NNmaxT) NNmaxT=n
              goto 110

              199 continue
              allocate(irecT(0:LLmaxT,0:NNmaxT),omtabT(0:LLmaxT,0:NNmaxT))
              allocate(qtabT(0:LLmaxT,0:NNmaxT),gctabT(0:LLmaxT,0:NNmaxT))
              irecT(:,:)=0
              rewind(unitinfo)

              ! relecture
              111 read(unitinfo,*,END=198) n,l,ww,qq,gc,indicepointeur,wdiff,flag
              irecT (l,n)=indicepointeur
              omtabT(l,n)=ww
              qtabT (l,n)=qq
              gctabT(l,n)=gc
              goto 111
              198 continue
              close(unitinfo)
              fichierdirect(:)=' '
              fichierinfo(:)=' '
              files_openT=.true.

      else
              stop 'open_fctp: type must be S or T'
      end if

      files_open=.true.
    end subroutine open_fctp


    subroutine get_len(file,LEN)
      !---------------------------------------------------------!
      ! retourne la longueur des records des fichiers de fctps  !
      ! a acces direct produit par classe T ou classe S         !
      ! INPUT :                                                 !
      !      file : character*90, nom du fichier a acces direct !
      ! OUTPUT :                                                !
      !      LEN : entier, longueur du record                   !
      !---------------------------------------------------------!
      implicit none
      character(len=90) :: file
      integer :: LEN

      open(22,file=file,status='old',access='direct',RECL=4)
      read(22,rec=1) len
      close(22)

    end subroutine get_len


    subroutine write_modeT_ascii(unit, n, l, modelfile)
      !--------------------------------------------------!
      ! write toroidal eigenfunctions into ascii files   !
      !--------------------------------------------------!
      implicit none
      integer, intent(in) :: unit, n, l
      character(len=90), intent(in) :: modelfile

      real(DP), dimension(3,nkT) :: xdum ! W, dW, T
      real(DP), parameter :: Rt=6371000.
      integer :: i, j, iflag

      if (.not.files_openT) stop 'write_modeT_ascii: use open_fctp with T type first'

      call readT(n, l, xdum, iflag, modelfile)

      do i=1,nkT
         write(unit,*) (Rt-rayT(i)), (sqrt(float(l*(l+1)))*xdum(j,i), j=1,3)
      end do

    end subroutine write_modeT_ascii


    subroutine write_modeS_ascii(unit, n, l, flag_pint, modin_fmt, modelfile)
      !--------------------------------------------------!
      ! write spheroidal eigenfunctions into ascii files !
      !--------------------------------------------------!
      implicit none
      integer, intent(in) :: unit, n, l
      logical, intent(in) :: flag_pint
      character(len=3), intent(in) :: modin_fmt
      character(len=90), intent(in) :: modelfile

      real(DP), dimension(8,nkS) :: xdum ! U, dU, V, dV, P, dP, R, S
      real(DP), parameter :: Rt=6371000.
      integer :: i, j, iflag

      if (.not.files_openS) stop 'write_modeS_ascii: use open_fctp with S type first'

      call readS(n, l, xdum, iflag, flag_pint, modin_fmt, modelfile)

      do i = 1,nkS
         write(unit,*) (Rt-rayS(i)), (xdum(j,i), j=1,2), (sqrt(float(l*(l+1)))*xdum(j,i), j=3,4), &
           (xdum(j,i), j=5,6), (sqrt(float(l*(l+1)))*xdum(j,i), j=7,8)
      end do

    end subroutine write_modeS_ascii


    subroutine readT(n, l, xdum, iflag, modelfile)
      !----------------------------------------------!
      ! reads eigen functions (n,l) after open_fctp  !
      ! opened direct access file                    !
      !----------------------------------------------!
      use def_gparam
      implicit none
      integer, intent(in) :: l, n
      character(len=90), intent(in) :: modelfile
      integer, intent(out):: iflag
      real(DP), dimension(3,nkT), intent(out) ::  xdum

      integer ::  i, nn, ll, nvec, nb
      real(DP) :: WW, QQ, GC
      real(DP), dimension(nkT*6) :: tt

      real(DP) :: kk, rho_norm, vel_norm
      real(DP), dimension(nkT) :: rho, rad, vsv, mu
      real(DP), dimension(nkT) :: egw, degw, egt

      if (.not.files_open) stop 'readT: call open_fctp first!'

      ! nvec est la longueur necessaire pour atteindre
      ! la profondeur desiree pour w et dw :
      nb=nkT
      nvec=int(nb*2)

      if (l<=LLmaxT.and.n<=NNmaxT) then

              if (irecT(l,n)/=0) then
                      read(unitfctpT,rec=irecT(l,n)) nn,ll,WW,QQ,GC,(tt(I),I=1,nvec)

                      do i=1,nkT
                        xdum(1,i)=dble(tt(2*(nkT-i)+1)) ! read eigenfunction W(r)
                        xdum(2,i)=dble(tt(2*(nkT-i)+2)) ! read eigenfunction dW(r)
                      end do

                      iflag=0
              else
                      iflag=2
                      xdum(:,:)=0.0_DP
              end if

      else
              iflag=1
      end if

      !----------------------------!
      ! compute eigenfunction T(r) !
      !----------------------------!

      egw = xdum(1,:); degw = xdum(2,:)
      egt(:) = 0.0_DP

      ! read the model properties
      call read_modele(modelfile, modele1D)

      rad = rayT(:) / RA

      kk = dble(l*(l+1))

      rho_norm = 5515.0_DP
      vel_norm = RA*sqrt(pi_d*bigg*rho_norm)

      rho = modele1D%rho / rho_norm
      vsv = modele1D%vsv / vel_norm

      mu = rho*vsv**2

      egt = mu*(degw - egw/rad)

      xdum(3,:) = egt(:)


    end subroutine readT


    subroutine readS(n, l, xdum, iflag, flag_pint, modin_fmt, modelfile)
      !-------------------------------------------------------------------!
      ! reads eigen functions (n,l) after open_fctp opened access file    !
      !-------------------------------------------------------------------!
      !-------------------------------------------------------------------!
      ! INPUT :                                                           !
      !    ifd : entier, unite logique du fichier a acces direct          !
      !    irec(0:LLmax,0:NNmax) : tableau des numeros de records         !
      !                          sortie de open_fctp                      !
      !    n et l : entiers                                               !
      !    nk entier : nombre de couches du model                         !
      !    np entier : profondeur jusqu'a laquelle on veut lire ls fctps  !
      !                 (de np a nk=surface)                              !
      ! OUTPUT :                                                          !
      !    xdum1..6(NCM) : fonction propres et derivees                   !
      !    iflag : 1 pas de pb                                            !
      !            0 fin du fichier (ne devrait jamais arriver)           !
      !           -1 erreur a la lecture                                  !
      !           -2 l ou n depasse LLmax ou NNmax                        !
      !-------------------------------------------------------------------!
      use def_gparam
      use module_model
      implicit none
      integer, intent(in) :: l, n
      logical, intent(in) :: flag_pint
      character(len=3), intent(in) :: modin_fmt
      character(len=90), intent(in) :: modelfile
      integer, intent(out):: iflag
      real(DP), dimension(8,nkS), intent(out) ::  xdum

      integer :: nvec, i, k, ll, nn, nb, llp1
      real(DP) :: WW, QQ, GC
      real(DP), dimension(nkS*6) :: tt

      real(DP) :: zeta2, pibigg, rho_norm, vel_norm, int1, int2, kk
      real(DP), dimension(nkS-1) :: dr
      real(DP), dimension(nkS) :: r_int, rho, rad, vpv, vsv, lambda, mu
      real(DP), dimension(nkS) :: egu, egv, egp, egr, egs, degu, degv, degp


      if (.not.files_open) stop 'readS: call open_fctp first!'
      nb=nkS

      ! nvec est la longueur necessaire pour atteindre
      ! la profondeur desiree pour u, du, v, dv, p, dp
      nvec=int(nb*6)

      if (l<=LLmaxS.and.n<=NNmaxS) then

              if (irecS(l,n)/=0) then
                      ! reading and killing discontinuities
                      read(unitfctpS,rec=irecS(l,n)) nn,ll,WW,QQ,GC,(tt(I),I=1,nvec)

                      if (modin_fmt == 'uni') then
                        do i=1,nkS
                           xdum(1,i)=dble(tt(6*(nkS-i)+1)) ! read eigenfunction U(r)
                           xdum(2,i)=dble(tt(6*(nkS-i)+2)) ! read eigenfunction dU(r)
                           xdum(3,i)=dble(tt(6*(nkS-i)+3)) ! read eigenfunction V(r)
                           xdum(4,i)=dble(tt(6*(nkS-i)+4)) ! read eigenfunction dV(r)
                           xdum(5,i)=dble(tt(6*(nkS-i)+5)) ! read eigenfunction P(r)
                           xdum(6,i)=dble(tt(6*(nkS-i)+6)) ! read eigenfunction dP(r)
                        end do

                      else if (modin_fmt == 'yns') then
                        do i = 1,nkS
                           xdum(1,i) = dble(tt(4*(nkS-i)+1)) ! read eigenfunction U(r)
                           xdum(2,i) = dble(tt(4*(nkS-i)+2)) ! read eigenfunction dU(r)
                           xdum(3,i) = dble(tt(4*(nkS-i)+3)) ! read eigenfunction V(r)
                           xdum(4,i) = dble(tt(4*(nkS-i)+4)) ! read eigenfunction dV(r)
                        end do
                        do i = 1,nkS
                           xdum(5,i) = dble(tt((nkS-i)+1+4*nkS)) ! read eigenfunction P(r)
                           xdum(6,i) = dble(tt((nkS-i)+1+5*nkS)) ! read eigenfunction dP(r)
                        end do

                      else
                        print *, 'unknown input format for the catalogue !'
                      end if

                      iflag=0
              else
                      iflag=2
                      xdum(:,:)=0.0_DP
              end if

      else
              iflag=1
      end if


      !--------------------------------------!
      ! compute eigenfunctions R(r) and S(r) !
      !--------------------------------------!

      egu = xdum(1,:); degu = xdum(2,:)
      egv = xdum(3,:); degv = xdum(4,:)

      egr(:) = 0.0_DP; egs(:) = 0.0_DP

      ! read the model properties
      call read_modele(modelfile, modele1D)

      rad = rayS(:) / RA

      kk = dble(l*(l+1))

      rho_norm = 5515.0_DP
      vel_norm = RA*sqrt(pi_d*bigg*rho_norm)

      rho = modele1D%rho / rho_norm
      vpv = modele1D%vpv / vel_norm
      vsv = modele1D%vsv / vel_norm

      mu = rho*vsv**2
      lambda = rho*vpv**2 - 2.0_DP*mu

      egr = (lambda+2.0_DP*mu)*degu + lambda*(2.0_DP*egu-kk*egv)/rad
      egs = mu*(degv + (egu-egv)/rad)

      xdum(7,:) = egr(:)
      xdum(8,:) = egs(:)


      !-----------------------------------------------------------!
      ! compute eigenfunctions P_int(r) and dP_int(r) when needed !
      !-----------------------------------------------------------!

      if ((modin_fmt=='yns').and.(flag_pint .eqv. .true.)) then

        egu(:) = xdum(1,:)
        egv(:) = xdum(3,:)

        egp(:)  = 0.0_DP
        degp(:) = 0.0_DP

        ! read the model properties
        call read_modele(modelfile, modele1D)
        rho = modele1D%rho / 5515.0_DP

        r_int = rayS(:) / RA
        dr = 0.0_DP

        do i = 2,nkS
           dr(i-1) = r_int(i) - r_int(i-1)
        end do

        pibigg = 1.0_DP
        llp1 = ll + 1
        zeta2 = real(ll*llp1, DP)

        if (ll == 0) then

          !--------------!
          ! radial modes !
          !--------------!

          do i = 1,nkS-1

             egp(i) = sum((rho(i:nkS-1)*egu(i:nkS-1) + rho(i+1:nkS)*egu(i+1:nkS)) * 0.5_DP*dr(i:nkS-1))
             egp(i) = 4.0_DP*pibigg * egp(i)

             degp(i) = -4.0_DP*pibigg*rho(i)*egu(i)

             ! detection of NaN
             if ( egp(i)*0.0_DP.ne.0.0_DP)  egp(i) = 0.0_DP
             if (degp(i)*0.0_DP.ne.0.0_DP) degp(i) = 0.0_DP

          end do

          egp(nkS)  = 0.0_DP
          degp(nkS) = -4.0_DP*pibigg*rho(nkS)*egu(nkS)

        else

          !------------------!
          ! spheroidal modes !
          !------------------!

          do i = 1,nkS

             int1 = 0.0_DP
             int2 = 0.0_DP

             if (i .gt. 1) then
               int1 = sum((rho(1:i-1) * (ll*egu(1:i-1) + zeta2*egv(1:i-1)) * r_int(1:i-1)**llp1) * 0.5_DP*dr(1:i-1))
               int1 = int1 + sum((rho(2:i) * (ll*egu(2:i) + zeta2*egv(2:i)) * r_int(2:i)**llp1) * 0.5_DP*dr(1:i-1))
             end if

             if (i .lt. nkS) then
               int2 = sum(rho(i:nkS-1) * (-llp1*egu(i:nkS-1) + zeta2*egv(i:nkS-1)) / r_int(i:nkS-1)**ll * 0.5_DP*dr(i:nkS-1))
               int2 = int2 + sum(rho(i+1:nkS) * (-llp1*egu(i+1:nkS) + zeta2*egv(i+1:nkS)) / r_int(i+1:nkS)**ll * 0.5_DP*dr(i:nkS-1))
             end if

             egp(i) = -4.0_DP*pibigg/(2*ll+1) * (int1/r_int(i)**llp1 + int2*r_int(i)**ll)

             degp(i) = -llp1*int1/r_int(i)**(ll+2) + integrand1(i)/r_int(i)**llp1
             degp(i) = degp(i) + ll*r_int(i)**(ll-1)*int2 - integrand2(i)*r_int(i)**ll
             degp(i) = -4.0_DP*pibigg/(2*ll+1) * degp(i)

             ! detection of NaN
             if ( egp(i)*0.0_DP.ne.0.0_DP)  egp(i) = 0.0_DP
             if (degp(i)*0.0_DP.ne.0.0_DP) degp(i) = 0.0_DP

          end do

        end if

        !--------------------------------------!
        ! update P(r) and dP(r) eigenfunctions !
        !--------------------------------------!

        xdum(5,:) =  egp(:)
        xdum(6,:) = degp(:)

      end if

    contains

        double precision function integrand1(idx)
          !---------------------------------------------!
          ! computes first integrand of equ (8.55) from !
          ! Dahlen and Tromp at given radius rr(idx)    !
          !---------------------------------------------!
          implicit none
          integer, intent(in) :: idx
          integrand1 = rho(idx) * (ll*egu(idx) + zeta2*egv(idx)) * r_int(idx)**llp1
        end function integrand1


        double precision function integrand2(idx)
          !----------------------------------------------!
          ! computes second integrand of equ (8.55) from !
          ! Dahlen and Tromp at given radius rr(idx)     !
          !----------------------------------------------!
          implicit none
          integer, intent(in) :: idx
          integrand2 = rho(idx) * (-llp1*egu(idx) + zeta2*egv(idx)) / r_int(idx)**ll
        end function integrand2


    end subroutine readS


    subroutine extension(fichier1, fichier2, ext)
      !-------------------------------------!
      ! adds extension ext to file fichier1 !
      !-------------------------------------!
      implicit none
      character(len=*), intent(in)  :: fichier1, ext
      character(len=*), intent(out) :: fichier2
      integer :: lenfichier1, lenext

      lenfichier1=INDEX (fichier1,' ') -1
      lenext=INDEX (ext,' ') -1
      if ((lenfichier1+lenext).gt.80) then
              print *, 'character ', fichier1, 'is too long !'
      end if

      fichier2(1:lenfichier1)=fichier1(1:lenfichier1)
      fichier2(lenfichier1+1:(lenfichier1+lenext))=ext(1:lenext)
      fichier2(lenfichier1+lenext+1:)=' '
    end subroutine  extension


end module util_fctp
