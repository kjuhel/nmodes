!---------------------------------------------------------------------------------!
! Author (partial) : Yann Capdeville                                              !
!                                                                                 !
! Contact: yann.capdeville at univ-nantes.fr                                      !
!                                                                                 !
! This program is free software: you can redistribute it and/or modify it under   !
! the terms of the GNU General Public License as published by the Free Software   !
! Foundation, either version 3 of the License, or (at your option) any later      !
! version.                                                                        !
!                                                                                 !
! This program is distributed in the hope that it will be useful, but WITHOUT ANY !
! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A !
! PARTICULAR PURPOSE. See the GNU General Public License for more details.        !
!                                                                                 !
! You should have received a copy of the GNU General Public License along with    !
! this program. If not, see http://www.gnu.org/licenses/.                         !
!---------------------------------------------------------------------------------! 

program get_fctp
  use util_fctp
  implicit none

  character(len=90) :: fileTOR, fileSPH, modelfile
  character(len=9)  :: name1, name2
  character(len=4)  :: nstrmin, nstrmax, lstrmin, lstrmax
  character(len=3)  :: modin_fmt
  character(len=1)  :: strflag
  integer :: count, nn, nnmin, nnmax, ll, llmin, llmax, iflag, unit1, unit2
  logical :: flag_pint

  unit1 = 117
  unit2 = 118

  count = command_argument_count()

  if (count == 0) then
    print *, './get_fctp fileTOR fileSPH lmin lmax nmin nmax flag_pint modin_fmt modelfile'
    print *, './get_fctp fileSPH lmin lmax nmin nmax flag_pint modin_fmt modelfile'

  else if (count == 9) then
    call get_command_argument(1, fileTOR)
    call get_command_argument(2, fileSPH)
    call get_command_argument(3, lstrmin)
    call get_command_argument(4, lstrmax)
    call get_command_argument(5, nstrmin)
    call get_command_argument(6, nstrmax)
    call get_command_argument(7, strflag)
    call get_command_argument(8, modin_fmt)
    call get_command_argument(9, modelfile)

  else if (count == 8) then
    call get_command_argument(1, fileSPH)
    call get_command_argument(2, lstrmin)
    call get_command_argument(3, lstrmax)
    call get_command_argument(4, nstrmin)
    call get_command_argument(5, nstrmax)
    call get_command_argument(6, strflag)
    call get_command_argument(7, modin_fmt)
    call get_command_argument(8, modelfile)

  else
    print *, 'not a valid number of input arguments !'
  end if


  read(nstrmin,'(I4)') nnmin
  read(nstrmax,'(I4)') nnmax
  read(lstrmin,'(I4)') llmin
  read(lstrmax,'(I4)') llmax
  read(strflag,'(I1)') iflag

  if (iflag == 0) then
    flag_pint = .false.
  else if (iflag == 1) then
    flag_pint = .true.
  else
    print *, 'flag for numerical integration of P(r) and dP(r) must be specified'
  end if


  if (count == 9) then
    call open_fctp(fileTOR, 'T')

    ! write toroidal eigenfunctions into file
    do ll = llmin, llmax
      if ( ll >= 1) then
        do nn = nnmin, nnmax

          write(name1,'(i4.4,"T",i4.4)') nn, ll
          open(unit1, file=name1, status='replace')
          call write_modeT_ascii(unit1, nn, ll, modelfile)
          close(unit1)
          print *, 'Output has been written into ', name1
        end do
      end if
    end do
  end if

  if ((count == 9) .or. (count == 8)) then

    call open_fctp(fileSPH, 'S')

    ! write spheroidal eigenfunctions into file
    do ll = llmin, llmax
      do nn = nnmin, nnmax

        write(name2,'(i4.4,"S",i4.4)') nn, ll
        open(unit2, file=name2, status='replace')
        call write_modeS_ascii(unit2, nn, ll, flag_pint, modin_fmt, modelfile)
        close(unit2)
        print *, 'Output has been written into ', name2

      end do
    end do
  end if

end program get_fctp
