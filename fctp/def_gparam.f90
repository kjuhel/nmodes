module def_gparam
  ! module de parametres generaux :
  implicit none

  integer, parameter :: DP = KIND(1.0D0)
  integer, parameter :: SP = KIND(1.0)

end module def_gparam
