module module_grav

  use def_param
  use module_model
  implicit none

  ! normalization parameters
  real(dp), parameter :: big=1.0e+40_dp
  real(dp), parameter :: r_norm=6.371e+6_dp
  real(dp), parameter :: rho_norm=5.515e+3_dp
  real(dp), parameter :: pi_d=3.141592653589793238462643383279502884197_dp

  ! arrays holding the model properties
  integer(i4), save                              :: nknot
  integer(i4), save                              :: nlayer
  integer(i4), dimension(:,:), allocatable, save :: layer_index
  real(dp), dimension(:), allocatable, save      :: grav, grav_cs, rho_cs, vpv_cs, vsv_cs

  type(modele) :: modele1D
  logical      :: modele1D_read=.false.

  ! arrays used in numerical quadrature
  real(dp), dimension(:), allocatable, save, private :: xint, intg, qint


  contains


    subroutine get_grav(rad, gg, rho)
      !----------------------------------------------------!
      ! computes gravity using the radial integral formula !
      !----------------------------------------------------!
      use nrtype
      use module_main, only: model
      use module_model
      use module_spline
      implicit none
      real(dp), intent(in) :: rad
      real(dp), intent(out) :: gg, rho

      integer(i4b) :: i,j,ir
      real(dp) :: fint, vel_norm
      character(len=100) :: model1Dfile

      vel_norm = r_norm * sqrt(pi_d*6.6723e-11_dp*rho_norm)

      ! read the model properties
      model1Dfile = model
      call read_modele(model1Dfile, modele1D)
      modele1D_read = .true.

      ! determine nb of radial knots in the model, and
      ! allocate the arrays for numerical quadrature
      nknot = modele1D%nbcou
      allocate(grav(nknot), grav_cs(nknot), xint(nknot), intg(nknot), qint(nknot))
      allocate(rho_cs(nknot))

      ! determine the number of layers in the model
      nlayer=1
      do i=2,nknot
         if(modele1D%r(i) == modele1D%r(i-1)) nlayer=nlayer+1
      end do

      allocate(layer_index(nlayer,2))
      layer_index(1,1)=1
      layer_index(nlayer,2)=nknot
      j=0
      do i=2,nknot
         if(modele1D%r(i) == modele1D%r(i-1)) then
            j=j+1
            layer_index(j,2)=i-1
            layer_index(j+1,1)=i
         end if
      end do

      ! computes gravity using the radial integral formula
      xint = modele1D%r / r_norm
      intg = xint**2 * (modele1D%rho/rho_norm)

      do i = 1,nlayer
         call spline(xint(layer_index(i,1):layer_index(i,2)), &
                     intg(layer_index(i,1):layer_index(i,2)), &
                     big,big,qint(layer_index(i,1):layer_index(i,2)) )
      end do
      grav(1) = 0.0_dp
      fint = 0.0_dp
      do i = 1,nknot-1
         call gauslv(xint(i), xint(i+1), i, fint)
         grav(i+1) = fint
      end do
      where(xint > 0.0_dp) grav=(4.0_dp*grav)/xint**2

      ! compute cubic spline coefficients for density and gravity
      do i = 1,nlayer
         call spline(xint(layer_index(i,1):layer_index(i,2)), &
                     grav(layer_index(i,1):layer_index(i,2)), &
                     big,big,grav_cs(layer_index(i,1):layer_index(i,2)))

         call spline(xint(layer_index(i,1):layer_index(i,2)), &
                     modele1D%rho(layer_index(i,1):layer_index(i,2)), &
                     big,big,rho_cs(layer_index(i,1):layer_index(i,2)))
      end do

      gg = splint(xint, grav, grav_cs, rad)
      rho = splint(xint, modele1D%rho, rho_cs, rad) / rho_norm

      ! ir = find_radius_index(rad(i), -1)

      ! print *, rad(i)*r_norm, modele1D%r(ir), modele1D%r(ir+1)
      ! print *, gg(i), grav(ir), grav(ir+1)
      ! print *, rho(i), modele1D%rho(ir), modele1D%rho(ir+1)

      deallocate(xint, intg, qint)

    end subroutine get_grav


    function intgds(y, iq)
      use nrtype
      use module_spline
      implicit none
      real(dp) :: intgds
      real(dp), intent(in) :: y
      integer(i4b), intent(in) :: iq

      intgds = splint_dis(xint, intg, qint, y, iq)

      return
    end function intgds


    subroutine gauslv(r1, r2, iq, fint)
      !-----------------------------------------------------!
      ! Numerical integration by Gauss-Legendre quadrature. !
      ! This routine has been copied from that in minos.    !
      !-----------------------------------------------------!
      use nrtype
      implicit none
      real(dp), intent(in) :: r1,r2
      integer(i4b), intent(in) :: iq
      real(dp), intent(inout) :: fint

      integer(i4b) :: i,j
      real(dp), dimension(2), parameter :: &
        w = (/.478628670499366_dp, .236926885056189_dp/), &
        x = (/.538469310105683_dp, .906179845938664_dp/)
      real(dp) :: t1,y1,y2
      real(dp) :: vals,vals1,sum

      y1   = 0.5_dp*(r2+r1)
      y2   = 0.5_dp*(r2-r1)
      vals = intgds(y1, iq)
      sum  = .568888888888889_dp*vals

      do i = 1,2
         t1    = x(i)*y2
         vals  = intgds(y1+t1, iq)
         vals1 = intgds(y1-t1, iq)
         sum   = sum + w(i)*(vals+vals1)
      end do

      fint = fint + y2*sum

      return
    end subroutine gauslv


    function find_radius_index(rr, up)
      use nrtype
      implicit none
      integer(i4b) :: find_radius_index
      real(dp), intent(in) :: rr
      integer(i4b), intent(in), optional :: up
      integer(i4b) :: iup,i

      if(present(up)) then
         iup = up
      else
         iup = 1
      end if

      if (iup /= 1 .and. iup /= -1) stop 'bad input to fri'
      if (rr < 0.0_dp .or. rr > 1.0_dp) then
         find_radius_index = 0
         return
      end if

      do i = 1,nknot-1
         if (iup == 1) then
            if (rr == xint(i) .and. rr == xint(i+1)) then
               find_radius_index = i + 1
               exit
            end if
            if(rr >= xint(i) .and. rr < xint(i+1)) then
               find_radius_index = i
               exit
            end if
         else
            if(rr == xint(i) .and. rr == xint(i+1)) then
               find_radius_index = i
               exit
            end if
            if(rr > xint(i) .and. rr <= xint(i+1)) then
               find_radius_index = i
               exit
            end if
         end if
      end do

      return
    end function find_radius_index


end module module_grav
