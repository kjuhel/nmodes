module module_modes
  use def_param
  implicit none

  public :: init_modes, get_and_sum

  private
  integer :: LmaxS, LmaxT, Lmax_all, Nmax_allS, Nmax_allT, Nmax_all

  integer :: coef_nbt, NBF, NBF2
  real(DP) :: dtf, dtf2, wmax3, wmax4
  real(DP), dimension(1) :: Vs, Vp, Qshear, radtmp
  real(DP), dimension(:,:), allocatable :: src_coord, rec_coord, Msrc

  integer, dimension(:), allocatable :: LLLmax, NmaxS, NmaxT
  integer, dimension(:,:), allocatable :: NNNmax

  ! freq propre et attenuation
  real(DP), dimension(:,:,:), allocatable :: eigw, qw

  real(DP), dimension(:), allocatable :: al, b0, b1, b2
  real(DP), dimension(:,:), allocatable :: UU, UUp, VV, VVp, PP, PPp, WW, WWp
  real(DP), dimension(:,:,:), allocatable :: UUs, UUps, VVs, VVps, PPs, PPps, WWs, WWps

  integer :: qnldeb, qnlfin, iqnld
  integer :: length_qnl, length_qnl_all, nbchunk
  integer, dimension(:), allocatable :: qnldeb_chunk, qnlfin_chunk
  integer, parameter :: memorymax=512

  complex*16, dimension(:,:), allocatable :: time_serie
  integer, dimension(2), parameter :: ldeb=(/0,1/)

  complex(DP), parameter :: czero=(0._DP,0._DP), II=(0._DP,1._DP)
  complex(SP), parameter :: czero_sp=(0._SP,0._SP)

  complex(DP), dimension(:,:,:,:,:), allocatable :: SkN
  complex(DP), dimension(:,:,:,:,:), allocatable :: RkN

  real(DP), parameter :: rhobar=5515._DP
  real(DP) :: scale, dscale, receiver_rad, om_norm
  real(DP) :: scale1, scale2

  ! gravitation
  real(DP) :: gg, rho

  contains


    subroutine init_modes()
      !------------------------------------------------!
      !  retrieve eigenfrequencies and eigenfunctions  !
      !   and initialize parameters related to modes   !
      !------------------------------------------------!
      use def_param
      use module_main, only : get_source_dat, get_receivers, eigenfileS, eigenfileT, &
        RA, NBT, dt, f4, l4, NBE, NBR, NBE_layer, &
        nbcomp, nmax_in, stations_depth, sources_depth, homogenization
      use module_fctp
      use module_grav
      use corr_modes
      implicit none

      integer :: l, n, NBFtmp
      real(DP) :: t0delay
      real(DP), dimension(:), allocatable   :: rad
      real(DP), dimension(:,:), allocatable :: xdum
      
      t0delay = 0.d0

      ! nombre de frequence, on va a 1.1 fois Nyquist
      ! mais aussi nombre de pas de temps effectif
      NBFtmp = int(2*f4*((NBT-1)*dt+t0delay))+1
      call get_power(int(1.1*NBFtmp), NBF)

      ! on utilise deux fois plus long pour la convolution
      ! (zero padding sur la moitie du signal)
       NBF2 = 2*NBF

      ! on recupere le plus grand coef_nbt
      ! tel que NBF*coef_nbt < NBT
      coef_nbt = 1
      do while (coef_nbt*2*NBF < NBT)
         coef_nbt = 2*coef_nbt
      end do

      dtf = ((NBT-1)*dt) / real(NBF-1)
      ! pas de temps apres coef_nbt
      dtf2  = dtf/coef_nbt

      ! ouverture de fichiers de fonctions propres
      print *, 'Opening eigenfunction files ...'
      call open_fctp(eigenfileS, eigenfileT, rad_recepteur=receiver_rad)
      
      ! take into account stations depth
      receiver_rad = receiver_rad - stations_depth

      ! RA is now initialized
      scale2 = 1.D0/(rhobar*RA*RA*RA)
      scale1 = scale2/RA/RA

      ! lecture des sources :
      print *, 'Getting sources and receivers informations ...'
      allocate(src_coord(2,NBE), Msrc(6,NBE), rec_coord(2,NBR))
      call get_source_dat(src_coord, Msrc)
      call get_receivers(rec_coord)

      ! calcul des LmaxS, LmaxT, NmaxS, NmaxT
      call get_Lmax(f4, LmaxS, LmaxT)
      LmaxS = min(LmaxS, l4)
      LmaxT = min(LmaxT, l4)

      allocate(NmaxS(0:LmaxS), NmaxT(0:LmaxT))
      call get_Nmax(f4, NmaxS, NmaxT)
      Nmax_all = max(maxval(NmaxS(:)), maxval(NmaxT(:)))
      Lmax_all = max(LmaxS, LmaxT)
      Nmax_allS = maxval(NmaxS(:))
      Nmax_allT = maxval(NmaxT(:))

      allocate(LLLmax(2))
      allocate(NNNmax(0:lmax_all,2))
      NNNmax(:,:) = 0
      LLLmax(1) = LmaxS
      LLLmax(2) = LmaxT
      NNNmax(0:LmaxS,1) = NmaxS(:)
      NNNmax(0:LmaxT,2) = NmaxT(:)
      if (nmax_in/=-1) NNNmax(:,:) = min(nmax_in, NNNmax(:,:))

      ! init b0 etc ...
      allocate(al(0:Lmax_all), b0(0:Lmax_all), b1(0:Lmax_all), b2(0:Lmax_all))
      do l = 0,lmax_all
        al(l) = dble(l*(l+1))
        b0(l) = dsqrt(dble(2*l+1)/(4.0d0*PI))
        b1(l) = dsqrt(al(l))*b0(l)/2.0d0
        b2(l) = dsqrt(dble((dble(l)-1)*dble(l)*(dble(l)+1)*(dble(l)+2)))*b0(l)/4.0d0
      end do

      print *, 'Retrieving eigenfrequencies for ', 1+NBE, ' sources and receiver depth'
      allocate(eigw(0:Nmax_all,0:Lmax_all,2), qw(0:Nmax_all,0:Lmax_all,2))
      call get_eigenw(eigw, qw, LmaxS, NmaxS, LmaxT, NmaxT)

      print *, 'Allocating eigenfunctions (', &
        ((Nmax_all+1)*(LmaxS+1)*4+(maxval(NmaxT(:))+1)*(LmaxT+1)*2)*8/1024./1024., &
        ' Mb/proc)'

      allocate(UU(0:Nmax_allS,0:LmaxS), UUp(0:Nmax_allS,0:LmaxS), &
        VV(0:Nmax_allS,0:LmaxS), VVp(0:Nmax_allS,0:LmaxS), &
        PP(0:Nmax_allS,0:LmaxS), PPp(0:Nmax_allS,0:LmaxS))
      
      print *, 'Allocating temporary eigenfunctions (', &
        real(NBE_layer)*((Nmax_all+1)*(LmaxS+1)*4+(maxval(NmaxT(:))+1)*(LmaxT+1)*2)*8/1024./1024., &
        ' Mb/proc)'

      allocate(UUs(NBE_layer,0:Nmax_allS,0:LmaxS), UUps(NBE_layer,0:Nmax_allS,0:LmaxS), &
        VVs(NBE_layer,0:Nmax_allS,0:LmaxS), VVps(NBE_layer,0:Nmax_allS,0:LmaxS), &
        PPs(NBE_layer,0:Nmax_allS,0:LmaxS), PPps(NBE_layer,0:Nmax_allS,0:LmaxS))

      allocate(WW(0:Nmax_allT,0:LmaxT), WWp(0:Nmax_allT,0:LmaxT))

      allocate(WWs(NBE_layer,0:Nmax_allT,0:LmaxT), WWps(NBE_layer,0:Nmax_allT,0:LmaxT))

      UU=0.0_DP; VV=0.0_DP; UUp=0.0_DP; VVp=0.0_DP; PP=0.0_DP; PPp=0.0_DP; WW=0.0_DP; WWp=0.0_DP
      UUs=0.0_DP; UUps=0.0_DP; VVs=0.0_DP; VVps=0.0_DP; PPs=0.0_DP; PPps=0.0_DP; WWs=0.0_DP; WWps=0.0_DP

      print *, 'Reading and interpolating eigenfunctions ...'
      allocate(xdum(6,1+NBE_layer), rad(1+NBE_layer))

      rad(1) = receiver_rad
      rad(2:NBE_layer+1) = sources_depth(:)

      do l = 0,LmaxS
         do n = 0,NmaxS(l)
           call read_interpS(n, l, 1+NBE_layer, rad, xdum)
           UU( n,l) = xdum(1,1)
           UUp(n,l) = xdum(2,1)
           VV( n,l) = xdum(3,1)
           VVp(n,l) = xdum(4,1)
           PP( n,l) = xdum(5,1)
           PPp(n,l) = xdum(6,1)

           UUs( :,n,l) = xdum(1,2:)
           UUps(:,n,l) = xdum(2,2:)
           VVs( :,n,l) = xdum(3,2:)
           VVps(:,n,l) = xdum(4,2:)
           PPs( :,n,l) = xdum(5,2:)
           PPps(:,n,l) = xdum(6,2:)
         end do
      end do

      call get_grav(rad(1)/RA, gg, rho)

      deallocate(xdum)

      allocate(xdum(2, NBE_layer+1))
      do l = 0,LmaxT
         do n = 0,NmaxT(l)
            call read_interpT(n, l, 1+NBE_layer, rad, xdum)
            WW( n,l) = xdum(1,1)
            WWp(n,l) = xdum(2,1)

            WWs( :,n,l) = xdum(1,2:)
            WWps(:,n,l) = xdum(2,2:)
         end do
      end do

      deallocate(xdum, rad)

      if (homogenization >= 0) then
        print *, 'First order correction of the moment tensor for homogenization requested'
        print *, 'Init of module_corr_modes ...'
        call init_corr_modes(NBE_layer, sources_depth(:), homogenization)
      endif

      call split_work()

      print *, 'init_modes done!'

    end subroutine init_modes


    subroutine split_work()
      !-------------------------------------------!
      !  splitting work to avoid memory overload  !
      !-------------------------------------------!
      use module_main, only: nmin
      implicit none

      integer :: q, l, n, reste, pas, ip
      real(DP) :: memall

      length_qnl_all = 0

      do q = 1,2
         do l = ldeb(q),LLLmax(q)
            do n = nmin,NNNmax(l,q)
               length_qnl_all = length_qnl_all+1
            end do
         end do
      end do
      
      memall = real(length_qnl_all)*real(NBF2)*16./1024./1024.
      nbchunk = int(memall/real(memorymax))+1
      
      if (nbchunk/=1) print*,'The job will be splitted in ',nbchunk,' parts of ',memorymax,' Mbytes'

      allocate(qnldeb_chunk(0:nbchunk), qnlfin_chunk(0:nbchunk))

      pas = int(length_qnl_all/nbchunk)
      reste = mod(length_qnl_all,nbchunk)

      qnldeb_chunk(0) = 1
      qnlfin_chunk(0) = pas

      if (0<reste) qnlfin_chunk(0) = qnlfin_chunk(0)+1

      do ip = 1,nbchunk-1
         qnldeb_chunk(ip) = qnlfin_chunk(ip-1)+1
         qnlfin_chunk(ip) = qnldeb_chunk(ip)+pas-1
         if (ip<reste) qnlfin_chunk(ip) = qnlfin_chunk(ip)+1
      end do

    end subroutine split_work


    subroutine get_and_sum()
      !---------------------------------------------------------!
      !  get time series and excitation coefficients, then sum  !
      !---------------------------------------------------------!
      use def_param
      use module_main, only: NBR, nbcomp, NBT, dt, comp, compr, &
        stations_name, sources_name, rotation, NBE, out_type, stacking_sources
      implicit none

      integer :: i, ir, ic, is, ichunk

      real(SP), dimension(NBT,nbcomp) :: trace_time
      real(SP), dimension(NBT) :: tmptr

      complex(DP), dimension(NBF2) :: tracestmp
      complex(DP), dimension(NBF2,nbcomp,NBR,NBE) :: traces
      complex(DP), dimension(:,:), allocatable :: RSkN

      complex*16 :: alpha=(1.d0,0.d0), beta=(0.d0,0.d0)
      character(len=30) :: name

      traces(:,:,:,:) = cmplx(0.0d0, 0.0d0)

      print *, 'Computing RkN and SkN terms ...'
      call init_RSkN()
      print *, 'init_RSkN done!'

      do ichunk = 0,nbchunk-1
        qnldeb = qnldeb_chunk(ichunk)
        qnlfin = qnlfin_chunk(ichunk)

        call init_time_serie()
        allocate(RSkN(length_qnl,nbcomp))

        do is = 1,NBE
          if (NBE/=1) print *,'source :',is,NBE
          do ir = 1,NBR
            RSkN(:,:) = czero
            call get_RSkN(ir, is, RSkN)
            do ic = 1,nbcomp
              call ZGEMV('T', length_qnl, NBF2, alpha, time_serie, length_qnl, &
                         RSkN(1,ic), 1, beta, tracestmp, 1)
              traces(:,ic,ir,is) = traces(:,ic,ir,is) + tracestmp(:)
            end do
          end do
        end do

        deallocate(time_serie, RSkN)
      end do

      print *, 'Converting in time and writing ascii output ...'

      if (NBE==1 .or. stacking_sources) then
        do ir = 1,NBR
          if (stacking_sources) then
            trace_time(:,:) = 0.
            do ic = 1,nbcomp
              do is = 1,NBE
                call convert_f2t(traces(:,ic,ir,is), tmptr(:), is)
                trace_time(:,ic) = trace_time(:,ic) + tmptr(:)
              end do
            end do
          else
            do ic = 1,nbcomp
              call convert_f2t(traces(:,ic,ir,1), trace_time(:,ic), 1)
            end do
          end if
          if (rotation) call rotate(trace_time, ir, 1)
          do ic = 1,nbcomp
            if (rotation) then
              write(name, '(a, ".", a, ".", a, ".", a)') trim(stations_name(ir)), out_type, compr(ic), sources_name(1)
            else
              write(name, '(a, ".", a, ".", a, ".", a)') trim(stations_name(ir)), out_type, comp(ic), sources_name(1)
            end if
            open(17, file=name)
            write(17,'(f8.1,1x,e14.7)') ((i-1)*dt, trace_time(i,ic), i=1,NBT)
            close(17)
          end do
        end do

      else
        do is = 1,NBE
          do ir = 1,NBR
            do ic = 1,nbcomp
              call convert_f2t(traces(:,ic,ir,is), trace_time(:,ic), is)
            end do
            if (rotation) call rotate(trace_time, ir, is)
            do ic = 1,nbcomp
              if (rotation) then
                write(name, '(a, ".", a, ".", a, ".", a)') trim(stations_name(ir)), out_type, compr(ic), sources_name(is)
              else
                write(name, '(a, ".", a, ".", a, ".", a)') trim(stations_name(ir)), out_type, comp(ic), sources_name(is)
              end if
              open(17, file=name)
              write(17,'(f8.1,1x,e14.7)') ((i-1)*dt, trace_time(i,ic), i=1,NBT)
              close(17)
            end do
          end do
        end do
      end if

    end subroutine get_and_sum


    subroutine rotate(trace, ir, is)
      !--------------------------------------!
      !  rotate from (Z, N, E) to (Z, R, T)  !
      !--------------------------------------!
      use def_param
      use module_main, only: NBT, stations_name
      use module_fctp
      implicit none
      integer, intent(in) :: ir, is
      real(SP), dimension(:,:), intent(out) :: trace

      real(DP) :: tr, pr, ts, ps, asr, bsr, gsr, bazm
      real(SP), dimension(NBT,2) :: tmp

      tr = rec_coord(1,ir)
      pr = rec_coord(2,ir)
      ts = src_coord(1,is)
      ps = src_coord(2,is)

      call euler(ts, ps, tr, pr, asr, bsr, gsr)

      bazm = -asr
      print *, stations_name(ir), ', bazm = ', bazm*180./3.1415926, ', delta=', bsr*180./3.1415926
      tmp(:,:) = trace(:,2:3)
      trace(:,2) = -tmp(:,1)*cos(bazm)+tmp(:,2)*sin(bazm)
      trace(:,3) = -tmp(:,1)*sin(bazm)-tmp(:,2)*cos(bazm)

    end subroutine rotate


    subroutine convert_f2t(tracef, tracet, is)
      !-----------------------------!
      !  inverse Fourier transform  !
      !-----------------------------!
      use def_param
      use module_fctp
      use module_main, only: NBT, dt, nbcomp, t_delay_sources
      implicit none
      complex(DP), dimension(NBF2), intent(inout) :: tracef
      real, dimension(NBT), intent(out) :: tracet
      integer, intent(in) :: is

      real(DP), dimension(NBF*coef_nbt) :: trace_testr
      complex(DP), dimension(NBF2*coef_nbt) :: traces_testf

      traces_testf(:) = (0.d0,0.d0)
      traces_testf(1:NBF+1) = tracef(1:NBF+1)
      traces_testf((2*coef_nbt*NBF)-NBF+1:coef_nbt*NBF2) = tracef(NBF+1:NBF2)

      call dfour1(traces_testf, NBF2*coef_nbt, -1)
      ! SP + * frequency step
      trace_testr(1:NBF*coef_nbt) = real(traces_testf(1:NBF*coef_nbt)/NBF2/dtf,DP)
      call resample(trace_testr, t_delay_sources(is), dtf2, NBF*coef_nbt, tracet, dt, NBT)

    end subroutine convert_f2t


    subroutine init_RSkN()
      !----------------------------------------------!
      !  initialize excitation coefficient arrays,   !
      !  related to source (SkN) and receiver (RkN)  !
      !----------------------------------------------!
      use def_param
      use module_main, only: nbcomp, NBE, RA
      implicit none

      scale  = 1.0_DP/sqrt(rhobar*RA**3)
      dscale = 1.0_DP/sqrt(rhobar*RA**3)/RA

      allocate(SkN(-2:2,0:Nmax_all,0:Lmax_all,2,NBE))
      allocate(RkN(-1:1,nbcomp,0:Nmax_all,0:Lmax_all,2))

      SkN = 0.0_DP
      RkN = 0.0_DP

      call SkNfun(SkN)
      call RkNfun(RkN)

      ! cleaning unecessary arrays :
      deallocate(UUs, UUps, VVs, VVps, WWs, WWps, PPs, PPps, Msrc)

    end subroutine init_RSkN


    subroutine SkNfun(S)
      !-------------------------------------------------!
      !  computes SkN terms, ie the terms described by  !
      !  equations (10.53)-(10.59) of Dahlen and Tromp  !
      !-------------------------------------------------!
      use def_param
      use module_main, only: RA, NBE, source_force, sources_depth, sources_layer, homogenization
      use corr_modes
      implicit none
      complex(DP), dimension(-2:,0:,0:,:,:), intent(out) :: S
      integer :: is,izs,n,l
      real(DP) :: ff,xx,vor,dmtp,egu,egv,degu,degv,rr,x
      real(DP) :: zz,wor,fw,dfw,u,v,xu,xv,vir,vit,vip
      real(DP), dimension(6) :: am

      if (source_force) then
        do l = 0,LmaxS
          do n = 0,NmaxS(l)
            do is = 1,NBE
              izs = sources_layer(is)
              rr  = sources_depth(izs) / RA

              vir = Msrc(1,is)
              vit = Msrc(2,is)
              vip = Msrc(3,is)

              u = UUs(izs,n,l)
              v = VVs(izs,n,l)

              if (homogenization >= 0) then
                degu = UUps(izs,n,l)
                degv = VVps(izs,n,l)
                call corrmodesS(izs, eigw(n,l,1), l, rr, u, v, degu, degv)
              end if

              xu = b0(l)*u
              xv = b1(l)*v

              S( 0,n,l,1,is) = xu*vir*scale
              S( 1,n,l,1,is) = xv*dcmplx(-vit,vip)*scale
              S(-1,n,l,1,is) = xv*dcmplx(+vit,vip)*scale
            end do
          end do
        end do

        do l = 1,LmaxT
          do n = 0,NmaxT(l)
            do is = 1,NBE
              izs = sources_layer(is)

              vit = Msrc(2,is)
              vip = Msrc(3,is)

              if (homogenization >= 0) then
                dfw = WWps(izs,n,l)
                call corrmodesT(izs, eigw(n,l,2), l, rr, fw, dfw)
              end if

              x = b1(l)*WWs(izs,n,l)

              S( 1,n,l,2,is) = x*dcmplx(+vip,vit)*scale
              S(-1,n,l,2,is) = x*dcmplx(-vip,vit)*scale
            end do
          end do
        end do

        ! dans le cas d'une force, il n'y a pas d'integration par partie
        ! tout - pour tout le monde : (? ... j'ai quand meme un doute)
        S = -S

      else
        do l = 0,LmaxS
          do n = 0,NmaxS(l)
            do is = 1,NBE
              izs = sources_layer(is)
              rr  = sources_depth(izs) / RA

              egu   = UUs (izs,n,l)
              egv   = VVs (izs,n,l)
              degu  = UUps(izs,n,l)
              degv  = VVps(izs,n,l)

              !if ((n.eq.0 .or. n.eq.1) .and. l.eq.1) then
              !  egu = 0.0_dp; egv = 0.0_dp; degu = 0.0_dp; degv = 0.0_dp
              !end if

              if (homogenization >= 0) call corrmodesS(izs, eigw(n,l,1), l, rr, egu, egv, degu, degv)
              am(:) = Msrc(:,is)

              ff   = (2.0d0*egu-al(l)*egv)/rr
              xx   = -b1(l)*(degv+(egu-egv)/rr)
              vor  = b2(l)*egv/rr
              dmtp = am(3)-am(2)

              S( 0,n,l,1,is) = -b0(l)*(degu*am(1)+ff*0.5*(am(2)+am(3)))*dscale
              S( 1,n,l,1,is) = xx*dcmplx(-am(4),am(5))*dscale
              S(-1,n,l,1,is) = xx*dcmplx( am(4),am(5))*dscale
              S( 2,n,l,1,is) = vor*dcmplx(dmtp, 2.0d0*am(6))*dscale
              S(-2,n,l,1,is) = vor*dcmplx(dmtp,-2.0d0*am(6))*dscale
            end do
          end do
        end do

        do l = 1,LmaxT
          do n = 0,NmaxT(l)
            do is = 1,NBE
              izs = sources_layer(is)
              rr  = sources_depth(izs) / RA

              fw  = WWs (izs,n,l)
              dfw = WWps(izs,n,l)

              if (homogenization >= 0) call corrmodesT(izs, eigw(n,l,2), l, rr, fw, dfw)
              am(:) = Msrc(:,is)

              zz   = -b1(l)*(dfw-fw/rr)
              wor  = b2(l)*fw/rr
              dmtp = am(2)-am(3)

              S( 1,n,l,2,is) = zz *dcmplx( am(5),am(4))*dscale
              S(-1,n,l,2,is) = zz *dcmplx(-am(5),am(4))*dscale
              S( 2,n,l,2,is) = wor*dcmplx(2.0d0*am(6), dmtp)*dscale
              S(-2,n,l,2,is) = wor*dcmplx(2.0d0*am(6),-dmtp)*dscale
            end do
          end do
        end do

      end if

    end subroutine SkNfun


    subroutine RkNfun(R)
      !-------------------------------------------------!
      !  computes RkN terms, ie the components of the   !
      !  displacement operator D described by equation  !
      !  (10.60) of Dahlen and Tromp                    !
      !  for gravitationally modified operator D,       !
      !  see equation (10.70) of Dahlen and Tromp       !
      !-------------------------------------------------!
      use def_param
      use module_main, only: NBE_layer, RA, nbcomp, out_type, homogenization
      use corr_modes
      implicit none
      complex(DP), dimension(-1:,:,0:,0:,:), intent(out) :: R
      integer :: ic,n,l,izs
      real(DP) :: xu,xv,u,v,p,degu,degv,degp,vir,vip,vit,x,rr,fw,dfw

      om_norm = sqrt(pi*6.6723e-11_dp*rhobar)

      do l = 0,LmaxS
         do n = 0,NmaxS(l)
            do ic = 1,nbcomp

               select case(ic)
               case(1) ! verticale
                 vir = 1.0_DP
                 vit = 0.0_DP
                 vip = 0.0_DP
               case(2) ! nord = -theta
                 vir = 0.0_DP
                 vit = -1.0_DP
                 vip = 0.0_DP
               case(3) ! est = phi
                 vir = 0.0_DP
                 vit = 0.0_DP
                 vip = 1.0_DP
               end select

               u = UU(n,l)
               v = VV(n,l)
               p = PP(n,l); degp = PPp(n,l)

               !if ((n.eq.0 .or. n.eq.1) .and. l.eq.1) then
               !  u = 0.0_dp; v = 0.0_dp; p = 0.0_dp; degp = 0.0_dp
               !end if
 
               if (homogenization >= 0) then
                 rr = 1.d0
                 izs = NBE_layer+1
                 degu = UUp(n,l)
                 degv = VVp(n,l) 
                 call corrmodesS(izs, eigw(n,l,1), l, rr, u, v, degu, degv)
               end if

               rr = receiver_rad / RA

               ! see Dahlen & Tromp (1998), section 10.4

               select case(out_type)
               case('DIS','VEL','ACC') ! inertial displacement, velocity or acceleration
                 xu = +b0(l)*u
                 xv = -b1(l)*v

               case('POT') ! gravity change due to mass variation (u_pot and v_pot)
                 xu = -b0(l)* (dble(l+1)*p/rr) * om_norm**2
                 xv = -b1(l)* (p/rr) * om_norm**2

               case('FAC') ! free air change (u_free and v_tilt)
                 xu = -b0(l)* (2.0_DP*gg*u/rr) * om_norm**2
                 xv = -b1(l)* (gg*u/rr) * om_norm**2

               case('TOT') ! total gravity acceleration variation
                 xu = -b0(l)* (dble(l+1)*p + 2.0_DP*gg*u)/rr * om_norm**2
                 xv = -b1(l)* (p + gg*u)/rr * om_norm**2

               case('SRF') ! apparent surface mass due to radial displacement
                 xu = b0(l)* 4.0_DP*rho*u * om_norm**2
                 xv = 0.0_dp

               case('DG1') ! perturbation of gravity on undeformed surface
                 if (l /= 0) then
                   xu = b0(l)* degp * om_norm**2
                 else
                   xu = -b0(l)* 4.0_DP*rho*u * om_norm**2
                 end if
                 xv = -b1(l) * (p/rr) * om_norm**2
               end select

               R( 0,ic,n,l,1) = xu*vir*scale
               R( 1,ic,n,l,1) = xv*dcmplx(+vit,vip)*scale
               R(-1,ic,n,l,1) = xv*dcmplx(-vit,vip)*scale

            end do
         end do
      end do


      do l = 1,LmaxT
         do n = 0,NmaxT(l)
            do ic = 1,nbcomp ! boucle sur les composantes

               select case(ic)
               case(1) ! verticale
                 vir = 1.0_DP
                 vit = 0.0_DP
                 vip = 0.0_DP
               case(2) ! theta
                 vir = 0.0_DP
                 vit = -1.0_DP
                 vip = 0.0_DP
               case(3) ! phi
                 vir = 0.0_DP
                 vit = 0.0_DP
                 vip = 1.0_DP
               end select

               fw = WW(n,l)
               if (homogenization >= 0) then
                 rr = 1.d0
                 izs = NBE_layer+1
                 dfw = WWp(n,l)
                 call corrmodesT(izs, eigw(n,l,2), l, rr, fw, dfw)
               endif

               select case(out_type)
               case('DIS','VEL','ACC') ! inertial displacement, velocity or acceleration
                 x = -b1(l)*fw

               case default
                 x = 0.0_dp
               end select

               R( 1,ic,n,l,2) = x*dcmplx(-vip,vit)*scale
               R(-1,ic,n,l,2) = x*dcmplx(+vip,vit)*scale
            end do
         end do
      end do

    end subroutine RkNfun


    subroutine get_RSkN(ir, is, RSkN)
      !-------------------------------------------!
      !  get overall RSkN array from SkN and RkN  !
      !-------------------------------------------!
      use def_param
      use module_fctp
      use module_main, only: nbcomp, nmin
      implicit none
      integer, intent(in) :: ir,is
      complex(DP), dimension(:,:), intent(out) :: RSkN

      integer  :: iqnl,bNp,qdeb,qfin,l,n,q,bN,ird,ic
      real(DP) :: ts,ps,tr,pr,asr,bsr,gsr,x
      real(DP), dimension(-2:2,-2:2,0:Lmax_all) :: dsr
      complex(DP) :: sd,wq,eiwtd,rs
      complex(DP), dimension(-2:2) :: csd
      complex(DP), dimension(-1:1) :: csr

      tr = rec_coord(1,ir)
      pr = rec_coord(2,ir)
      ts = src_coord(1,is)
      ps = src_coord(2,is)

      call euler(ts, ps, tr, pr, asr, bsr, gsr)

      do bNp = -2,2
         x = bNp*gsr
         csd(bNp) = dcmplx(dcos(x), dsin(x))
      end do

      do bNp = -1,1
         x = bNp*asr
         csr(bNp) = dcmplx(dcos(x), dsin(x))
      end do

      call legendre(bsr, dsr, Lmax_all)

      do ic = 1,nbcomp
         iqnl = 0
         qdeb = 1; qfin = 2
         do q = qdeb,qfin ! 1: spheroidal, 2: toroidal
            do l = ldeb(q),LLLmax(q)
               do n = nmin,NNNmax(l,q)
                  if (mode_existe(n,l,q)) then
                    iqnl = iqnl + 1
                    if (iqnl>=qnldeb .and. iqnl<=qnlfin) then
                      rs = czero
                      do bN = -1,1
                         sd = czero
                         do bNp = -2,2
                            sd = sd + dsr(bN,bNp,l)*SkN(bNp,n,l,q,is)*csd(bNp)
                         end do
                         rs = rs + sd*RkN(bN,ic,n,l,q)*csr(bN)
                      end do
                      !if (rs*0.0 /= 0.0) then
                      !  print *, 'NaN in RSkN for l =', l, 'and n =', n, '!!'
                      !  rs = 0.0
                      !end if
                      RSkN(iqnl-iqnld,ic) = rs
                    end if
                  end if
               end do
            end do
         end do
      end do

    end subroutine get_RSkN


    subroutine init_time_serie()
      !--------------------------!
      !  initialize time series  !
      !--------------------------!
      use module_fctp
      use module_main, only: NBT, dt, nmin, f3, f4, l3, l4, out_type
      implicit none
      integer :: q, l, n, it, iqnl, qdeb, qfin, lmax1, lmax2
      real(DP) :: t, wt_f, wt_l
      complex(DP) :: eiomt, om, cc
      complex(DP), dimension(NBF2) :: serie

      wmax3 = f3*2.0_DP*PI
      wmax4 = f4*2.0_DP*PI
      lmax1 = l3
      lmax2 = l4

      length_qnl = qnlfin-qnldeb+1
      iqnld = qnldeb-1

      allocate(time_serie(length_qnl, NBF2))
      time_serie(:,:) = (0.0d0, 0.0d0)
      serie(:) = (0.0d0, 0.0d0)

      iqnl = 0
      qdeb=1;qfin=2

      do q = qdeb,qfin
         do l = ldeb(q),LLLmax(q)
            call wtcoef(1.0_dp*l, 0.0_dp, 0.0_dp, 1.0_dp*lmax1, 1.0_dp*lmax2, wt_l)
            do n = nmin,NNNmax(l,q)
               if (mode_existe(n,l,q)) then
                 iqnl = iqnl+1
                 if (iqnl>=qnldeb .and. iqnl<=qnlfin) then
                   serie(:) = (0.d0, 0.d0)
                   om = cmplx(eigw(n,l,q), qw(n,l,q))
                   call wtcoef(eigw(n,l,q), 0.0_dp, 0.0_dp, wmax3, wmax4, wt_f)
                   do it = 1,NBF
                      t = (it-1)*dtf
                      select case(out_type)
                      case('ACC')
                        cc = -1.0_DP
                      case('VEL')
                        cc = II / om
                      case default
                        cc = 1.0_DP / om**2
                      end select
                      cc = cc * wt_f * wt_l
                      eiomt = cc * exp(II*om*t)
                      serie(it) = eiomt
                   end do
                   call dfour1(serie,NBF2,1)
                   time_serie(iqnl-iqnld, :) = serie(:)*dtf
                 end if
               end if
            end do
         end do
      end do

    end subroutine init_time_serie


    subroutine get_power(n_in, n_out)
      !-------------------------------------------------!
      !  subroutine qui retourne le nombre entier       !
      !  superieur le plus proche de n_in qui soit une  !
      !  decomposable en puissance de 2 de 3 et de 5    !
      !-------------------------------------------------!
      implicit none
      integer, intent(in)  :: n_in
      integer, intent(out) :: n_out

      integer:: n_div, n_tmp, i

      n_tmp = n_in-1
      n_div = n_in-1

      do while(n_div/=1) ! on veux un nb divisible par 4
         n_tmp = n_tmp+1
         n_div = n_tmp
         do i = 2,5
            if (i/=4) then
               do while (mod(n_div,i)==0.and.(n_div/=1))
                  n_div = n_div/i
               end do
            end if
         end do
      end do
      n_out = n_tmp

    end subroutine get_power


    subroutine wtcoef(f, f1, f2, f3, f4, wt)
      !----------------------------------!
      !  compute weighting coefficients  !
      !----------------------------------!
      use def_param
      implicit none

      real(DP), intent(in) :: f, f1, f2, f3, f4
      real(DP), intent(out) :: wt

      if (f3.gt.f4) stop 'wtcoef : f3 > f4 !'
      if (f1.gt.f2) stop 'wtcoef : f1 > f2 !'

      if (f.lt.f1.or.f.gt.f4) then
        wt = 0.0_DP
      elseif (f.ge.f2.and.f.lt.f3) then
        wt = 1.0_DP
      else if (f.ge.f1.and.f.lt.f2) then
        wt = 0.5_DP*(1.0-cos(pi*(f-f1)/(f2-f1)))
      else if (f.ge.f3.and.f.lt.f4) then
        wt = 0.5_DP*(1.0-cos(pi*(f4-f)/(f4-f3)))
      end if

    end subroutine wtcoef


    subroutine legendre(thetad, PlmN, ll)
      !--------------------------------------------------!
      ! calcule la matrice TLN(theta,phi) de rotation    !
      ! sur la sphere pour le calcul a la source         !
      !                                                  !
      ! La base choisie est celle des fonctions beta+ et !
      ! eta+ associee aux modes reels (Lognonne et al)   !
      !                                                  !
      ! Les polynomes de Legendre sont normalises        !
      ! (i.e. * par (2l+1)/2 )                           !
      !                                                  !
      ! Attention, version modifiee par Yann Capdeville  !
      ! en 02/96 puis remodifie en 2001                  !
      !--------------------------------------------------!
      implicit none
      integer, intent(in) :: ll
      doubleprecision, intent(in) :: thetad
      doubleprecision, dimension(-2:2,-2:2,0:ll), intent(out) :: PlmN

      integer, parameter :: Nmax=2, Mmax=3
      integer :: L,m,Mm,n,N1,Mbeg,in,im
      doubleprecision :: theta,tsq,gaus,aa,tsqi, &
        a,b,c,d,e,tsq1,tsq2,val,coef
      doubleprecision, dimension(0:ll,0:ll+1,0:Nmax) :: Ppleg,Pmleg,Qpleg,Qmleg

      theta = thetad

      gaus = dcos(theta)
      tsq = dsqrt(1.d0-gaus*gaus)

      if (tsq.le.1.d-10) then
        ! boucle sur L
        do L = 0,ll
           aa = dsqrt(dfloat(L)+.5d0)
           Mm = min(L,Mmax)
           do m = 0,Mm
              Ppleg(L,m,0) = 0.d0
              Ppleg(L,m,1) = 0.d0
              Pmleg(L,m,1) = 0.d0
              Ppleg(L,m,2) = 0.d0
              Pmleg(L,m,2) = 0.d0
           end do

           if (gaus.gt.0.0d0) then
             Ppleg(L,0,0) = aa
             Ppleg(L,1,1) = aa
             Ppleg(L,2,2) = aa
           else
             Ppleg(L,0,0) = (-1)**L*aa
             Ppleg(L,1,1) = 0.0d0
             Ppleg(L,2,2) = 0.0d0
           end if
        ! fin boucle sur L
        end do
        Ppleg(0,1,1) = 0.d0
        Ppleg(0,2,2) = 0.d0
        Ppleg(1,2,2) = 0.d0
      else
        tsqi = 1.d0/tsq
        ! --> calcul des polynomes de Legendre N = 0
        Ppleg(0,0,0) = dsqrt(.5d0)
        Ppleg(1,0,0) = gaus*dsqrt(1.5d0)
        Ppleg(1,1,0) = -tsq*dsqrt(.75d0)
        Ppleg(2,1,0) = -3.d0*tsq*gaus*dsqrt(5.d0/12.d0)
        Ppleg(2,2,0) = 3.d0*tsq**2*dsqrt(5.d0/48.d0)

        ! calcul des polynomes m = 0, m = 1
        do m = 0,1
           do l = m+2,ll
              a = dsqrt((dble(l*l)-.25d0)/dble(l*l-m*m))
              b = dsqrt(dble(2*l+1)*dble(l-m-1)*dble(l+m-1)/(dble(2*l-3)*dble(l-m)*dble(l+m)))
              Ppleg(l,m,0) = 2.d0*a*gaus*Ppleg(l-1,m,0)-b*Ppleg(l-2,m,0)
           end do
        end do

        ! calcul des polynomes de Legendre pour m >= 0
        do m = 2,Mmax
           do l = m,ll
              c = dsqrt(dble(2*l+1)*dble(m+l-1)*dble(l+m-3)/(dble(2*l-3)*dble(l+m)*dble(m+l-2)))
              d = dsqrt(dble(2*l+1)*dble(l+m-1)*dble(l-m+1)/(dble(2*l-1)*dble(l+m)*dble(l+m-2)))
              e = dsqrt(dble(2*l+1)*dble(l-m)/(dble(2*l-1)*dble(l+m)))
              Ppleg(l,m,0)=Ppleg(l-2,m-2,0)*c-d*gaus*Ppleg(l-1,m-2,0)+e*gaus*Ppleg(l-1,m,0)
           end do
        end do

        ! calcul des pseudo-derivees des polynomes de Legendre
        tsq1 = gaus*tsqi
        do m = 0,Mmax
           tsq2 = tsq1*dble(m)
           Qpleg(m,m,0) = -tsq2*Ppleg(m,m,0)
           do l = m+1,ll
              a = dsqrt(dble(l-m)*dble(l+m+1))
              Qpleg(l,m,0) = -a*Ppleg(l,m+1,0)-tsq2*Ppleg(l,m,0)
           end do
        end do

        ! -->  duplication pour N = 0
        do m = 0,Mmax
           do l = m,ll
              Pmleg(l,m,0) = Ppleg(l,m,0)
              Qmleg(l,m,0) = Qpleg(l,m,0)
           end do
        end do

        ! --> calcul des polynomes de legendre generalises N
        do N = 0,Nmax-1
           N1 = N+1
           do m = 0,Mmax
              tsq  = tsqi*(dble(N)*gaus-dble(m))
              tsq1 = tsqi*(dble(N)*gaus+dble(m))
              tsq2 = tsqi*gaus
              Mbeg = max0(m,N1)
              ! relation de recurence sur N
              do l = Mbeg,ll
                 a  = dsqrt(dble((l-N)*(l+N+1)))
                 aa = 1.d0/a
                 Ppleg(l,m,N1) = aa*(Qpleg(l,m,N)+tsq*Ppleg(l,m,N))
                 Pmleg(l,m,N1) = -aa*(Qmleg(l,m,N)+tsq1*Pmleg(l,m,N))
                 Qpleg(l,m,N1) = -a*Ppleg(l,m,N)+(tsq+tsq2)*Ppleg(l,m,N1)
                 Qmleg(l,m,N1) = a*Pmleg(l,m,N)+(tsq1+tsq2)*Pmleg(l,m,N1)
              end do
           end do
        end do
      end if

      do l = 0,ll
         coef = sqrt(float((2*l+1))/2.0)
         do n = -2,2
            do m = -2,2
               in = abs(n)
               im = abs(m)
               if (m.gt.0) then
                  if (n.gt.0) then
                     val = Ppleg(l,im,in)
                  else
                     val = Pmleg(l,im,in)
                  end if
               else
                  if (n.gt.0) then
                     val = (-1)**(n+m)*Pmleg(l,in,im)
                  else
                     val = (-1)**(m+n)*Ppleg(l,im,in)
                  end if
               end if
               if (dsin(theta).lt.0.0d0) val = (-1)**(n+m)*val
               PlmN(n,m,l) = val / coef
            end do
         end do
      end do

    end subroutine legendre


    subroutine resample(ya, t0, dta, na, yb, dtb, nb)
      !-------------------!
      !  resample traces  !
      !-------------------!
      use module_spline
      implicit none

      integer, intent(in) :: na,nb
      doubleprecision, dimension(na), intent(in) :: ya
      doubleprecision, intent(in) :: dta, dtb, t0
      real, dimension(nb), intent(out):: yb

      integer :: i
      doubleprecision :: yp1, ypn, x
      doubleprecision, dimension(na) :: xa, y2

      do i = 1,na
         xa(i) = (i-1)*dta + t0
      end do

      yp1 = (ya(2)-ya(1)) / dta
      ypn = (ya(na)-ya(na-1)) / dta

      call spline(xa, ya, yp1, ypn, y2)

      do i = 1,nb
        x = (i-1)*dtb
        if (x<=xa(na) .and. x>=xa(1)) then
          yb(i) = real(splint(xa, ya, y2, x))
        else
          yb(i) = 0.0
        end if
      end do

    end subroutine resample

end module module_modes
