!---------------------------------------------------------------------------------!
! Author (partial) : Yann Capdeville                                              !
!                                                                                 !
! Contact: yann.capdeville at univ-nantes.fr                                      !
!                                                                                 !
! This program is free software: you can redistribute it and/or modify it under   !
! the terms of the GNU General Public License as published by the Free Software   !
! Foundation, either version 3 of the License, or (at your option) any later      !
! version.                                                                        !
!                                                                                 !
! This program is distributed in the hope that it will be useful, but WITHOUT ANY !
! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A !
! PARTICULAR PURPOSE. See the GNU General Public License for more details.        !
!                                                                                 !
! You should have received a copy of the GNU General Public License along with    !
! this program. If not, see http://www.gnu.org/licenses/.                         !
!---------------------------------------------------------------------------------! 

program nms
  !----------------------------------------------------------!
  ! there are 3 configurations files :                       !
  !   - source.dat    : contains sources info + frequency    !
  !   - recepteur.dat : contains receivers info              !
  !   - nms.dat       : contains the normal modes catalogue  !
  !                      information and some more info      !
  !----------------------------------------------------------!
  use module_main, only : init_global_main
  use module_modes
  implicit none

  call init_global_main()
  call init_modes()
  call get_and_sum()

end program nms
