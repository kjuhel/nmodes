module module_main

  use def_param
  implicit none

  public :: init_global_main, nms_dat, recepteur_dat, &
    get_source_dat, get_receivers, load_sources, load_receivers, &

    ! content of nms.dat file
    dt, NBT, f1, f2, f3, f4, l3, l4, &
    nmin, nmax_in, rotation, eigenfileS, eigenfileT, &
    model, modin_fmt, out_type, source_force, homogenization, &

    ! content of sources.dat file
    NBE, NBE_layer, stacking_sources, sources_name, &
    coord_sources, sources_layer, sources_depth, &
    t_delay_sources, &

    ! content of receivers.dat file
    NBR, stations_name, coord_stations, stations_depth, &

    RA, GEOC, SEXP, nbcomp, comp, compr

  private
  ! configuration files
  character(len=30)            :: source_dat    = 'sources.dat'
  character(len=30)            :: recepteur_dat = 'receivers.dat'
  character(len=30), parameter :: nms_dat       = 'nms.dat'
  character(len=1), dimension(3), parameter :: compr=(/'Z','R','T'/)
  character(len=1), dimension(3), parameter :: comp =(/'Z','N','E'/)

  ! sources, stations
  integer :: NBE, NBE_layer, NBR
  integer, parameter :: nbcomp=3 ! nombre de composantes (1== vertical, 3 == all)
  integer, dimension(:), allocatable :: sources_layer
  real(DP) :: stations_depth
  real(DP), dimension(:), allocatable :: sources_depth, t_delay_sources
  real(DP), dimension(:,:), allocatable :: coord_stations, coord_sources, Mtmp
  doubleprecision, parameter :: SEXP = 1.d18
  logical :: sources_available=.false., stacking_sources, source_force
  logical :: geoc_corr, rotation
  character(len=13), dimension(:), allocatable :: sources_name
  character(len=9), dimension(:), allocatable :: stations_name

  ! temps, frequence
  integer :: NBT, l3, l4
  real(DP):: dt, f1, f2, f3, f4

  ! catalogue
  character(len=100) :: eigenfileT, eigenfileS
  integer :: nmin, nmax_in

  ! gravitation
  character(len=3) :: out_type

  ! input model
  character(len=100) :: model

  ! input format
  character(len=3) :: modin_fmt

  real(DP), parameter :: GEOC=0.993277d0
  real(DP) :: RA = 6371000.0d0  ! will be reinitialized by util_fctp

  ! homogenization
  integer :: homogenization = -1 ! no homogenization: -1, order 0: 0, order 1: 1

  contains

    subroutine init_global_main()
      !-----------------------------------!
      ! call routines to read *.dat files !
      !-----------------------------------!
      implicit none

      call load_nms()
      call init_rcv_src()

    end subroutine init_global_main


    subroutine load_nms()
      !-------------------------!
      ! reads the nms.dat file  !
      !-------------------------!
      implicit none
      integer, parameter:: unit=13
      integer :: i

      open(unit, file=nms_dat, status='old')
      read(unit,*)
      read(unit,*) dt
      read(unit,*)
      read(unit,*) NBT
      read(unit,*)
      read(unit,*) f1, f2, f3, f4
      read(unit,*)
      read(unit,*) l3, l4
      read(unit,*)

      read(unit,*) nmin, nmax_in
      if (nmin < 0) nmin = 0
      if (nmax_in < 0) nmax_in = -1

      read(unit,*)
      read(unit,*) geoc_corr
      read(unit,*)
      read(unit,*) rotation
      read(unit,*)
      read(unit,'(a)') eigenfileS
      read(unit,*)
      read(unit,'(a)') eigenfileT
      read(unit,*)
      read(unit,*) source_force
      read(unit,*)
      read(unit,'(a)') model
      read(unit,*)
      read(unit,*) homogenization
      read(unit,*)
      read(unit,'(a3)') modin_fmt
      read(unit,*)
      read(unit,'(a)') out_type

    end subroutine load_nms


    subroutine init_rcv_src()
      !----------------------------------------------------!
      ! loads information in sources.dat and receivers.dat !
      !----------------------------------------------------!
      implicit none
      logical :: err

      call load_sources()
      call load_receivers(err)
      if (err) STOP 'Error when loading the receivers file'

    end subroutine init_rcv_src


    subroutine load_sources()
      !----------------------------!
      ! reads the sources.dat file !
      !----------------------------!
      implicit none
      integer :: i, j, units=527

      ! lecture du fichier de source :
      open(units, file=source_dat, status='old')
      read(units,*)
      read(units,*) NBE
      read(units,*)
      read(units,*) NBE_layer
      read(units,*)
      read(units,*) stacking_sources
      read(units,*)

      allocate(sources_name(NBE), Mtmp(6,NBE), coord_sources(2,NBE), &
        t_delay_sources(NBE), sources_layer(NBE), sources_depth(NBE_layer))

      do i = 1,NBE
         read(units,'(a13,x,6(e12.5,x),2(f10.5,x),i2,x,f6.2)') sources_name(i), &
           (Mtmp(j,i), j=1,6), (coord_sources(j,i), j=1,2), sources_layer(i), &
           t_delay_sources(i)

         ! conversion latitude -> colatitude
         coord_sources(1,i) = 90.d0 - coord_sources(1,i)
      end do

      read(units,*)

      do i = 1,NBE_layer
         read(units, '(f10.5)') sources_depth(i)

         ! conversion profondeur (en km) -> rayon (en m)
         sources_depth(i) = RA - sources_depth(i)*1000.d0 - 0.1d0
      end do

      close(units)

      if (rotation.and.stacking_sources) then
        print *, 'Warning, can''t rotate component when stacking sources'
        rotation = .false.
      end if

    end subroutine load_sources


    subroutine load_receivers(error)
      !------------------------------!
      ! reads the receivers.dat file !
      !------------------------------!
      implicit none
      logical, intent(out) :: error
      integer :: irec, j, unitrec

      ! lecture des donnees dans recepteur.dat
      unitrec = 135
      open(unitrec, file=recepteur_dat, status='old', err=100)
      read(unitrec,*)
      read(unitrec,*) NBR
      read(unitrec,*)
      read(unitrec, '(f10.5)') stations_depth
      read(unitrec,*)

      ! conversion profondeur (en km) -> rayon (en m)
      ! stations_depth = RA - stations_depth*1000.d0 - 0.1d0

      ! conversion profondeur (en km) -> (en m)
      stations_depth = stations_depth*1000.d0

      allocate(stations_name(NBR), coord_stations(2,NBR))

      do irec = 1,NBR
        read(unitrec,'(a9,2(x,f10.5))') stations_name(irec), (coord_stations(j,irec), j=1,2)

        ! latitude -> colatitude
        coord_stations(1,irec) = 90.d0 - coord_stations(1,irec)
      end do

      close(unitrec)

      error = .false.

      return
      100 print *, 'No recepteur.dat file found ! ... skeeping receivers management..'
      error = .true.

    end subroutine load_receivers


    subroutine get_source_dat(coord_out, Mtmp_out, name_out)
      !----------------------------------------!
      ! transfer source info into module_modes !
      !----------------------------------------!
      implicit none
      doubleprecision, dimension(2,NBE), intent(out) :: coord_out
      doubleprecision, dimension(6,NBE), intent(out) :: Mtmp_out
      character(len=13), dimension(NBE), optional, intent(out) :: name_out
      integer :: i, j

      coord_out(:,:) = coord_sources(:,:)

      Mtmp_out = Mtmp

      if (geoc_corr) then
        coord_out(1,:) = rad2deg*(PI/2.d0-atan(GEOC*tan((PI/2.d0-coord_out(1,:)/rad2deg))))
      end if

      ! passage en radian
      coord_out(:,:) = coord_out(:,:)*deg2rad

      if (present(name_out)) name_out(:) = sources_name(:)

    end subroutine get_source_dat


    subroutine get_receivers(rec_coord)
      !------------------------------------------!
      ! transfer receiver info into module_modes !
      !------------------------------------------!
      use def_param
      implicit none
      real(DP), dimension(:,:), intent(out) :: rec_coord

      rec_coord(:,:) = coord_stations(:,:)

      if (geoc_corr) then
        rec_coord(1,:) = rad2deg*(PI/2.d0-atan(GEOC*tan((PI/2.d0-rec_coord(1,:)/rad2deg))))
      end if

      ! degrees --> radians
      rec_coord(:,:) = rec_coord(:,:)*deg2rad

    end subroutine get_receivers


end module module_main
