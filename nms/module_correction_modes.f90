module corr_modes
  !-----------------------------------------------------------------------------------!
  !           First order correction for moment tensor with homogeneization           !
  ! WARNING: the 0 order correction MUST have been applyed to the moment tensor first !
  !-----------------------------------------------------------------------------------!
  use def_param
  implicit none  
  public :: init_corr_modes, corrmodesS, corrmodesT
  private
  integer :: nbc, NBE, istart
  doubleprecision :: wn
  doubleprecision, dimension(:), allocatable :: x1l_s,FFs,FF,CCs,CC,x1a1_s,x1a0_s,x1p_s,x1c_s,x2a1_s,x1n_s,LLs,LL

  contains
    subroutine init_corr_modes(NBE_, rad_src, idh_)
      !------------!
      !            !
      !------------!
      implicit none
      
      integer, intent(in) :: NBE_,idh_
      real(DP), dimension(:), intent(in) :: rad_src

      real(DP), dimension(:), allocatable :: rad,Cn,Co,Ln,Lo,Fn,Fo
      real(DP), dimension(:), allocatable :: x1n,x1l,x1a0,x1a1,x1c,x1p

      integer :: unit,i,is
      real(DP) :: c1,c2,rads
      
      unit=62

      NBE=NBE_
      open(unit,file='info_layer_corr.dat')
      read(unit,*) 
      read(unit,*) 
      read(unit,*) 
      read(unit,*) 
      read(unit,*) 
      read(unit,*) 
      read(unit,*) 
      read(unit,*)
      read(unit,*) istart,nbc,wn
      allocate (rad(nbc),Cn(nbc),Co(nbc),Ln(nbc),Lo(nbc),Fn(nbc),Fo(nbc))
      allocate (x1l(nbc),x1n(nbc),x1a0(nbc),x1a1(nbc),x1c(nbc),x1p(nbc))
      read(unit,*) (rad(i),i=1,nbc)
      read(unit,*) (Cn(i),Co(i),Ln(i),Lo(i),Fn(i),Fo(i)   ,i=istart,nbc)

      read(unit,*) (x1N(i),x1L(i),x1C(i),i=istart,nbc)
      read(unit,*) (x1a0(i),x1a1(i),x1p(i),i=istart,nbc)
      close(unit)
      allocate(x1p_s(NBE+1),x1n_s(NBE+1),x1c_s(NBE+1),x1l_s(NBE+1),x1a0_s(NBE+1),x1a1_s(NBE+1))
      allocate(CCs(NBE+1),CC(NBE+1),LLs(NBE+1),LL(NBE+1),FFs(NBE+1),FF(NBE+1))

      do i=1,NBE+1
        ! preparation
        if (i==NBE+1) then !receiver
          is=nbc-1
          rads=rad(nbc)
        else
          is=locate(rad,nbc-1,rad_src(i))
          rads=rad_src(i)
        endif
        if (is<istart) STOP 'is < istart!'

        ! linear interpolation    
        c2=(rad(is)-rads)/(rad(is)-rad(is+1))
        c1=(rads -rad(is+1) )/(rad(is)-rad(is+1))

        CCs(i)=Cn(is)*c1+Cn(is+1)*c2
        FFs(i)=Fn(is)*c1+Fn(is+1)*c2
        LLs(i)=Ln(is)*c1+Ln(is+1)*c2

        CC(i)= Co(is)*c1+Co(is+1)*c2
        FF(i)= Fo(is)*c1+Fo(is+1)*c2
        LL(i)= Lo(is)*c1+Lo(is+1)*c2

        x1p_s(i) =x1p (is)*c1+x1p (is+1)*c2
        x1n_s(i) =x1n (is)*c1+x1n (is+1)*c2
        x1l_s(i) =x1l (is)*c1+x1l (is+1)*c2
        x1c_s(i) =x1c (is)*c1+x1c (is+1)*c2
        x1a0_s(i)=x1a0(is)*c1+x1a0(is+1)*c2
        x1a1_s(i)=x1a1(is)*c1+x1a1(is+1)*c2
      enddo

      deallocate(rad,Cn,Co,Ln,Lo,Fn,Fo)
      deallocate(x1l,x1n,x1a0,x1a1,x1c,x1p)

    end subroutine init_corr_modes


    subroutine corrmodesS(is,w,l,rr,u,v,up,vp)
      !------------------!
      !                  !
      !------------------!
      use module_main, only: homogenization
      implicit none
      integer, intent(in) :: l,is
      real(DP), intent(in) :: w,rr
      real(DP), intent(inout) :: u,v,up,vp

      integer :: i,j
      real(DP) :: z,gl,gl2,gl3,w2,ww,dy3,x43,x241,x223
      real(DP), dimension(4) :: y,yn

      if (l==0) return

      gl=sqrt(real(l*(l+1)))
      gl2=gl*gl
      gl3=gl**3
      ww=w/wn
      w2=w*w/wn**2
      z=1./rr
      y(1)=u
      y(2)=CCs(is)*up+FFs(is)*z*(2*u-l*(l+1)*v)
      y(3)=v
      y(4)=LLs(is)*(vp+z*(u-v))

      y(3)=y(3)*gl
      y(4)=y(4)*gl

      if (homogenization>0) then
        yn(1)=y(1)-gl*x1a0_s(is)*y(3)-x1c_s(is)*y(2)
        yn(2)=y(2)+w2*x1p_s(is)*y(1)-2.*gl*(x1a1_s(is)-x1n_s(is))*y(1)
        yn(3)=y(3)-x1l_s(is)*y(4)
        yn(4)=y(4)+gl*x1a0_s(is)*y(2)-(gl2*x1a1_s(is)-w2*x1p_s(is))*y(3)-2.*gl*(x1a1_s(is)-x1n_s(is))*y(1)
      else
        yn(:)=y(:)
      endif

      yn(3)=yn(3)/gl
      yn(4)=yn(4)/gl

      u=yn(1)
      v=yn(3)
      up=(yn(2)-FF(is)*z*(2*u-l*(l+1)*v))/CC(is)
      vp=yn(4)/LL(is)-z*(u-v)

    end subroutine corrmodesS


    subroutine corrmodesT(is,wfreq,l,rr,w,wp)
      !------------------!
      !                  !
      !------------------!
      use module_main, only: homogenization
      implicit none
      integer, intent(in) :: l,is
      real(DP), intent(in) :: wfreq,rr
      real(DP), intent(inout) :: w,wp

      integer :: i
      real(DP) :: z,ol,w2,ww,gl
      real(DP), dimension(2) :: y,yn

      Ol=real((l-1)*(l+2))
      gl=sqrt(real(l*(l+1)))
      ww=wfreq/wn
      w2=wfreq*wfreq/wn**2
      z=1.d0/rr
      y(1)=w
      y(2)=LLs(is)*(wp-z*w) 

      if (homogenization>0) then
        yn(1)=y(1)-x1l_s(is)*y(2)
        yn(2)=y(2)-gl*gl*x1n_s(is)*y(1)
      else
        yn(:)=y(:)
      endif

      w =yn(1)
      wp=(yn(2)/LL(is)+z*w)

    end subroutine corrmodesT


    integer function locate(inter,n,x)
      !------------------!
      !                  !
      !------------------!
      use def_param
      implicit none      
      integer, intent(in) :: n
      real(DP), intent(in) :: x         
      real(DP), dimension(0:n+1), intent(in) :: inter

      integer :: i

      i=0
      if (abs(x-inter(0))/max(x,inter(0)) < 1.E-10_DP) then
        i=1
      else if ( abs (x-inter(n+1))/max(x,inter(n+1))  <1.E-10_DP) then
        i=n
      else
        do while (  x < inter(i) .or. x > inter(i+1) )
          i=i+1
          if (i > n) then
            print *, 'i=', i, 'n=', n
            print *, 'x=', x
            print *, 'inter=', inter
            stop 'locate: failed to locate x in inter!'
          endif
        enddo
      endif

      locate=i+1
    end function locate

end module corr_modes
