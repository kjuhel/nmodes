module module_fctp

  use def_param
  implicit none

  public :: open_fctp, readS, readT, read_interpS, read_interpT, &
    get_Lmax, get_Nmax, get_eigenw, mode_existe, euler

  private
  integer :: LLmaxS, NNmaxS, LLmaxT, NNmaxT, nkS, nkT, nb_discT, nb_discS
  integer, parameter :: unitfctpS=155, unitfctpT=154
  integer , dimension(:) , allocatable :: discS_index, discT_index
  integer, dimension(:,:), allocatable :: irecS, irecT
  real(DP), dimension(:)  , allocatable :: rayS, rayT, discS_rad, discT_rad
  real(DP), dimension(:,:), allocatable :: omtabS, qtabS, gctabS
  real(DP), dimension(:,:), allocatable :: omtabT, qtabT, gctabT

  logical :: files_open=.false., files_openS=.false., files_openT=.false.

  contains

    subroutine open_fctp(fichierfctpS, fichierfctpT, rad_recepteur)
      !-----------------------------------------------------------------!
      ! fichierfctp : character*90, nom du fichier de fonction propre   !
      !                sans extension                                   !
      ! unitfctp    : entier, unite logique du fichier de fctp desiree  !
      !                                                                 !
      ! irec(0:LLmax,0:NNmax) : tableaux d'entier des numeros de record !
      !                      des fonctions propres                      !
      ! omtab(0:LLmax,0:NNmax) : tableau de real*8, w en rad/s          !
      ! qtab(0:LLmax,0:NNmax)  : tableau de real*8, attenuation         !
      ! gctab(0:LLmax,0:NNmax) : tableau de real*8, vitesses de groupe  !
      ! nocean   : entier, nb de couches de l'ocean                     !
      ! nk       : entier, nombre de couches du model                   !
      ! ray(NCM) : tableau de real*4, couches du model                  !
      !-----------------------------------------------------------------!
      use module_main, only: RA
      IMPLICIT NONE

      character(len=90), intent(in) :: fichierfctpS, fichierfctpT
      real(DP), optional, intent(out) :: rad_recepteur

      character(len=90) :: fichierdirect, fichierinfo, ext
      integer  :: LEN,unitinfo,indicepointeur,n,l,ibid,i,nocean,ier,k,iversion
      real(DP) :: ww,qq,gc,wdiff
      real(SP), dimension(:), allocatable :: ray_tmp, ray_tmp2
      logical :: flag

      ! read spheroidal file
      unitinfo = 73
      call extension(fichierfctpS, fichierdirect, '.direct ')
      call extension(fichierfctpS, fichierinfo, '.info ')

      inquire(file=fichierdirect, EXIST=flag)
      if (.not.flag) then
        print *, 'the file', fichierdirect, 'does not exist !'
        stop
      end if

      inquire(file=fichierinfo, EXIST=flag)
      if (.not.flag) then
        print *, 'the file', fichierinfo, 'does not exist !'
        stop
      end if

      call get_len(fichierdirect, LEN)
      open(unitfctpS, file=fichierdirect, status='old', access='direct', recl=LEN)
      open(unitinfo, file=fichierinfo)

      read(unitfctpS, rec=1) ibid, ibid, ibid, nocean, nkS
      allocate(rayS(nkS), ray_tmp(nkS))

      ! reading radius and track for discontinuities
      read(unitfctpS,rec=1) ibid,ibid,ibid,nocean,nkS,(ray_tmp(i),i=1,nkS),iversion,RA
      if (iversion/=4) STOP 'The program expect minosy version >= 0.4 !!'

      ! km SP -> m DP
      rayS(:) = dble(ray_tmp(1:nkS))*RA
      if (present(rad_recepteur)) rad_recepteur = rayS(nkS-nocean)-0.1
      nb_discS=0
      do i = 1,nkS-1
         if (abs(rayS(i)-rayS(i+1)) < 1e-8) nb_discS=nb_discS+1
      end do

      allocate(discS_index(0:nb_discS+1), discS_rad(0:nb_discS+1))
      discS_index(0) = 0
      discS_index(nb_discS+1) = nkS
      discS_rad(0) = rayS(1)
      discS_rad(nb_discS+1) = rayS(nkS)
      k = 0
      do i = 1,nkS-1
         if (abs(rayS(i)-rayS(i+1)) < 1e-8) then
            k = k + 1
            discS_index(k) = i
            discS_rad(k) = rayS(i)
         end if
      end do
      deallocate(ray_tmp)

      ! scanning for LLmax and NNmax:
      LLmaxS = 0; NNmaxS = 0
      10 read(unitinfo, *, END=99) n,l,ww,qq,gc,indicepointeur,wdiff,flag
      if (l > LLmaxS) LLmaxS = l
      if (n > NNmaxS) NNmaxS = n
      goto 10

      99 continue
      allocate(irecS(0:LLmaxS,0:NNmaxS),omtabS(0:LLmaxS,0:NNmaxS), &
               qtabS(0:LLmaxS,0:NNmaxS),gctabS(0:LLmaxS,0:NNmaxS), stat=ier)
      irecS(:,:) = 0
      if (ier /= 0) then
         print *, 'LLmaxS = ', LLmaxS
         print *, 'NNmaxS = ', NNmaxS
         print *, (4.+8.*3.)*dble(LLmaxS+1)*dble(NNmaxS+1)/1024./1024., ' Mb'
         stop 'open_fctp: probleme d''allocation memoire'
      end if
      rewind(unitinfo)

      ! relecture
      11 read(unitinfo,*,END=98) n,l,ww,qq,gc,indicepointeur,wdiff,flag
      irecS (l,n) = indicepointeur
      omtabS(l,n) = ww
      qtabS (l,n) = qq
      gctabS(l,n) = gc
      ! if ((abs(wdiff).gt.1.d-9.and.flag).or.abs(wdiff).gt.1d-5) irecS(l,n) = 0
      ! if ((abs(wdiff).gt.1.d-9.and.flag).or.abs(wdiff).gt.1d-3) irecS(l,n) = 0
      goto 11
      98 continue
      close(unitinfo)
      fichierdirect(:) = ' '
      fichierinfo(:) = ' '


      ! read toroidal file
      call extension(fichierfctpT, fichierdirect, '.direct ')
      call extension(fichierfctpT, fichierinfo, '.info ')

      inquire(file=fichierdirect, EXIST=flag)
      if (.not.flag) then
        print *, 'the file ', fichierdirect, 'does not exist !'
        stop
      end if
      inquire(file=fichierinfo, EXIST=flag)
      if (.not.flag) then
        print *, 'the file', fichierinfo, 'does not exist !'
         stop
      endif

      call get_len(fichierdirect, LEN)
      open(unitfctpT, file=fichierdirect, status='old', access='direct', recl=LEN)
      open(unitinfo, file=fichierinfo)

      read(unitfctpT, rec=1) ibid, ibid, ibid, nocean, nkT
      allocate(rayT(nkT), ray_tmp(nkT))

      ! reading radius and track for discontinuities
      read(unitfctpT, rec=1) ibid,ibid,ibid,nocean,nkT,(ray_tmp(i),i=1,nkT),iversion,RA
      if (iversion/=4) STOP 'The program expect minosy version >= 0.4 !!'

      ! km SP -> m DP
      rayT(:) = dble(ray_tmp(1:nkT))*RA

      nb_discT = 0
      do i = 1,nkT-1
         if (abs(rayT(i)-rayT(i+1)) < 1e-8) nb_discT = nb_discT + 1
      end do

      allocate(discT_index(0:nb_discT+1), discT_rad(0:nb_discT+1))
      discT_index(0) = 0
      discT_index(nb_discT+1) = nkT
      discT_rad(0) = rayT(1)
      discT_rad(nb_discT+1) = rayT(nkT)
      k = 0
      do i = 1,nkT-1
         if (abs(rayT(i)-rayT(i+1)) < 1e-8) then
            k = k + 1
            discT_index(k) = i
            discT_rad(k) = rayT(i)
         end if
      end do
      deallocate(ray_tmp)

      ! scanning for LLmax and NNmax:
      LLmaxT = 0; NNmaxT = 0
      110 read(unitinfo,*,END=199) n,l,ww,qq,gc,indicepointeur,wdiff,flag
      if (l>LLmaxT) LLmaxT = l
      if (n>NNmaxT) NNmaxT = n
      goto 110
      199 continue
      allocate(irecT(0:LLmaxT,0:NNmaxT), omtabT(0:LLmaxT,0:NNmaxT))
      allocate(qtabT(0:LLmaxT,0:NNmaxT), gctabT(0:LLmaxT,0:NNmaxT))
      irecT(:,:) = 0
      rewind(unitinfo)

      ! relecture
      111 read(unitinfo, *, END=198) n,l,ww,qq,gc,indicepointeur,wdiff,flag
      irecT (l,n) = indicepointeur
      omtabT(l,n) = ww
      qtabT (l,n) = qq
      gctabT(l,n) = gc
      ! if ((abs(wdiff).gt.1.d-9.and.flag).or.abs(wdiff).gt.1d-5) irecT(l,n)=0
      ! if ((abs(wdiff).gt.1.d-9.and.flag).or.abs(wdiff).gt.1d-5) irecT(l,n)=0
      goto 111
      198 continue
      close(unitinfo)
      fichierdirect(:) = ' '
      fichierinfo(:) = ' '

      files_open = .true.

    end subroutine open_fctp


    subroutine get_Lmax(fmax, LmaxS, LmaxT)
      !-------------------!
      !                   !
      !-------------------!
      use def_param
      implicit none
      real(DP), intent(in) :: fmax
      integer, intent(out) :: LmaxS, LmaxT

      integer :: l
      real(DP) :: wmax

      wmax = 2._DP*PI*fmax
      LmaxS = -1; LmaxT = -1

      if (.not.files_open) stop 'get_lmax: call open_fctp first!'

      ! on ne cherche que sur le mode fondamental
      do l = 0,LLmaxS
         if (irecS(l,0)/=0.and.omtabS(l,0)<=wmax.and.l>LmaxS) LmaxS=l
      end do

      do l = 1,LLmaxT
         if (irecT(l,0)/=0.and.omtabT(l,0)<=wmax.and.l>LmaxT) LmaxT=l
      end do

    end subroutine get_Lmax


    subroutine get_eigenw(eigw, qw, LmaxS, NmaxS, LmaxT, NmaxT)
      !-------------------!
      !                   !
      !-------------------!
      use def_param
      implicit none
      integer, intent(in) :: LmaxS, LmaxT
      integer, dimension(0:), intent(in) :: NmaxS, NmaxT
      real(DP), dimension(0:,0:,:), intent(out) :: eigw, qw

      integer :: l, n

      do l = 0,LmaxS
         do n = 0,NmaxS(l)
            eigw(n,l,1) = omtabS(l,n)
            if (qtabS(l,n) /= 0.0_DP) then
              qw(n,l,1) = omtabS(l,n) / (2.0_DP*qtabS(l,n))
            else
              qw(n,l,1) = 0.0_DP
            end if
         end do
      end do

      do l = 1,LmaxT
         do n = 0,NmaxT(l)
            eigw(n,l,2) = omtabT(l,n)
            if (qtabT(l,n) /= 0.0_DP) then
              qw(n,l,2) = omtabT(l,n) / (2.0_DP*qtabT(l,n))
            else
              qw(n,l,2) = 0.0_DP
            end if
         end do
      end do

    end subroutine get_eigenw


    subroutine get_Nmax(fmax, NmaxS, NmaxT)
      !-------------------!
      !                   !
      !-------------------!
      use def_param
      use module_main, only : l4
      implicit none
      real(DP), intent(in) :: fmax
      integer, dimension(0:), intent(out) :: NmaxS, NmaxT

      integer :: l, Lmax, LmaxT, LmaxS, n
      real(DP) :: wmax

      wmax = 2.0_DP*PI*fmax
      ! on recalcule Lmax. Normalement cette operation a deja ete effectue a
      ! l'exterieur pour allouer NmaxS et NmaxT
      call get_lmax(fmax, LmaxS, LmaxT)
      LmaxS = min(LmaxS, l4)
      LmaxT = min(LmaxT, l4)

      NmaxS(:) = -1
      NmaxT(:) = -1

      do l = 0,LmaxS
         NmaxS(l) = -1
         do n = 0,NNmaxS
            if (irecS(l,n)/=0 .and. omtabS(l,n)<=wmax .and. n>NmaxS(l)) NmaxS(l) = n
         end do
      end do

      do l = 1,LmaxT
         NmaxT(l) = -1
         do n = 0,NNmaxT
            if (irecT(l,n)/=0 .and. omtabT(l,n)<=wmax .and. n>NmaxT(l)) NmaxT(l) = n
         end do
      end do

      NmaxT(0) = 0

    end subroutine get_Nmax


    subroutine get_len(file, LEN)
      !--------------------------------------------------------!
      ! retourne la longueur des records des fichiers de fctps !
      ! a acces direct produit par classeT ou classeS          !
      ! INPUT :                                                !
      !    file : character*90, nom du fichier a acces direct  !
      ! OUTPUT :                                               !
      !    LEN : entier, longueur du record                    !
      !--------------------------------------------------------!
      implicit none
      character(len=90) :: file
      integer LEN

      open(22, file=file, status='old', access='direct', RECL=4)
      read(22, rec=1) len
      close(22)

    end subroutine get_len


    subroutine read_interpT(n, l, NBV, rad, fctp)
      !-------------------!
      !                   !
      !-------------------!
      use def_param
      use module_spline, only: spline, splint
      implicit none
      integer, intent(in) :: n,l,NBV
      real(DP), dimension(NBV), intent(in)  :: rad
      real(DP), dimension(2,NBV),intent(out):: fctp

      integer :: iflag,i,k,ka,kb,nd
      real(DP) :: yp1,ypn
      real(DP), dimension(2,nkT) :: xdum
      real(DP), dimension(nkT,nb_discT+1) :: y2

      call readT(n, l, xdum, iflag)

      if (iflag == 0) then
         do k = 1,2
            do nd = 1,nb_discT+1
               ka = discT_index(nd-1) + 1
               kb = discT_index(nd)
               yp1 = (xdum(k,ka+1)-xdum(k,ka  ))/(rayT(ka+1)-rayT(ka  ))
               ypn = (xdum(k,kb  )-xdum(k,kb-1))/(rayT(kb  )-rayT(kb-1))
               call spline(rayT(ka:kb), xdum(k,ka:kb), yp1, ypn, y2(ka:kb,nd))
            end do
            do i = 1,NBV
               nd = locate(discT_rad, nb_discT, rad(i))
               ka = discT_index(nd-1) + 1
               kb = discT_index(nd)
               fctp(k,i) = splint(rayT(ka:kb), xdum(k,ka:kb), y2(ka:kb,nd), rad(i))
            end do
         end do
      else
         fctp(:,:) = 0.0_DP
      end if

    end subroutine read_interpT


    subroutine read_interpS(n, l, NBV, rad, fctp)
      !-------------------!
      !                   !
      !-------------------!
      use def_param
      use module_spline, only: spline, splint
      implicit none
      integer, intent(in) :: n,l,NBV
      real(DP), dimension(NBV), intent(in) :: rad
      real(DP), dimension(6,NBV), intent(out) :: fctp

      integer :: iflag,i,k,ka,kb,nd
      real(DP) :: yp1,ypn
      real(DP), dimension(6,nkS) :: xdum
      real(DP), dimension(nkS,nb_discS+1) :: y2

      call readS(n, l, xdum, iflag)

      if (iflag == 0) then
        do k = 1,6
           do nd = 1,nb_discS + 1
              ka = discS_index(nd-1) + 1
              kb = discS_index(nd)
              yp1 = (xdum(k,ka+1)-xdum(k,ka  ))/(rayS(ka+1)-rayS(ka  ))
              ypn = (xdum(k,kb  )-xdum(k,kb-1))/(rayS(kb  )-rayS(kb-1))
              call spline(rayS(ka:kb), xdum(k,ka:kb), yp1, ypn, y2(ka:kb,nd))
           end do
           do i = 1,NBV
              nd = locate(discS_rad,nb_discS,rad(i))
              ka = discS_index(nd-1) + 1
              kb = discS_index(nd)
              fctp(k,i) = splint(rayS(ka:kb), xdum(k,ka:kb), y2(ka:kb,nd), rad(i))
           end do
        end do
      else
        fctp(:,:) = 0.0_DP
      end if

    end subroutine read_interpS


    integer function locate(inter, n, x)
      !-------------------!
      !                   !
      !-------------------!
      use def_param
      implicit none
      integer, intent(in) :: n
      real(DP), dimension(0:n+1), intent(in) :: inter
      real(DP), intent(in) :: x

      integer :: i

      i = 0
      if (abs(x-inter(0))/max(x,inter(0)) < 1.E-10_DP) then
        i = 1

      else if (abs(x-inter(n+1))/max(x,inter(n+1)) < 1.E-10_DP) then
        i = n

      else
        do while (  x < inter(i) .or. x > inter(i+1) )
           i = i + 1
           if (i > n) then
             print *, 'i=', i,'n=', n
             print *, 'x=', x
             print *, 'inter=', inter
             stop 'locate: failed to locate x in inter !'
           end if
        end do

      end if

      locate = i + 1

    end function locate


    subroutine readT(n, l, xdum, iflag)
      !----------------------------------------------!
      ! reads eigen functions (n,l) after open_fctp  !
      ! opened direct access file                    !
      !----------------------------------------------!
      use def_param
      implicit none
      integer, intent(in) :: l,n
      integer, intent(out):: iflag
      real(DP), dimension(2,nkT), intent(out) ::  xdum

      integer :: i,nn,ll,nvec,nb
      real(DP) :: WW,QQ,GC

      real(DP), dimension(nkT*6) :: tt
      !real(SP), dimension(nkT*6) :: tt

      if (.not.files_open) stop 'readT: call open_fctp first!'

      !nvec est la longueur necessaire pour atteindre
      ! la profondeur desiree pour w wt dw :
      nb=nkT
      nvec=int(nb*2)

      if (l <= LLmaxT .and. n<= NNmaxT) then
         if (irecT(l,n) /= 0) then
            read(unitfctpT, rec=irecT(l,n)) nn, ll, WW, QQ, GC, (tt(I),I=1,nvec)

            do i = 1,nkT
               xdum(1,i) = dble(tt(2*(nkT-i)+1))
               xdum(2,i) = dble(tt(2*(nkT-i)+2))
            end do
            iflag = 0

          else
            iflag = 2
            xdum(:,:) = 0.0_DP

          end if
      else
         iflag = 1
      end if

    end subroutine readT


    subroutine readS(n, l, xdum, iflag)
      !-------------------------------------------------------------------!
      ! reads eigen functions (n,l) after open_fctp opened access file    !
      ! INPUT :                                                           !
      !    ifd : entier, unite logique du fichier a acces direct          !
      !    irec(0:LLmax,0:NNmax) : tableau des numeros de records         !
      !                          sortie de open_fctp                      !
      !    n et l : entiers                                               !
      !    nk entier : nombre de couches du model                         !
      !    np entier : profondeur jusqu'a laquelle on veut lire ls fctps  !
      !                 (de np a nk=surface)                              !
      ! OUTPUT :                                                          !
      !    xdum1..6(NCM) : fonction propres et derivees                   !
      !    iflag : 1 pas de pb                                            !
      !            0 fin du fichier (ne devrait jamais arriver)           !
      !           -1 erreur a la lecture                                  !
      !           -2 l ou n depasse LLmax ou NNmax                        !
      !-------------------------------------------------------------------!
      use def_param
      use module_main, only: RA, modin_fmt
      implicit none
      integer, intent(in) :: l,n
      integer, intent(out):: iflag
      real(DP), dimension(6,nkS), intent(out) :: xdum

      integer :: nvec,i,nn,ll,nb
      real(DP) :: WW,QQ,GC

      real(DP), dimension(nkS*6) :: tt
      !real(SP), dimension(nkS*6) :: tt

      if (.not.files_open) stop 'readS: call open_fctp first!'
      nb=nkS

      ! nvec est la longueur necessaire pour atteindre
      ! la profondeur desiree pour u, du, v, dv, p, dp
      nvec=int(nb*6)

      if (l <= LLmaxS .and. n <= NNmaxS) then

        if (irecS(l,n)/=0) then
          ! reading and killing discontinuities
          read(unitfctpS,rec=irecS(l,n)) nn, ll, WW, QQ, GC, (tt(I), I=1,nvec)

          if (modin_fmt == 'uni') then
            do i = 1,nkS
               xdum(1,i) = dble(tt(6*(nkS-i)+1))
               xdum(2,i) = dble(tt(6*(nkS-i)+2))
               xdum(3,i) = dble(tt(6*(nkS-i)+3))
               xdum(4,i) = dble(tt(6*(nkS-i)+4))
               xdum(5,i) = dble(tt(6*(nkS-i)+5))
               xdum(6,i) = dble(tt(6*(nkS-i)+6))
            end do
          else if (modin_fmt == 'yns') then
            do i = 1,nkS
               xdum(1,i) = dble(tt(4*(nkS-i)+1))
               xdum(2,i) = dble(tt(4*(nkS-i)+2))
               xdum(3,i) = dble(tt(4*(nkS-i)+3))
               xdum(4,i) = dble(tt(4*(nkS-i)+4))
            end do
            do i = 1,nkS
               xdum(5,i) = dble(tt((nkS-i)+1+4*nkS))
               xdum(6,i) = dble(tt((nkS-i)+1+5*nkS))
            end do
          else
            print *, 'unknown input format for the catalogue !'
          end if
          iflag = 0

        else
          iflag = 2
          xdum(:,:) = 0.0_DP
        end if

      else
        iflag = 1
      end if

    end subroutine readS


    subroutine extension(fichier1,fichier2,ext)
      !-------------------------------------!
      ! adds extension ext to file fichier1 !
      !-------------------------------------!
      implicit none
      character(len=*), intent(in)  :: fichier1,ext
      character(len=*), intent(out) :: fichier2
      integer :: lenfichier1,lenext

      lenfichier1=INDEX (fichier1,' ') -1
      lenext=INDEX (ext,' ') -1
      if ((lenfichier1+lenext).gt.80) then
              print *, 'character ', fichier1, 'is too long !'
      end if

      fichier2(1:lenfichier1)=fichier1(1:lenfichier1)
      fichier2(lenfichier1+1:(lenfichier1+lenext))=ext(1:lenext)
      fichier2(lenfichier1+lenext+1:)=' '

    end subroutine extension


    logical function mode_existe(n, l, q)
      !-------------------------------!
      !                               !
      !-------------------------------!
      implicit none
      integer :: n,l,q

      if (q == 1) then
        if (irecS(l,n) /= 0) then
          mode_existe=.true.
        else
          mode_existe=.false.
        end if
      else
        if (irecT(l,n) /= 0) then
          mode_existe=.true.
        else
          mode_existe=.false.
        end if
      end if

    end function mode_existe


    subroutine euler(t1, p1, t2, p2, a, b, g)
      !-------------------------------------------------------------!
      ! calcule les 3 angles d'euler de la rotation definis par :   !
      ! D(a b g)= D(-p1 -t1 0) o D(0 t2 p2)                         !
      ! (notation cf Edmonds)                                       !
      !                                                             !
      ! Inputs :                                                    !
      !    angles p1 p2 dans [-pi,pi] (en fait peu importe mais en  !
      !                                radians)                     !
      !    angles t1 t2 DANS [ 0 ,pi] (pour etre avec utilise avec  !
      !    la regle  de sommation d'harmoniques spheriques)         !
      !                                                             !
      ! Outputs :                                                   !
      !    angles a et g dans [-pi,pi]                              !
      !    angle  b      dans [ 0 ,pi]                              !
      !-------------------------------------------------------------!
      IMPLICIT NONE
      real*8 t1,t2,p1,p2,a,b,g,ca,sa,cg,sg,sb,cb,pi
      real*8 rt1(3,3),rt2(3,3),rp1(3,3),rp2(3,3),prod1(3,3), &
           prod2(3,3),rot(3,3)
      integer i,j,k

      pi = 3.141592653589793d0
      do i = 1,3
         do j = 1,3
            rt1(i,j)   = 0.0d0
            rt2(i,j)   = 0.0d0
            rp1(i,j)   = 0.0d0
            rp2(i,j)   = 0.0d0
            prod1(i,j) = 0.0d0
            prod2(i,j) = 0.0d0
            rot(i,j)   = 0.0d0
         end do
      end do

      rt1(1,1) = dcos(t1)
      rt1(2,2) = 1.0d0
      rt1(3,3) = rt1(1,1)
      rt1(1,3) = dsin(t1)
      rt1(3,1) = -rt1(1,3)

      rt2(1,1) = dcos(t2)
      rt2(2,2) = 1.0d0
      rt2(3,3) = rt2(1,1)
      rt2(1,3) = -dsin(t2)
      rt2(3,1) = -rt2(1,3)

      rp1(1,1) = dcos(p1)
      rp1(2,2) = rp1(1,1)
      rp1(3,3) = 1.0d0
      rp1(1,2) = -dsin(p1)
      rp1(2,1) = -rp1(1,2)

      rp2(1,1) = dcos(p2)
      rp2(2,2) = rp2(1,1)
      rp2(3,3) = 1.0d0
      rp2(1,2) = dsin(p2)
      rp2(2,1) = -rp2(1,2)

      do i = 1,3
         do j = 1,3
            do k = 1,3
               prod1(i,j) = prod1(i,j) + rp2(i,k)*rt2(k,j)
            end do
         end do
      end do
      do i = 1,3
         do j = 1,3
            do k = 1,3
               prod2(i,j) = prod2(i,j) + rp1(i,k)*prod1(k,j)
            end do
         end do
      end do
      do i = 1,3
         do j = 1,3
            do k = 1,3
               rot(i,j) = rot(i,j) + rt1(i,k)*prod2(k,j)
            end do
         end do
      end do

      cb = rot(3,3)
      if (cb >= 1.d0) then
        b = 0.d0
      else if (cb <= -1.d0) then
        b = pi
      else
        b = dacos(cb)
      end if
      sb = dsin(b)
      if (abs(sb).le.1.0d-15) then
        a = p2-p1
        g = 0.
      else
        ca = rot(3,1)/sb
        sa = rot(3,2)/sb
        if (abs(ca-1.0d0).lt.1.0d-8) then
          a = 0.0d0
        else
          if (abs(ca+1.0d0).lt.1.0d-8) then
            a = pi
          else
            a = dacos(ca)
          end if
        end if
        if (sa.lt.(0.0d0)) a = -1.0d0*a
        cg = -rot(1,3)/sb
        sg = rot(2,3)/sb
        if (abs(cg-1.0d0).lt.1.0d-8) then
          g = 0.0d0
        else
          if (abs(cg+1.0d0).lt.1.0d-8) then
            g = pi
          else
            g = dacos(cg)
          end if
        end if
        if (sg.lt.(0.0d0)) g = -1.0d0*g
      end if

    end subroutine euler

end module module_fctp
