program generate_PREM
  !------------------------------------------------------!
  !  generate preliminary reference earth models (PREM)  !
  !------------------------------------------------------!
  implicit none

  character(len=35) :: filename

  integer :: nb_couches, choix, nbinter, i, j, unit, &
    icb, ocb, oce, nz, iz, ii, ideb, choix_derivees, nbpi

  integer, dimension(:), allocatable :: iinter

  doubleprecision :: rmax, bid, ro, vp, vs, x0, eps, ra, vpht, vsht, &
    Qmu, eta_a, rdeb, rop, vpp, vsp, ropp, vppp, vspp, Qkap, r1test

  doubleprecision, dimension(:), allocatable :: r, rho, &
    vpv, vph, vsv, vsh, eta, qkappa, qshear, rinter, dr, &
    xint, wint, a1p, a2p, a3p, a4p, a5p, qmup, rhop

  doubleprecision, dimension(:,:), allocatable :: bord

  logical :: ocean=.false., derivees, first_in_premF=.true., cancel_att
  logical, parameter :: gravity=.false.

  unit = 111
  eps  = 1.0D-8

  !--------------------!
  !       INPUT        !
  !--------------------!

  print *, 'enter the model output name :'
  read *, filename

  print *, 'do you need ocean (1) or not (2) ?'
  read *, i

  if (i==1) then
    ocean=.true.
  else
    ocean=.false.
  end if

  print *, 'enter the number of layers of the model :'
  read *, nb_couches

  print *, 'do you want to cancel attenuation (yes=T, no=F) ?'
  read *, cancel_att

  rdeb=0.; rmax=0.

  choix_derivees=0
  select case(choix_derivees)
  case(1)
     derivees=.true.
  case(0)
     derivees=.false.
  case default
     print *, 'choix non prevu, je mets les derivees ...'
     derivees=.true.
  end select

  !--------------------!
  !     FIN INPUT      !
  !--------------------!

  call prem(0.5d0, bid, bid, bid, bid, gravity, nbi=nbinter)
  allocate(rinter(nbinter), iinter(nbinter), dr(nbinter))
  call prem(0.5d0, bid, bid, bid, bid, gravity, ri=rinter)

  ra = rinter(nbinter)

  if (rmax==0.d0) then
    rmax=rinter(nbinter)
  else
    j = 0
    do i = 1,nbinter
      if (rinter(i)<rmax) j=j+1
    end do
    nbinter=j+1
    rinter(nbinter)=rmax
    do i = 1,nbinter
      print *, i, rinter(i)
    end do
  end if

  if (rdeb<1.E-8) then
    ideb = 1
  else
    j = 0
    do i = 1,nbinter
      if (rinter(i)<=rdeb) j=j+1
    end do
    ideb=j
  end if
  
  print *, 'ideb=', ideb
  if (ideb/=1) rinter(ideb)=rdeb

  do i = ideb,nbinter
    if (i==ideb) then
      iinter(ideb)=0
    else
      iinter(i)=nint((rinter(i)-rinter(i-1))/(rmax-rdeb)*nb_couches)+iinter(i-1)
      if (iinter(i)<=iinter(i-1)+1) iinter(i)=iinter(i-1)+2
    end if
    if ( i > ideb ) dr(i)=(rinter(i)-rinter(i-1))/(iinter(i)-(iinter(i-1)+1))
  end do

  if (iinter(nbinter)/=nb_couches) then
    nb_couches=iinter(nbinter)
    print *, 'The final number of layers is :', nb_couches
  end if

  allocate(r(0:nb_couches), rho(nb_couches), vpv(nb_couches), vph(nb_couches), &
    vsv(nb_couches), vsh(nb_couches), eta(nb_couches), qkappa(nb_couches), qshear(nb_couches))

  r=0.0d0; rho=0.0d0; vpv=0.0d0; vph=0.0d0; vsv=0.0d0; vsh=0.0d0
  eta=0.0d0; qkappa=0.0d0; qshear=0.0d0

  do iz=ideb,nbinter
    if (iz==ideb) then
      r(0)=rinter(iz)
    else
      do ii=iinter(iz-1)+1,iinter(iz)
        if (ii==iinter(iz-1)+1) then
          r(ii)=r(ii-1)
        else
          r(ii)=r(ii-1)+dr(iz)
        end if
        
        x0=r(ii)/ra
        
        if (ii==iinter(iz-1)+1) then
          x0=x0+eps
        else if (ii==iinter(iz)) then
          x0=x0-eps
        end if
        
        call prem(x0, ro, vp, vs, Qmu, gravity)
        
        vpht=vp
        vsht=vs
        eta_a=1.D0
        Qkap =1.E5
        
        rho(ii)    = ro  *1000.d0
        vpv(ii)    = vp  *1000.d0
        vph(ii)    = vpht*1000.d0
        vsv(ii)    = vs  *1000.d0
        vsh(ii)    = vsht*1000.d0
        eta(ii)    = eta_a
        qkappa(ii) = Qkap
        qshear(ii) = Qmu
      end do
    end if
  end do

  nz=0
  icb=0
  ocb=0
  oce=0

  do i = nb_couches-1,1,-1
    if (abs(vsv(i+1))<1.d-10 .and. abs(vsv(i))>1.d-10) then
      if (ocb/=0) then
        icb=i
      else
        oce=nb_couches-i
      end if
      nz=nz+1
    else if (abs(vsv(i+1))>1.d-10 .and. abs(vsv(i))<1.d-10) then
      ocb=i
      nz=nz+1
    end if
  end do
  
  if (nz>3) then
    ! stop 'Il y a trop de couches liquide'
  else if (nz==0) then
    if (abs(vsv(1)) < 1.d-10) then
      ! tout liquide
      ocb=nb_couches
      icb=0
      oce=0
    else if (abs(vsv(1)) < 1.d-10) then
      ! tout solide
      ocb=0
      icb=0
      oce=0
    end if
  else if (nz==1) then
    icb=nb_couches-oce
    ocb=nb_couches
    oce=0
  end if
  
  if (derivees) call get_derive(rdeb,rmax,ro,vp,vs,rop,vpp,vsp,ropp,vppp,vspp)

  if (cancel_att) then
    qkappa(:)=1e5
    qshear(:)=1e5
  end if

  r(:)=r(:)*1000.  
  
  open(unit,file=filename)
  
  write(unit,*) filename
  write(unit,*) 1, 1., 1
  write(unit,*) nb_couches, icb, ocb, oce

  if (derivees) then 
    write(unit,*) ro, vp, vs
    write(unit,*) rop, vpp, vsp
    write(unit,*) ropp, vppp, vspp
  end if

  do i = 1,nb_couches
    write(unit,105) r(i), rho(i), vpv(i), vsv(i), qkappa(i), qshear(i), vph(i), vsh(i), eta(i)
  end do
  close(unit)

 105 format(f8.0, 3f9.2, 2f9.1, 2f9.2, f9.5)

contains
  subroutine get_derive(rdeb, rmax, ro, vp, vs, rop, vpp, vsp, ropp, vppp, vspp)
    !-------------
    !
    !-------------
    implicit none

    doubleprecision, intent(in) :: rdeb,rmax
    doubleprecision, intent(out):: ro,vp,vs,rop,vpp,vsp,ropp,vppp,vspp
    
    doubleprecision :: rdtn,eps=1d-5,x0,h,bid
    doubleprecision,dimension(3) :: ro_t,vp_t,vs_t,rd

    logical :: coq
    integer :: i
    
    h=eps*rmax
    
    if (rdeb > 1.d-8) then
      rdtn=rdeb
      coq=.true.
      h=-h
    else
      rdtn=rmax
      coq=.false.
    end if

    rd(1)=rdtn-eps*sign(h,1.d0)
    rd(2)=rdtn-h-eps*sign(h,1.d0)
    rd(3)=rdtn-2*h-eps*sign(h,1.d0)
    
    do i=1,3
      x0 = rd(i)/ra
      call prem(x0,ro_t(i),vp_t(i),vs_t(i),bid,gravity)
    end do

  ro   = ro_t(1)*1000.d0
  rop  = (ro_t(1)-ro_t(2))/h
  ropp = (ro_t(1)-2*ro_t(2)+ro_t(3))/h**2/1000.d0

  vp   = vp_t(1)*1000.d0 
  vpp  = (vp_t(1)-vp_t(2))/h
  vppp = (vp_t(1)-2*vp_t(2)+vp_t(3))/h**2/1000.d0

  vs   = vs_t(1)*1000.d0 
  vsp  = (vs_t(1)-vs_t(2))/h
  vspp = (vs_t(1)-2*vs_t(2)+vs_t(3))/h**2/1000.d0

  end subroutine get_derive

  subroutine prem(x0, ro, vp, vs, Qmu, flag_g, GR, nbi, ri)
    !-----------------------------------------------------------------------------!
    !  Given the normalized radius x0 = r/rt, PREM returns the density (RO), the  !
    !  compressional velocity (VP), the shear velocity (VS) and the gravity (GR)  !
    !  according to the PREM model (see Anderson and Dziewonski, 1981, TABLE 1)   !
    !  The parameters listed are valid at a reference period of 1 second.         !
    !-----------------------------------------------------------------------------!

    integer :: i, j
    integer, optional :: nbi
    logical :: flag_g, pastrouve
    doubleprecision :: x0, x, ro, vp, vs, Qmu
    doubleprecision :: CST, r1, r2, t1, t2, t3, t4
    doubleprecision, optional :: GR
    doubleprecision, parameter :: Rt = 6371000.d0, epsilon = 1.d-9, bigg = 6.6723d-11
    doubleprecision, dimension(13) :: cumul
    doubleprecision, dimension(14) :: r, q
    doubleprecision, dimension(13,4) :: d, p, s
    doubleprecision, optional, dimension(14) :: ri

    ! initialize arrays
    ro = 0.0d0; vp = 0.d0; vs = 0.d0;
    
    !------------------!
    !  RADIUS (in km)  !
    !------------------!

    ! center of the earth
    r(1) = 0.d0

    ! inner / outer core
    r(2) = 1221.5d0

    ! outer couter / lower mantle
    r(3) = 3480.d0

    r(4) = 3630.d0
    r(5) = 5600.d0

    ! lower mantle / transition zone
    r(6) = 5701.d0
    r(7) = 5771.d0
    r(8) = 5971.d0

    ! transition zone / low velocity zone (LVZ)
    r(9) = 6151.d0

    ! LVZ / LID
    r(10) = 6291.d0

    ! LID / crust
    r(11) = 6346.6d0
    r(12) = 6356.d0

    ! crust / ocean
    r(13) = 6368.d0
    r(14) = 6371.d0

    !-------!
    !  Qmu  !
    !-------!

    ! inner core
    q(1)=84.6d0

    ! outer core
    q(2)=1.d5
    
    ! lower mantle and transition zone
    q(3)=312.d0; q(4)=312.d0; q(5)=312.d0
    q(6)=312.d0; q(7)=143.d0; q(8)=143.d0
    
    ! LVZ, LID and crust
    q(9)=80.d0; q(10)=80.d0;
    q(11)=600.d0; q(12)=600.d0; q(13)=600.d0

    if (present(nbi)) then
      nbi = 14
    end if

    if (present(ri)) then
      ri(:) = r(:)
    end if

    !-------------!
    !  DENSITY D  !
    !-------------!

    d(:,:) = 0.d0
    
    d(1,1) = 13.0885d0; d(1,3) = -8.8381d0 
    d(2,1) = 12.5815d0; d(2,2) = -1.2638d0; d(2,3) = -3.6426d0; d(2,4) = -5.5281d0
    d(3,1) = 7.9565d0 ; d(3,2) = -6.4761;   d(3,3) = 5.5283d0;  d(3,4) = -3.0807d0
    d(4,1) = 7.9565d0 ; d(4,2) = -6.4761;   d(4,3) = 5.5283d0;  d(4,4) = -3.0807d0
    d(5,1) = 7.9565d0 ; d(5,2) = -6.4761;   d(5,3) = 5.5283d0;  d(5,4) = -3.0807d0
    d(6,1) = 5.3197d0 ; d(6,2) = -1.4836d0
    d(7,1) = 11.2494d0; d(7,2) = -8.0298d0
    d(8,1) = 7.1089d0 ; d(8,2) = -3.8045d0
    d(9,1) = 2.6910d0 ; d(9,2) = 0.6924d0
    d(10,1) = 2.6910d0; d(10,2) = 0.6924d0
    d(11,1) = 2.9d0  
    d(12,1) = 2.6d0
 
  if (ocean) then
    ! oceanique
    d(13,1) = 1.02d0 
  else
    ! continental
    d(13,1) = d(12,1)
  end if

  !----------------------------!
  !  COMPRESSIONAL VELOCITY P  !
  !----------------------------!

  p(:,:) = 0.d0

  p(1,1) = 11.2622d0 ; p(1,3) = -6.3640d0
  p(2,1) = 11.0487d0 ; p(2,2) = -4.0362d0; p(2,3)  = 4.8023d0; p(2,4) = -13.5732d0
  p(3,1) = 15.3891d0 ; p(3,2) = -5.3181d0; p(3,3)  = 5.5242d0; p(3,4) = -2.5514d0
  p(4,1) = 24.952d0 ; p(4,2)  = -40.4673d0; p(4,3) = 51.4832d0; p(4,4) = -26.6419d0
  p(5,1) = 29.2766d0 ; p(5,2) = -23.6027d0; p(5,3) = 5.5242d0; p(5,4) = -2.5514d0
  p(6,1) = 19.0957d0 ; p(6,2)  = -9.8672d0
  p(7,1) = 39.7027d0 ; p(7,2)  = -32.6166d0
  p(8,1) = 20.3926d0 ; p(8,2)  = -12.2569d0
  p(9,1) = 4.1875d0 ; p(9,2)  = 3.9382d0
  p(10,1) = 4.1875d0 ; p(10,2) = 3.9382d0
  p(11,1) = 6.8d0 
  p(12,1) = 5.8d0

  if (ocean) then
    ! oceanique
    p(13,1) = 1.45d0 
  else
    ! continental
    p(13,1) = p(12,1)
  end if

  !--------------------!
  !  SHEAR VELOCITY S  !
  !--------------------!

  s(:,:) = 0.d0

  s(1,1) = 3.6678d0; s(1,3) = -4.4475d0

  s(3,1) = 6.9254d0; s(3,2) = 1.4672d0; s(3,3) = -2.0834d0; s(3,4) = 0.9783d0
  s(4,1) = 11.1671d0; s(4,2) = -13.7818d0; s(4,3) = 17.4575d0; s(4,4) = -9.2777d0
  s(5,1) = 22.3459d0; s(5,2) = -17.2473d0; s(5,3) = -2.0834d0; s(5,4) = 0.9783d0
  s(6,1) = 9.9839; s(6,2) = -4.9324
  s(7,1) = 22.3512d0; s(7,2) = -18.5856d0 
  s(8,1) = 8.9496d0; s(8,2) = -4.4597
  s(9,1) = 2.1519d0; s(9,2) = 2.3481d0
  s(10,1) = 2.1519d0; s(10,2) = 2.3481d0
  s(11,1) = 3.9d0 
  s(12,1) = 3.2d0 
  
  ! oceanique = ne rien toucher
  ! continental
  if (.not. ocean) s(13,1) = s(12,1)

  r(:) = r(:) * 1000.d0
  x    = x0   * Rt

  pastrouve = .true.

  do i = 1,13
    if ( pastrouve .and. (x >= r(14)) ) then
      x0 = 1.d0
      ro = d(13,1) + x0*( d(13,2) + x0*( d(13,3) + x0*( d(13,4) )))
      vp = p(13,1) + x0*( p(13,2) + x0*( p(13,3) + x0*( p(13,4) )))
      vs = s(13,1) + x0*( s(13,2) + x0*( s(13,3) + x0*( s(13,4) )))
      Qmu=q(13)
      pastrouve = .false.
    end if

    if ( pastrouve .and. ((x >= r(i)) .and. (x < r(i+1))) ) then
      ro = d(i,1) + x0*( d(i,2) + x0*( d(i,3) + x0*( d(i,4) )))
      vp = p(i,1) + x0*( p(i,2) + x0*( p(i,3) + x0*( p(i,4) )))
      vs = s(i,1) + x0*( s(i,2) + x0*( s(i,3) + x0*( s(i,4) )))
      Qmu=q(i)
      pastrouve = .false.
    end if
  end do

  if (.not. flag_g) return
  
  !--------------!
  !  GRAVITY GR  !
  !--------------!

  CST = 16.d0*datan(1.d0)*bigg
  
  do i = 1,13
    t1 = d(i,1)/3.d0
    t2 = d(i,2)/(Rt*4.d0)
    t3 = d(i,3)/((Rt**2)*5.d0)
    t4 = d(i,4)/((Rt**3)*6.d0)
    r2 = r(i+1)
    r1 = r(i)

    cumul(i) =  t1*(r2**3) + t2*(r2**4) + t3*(r2**5) + t4*(r2**6) - &
      ( t1*(r1**3) + t2*(r1**4) + t3*(r1**5) + t4*(r1**6) )
  end do
  
  if ( x > r(14) ) x = r(14)
  
  do i =1,13
    if ((x > r(i)) .and. (x <= r(i+1))) then
      GR = 0.d0
      do j = 1,i-1
        GR = GR + cumul(j)
      end do
      
      t1 = d(i,1)/3.d0
      t2 = d(i,2)/(Rt*4.d0)
      t3 = d(i,3)/((Rt**2)*5.d0)
      t4 = d(i,4)/((Rt**3)*6.d0)
      r2 = x
      r1 = r(i)
      
      GR = GR + t1*(r2**3) + t2*(r2**4) + t3*(r2**5) + t4*(r2**6) &
        - ( t1*(r1**3) + t2*(r1**4) + t3*(r1**5) + t4*(r1**6) )
      
      GR = GR * CST
      
      if (x > epsilon) GR = GR/(x**2)
      return
    end if
  end do

  end subroutine prem

end program generate_PREM
