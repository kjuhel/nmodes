#!/usr/bin/env python3

"""
Generate Preliminary Reference Earth Models (PREM),
according to Dziewonski & Anderson (1981).
"""

import numpy as np
import argparse


# !----------------------!
# !  set some functions  !
# !----------------------!

def parse_args():
    """ Parsing and configuration """

    desc = "Computes PREM earth model"
    parser = argparse.ArgumentParser(description=desc)

    parser.add_argument('--fname', type=str, default='PREM.txt', help='Model output name')

    parser.add_argument('--nlayer', type=int, default=500, help='Number of layers')

    parser.add_argument('--ocean', default=1, type=int, choices=[0, 1], help='Ocean or not')
    parser.add_argument('--attenuation', default=0, type=int, choices=[0, 1], help='Attenuation or not')

    return parser.parse_args()


def prem(x0):
    """
    Coefficients of the polynomials describing PREM.
    The parameters listed are valid at a reference period of 1 second.

    Input
    -----
    * x0 = r/rt is the normalized radius

    Outputs
    -------
    * ro is the density (in g/cm3)
    * vp is the compressional velocity (in km/s)
    * vs is the shear velocity (in km/s)
    * Qmu and Qkappa
    * gr is the gravity

    See Table 1 of Dziewonski & Anderson (1981) for reference.
    """

    # initialize radius and Qmu arrays
    r, q = np.zeros(14), np.zeros(14)

    # initialize density, vp and vs arrays
    d, p, s = np.zeros((13, 4)), np.zeros((13, 4)), np.zeros((13, 4))

    # initialize interpolated radius
    ri = np.zeros(14)

    # !----------------------!
    # !  set radius (in km)  !
    # !----------------------!

    # center of the earth
    r[0] = 0.0

    # inner / outer core
    r[1] = 1221.5

    # outer couter / lower mantle
    r[2] = 3480.0

    r[3] = 3630.0
    r[4] = 5600.0

    # lower mantle / transition zone
    r[5] = 5701.0
    r[6] = 5771.0
    r[7] = 5971.0

    # transition zone / low velocity zone (LVZ)
    r[8] = 6151.0

    # LVZ / LID
    r[9] = 6291.0

    # LID / crust
    r[10] = 6346.6
    r[11] = 6356.0

    # crust / ocean
    r[12] = 6368.0
    r[13] = 6371.0

    # !-----------!
    # !  set Qmu  !
    # !-----------!

    # inner core
    q[0] = 84.6

    # outer core
    q[1] = 1.0e5

    # lower mantle and transition zone
    q[2], q[3], q[4] = 312.0, 312.0, 312.0
    q[5], q[6], q[7] = 312.0, 143.0, 143.0

    # LVZ, LID and crust
    q[8], q[9] = 80.0, 80.0
    q[10], q[11], q[12] = 600.0, 600.0, 600.0

    # !-------------------------------------------!
    # !  set coefficients for density (in g/cm3)  !
    # !-------------------------------------------!

    # inner core
    d[0,0], d[0,2] = 13.0885, -8.8381

    # outer core
    d[1,0], d[1,1], d[1,2], d[1,3] = 12.5815, -1.2638, -3.6426, -5.5281

    # lower mantle
    d[2,0], d[2,1], d[2,2], d[2,3] = 7.9565, -6.4761, 5.5283, -3.0807
    d[3,0], d[3,1], d[3,2], d[3,3] = 7.9565, -6.4761, 5.5283, -3.0807
    d[4,0], d[4,1], d[4,2], d[4,3] = 7.9565, -6.4761, 5.5283, -3.0807

    # transition zone
    d[5,0], d[5,1] = 5.31970, -1.4836
    d[6,0], d[6,1] = 11.2494, -8.0298
    d[7,0], d[7,1] = 7.10890, -3.8045

    # LVZ
    d[8,0], d[8,1] = 2.6910, 0.6924

    # LID
    d[9,0], d[9,1] = 2.6910, 0.6924

    # CRUST
    d[10,0] = 2.9
    d[11,0] = 2.6

    if ocean:
        # OCEAN
        d[12,0] = 1.02
    else:
        # CRUST
        d[12,0] = 2.6

    # !---------------------------------------------------------!
    # !  set coefficients for compressional velocity (in km/s)  !
    # !---------------------------------------------------------!

    # inner core
    p[0,0], p[0,2] = 11.2622, -6.3640

    # outer core
    p[1,0], p[1,1], p[1,2], p[1,3] = 11.0487, -4.0362, 4.8023, -13.5732

    # lower mantle
    p[2,0], p[2,1], p[2,2], p[2,3] = 15.3891, -5.31810, 5.52420, -2.5514
    p[3,0], p[3,1], p[3,2], p[3,3] = 24.9520, -40.4673, 51.4832, -26.6419
    p[4,0], p[4,1], p[4,2], p[4,3] = 29.2766, -23.6027, 5.52420, -2.5514

    # transition zone
    p[5,0], p[5,1] = 19.0957, -9.8672
    p[6,0], p[6,1] = 39.7027, -32.6166
    p[7,0], p[7,1] = 20.3926, -12.2569

    # LVZ
    p[8,0], p[8,1] = 4.1875, 3.9382

    # LID
    p[9,0], p[9,1] = 4.1875, 3.9382

    # CRUST
    p[10,0] = 6.8
    p[11,0] = 5.8

    if ocean:
        # OCEAN
        p[12,0] = 1.45
    else:
        # CRUST
        p[12,0] = 5.8

    # !-------------------------------------------------!
    # !  set coefficients for shear velocity (in km/s)  !
    # !-------------------------------------------------!

    # inner core
    s[0,0], s[0,2] = 3.6678, -4.4475

    # lower mantle
    s[2,0], s[2,1], s[2,2], s[2,3] = 6.92540, 1.467200, -2.0834, 0.9783
    s[3,0], s[3,1], s[3,2], s[3,3] = 11.1671, -13.7818, 17.4575, -9.2777
    s[4,0], s[4,1], s[4,2], s[4,3] = 22.3459, -17.2473, -2.0834, 0.9783

    # transition zone
    s[5,0], s[5,1] = 9.98390, -4.9324
    s[6,0], s[6,1] = 22.3512, -18.5856
    s[7,0], s[7,1] = 8.94960, -4.4597

    # LVZ
    s[8,0], s[8,1] = 2.1519, 2.3481

    # LID
    s[9,0], s[9,1] = 2.1519, 2.3481

    # CRUST
    s[10,0] = 3.9
    s[11,0] = 3.2

    if not ocean:
        # CRUST
        s[12,0] = 3.2

    # !-----
    # !
    # !

    # km --> m
    r = r * 1000.0

    # get un-normalized radius
    x = x0 * rt * 1000.0

    flag = True

    for i in range(13):
        if flag and (x >= r[-1]):
            x0 = 1.0

            ro = d[12,0] + d[12,1]*x0 + d[12,2]*x0**2 + d[12,3]*x0**3
            vp = p[12,0] + p[12,1]*x0 + p[12,2]*x0**2 + p[12,3]*x0**3
            vs = s[12,0] + s[12,1]*x0 + s[12,2]*x0**2 + s[12,3]*x0**3
            Qmu = q[12]

            flag = False

        if flag and x >= r[i] and x < r[i+1]:
            ro = d[i,0] + d[i,1]*x0 + d[i,2]*x0**2 + d[i,3]*x0**3
            vp = p[i,0] + p[i,1]*x0 + p[i,2]*x0**2 + p[i,3]*x0**3
            vs = s[i,0] + s[i,1]*x0 + s[i,2]*x0**2 + s[i,3]*x0**3
            Qmu = q[i]

            flag = False

    return ro*1000.0, vp*1000.0, vs*1000.0, Qmu

# !-----------------------!
# !  set some parameters  ! 
# !-----------------------!

# parse arguments
args = parse_args()

# set filename
filename = args.fname

# set ocean boolean
ocean = args.ocean

# set attenuation boolean
attenuation = args.attenuation

# set number of layers
nlayer = args.nlayer

# set epsilon
eps = 1.0e-8

# set Earth radius (in km)
rt = 6371.0

# set minimum and maximum radius
rmin, rmax = 0.0, rt

# set radius array
rinter = np.array([   0.0, 1221.5, 3480.0, 3630.0, 5600.0, 5701.0, 5771.0,
                   5971.0, 6151.0, 6291.0, 6346.6, 6356.0, 6368.0, 6371.0])

iinter, dr = np.zeros(14, dtype=int), np.zeros(14)

for i in range(1,14):
    iinter[i] = round((rinter[i]-rinter[i-1])/(rmax-rmin)*nlayer,0)+iinter[i-1]

    if iinter[i] <= iinter[i-1]+1:
        iinter[i] = iinter[i-1]+2

    dr[i] = (rinter[i]-rinter[i-1]) / (iinter[i]-(iinter[i-1]+1))

nlayer = iinter[-1]
print(f'The final number of layers is {nlayer}.')

# initialize radius and density arrays
r, rho = np.zeros(nlayer+1), np.zeros(nlayer)

# initialize vp and vs arrays
vpv, vph = np.zeros(nlayer), np.zeros(nlayer)
vsv, vsh = np.zeros(nlayer), np.zeros(nlayer)

# initialize eta, Qk and Qmu arrays
eta, qkappa, qshear = np.zeros(nlayer), np.zeros(nlayer), np.zeros(nlayer)

for iz in range(14):
    if iz == 0:
        r[0] = rinter[iz]
    else:
        for ii in range(iinter[iz-1], iinter[iz]):
            if ii == iinter[iz-1]:
                r[ii] = r[ii-1]
            else:
                r[ii] = r[ii-1] + dr[iz]

            # set normalized radius
            x0 = r[ii] / rt

            if ii == iinter[iz-1]:
                x0 = x0 + eps
            elif ii == iinter[iz]-1:
                x0 = x0 - eps

            rho[ii], vpv[ii], vsv[ii], qshear[ii] = prem(x0)
            vph[ii], vsh[ii], qkappa[ii], eta[ii] = vpv[ii], vsv[ii], 1.0e5, 1.0

nz, icb, ocb, oce = 0, 0, 0, 0

for i in range(nlayer-2, -1, -1):
    if abs(vsv[i+1]) < 1.0e-10 and abs(vsv[i]) > 1.0e-10:
        if (ocb != 0):
            icb = i+1
        else:
            oce = nlayer - (i+1)
        nz = nz + 1

    elif abs(vsv[i+1]) > 1.0e-10 and abs(vsv[i]) < 1.0e-10:
        ocb = i+1
        nz = nz + 1

if nz>3:
    print('There are too many liquid layers! Exit...')
    exit
elif nz == 0:
    if abs(vsv[0]) < 1.0e-10:
        # all liquid
        ocb, icb, oce = nlayer, 0, 0
    elif abs(vsv[0]) < 1.0e-10:
        # all solid
        ocb, icb, oce = 0, 0, 0
elif nz == 1:
    icb = nlayer - oce
    ocb = nlayer
    oce = 0

# cancel attenuation, if required
if not attenuation:
    qkappa[:] = 1.0e5
    qshear[:] = 1.0e5

# km --> m
r = r * 1000.0

header = f' {filename}\n           1   1.00000000               1\n        {nlayer}         {icb}         {ocb}           {oce}'

# set NumPy structured array
dtype = [('radius', 'f4'), ('rho', 'f4'), ('vpv', 'f4'), ('vsv', 'f4'),
         ('Qk', 'f4'), ('Qmu', 'f4'), ('vph', 'f4'), ('vsh', 'f4'), ('eta', 'f4')]

a = np.array([i for i in zip(r, rho, vpv, vsv, qkappa, qshear, vph, vsh, eta)], dtype=dtype)

# write selection to .txt file
fmt = ["%7.0f", "%8.2f", "%8.2f", "%8.2f", "%8.1f", "%8.1f", "%8.2f", "%8.2f", "%8.5f"]

np.savetxt(filename, a, fmt=fmt, header=header, comments='')
